(ns storm.cgol.web.routes.api
  (:require
    [storm.cgol.web.controllers.health :as health]
    [storm.cgol.web.middleware.exception :as exception]
    [storm.cgol.web.middleware.formats :as formats]
    [integrant.core :as ig]
    [reitit.coercion.malli :as malli]
    [reitit.ring.coercion :as coercion]
    [reitit.ring.middleware.muuntaja :as muuntaja]
    [reitit.ring.middleware.parameters :as parameters]
    [reitit.swagger :as swagger]
    [xtdb.api :as xt]))

(def route-data
  {:coercion   malli/coercion
   :muuntaja   formats/instance
   :swagger    {:id ::api}
   :middleware [;; query-params & form-params
                parameters/parameters-middleware
                ;; content-negotiation
                muuntaja/format-negotiate-middleware
                ;; encoding response body
                muuntaja/format-response-middleware
                ;; exception handling
                coercion/coerce-exceptions-middleware
                ;; decoding request body
                muuntaja/format-request-middleware
                ;; coercing response bodys
                coercion/coerce-response-middleware
                ;; coercing request parameters
                coercion/coerce-request-middleware
                ;; exception handling
                exception/wrap-exception]})

;; Routes
(defn api-routes [_opts db-node]
  [["/swagger.json"
    {:get {:no-doc  true
           :swagger {:info {:title "storm.cgol API"}}
           :handler (swagger/create-swagger-handler)}}]
   ["/health"
    {:get health/healthcheck!}]
   ["/write"
    {:put (fn [_] {:status 200
                   :body   (xt/submit-tx db-node [[::xt/put
                                                   {:xt/id     "hi2u"
                                                    :user/name "zig"}]])})}]
   ["/read"
    {:get (fn [_] {:status 200
                   :body   (xt/q (xt/db db-node) '{:find  [e]
                                                   :where [[e :user/name "zig"]]})})}]])

(derive :reitit.routes/api :reitit/routes)

(defmethod ig/init-key :reitit.routes/api
  [_ {:keys [base-path db-node]
      :or   {base-path ""}
      :as   opts}]
  (fn [] [base-path route-data (api-routes opts db-node)]))
