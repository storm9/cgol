(ns storm.cgol.gol
  (:require
    [clojure.test :as t]))

(defn make-board [rows cols cell-fn]
  (vec (for [_x (range rows)]
         (vec (for [_y (range cols)]
                (cell-fn))))))

(defn blank-board [rows cols]
  (make-board rows cols (constantly false)))

(defn random-board [rows cols]
  (make-board rows cols #(< 0.8 (rand))))

(defn neighbors [{:keys [rows cols cells]} x y]
  (let [n (get-in cells [(mod (dec x) rows) y])
        ne (get-in cells [(mod (dec x) rows) (mod (inc y) cols)])
        e (get-in cells [x (mod (inc y) cols)])
        se (get-in cells [(mod (inc x) rows) (mod (inc y) cols)])
        s (get-in cells [(mod (inc x) rows) y])
        sw (get-in cells [(mod (inc x) rows) (mod (dec y) cols)])
        w (get-in cells [x (mod (dec y) cols)])
        nw (get-in cells [(mod (dec x) rows) (mod (dec y) cols)])]
    [n ne e se s sw w nw]))

(defn live-neighbors-count [neighbors]
  (count (filter true? neighbors)))

(def rules
  [{:description  "1. Any live cell with fewer than two live neighbors dies, as if by underpopulation."
    :cell-state   true
    :will-live-fn (fn [neighbors] (not (< (live-neighbors-count neighbors) 2)))}
   {:description  "2. Any live cell with two or three live neighbors lives on to the next generation."
    :cell-state   true
    :will-live-fn (fn [neighbors] (or (= (live-neighbors-count neighbors) 2)
                                      (= (live-neighbors-count neighbors) 3)))}
   {:description  "3. Any live cell with more than three live neighbors dies, as if by overpopulation."
    :cell-state   true
    :will-live-fn (fn [neighbors] (not (> (live-neighbors-count neighbors) 3)))}
   {:description  "4. Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction."
    :cell-state   false
    :will-live-fn (fn [neighbors] (= (live-neighbors-count neighbors) 3))}])

(defn tick-cell [cell-state neighbors rules]
  (let [fns (map :will-live-fn (filter #(= (:cell-state %) cell-state) rules))
        results ((apply juxt fns) neighbors)
        will-live? (every? true? results)]
    will-live?))

;(def b {:rows 5,
;        :cols 5,
;        :cells [[false false false false false]
;                [true true true false false]
;                [false false false false true]
;                [false false false true true]
;                [false false false false false]]})
;(def bo (:cells b))

(defn tick-board [{:keys [cells] :as board} rules]
  (vec (map-indexed
         (fn [x row]
           (vec (map-indexed
                  (fn [y cell]
                    (tick-cell cell (neighbors board x y) rules))
                  row)))
         cells)))

(t/deftest tick-cell-tests
  (t/testing "live cell"
    ; no one lives
    (t/is (= false (tick-cell true [false false false false false false false false] rules)))
    ; 2 lives
    (t/is (= true (tick-cell true [true true false false false false false false] rules)))
    ; 3 lives
    (t/is (= true (tick-cell true [false true false false true false false true] rules)))
    ; all lives
    (t/is (= false (tick-cell true [true true true true true true true true] rules))))
  (t/testing "dead cell"
    ; no one lives
    (t/is (= false (tick-cell false [false false false false false false false false] rules)))
    ; 2 lives
    (t/is (= false (tick-cell false [true true false false false false false false] rules)))
    ; 3 lives
    (t/is (= true (tick-cell false [false true false false true false false true] rules)))
    ; all lives
    (t/is (= false (tick-cell false [true true true true true true true true] rules)))))
