(ns storm.cgol.views.home
  (:require
    [re-frame.core :as rf]))

(rf/reg-event-db
  :say-hello-change
  (fn [_db [_event name]]
    {:say-hello name}))

(rf/reg-sub
  :say-hello
  (fn [db _]
    (:say-hello db)))

(defn hello-input []
  [:div.color-input
   "Say hello to: "
   [:input {:type      "text"
            :value     @(rf/subscribe [:say-hello])
            :on-change #(rf/dispatch [:say-hello-change (-> % .-target .-value)])}]])

(defn hello-component []
  [:div (str "hello " @(rf/subscribe [:say-hello]))])

(defn home-page []
  [:div (str "This is the Home Page.")
   [:div
    [hello-input]
    [hello-component]]])
