(ns storm.cgol.views.grid
  (:require
    [re-frame.core :as rf]
    [storm.cgol.gol :as engine]))

(def cell-size 15)
(def cell-border 1)
(def rows 50)
(def cols 100)
(def height (* rows cell-size))
(def width (* cols cell-size))

(re-frame.core/reg-event-db
  :initialize
  (fn [_ _]
    {:draw? false
     :timer {:timer    nil
             :started? false}
     :cells (engine/blank-board rows cols)}))

(rf/reg-sub
  :cells
  :cells)

(rf/reg-sub
  :draw?
  :draw?)

(rf/reg-sub
  :started
  (comp :started? :timer))

(rf/reg-sub
  :row
  (fn [db [_ row]]
    (get-in (:cells db) [row])))

(defn toggle-one-cell [cells x y]
  (update-in cells [x y] not))

(rf/reg-event-db
  :cell-state-change
  (rf/path [:cells])
  (fn [cells [_event x y]]
    (toggle-one-cell cells x y)))

(rf/reg-event-db
  :cell-entered
  (fn [{:keys [cells draw?] :as db} [_event x y]]
    (if draw?
      (assoc db :cells (toggle-one-cell cells x y))
      db)))

(defn make-key [x y]
  (str x "_" y))

(defn cell-component [x y alive?]
  [:div {:key            (make-key x y)
         :style          {:height (- cell-size cell-border) :width (- cell-size cell-border)
                          :top    (+ (* x cell-size) cell-border) :left (+ (* y cell-size) cell-border) :position "absolute"
                          :border (str "solid white " cell-border "px")}
         :on-click       #(rf/dispatch [:cell-state-change x y])
         :on-mouse-enter #(rf/dispatch [:cell-entered x y])
         :class-name     (if alive? "cell-alive" "cell-dead")}])

(def cell-component-memo (memoize cell-component))

(defn row-component [row]
  (let [row-cells @(rf/subscribe [:row row])]
    (into [:span {:key (str "row_" row)}]
          (map-indexed
            (fn [coll cell] [cell-component-memo row coll cell])
            row-cells))))

(defn grid-view []
  (into [:div.board
         {:style {:height (+ 2 height) :width (+ 2 width)}}]
        (for [row (range rows)]
          [row-component row])))

(rf/reg-event-db
  :tick
  (rf/path [:cells])
  (fn [cells _]
    (engine/tick-board {:rows rows :cols cols :cells cells} engine/rules)))

(rf/reg-event-db
  :start
  (rf/path [:timer])
  (fn [timer _]
    (js/clearInterval (:timer timer))
    (rf/dispatch [:tick])
    {:timer   (js/setInterval #(rf/dispatch [:tick]) 200)
     :started true}))

(rf/reg-event-db
  :stop
  (rf/path [:timer])
  (fn [timer _]
    {:timer    (js/clearInterval (:timer timer))
     :started? false}))

(rf/reg-event-db
  :draw?
  (rf/path [:draw?])
  (fn [_ [_ draw?]]
    draw?))

(rf/reg-event-db
  :random
  (rf/path [:cells])
  (fn [_ _]
    (engine/random-board rows cols)))

(defonce pressed-listener (.addEventListener
                            js/document
                            "keydown"
                            (fn [e] (when (.-ctrlKey e)
                                      (rf/dispatch [:draw? true])))))
(defonce up-listener (.addEventListener
                       js/document
                       "keyup"
                       #(rf/dispatch [:draw? false])))

(defn gol []
  [:div
   [:button {:on-click #(rf/dispatch [:tick])
             :disabled (true? @(rf/subscribe [:started]))} "tick"]
   [:button {:on-click #(rf/dispatch [:random])} "random"]
   [:button {:on-click #(rf/dispatch [:start])
             :disabled (true? @(rf/subscribe [:started]))} "start"]
   [:button {:on-click #(rf/dispatch [:stop])
             :disabled (false? @(rf/subscribe [:started]))} "stop"]
   [grid-view]])
