(ns storm.cgol.core
  (:require
    [storm.cgol.views.grid :as grid]
    [reagent.dom :as d]
    [devtools.core :as devtools]
    [re-frame.core :as rf]))

(defn ui []
  [grid/gol])

;; -------------------------
;; Initialize app

(defn dev-setup []
  (when true
    (println "dev mode")
    (devtools/install!)))

(defn ^:dev/after-load mount-root []
  (d/render [ui] (.getElementById js/document "app")))

(defn ^:export ^:dev/once init! []
  (rf/dispatch-sync [:initialize])
  (dev-setup)
  (mount-root))
