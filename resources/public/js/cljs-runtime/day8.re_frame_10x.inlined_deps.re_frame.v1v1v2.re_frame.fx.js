goog.provide('day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx');
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.kind = new cljs.core.Keyword(null,"fx","fx",-1237829572);
if(cljs.core.truth_((day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.registrar.kinds.cljs$core$IFn$_invoke$arity$1 ? day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.registrar.kinds.cljs$core$IFn$_invoke$arity$1(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.kind) : day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.registrar.kinds.call(null, day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.kind)))){
} else {
throw (new Error("Assert failed: (day8.re-frame-10x.inlined-deps.re-frame.v1v1v2.re-frame.registrar/kinds kind)"));
}
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.reg_fx = (function day8$re_frame_10x$inlined_deps$re_frame$v1v1v2$re_frame$fx$reg_fx(id,handler){
return day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.registrar.register_handler(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.kind,id,handler);
});
/**
 * An interceptor whose `:after` actions the contents of `:effects`. As a result,
 *   this interceptor is Domino 3.
 * 
 *   This interceptor is silently added (by reg-event-db etc) to the front of
 *   interceptor chains for all events.
 * 
 *   For each key in `:effects` (a map), it calls the registered `effects handler`
 *   (see `reg-fx` for registration of effect handlers).
 * 
 *   So, if `:effects` was:
 *    {:dispatch  [:hello 42]
 *     :db        {...}
 *     :undo      "set flag"}
 * 
 *   it will call the registered effect handlers for each of the map's keys:
 *   `:dispatch`, `:undo` and `:db`. When calling each handler, provides the map
 *   value for that key - so in the example above the effect handler for :dispatch
 *   will be given one arg `[:hello 42]`.
 * 
 *   You cannot rely on the ordering in which effects are executed, other than that
 *   `:db` is guaranteed to be executed first.
 */
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.do_fx = day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.interceptor.__GT_interceptor.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"do-fx","do-fx",1194163050),new cljs.core.Keyword(null,"after","after",594996914),(function day8$re_frame_10x$inlined_deps$re_frame$v1v1v2$re_frame$fx$do_fx_after(context){
if(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.trace.is_trace_enabled_QMARK_()){
var _STAR_current_trace_STAR__orig_val__11758 = day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.trace._STAR_current_trace_STAR_;
var _STAR_current_trace_STAR__temp_val__11759 = day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.trace.start_trace(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"op-type","op-type",-1636141668),new cljs.core.Keyword("event","do-fx","event/do-fx",1357330452)], null));
(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR__temp_val__11759);

try{try{var effects = new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context);
var effects_without_db = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(effects,new cljs.core.Keyword(null,"db","db",993250759));
var temp__5804__auto___11909 = new cljs.core.Keyword(null,"db","db",993250759).cljs$core$IFn$_invoke$arity$1(effects);
if(cljs.core.truth_(temp__5804__auto___11909)){
var new_db_11912 = temp__5804__auto___11909;
var fexpr__11763_11913 = day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.kind,new cljs.core.Keyword(null,"db","db",993250759),false);
(fexpr__11763_11913.cljs$core$IFn$_invoke$arity$1 ? fexpr__11763_11913.cljs$core$IFn$_invoke$arity$1(new_db_11912) : fexpr__11763_11913.call(null, new_db_11912));
} else {
}

var seq__11764 = cljs.core.seq(effects_without_db);
var chunk__11765 = null;
var count__11766 = (0);
var i__11767 = (0);
while(true){
if((i__11767 < count__11766)){
var vec__11782 = chunk__11765.cljs$core$IIndexed$_nth$arity$2(null, i__11767);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11782,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11782,(1),null);
var temp__5802__auto___11917 = day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5802__auto___11917)){
var effect_fn_11919 = temp__5802__auto___11917;
(effect_fn_11919.cljs$core$IFn$_invoke$arity$1 ? effect_fn_11919.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_11919.call(null, effect_value));
} else {
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__11920 = seq__11764;
var G__11921 = chunk__11765;
var G__11922 = count__11766;
var G__11923 = (i__11767 + (1));
seq__11764 = G__11920;
chunk__11765 = G__11921;
count__11766 = G__11922;
i__11767 = G__11923;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__11764);
if(temp__5804__auto__){
var seq__11764__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__11764__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__11764__$1);
var G__11930 = cljs.core.chunk_rest(seq__11764__$1);
var G__11931 = c__5525__auto__;
var G__11932 = cljs.core.count(c__5525__auto__);
var G__11933 = (0);
seq__11764 = G__11930;
chunk__11765 = G__11931;
count__11766 = G__11932;
i__11767 = G__11933;
continue;
} else {
var vec__11787 = cljs.core.first(seq__11764__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11787,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11787,(1),null);
var temp__5802__auto___11936 = day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5802__auto___11936)){
var effect_fn_11939 = temp__5802__auto___11936;
(effect_fn_11939.cljs$core$IFn$_invoke$arity$1 ? effect_fn_11939.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_11939.call(null, effect_value));
} else {
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__11941 = cljs.core.next(seq__11764__$1);
var G__11942 = null;
var G__11943 = (0);
var G__11944 = (0);
seq__11764 = G__11941;
chunk__11765 = G__11942;
count__11766 = G__11943;
i__11767 = G__11944;
continue;
}
} else {
return null;
}
}
break;
}
}finally {if(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.trace.is_trace_enabled_QMARK_()){
var end__11389__auto___11955 = day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.interop.now();
var duration__11390__auto___11956 = (end__11389__auto___11955 - new cljs.core.Keyword(null,"start","start",-355208981).cljs$core$IFn$_invoke$arity$1(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.trace._STAR_current_trace_STAR_));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.trace.traces,cljs.core.conj,cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.trace._STAR_current_trace_STAR_,new cljs.core.Keyword(null,"duration","duration",1444101068),duration__11390__auto___11956,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"end","end",-268185958),day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.interop.now()], 0)));

day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.trace.run_tracing_callbacks_BANG_(end__11389__auto___11955);
} else {
}
}}finally {(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR__orig_val__11758);
}} else {
var effects = new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context);
var effects_without_db = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(effects,new cljs.core.Keyword(null,"db","db",993250759));
var temp__5804__auto___11958 = new cljs.core.Keyword(null,"db","db",993250759).cljs$core$IFn$_invoke$arity$1(effects);
if(cljs.core.truth_(temp__5804__auto___11958)){
var new_db_11959 = temp__5804__auto___11958;
var fexpr__11791_11960 = day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.kind,new cljs.core.Keyword(null,"db","db",993250759),false);
(fexpr__11791_11960.cljs$core$IFn$_invoke$arity$1 ? fexpr__11791_11960.cljs$core$IFn$_invoke$arity$1(new_db_11959) : fexpr__11791_11960.call(null, new_db_11959));
} else {
}

var seq__11792 = cljs.core.seq(effects_without_db);
var chunk__11793 = null;
var count__11794 = (0);
var i__11795 = (0);
while(true){
if((i__11795 < count__11794)){
var vec__11815 = chunk__11793.cljs$core$IIndexed$_nth$arity$2(null, i__11795);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11815,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11815,(1),null);
var temp__5802__auto___11962 = day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5802__auto___11962)){
var effect_fn_11963 = temp__5802__auto___11962;
(effect_fn_11963.cljs$core$IFn$_invoke$arity$1 ? effect_fn_11963.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_11963.call(null, effect_value));
} else {
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__11966 = seq__11792;
var G__11967 = chunk__11793;
var G__11968 = count__11794;
var G__11969 = (i__11795 + (1));
seq__11792 = G__11966;
chunk__11793 = G__11967;
count__11794 = G__11968;
i__11795 = G__11969;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__11792);
if(temp__5804__auto__){
var seq__11792__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__11792__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__11792__$1);
var G__11971 = cljs.core.chunk_rest(seq__11792__$1);
var G__11972 = c__5525__auto__;
var G__11973 = cljs.core.count(c__5525__auto__);
var G__11974 = (0);
seq__11792 = G__11971;
chunk__11793 = G__11972;
count__11794 = G__11973;
i__11795 = G__11974;
continue;
} else {
var vec__11820 = cljs.core.first(seq__11792__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11820,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11820,(1),null);
var temp__5802__auto___11976 = day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5802__auto___11976)){
var effect_fn_11977 = temp__5802__auto___11976;
(effect_fn_11977.cljs$core$IFn$_invoke$arity$1 ? effect_fn_11977.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_11977.call(null, effect_value));
} else {
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__11980 = cljs.core.next(seq__11792__$1);
var G__11981 = null;
var G__11982 = (0);
var G__11983 = (0);
seq__11792 = G__11980;
chunk__11793 = G__11981;
count__11794 = G__11982;
i__11795 = G__11983;
continue;
}
} else {
return null;
}
}
break;
}
}
})], 0));
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.dispatch_later = (function day8$re_frame_10x$inlined_deps$re_frame$v1v1v2$re_frame$fx$dispatch_later(p__11837){
var map__11838 = p__11837;
var map__11838__$1 = cljs.core.__destructure_map(map__11838);
var effect = map__11838__$1;
var ms = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__11838__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__11838__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if(((cljs.core.empty_QMARK_(dispatch)) || ((!(typeof ms === 'number'))))){
return day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch-later value:",effect], 0));
} else {
return day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.interop.set_timeout_BANG_((function (){
return day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.router.dispatch(dispatch);
}),ms);
}
});
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch-later","dispatch-later",291951390),(function (value){
if(cljs.core.map_QMARK_(value)){
return day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.dispatch_later(value);
} else {
var seq__11842 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,value));
var chunk__11843 = null;
var count__11844 = (0);
var i__11845 = (0);
while(true){
if((i__11845 < count__11844)){
var effect = chunk__11843.cljs$core$IIndexed$_nth$arity$2(null, i__11845);
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.dispatch_later(effect);


var G__11985 = seq__11842;
var G__11986 = chunk__11843;
var G__11987 = count__11844;
var G__11988 = (i__11845 + (1));
seq__11842 = G__11985;
chunk__11843 = G__11986;
count__11844 = G__11987;
i__11845 = G__11988;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__11842);
if(temp__5804__auto__){
var seq__11842__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__11842__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__11842__$1);
var G__11989 = cljs.core.chunk_rest(seq__11842__$1);
var G__11990 = c__5525__auto__;
var G__11991 = cljs.core.count(c__5525__auto__);
var G__11992 = (0);
seq__11842 = G__11989;
chunk__11843 = G__11990;
count__11844 = G__11991;
i__11845 = G__11992;
continue;
} else {
var effect = cljs.core.first(seq__11842__$1);
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.dispatch_later(effect);


var G__11993 = cljs.core.next(seq__11842__$1);
var G__11994 = null;
var G__11995 = (0);
var G__11996 = (0);
seq__11842 = G__11993;
chunk__11843 = G__11994;
count__11844 = G__11995;
i__11845 = G__11996;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.reg_fx(new cljs.core.Keyword(null,"fx","fx",-1237829572),(function (seq_of_effects){
if((!(cljs.core.sequential_QMARK_(seq_of_effects)))){
return day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: \":fx\" effect expects a seq, but was given ",cljs.core.type(seq_of_effects)], 0));
} else {
var seq__11853 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,seq_of_effects));
var chunk__11854 = null;
var count__11855 = (0);
var i__11856 = (0);
while(true){
if((i__11856 < count__11855)){
var vec__11870 = chunk__11854.cljs$core$IIndexed$_nth$arity$2(null, i__11856);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11870,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11870,(1),null);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"db","db",993250759),effect_key)){
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: \":fx\" effect should not contain a :db effect"], 0));
} else {
}

var temp__5802__auto___11997 = day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5802__auto___11997)){
var effect_fn_11998 = temp__5802__auto___11997;
(effect_fn_11998.cljs$core$IFn$_invoke$arity$1 ? effect_fn_11998.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_11998.call(null, effect_value));
} else {
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: in \":fx\" effect found ",effect_key," which has no associated handler. Ignoring."], 0));
}


var G__11999 = seq__11853;
var G__12000 = chunk__11854;
var G__12001 = count__11855;
var G__12002 = (i__11856 + (1));
seq__11853 = G__11999;
chunk__11854 = G__12000;
count__11855 = G__12001;
i__11856 = G__12002;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__11853);
if(temp__5804__auto__){
var seq__11853__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__11853__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__11853__$1);
var G__12003 = cljs.core.chunk_rest(seq__11853__$1);
var G__12004 = c__5525__auto__;
var G__12005 = cljs.core.count(c__5525__auto__);
var G__12006 = (0);
seq__11853 = G__12003;
chunk__11854 = G__12004;
count__11855 = G__12005;
i__11856 = G__12006;
continue;
} else {
var vec__11875 = cljs.core.first(seq__11853__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11875,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11875,(1),null);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"db","db",993250759),effect_key)){
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: \":fx\" effect should not contain a :db effect"], 0));
} else {
}

var temp__5802__auto___12007 = day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5802__auto___12007)){
var effect_fn_12008 = temp__5802__auto___12007;
(effect_fn_12008.cljs$core$IFn$_invoke$arity$1 ? effect_fn_12008.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_12008.call(null, effect_value));
} else {
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: in \":fx\" effect found ",effect_key," which has no associated handler. Ignoring."], 0));
}


var G__12009 = cljs.core.next(seq__11853__$1);
var G__12010 = null;
var G__12011 = (0);
var G__12012 = (0);
seq__11853 = G__12009;
chunk__11854 = G__12010;
count__11855 = G__12011;
i__11856 = G__12012;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch","dispatch",1319337009),(function (value){
if((!(cljs.core.vector_QMARK_(value)))){
return day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch value. Expected a vector, but got:",value], 0));
} else {
return day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.router.dispatch(value);
}
}));
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch-n","dispatch-n",-504469236),(function (value){
if((!(cljs.core.sequential_QMARK_(value)))){
return day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch-n value. Expected a collection, but got:",value], 0));
} else {
var seq__11882 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,value));
var chunk__11883 = null;
var count__11884 = (0);
var i__11885 = (0);
while(true){
if((i__11885 < count__11884)){
var event = chunk__11883.cljs$core$IIndexed$_nth$arity$2(null, i__11885);
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.router.dispatch(event);


var G__12013 = seq__11882;
var G__12014 = chunk__11883;
var G__12015 = count__11884;
var G__12016 = (i__11885 + (1));
seq__11882 = G__12013;
chunk__11883 = G__12014;
count__11884 = G__12015;
i__11885 = G__12016;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__11882);
if(temp__5804__auto__){
var seq__11882__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__11882__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__11882__$1);
var G__12017 = cljs.core.chunk_rest(seq__11882__$1);
var G__12018 = c__5525__auto__;
var G__12019 = cljs.core.count(c__5525__auto__);
var G__12020 = (0);
seq__11882 = G__12017;
chunk__11883 = G__12018;
count__11884 = G__12019;
i__11885 = G__12020;
continue;
} else {
var event = cljs.core.first(seq__11882__$1);
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.router.dispatch(event);


var G__12021 = cljs.core.next(seq__11882__$1);
var G__12022 = null;
var G__12023 = (0);
var G__12024 = (0);
seq__11882 = G__12021;
chunk__11883 = G__12022;
count__11884 = G__12023;
i__11885 = G__12024;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.reg_fx(new cljs.core.Keyword(null,"deregister-event-handler","deregister-event-handler",-1096518994),(function (value){
var clear_event = cljs.core.partial.cljs$core$IFn$_invoke$arity$2(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.registrar.clear_handlers,day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.events.kind);
if(cljs.core.sequential_QMARK_(value)){
var seq__11892 = cljs.core.seq(value);
var chunk__11893 = null;
var count__11894 = (0);
var i__11895 = (0);
while(true){
if((i__11895 < count__11894)){
var event = chunk__11893.cljs$core$IIndexed$_nth$arity$2(null, i__11895);
clear_event(event);


var G__12025 = seq__11892;
var G__12026 = chunk__11893;
var G__12027 = count__11894;
var G__12028 = (i__11895 + (1));
seq__11892 = G__12025;
chunk__11893 = G__12026;
count__11894 = G__12027;
i__11895 = G__12028;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__11892);
if(temp__5804__auto__){
var seq__11892__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__11892__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__11892__$1);
var G__12029 = cljs.core.chunk_rest(seq__11892__$1);
var G__12030 = c__5525__auto__;
var G__12031 = cljs.core.count(c__5525__auto__);
var G__12032 = (0);
seq__11892 = G__12029;
chunk__11893 = G__12030;
count__11894 = G__12031;
i__11895 = G__12032;
continue;
} else {
var event = cljs.core.first(seq__11892__$1);
clear_event(event);


var G__12034 = cljs.core.next(seq__11892__$1);
var G__12035 = null;
var G__12036 = (0);
var G__12037 = (0);
seq__11892 = G__12034;
chunk__11893 = G__12035;
count__11894 = G__12036;
i__11895 = G__12037;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return clear_event(value);
}
}));
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.reg_fx(new cljs.core.Keyword(null,"db","db",993250759),(function (value){
if((!((cljs.core.deref(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.db.app_db) === value)))){
return cljs.core.reset_BANG_(day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.db.app_db,value);
} else {
return null;
}
}));

//# sourceMappingURL=day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.fx.js.map
