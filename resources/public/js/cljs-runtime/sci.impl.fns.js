goog.provide('sci.impl.fns');
sci.impl.fns.throw_arity = (function sci$impl$fns$throw_arity(ctx,nsm,fn_name,macro_QMARK_,args){
if(cljs.core.truth_(new cljs.core.Keyword(null,"disable-arity-checks","disable-arity-checks",1131364206).cljs$core$IFn$_invoke$arity$1(ctx))){
return null;
} else {
throw (new Error((function (){var actual_count = (cljs.core.truth_(macro_QMARK_)?(cljs.core.count(args) - (2)):cljs.core.count(args));
return ["Wrong number of args (",cljs.core.str.cljs$core$IFn$_invoke$arity$1(actual_count),") passed to: ",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(nsm),"/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_name)].join('')].join('');
})()));
}
});

/**
* @constructor
 * @implements {sci.impl.types.IBox}
*/
sci.impl.fns.Recur = (function (val){
this.val = val;
});
(sci.impl.fns.Recur.prototype.sci$impl$types$IBox$ = cljs.core.PROTOCOL_SENTINEL);

(sci.impl.fns.Recur.prototype.sci$impl$types$IBox$getVal$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.val;
}));

(sci.impl.fns.Recur.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"val","val",1769233139,null)], null);
}));

(sci.impl.fns.Recur.cljs$lang$type = true);

(sci.impl.fns.Recur.cljs$lang$ctorStr = "sci.impl.fns/Recur");

(sci.impl.fns.Recur.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"sci.impl.fns/Recur");
}));

/**
 * Positional factory function for sci.impl.fns/Recur.
 */
sci.impl.fns.__GT_Recur = (function sci$impl$fns$__GT_Recur(val){
return (new sci.impl.fns.Recur(val));
});

sci.impl.fns.fun = (function sci$impl$fns$fun(ctx,bindings,fn_body,fn_name,macro_QMARK_){
var bindings_fn = new cljs.core.Keyword(null,"bindings-fn","bindings-fn",300799951).cljs$core$IFn$_invoke$arity$1(fn_body);
var bindings__$1 = (bindings_fn.cljs$core$IFn$_invoke$arity$1 ? bindings_fn.cljs$core$IFn$_invoke$arity$1(bindings) : bindings_fn.call(null, bindings));
var fixed_arity = new cljs.core.Keyword(null,"fixed-arity","fixed-arity",1586445869).cljs$core$IFn$_invoke$arity$1(fn_body);
var var_arg_name = new cljs.core.Keyword(null,"var-arg-name","var-arg-name",-1100024887).cljs$core$IFn$_invoke$arity$1(fn_body);
var params = new cljs.core.Keyword(null,"params","params",710516235).cljs$core$IFn$_invoke$arity$1(fn_body);
var body = new cljs.core.Keyword(null,"body","body",-2049205669).cljs$core$IFn$_invoke$arity$1(fn_body);
var nsm = sci.impl.vars.current_ns_name();
var disable_arity_checks_QMARK_ = ctx.get(new cljs.core.Keyword(null,"disable-arity-checks","disable-arity-checks",1131364206));
var f = ((cljs.core.not(var_arg_name))?(function (){var G__24075 = (fixed_arity | (0));
switch (G__24075) {
case (0):
return (function sci$impl$fns$fun_$_arity_0(){
while(true){
var ret = sci.impl.evaluator.eval(ctx,bindings__$1,body);
var recur_QMARK_ = (ret instanceof sci.impl.fns.Recur);
if(recur_QMARK_){
continue;
} else {
return ret;
}
break;
}
});

break;
case (1):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24077 = cljs.core._nth(params,(0));
return (function sci$impl$fns$fun_$_arity_1(G__24076){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24077,G__24076);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$2,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25316 = cljs.core._nth(recur_val,(0));
G__24076 = G__25316;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24079 = cljs.core._nth(params,(0));
return (function sci$impl$fns$fun_$_arity_1(G__24078){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((1),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24079,G__24078);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$2,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25317 = cljs.core._nth(recur_val,(0));
G__24078 = G__25317;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (2):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24082 = cljs.core._nth(params,(0));
var G__24083 = cljs.core._nth(params,(1));
return (function sci$impl$fns$fun_$_arity_2(G__24080,G__24081){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24082,G__24080);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24083,G__24081);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$3,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25320 = cljs.core._nth(recur_val,(0));
var G__25321 = cljs.core._nth(recur_val,(1));
G__24080 = G__25320;
G__24081 = G__25321;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24090 = cljs.core._nth(params,(0));
var G__24091 = cljs.core._nth(params,(1));
return (function sci$impl$fns$fun_$_arity_2(G__24088,G__24089){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((2),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24090,G__24088);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24091,G__24089);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$3,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25324 = cljs.core._nth(recur_val,(0));
var G__25325 = cljs.core._nth(recur_val,(1));
G__24088 = G__25324;
G__24089 = G__25325;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (3):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24099 = cljs.core._nth(params,(0));
var G__24100 = cljs.core._nth(params,(1));
var G__24101 = cljs.core._nth(params,(2));
return (function sci$impl$fns$fun_$_arity_3(G__24096,G__24097,G__24098){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24099,G__24096);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24100,G__24097);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24101,G__24098);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$4,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25327 = cljs.core._nth(recur_val,(0));
var G__25328 = cljs.core._nth(recur_val,(1));
var G__25329 = cljs.core._nth(recur_val,(2));
G__24096 = G__25327;
G__24097 = G__25328;
G__24098 = G__25329;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24107 = cljs.core._nth(params,(0));
var G__24108 = cljs.core._nth(params,(1));
var G__24109 = cljs.core._nth(params,(2));
return (function sci$impl$fns$fun_$_arity_3(G__24104,G__24105,G__24106){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((3),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24107,G__24104);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24108,G__24105);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24109,G__24106);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$4,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25331 = cljs.core._nth(recur_val,(0));
var G__25332 = cljs.core._nth(recur_val,(1));
var G__25333 = cljs.core._nth(recur_val,(2));
G__24104 = G__25331;
G__24105 = G__25332;
G__24106 = G__25333;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (4):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24119 = cljs.core._nth(params,(0));
var G__24120 = cljs.core._nth(params,(1));
var G__24121 = cljs.core._nth(params,(2));
var G__24122 = cljs.core._nth(params,(3));
return (function sci$impl$fns$fun_$_arity_4(G__24115,G__24116,G__24117,G__24118){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24119,G__24115);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24120,G__24116);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24121,G__24117);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24122,G__24118);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$5,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25335 = cljs.core._nth(recur_val,(0));
var G__25336 = cljs.core._nth(recur_val,(1));
var G__25337 = cljs.core._nth(recur_val,(2));
var G__25338 = cljs.core._nth(recur_val,(3));
G__24115 = G__25335;
G__24116 = G__25336;
G__24117 = G__25337;
G__24118 = G__25338;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24127 = cljs.core._nth(params,(0));
var G__24128 = cljs.core._nth(params,(1));
var G__24129 = cljs.core._nth(params,(2));
var G__24130 = cljs.core._nth(params,(3));
return (function sci$impl$fns$fun_$_arity_4(G__24123,G__24124,G__24125,G__24126){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((4),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24127,G__24123);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24128,G__24124);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24129,G__24125);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24130,G__24126);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$5,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25343 = cljs.core._nth(recur_val,(0));
var G__25344 = cljs.core._nth(recur_val,(1));
var G__25345 = cljs.core._nth(recur_val,(2));
var G__25346 = cljs.core._nth(recur_val,(3));
G__24123 = G__25343;
G__24124 = G__25344;
G__24125 = G__25345;
G__24126 = G__25346;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (5):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24146 = cljs.core._nth(params,(0));
var G__24147 = cljs.core._nth(params,(1));
var G__24148 = cljs.core._nth(params,(2));
var G__24149 = cljs.core._nth(params,(3));
var G__24150 = cljs.core._nth(params,(4));
return (function sci$impl$fns$fun_$_arity_5(G__24141,G__24142,G__24143,G__24144,G__24145){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24146,G__24141);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24147,G__24142);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24148,G__24143);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24149,G__24144);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24150,G__24145);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$6,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25348 = cljs.core._nth(recur_val,(0));
var G__25349 = cljs.core._nth(recur_val,(1));
var G__25350 = cljs.core._nth(recur_val,(2));
var G__25351 = cljs.core._nth(recur_val,(3));
var G__25352 = cljs.core._nth(recur_val,(4));
G__24141 = G__25348;
G__24142 = G__25349;
G__24143 = G__25350;
G__24144 = G__25351;
G__24145 = G__25352;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24160 = cljs.core._nth(params,(0));
var G__24161 = cljs.core._nth(params,(1));
var G__24162 = cljs.core._nth(params,(2));
var G__24163 = cljs.core._nth(params,(3));
var G__24164 = cljs.core._nth(params,(4));
return (function sci$impl$fns$fun_$_arity_5(G__24155,G__24156,G__24157,G__24158,G__24159){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((5),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24160,G__24155);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24161,G__24156);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24162,G__24157);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24163,G__24158);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24164,G__24159);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$6,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25353 = cljs.core._nth(recur_val,(0));
var G__25354 = cljs.core._nth(recur_val,(1));
var G__25355 = cljs.core._nth(recur_val,(2));
var G__25356 = cljs.core._nth(recur_val,(3));
var G__25357 = cljs.core._nth(recur_val,(4));
G__24155 = G__25353;
G__24156 = G__25354;
G__24157 = G__25355;
G__24158 = G__25356;
G__24159 = G__25357;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (6):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24189 = cljs.core._nth(params,(0));
var G__24190 = cljs.core._nth(params,(1));
var G__24191 = cljs.core._nth(params,(2));
var G__24192 = cljs.core._nth(params,(3));
var G__24193 = cljs.core._nth(params,(4));
var G__24194 = cljs.core._nth(params,(5));
return (function sci$impl$fns$fun_$_arity_6(G__24183,G__24184,G__24185,G__24186,G__24187,G__24188){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24189,G__24183);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24190,G__24184);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24191,G__24185);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24192,G__24186);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24193,G__24187);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24194,G__24188);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$7,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25359 = cljs.core._nth(recur_val,(0));
var G__25360 = cljs.core._nth(recur_val,(1));
var G__25361 = cljs.core._nth(recur_val,(2));
var G__25362 = cljs.core._nth(recur_val,(3));
var G__25363 = cljs.core._nth(recur_val,(4));
var G__25364 = cljs.core._nth(recur_val,(5));
G__24183 = G__25359;
G__24184 = G__25360;
G__24185 = G__25361;
G__24186 = G__25362;
G__24187 = G__25363;
G__24188 = G__25364;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24204 = cljs.core._nth(params,(0));
var G__24205 = cljs.core._nth(params,(1));
var G__24206 = cljs.core._nth(params,(2));
var G__24207 = cljs.core._nth(params,(3));
var G__24208 = cljs.core._nth(params,(4));
var G__24209 = cljs.core._nth(params,(5));
return (function sci$impl$fns$fun_$_arity_6(G__24198,G__24199,G__24200,G__24201,G__24202,G__24203){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((6),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24204,G__24198);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24205,G__24199);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24206,G__24200);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24207,G__24201);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24208,G__24202);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24209,G__24203);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$7,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25367 = cljs.core._nth(recur_val,(0));
var G__25368 = cljs.core._nth(recur_val,(1));
var G__25369 = cljs.core._nth(recur_val,(2));
var G__25370 = cljs.core._nth(recur_val,(3));
var G__25371 = cljs.core._nth(recur_val,(4));
var G__25372 = cljs.core._nth(recur_val,(5));
G__24198 = G__25367;
G__24199 = G__25368;
G__24200 = G__25369;
G__24201 = G__25370;
G__24202 = G__25371;
G__24203 = G__25372;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (7):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24220 = cljs.core._nth(params,(0));
var G__24221 = cljs.core._nth(params,(1));
var G__24222 = cljs.core._nth(params,(2));
var G__24223 = cljs.core._nth(params,(3));
var G__24224 = cljs.core._nth(params,(4));
var G__24225 = cljs.core._nth(params,(5));
var G__24226 = cljs.core._nth(params,(6));
return (function sci$impl$fns$fun_$_arity_7(G__24213,G__24214,G__24215,G__24216,G__24217,G__24218,G__24219){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24220,G__24213);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24221,G__24214);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24222,G__24215);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24223,G__24216);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24224,G__24217);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24225,G__24218);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24226,G__24219);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$8,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25377 = cljs.core._nth(recur_val,(0));
var G__25378 = cljs.core._nth(recur_val,(1));
var G__25379 = cljs.core._nth(recur_val,(2));
var G__25380 = cljs.core._nth(recur_val,(3));
var G__25381 = cljs.core._nth(recur_val,(4));
var G__25382 = cljs.core._nth(recur_val,(5));
var G__25383 = cljs.core._nth(recur_val,(6));
G__24213 = G__25377;
G__24214 = G__25378;
G__24215 = G__25379;
G__24216 = G__25380;
G__24217 = G__25381;
G__24218 = G__25382;
G__24219 = G__25383;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24238 = cljs.core._nth(params,(0));
var G__24239 = cljs.core._nth(params,(1));
var G__24240 = cljs.core._nth(params,(2));
var G__24241 = cljs.core._nth(params,(3));
var G__24242 = cljs.core._nth(params,(4));
var G__24243 = cljs.core._nth(params,(5));
var G__24244 = cljs.core._nth(params,(6));
return (function sci$impl$fns$fun_$_arity_7(G__24231,G__24232,G__24233,G__24234,G__24235,G__24236,G__24237){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((7),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24238,G__24231);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24239,G__24232);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24240,G__24233);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24241,G__24234);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24242,G__24235);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24243,G__24236);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24244,G__24237);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$8,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25389 = cljs.core._nth(recur_val,(0));
var G__25390 = cljs.core._nth(recur_val,(1));
var G__25391 = cljs.core._nth(recur_val,(2));
var G__25392 = cljs.core._nth(recur_val,(3));
var G__25394 = cljs.core._nth(recur_val,(4));
var G__25395 = cljs.core._nth(recur_val,(5));
var G__25396 = cljs.core._nth(recur_val,(6));
G__24231 = G__25389;
G__24232 = G__25390;
G__24233 = G__25391;
G__24234 = G__25392;
G__24235 = G__25394;
G__24236 = G__25395;
G__24237 = G__25396;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (8):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24256 = cljs.core._nth(params,(0));
var G__24257 = cljs.core._nth(params,(1));
var G__24258 = cljs.core._nth(params,(2));
var G__24259 = cljs.core._nth(params,(3));
var G__24260 = cljs.core._nth(params,(4));
var G__24261 = cljs.core._nth(params,(5));
var G__24262 = cljs.core._nth(params,(6));
var G__24263 = cljs.core._nth(params,(7));
return (function sci$impl$fns$fun_$_arity_8(G__24248,G__24249,G__24250,G__24251,G__24252,G__24253,G__24254,G__24255){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24256,G__24248);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24257,G__24249);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24258,G__24250);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24259,G__24251);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24260,G__24252);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24261,G__24253);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24262,G__24254);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24263,G__24255);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$9,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25399 = cljs.core._nth(recur_val,(0));
var G__25400 = cljs.core._nth(recur_val,(1));
var G__25401 = cljs.core._nth(recur_val,(2));
var G__25402 = cljs.core._nth(recur_val,(3));
var G__25403 = cljs.core._nth(recur_val,(4));
var G__25404 = cljs.core._nth(recur_val,(5));
var G__25405 = cljs.core._nth(recur_val,(6));
var G__25406 = cljs.core._nth(recur_val,(7));
G__24248 = G__25399;
G__24249 = G__25400;
G__24250 = G__25401;
G__24251 = G__25402;
G__24252 = G__25403;
G__24253 = G__25404;
G__24254 = G__25405;
G__24255 = G__25406;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24276 = cljs.core._nth(params,(0));
var G__24277 = cljs.core._nth(params,(1));
var G__24278 = cljs.core._nth(params,(2));
var G__24279 = cljs.core._nth(params,(3));
var G__24280 = cljs.core._nth(params,(4));
var G__24281 = cljs.core._nth(params,(5));
var G__24282 = cljs.core._nth(params,(6));
var G__24283 = cljs.core._nth(params,(7));
return (function sci$impl$fns$fun_$_arity_8(G__24268,G__24269,G__24270,G__24271,G__24272,G__24273,G__24274,G__24275){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((8),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24276,G__24268);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24277,G__24269);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24278,G__24270);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24279,G__24271);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24280,G__24272);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24281,G__24273);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24282,G__24274);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24283,G__24275);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$9,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25412 = cljs.core._nth(recur_val,(0));
var G__25413 = cljs.core._nth(recur_val,(1));
var G__25414 = cljs.core._nth(recur_val,(2));
var G__25415 = cljs.core._nth(recur_val,(3));
var G__25416 = cljs.core._nth(recur_val,(4));
var G__25417 = cljs.core._nth(recur_val,(5));
var G__25418 = cljs.core._nth(recur_val,(6));
var G__25419 = cljs.core._nth(recur_val,(7));
G__24268 = G__25412;
G__24269 = G__25413;
G__24270 = G__25414;
G__24271 = G__25415;
G__24272 = G__25416;
G__24273 = G__25417;
G__24274 = G__25418;
G__24275 = G__25419;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (9):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24303 = cljs.core._nth(params,(0));
var G__24304 = cljs.core._nth(params,(1));
var G__24305 = cljs.core._nth(params,(2));
var G__24306 = cljs.core._nth(params,(3));
var G__24307 = cljs.core._nth(params,(4));
var G__24308 = cljs.core._nth(params,(5));
var G__24309 = cljs.core._nth(params,(6));
var G__24310 = cljs.core._nth(params,(7));
var G__24311 = cljs.core._nth(params,(8));
return (function sci$impl$fns$fun_$_arity_9(G__24294,G__24295,G__24296,G__24297,G__24298,G__24299,G__24300,G__24301,G__24302){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24303,G__24294);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24304,G__24295);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24305,G__24296);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24306,G__24297);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24307,G__24298);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24308,G__24299);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24309,G__24300);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24310,G__24301);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24311,G__24302);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$10,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25431 = cljs.core._nth(recur_val,(0));
var G__25432 = cljs.core._nth(recur_val,(1));
var G__25433 = cljs.core._nth(recur_val,(2));
var G__25434 = cljs.core._nth(recur_val,(3));
var G__25435 = cljs.core._nth(recur_val,(4));
var G__25436 = cljs.core._nth(recur_val,(5));
var G__25437 = cljs.core._nth(recur_val,(6));
var G__25438 = cljs.core._nth(recur_val,(7));
var G__25439 = cljs.core._nth(recur_val,(8));
G__24294 = G__25431;
G__24295 = G__25432;
G__24296 = G__25433;
G__24297 = G__25434;
G__24298 = G__25435;
G__24299 = G__25436;
G__24300 = G__25437;
G__24301 = G__25438;
G__24302 = G__25439;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24324 = cljs.core._nth(params,(0));
var G__24325 = cljs.core._nth(params,(1));
var G__24326 = cljs.core._nth(params,(2));
var G__24327 = cljs.core._nth(params,(3));
var G__24328 = cljs.core._nth(params,(4));
var G__24329 = cljs.core._nth(params,(5));
var G__24330 = cljs.core._nth(params,(6));
var G__24331 = cljs.core._nth(params,(7));
var G__24332 = cljs.core._nth(params,(8));
return (function sci$impl$fns$fun_$_arity_9(G__24315,G__24316,G__24317,G__24318,G__24319,G__24320,G__24321,G__24322,G__24323){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((9),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24324,G__24315);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24325,G__24316);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24326,G__24317);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24327,G__24318);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24328,G__24319);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24329,G__24320);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24330,G__24321);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24331,G__24322);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24332,G__24323);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$10,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25450 = cljs.core._nth(recur_val,(0));
var G__25451 = cljs.core._nth(recur_val,(1));
var G__25452 = cljs.core._nth(recur_val,(2));
var G__25453 = cljs.core._nth(recur_val,(3));
var G__25454 = cljs.core._nth(recur_val,(4));
var G__25455 = cljs.core._nth(recur_val,(5));
var G__25456 = cljs.core._nth(recur_val,(6));
var G__25457 = cljs.core._nth(recur_val,(7));
var G__25458 = cljs.core._nth(recur_val,(8));
G__24315 = G__25450;
G__24316 = G__25451;
G__24317 = G__25452;
G__24318 = G__25453;
G__24319 = G__25454;
G__24320 = G__25455;
G__24321 = G__25456;
G__24322 = G__25457;
G__24323 = G__25458;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (10):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24348 = cljs.core._nth(params,(0));
var G__24349 = cljs.core._nth(params,(1));
var G__24350 = cljs.core._nth(params,(2));
var G__24351 = cljs.core._nth(params,(3));
var G__24352 = cljs.core._nth(params,(4));
var G__24353 = cljs.core._nth(params,(5));
var G__24354 = cljs.core._nth(params,(6));
var G__24355 = cljs.core._nth(params,(7));
var G__24356 = cljs.core._nth(params,(8));
var G__24357 = cljs.core._nth(params,(9));
return (function sci$impl$fns$fun_$_arity_10(G__24338,G__24339,G__24340,G__24341,G__24342,G__24343,G__24344,G__24345,G__24346,G__24347){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24348,G__24338);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24349,G__24339);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24350,G__24340);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24351,G__24341);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24352,G__24342);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24353,G__24343);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24354,G__24344);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24355,G__24345);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24356,G__24346);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__24357,G__24347);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$11,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25464 = cljs.core._nth(recur_val,(0));
var G__25465 = cljs.core._nth(recur_val,(1));
var G__25466 = cljs.core._nth(recur_val,(2));
var G__25467 = cljs.core._nth(recur_val,(3));
var G__25468 = cljs.core._nth(recur_val,(4));
var G__25469 = cljs.core._nth(recur_val,(5));
var G__25470 = cljs.core._nth(recur_val,(6));
var G__25471 = cljs.core._nth(recur_val,(7));
var G__25472 = cljs.core._nth(recur_val,(8));
var G__25473 = cljs.core._nth(recur_val,(9));
G__24338 = G__25464;
G__24339 = G__25465;
G__24340 = G__25466;
G__24341 = G__25467;
G__24342 = G__25468;
G__24343 = G__25469;
G__24344 = G__25470;
G__24345 = G__25471;
G__24346 = G__25472;
G__24347 = G__25473;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24376 = cljs.core._nth(params,(0));
var G__24377 = cljs.core._nth(params,(1));
var G__24378 = cljs.core._nth(params,(2));
var G__24379 = cljs.core._nth(params,(3));
var G__24380 = cljs.core._nth(params,(4));
var G__24381 = cljs.core._nth(params,(5));
var G__24382 = cljs.core._nth(params,(6));
var G__24383 = cljs.core._nth(params,(7));
var G__24384 = cljs.core._nth(params,(8));
var G__24385 = cljs.core._nth(params,(9));
return (function sci$impl$fns$fun_$_arity_10(G__24366,G__24367,G__24368,G__24369,G__24370,G__24371,G__24372,G__24373,G__24374,G__24375){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((10),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24376,G__24366);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24377,G__24367);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24378,G__24368);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24379,G__24369);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24380,G__24370);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24381,G__24371);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24382,G__24372);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24383,G__24373);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24384,G__24374);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__24385,G__24375);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$11,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25482 = cljs.core._nth(recur_val,(0));
var G__25483 = cljs.core._nth(recur_val,(1));
var G__25484 = cljs.core._nth(recur_val,(2));
var G__25485 = cljs.core._nth(recur_val,(3));
var G__25486 = cljs.core._nth(recur_val,(4));
var G__25487 = cljs.core._nth(recur_val,(5));
var G__25488 = cljs.core._nth(recur_val,(6));
var G__25489 = cljs.core._nth(recur_val,(7));
var G__25490 = cljs.core._nth(recur_val,(8));
var G__25491 = cljs.core._nth(recur_val,(9));
G__24366 = G__25482;
G__24367 = G__25483;
G__24368 = G__25484;
G__24369 = G__25485;
G__24370 = G__25486;
G__24371 = G__25487;
G__24372 = G__25488;
G__24373 = G__25489;
G__24374 = G__25490;
G__24375 = G__25491;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (11):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24412 = cljs.core._nth(params,(0));
var G__24413 = cljs.core._nth(params,(1));
var G__24414 = cljs.core._nth(params,(2));
var G__24415 = cljs.core._nth(params,(3));
var G__24416 = cljs.core._nth(params,(4));
var G__24417 = cljs.core._nth(params,(5));
var G__24418 = cljs.core._nth(params,(6));
var G__24419 = cljs.core._nth(params,(7));
var G__24420 = cljs.core._nth(params,(8));
var G__24421 = cljs.core._nth(params,(9));
var G__24422 = cljs.core._nth(params,(10));
return (function sci$impl$fns$fun_$_arity_11(G__24401,G__24402,G__24403,G__24404,G__24405,G__24406,G__24407,G__24408,G__24409,G__24410,G__24411){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24412,G__24401);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24413,G__24402);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24414,G__24403);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24415,G__24404);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24416,G__24405);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24417,G__24406);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24418,G__24407);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24419,G__24408);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24420,G__24409);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__24421,G__24410);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__24422,G__24411);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$12,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25500 = cljs.core._nth(recur_val,(0));
var G__25501 = cljs.core._nth(recur_val,(1));
var G__25502 = cljs.core._nth(recur_val,(2));
var G__25503 = cljs.core._nth(recur_val,(3));
var G__25504 = cljs.core._nth(recur_val,(4));
var G__25505 = cljs.core._nth(recur_val,(5));
var G__25506 = cljs.core._nth(recur_val,(6));
var G__25507 = cljs.core._nth(recur_val,(7));
var G__25508 = cljs.core._nth(recur_val,(8));
var G__25509 = cljs.core._nth(recur_val,(9));
var G__25510 = cljs.core._nth(recur_val,(10));
G__24401 = G__25500;
G__24402 = G__25501;
G__24403 = G__25502;
G__24404 = G__25503;
G__24405 = G__25504;
G__24406 = G__25505;
G__24407 = G__25506;
G__24408 = G__25507;
G__24409 = G__25508;
G__24410 = G__25509;
G__24411 = G__25510;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24441 = cljs.core._nth(params,(0));
var G__24442 = cljs.core._nth(params,(1));
var G__24443 = cljs.core._nth(params,(2));
var G__24444 = cljs.core._nth(params,(3));
var G__24445 = cljs.core._nth(params,(4));
var G__24446 = cljs.core._nth(params,(5));
var G__24447 = cljs.core._nth(params,(6));
var G__24448 = cljs.core._nth(params,(7));
var G__24449 = cljs.core._nth(params,(8));
var G__24450 = cljs.core._nth(params,(9));
var G__24451 = cljs.core._nth(params,(10));
return (function sci$impl$fns$fun_$_arity_11(G__24430,G__24431,G__24432,G__24433,G__24434,G__24435,G__24436,G__24437,G__24438,G__24439,G__24440){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((11),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24441,G__24430);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24442,G__24431);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24443,G__24432);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24444,G__24433);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24445,G__24434);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24446,G__24435);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24447,G__24436);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24448,G__24437);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24449,G__24438);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__24450,G__24439);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__24451,G__24440);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$12,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25524 = cljs.core._nth(recur_val,(0));
var G__25525 = cljs.core._nth(recur_val,(1));
var G__25526 = cljs.core._nth(recur_val,(2));
var G__25527 = cljs.core._nth(recur_val,(3));
var G__25528 = cljs.core._nth(recur_val,(4));
var G__25529 = cljs.core._nth(recur_val,(5));
var G__25530 = cljs.core._nth(recur_val,(6));
var G__25531 = cljs.core._nth(recur_val,(7));
var G__25532 = cljs.core._nth(recur_val,(8));
var G__25533 = cljs.core._nth(recur_val,(9));
var G__25534 = cljs.core._nth(recur_val,(10));
G__24430 = G__25524;
G__24431 = G__25525;
G__24432 = G__25526;
G__24433 = G__25527;
G__24434 = G__25528;
G__24435 = G__25529;
G__24436 = G__25530;
G__24437 = G__25531;
G__24438 = G__25532;
G__24439 = G__25533;
G__24440 = G__25534;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (12):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24486 = cljs.core._nth(params,(0));
var G__24487 = cljs.core._nth(params,(1));
var G__24488 = cljs.core._nth(params,(2));
var G__24489 = cljs.core._nth(params,(3));
var G__24490 = cljs.core._nth(params,(4));
var G__24491 = cljs.core._nth(params,(5));
var G__24492 = cljs.core._nth(params,(6));
var G__24493 = cljs.core._nth(params,(7));
var G__24494 = cljs.core._nth(params,(8));
var G__24495 = cljs.core._nth(params,(9));
var G__24496 = cljs.core._nth(params,(10));
var G__24497 = cljs.core._nth(params,(11));
return (function sci$impl$fns$fun_$_arity_12(G__24474,G__24475,G__24476,G__24477,G__24478,G__24479,G__24480,G__24481,G__24482,G__24483,G__24484,G__24485){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24486,G__24474);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24487,G__24475);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24488,G__24476);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24489,G__24477);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24490,G__24478);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24491,G__24479);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24492,G__24480);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24493,G__24481);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24494,G__24482);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__24495,G__24483);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__24496,G__24484);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__24497,G__24485);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$13,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25540 = cljs.core._nth(recur_val,(0));
var G__25541 = cljs.core._nth(recur_val,(1));
var G__25542 = cljs.core._nth(recur_val,(2));
var G__25543 = cljs.core._nth(recur_val,(3));
var G__25544 = cljs.core._nth(recur_val,(4));
var G__25545 = cljs.core._nth(recur_val,(5));
var G__25546 = cljs.core._nth(recur_val,(6));
var G__25547 = cljs.core._nth(recur_val,(7));
var G__25548 = cljs.core._nth(recur_val,(8));
var G__25549 = cljs.core._nth(recur_val,(9));
var G__25550 = cljs.core._nth(recur_val,(10));
var G__25551 = cljs.core._nth(recur_val,(11));
G__24474 = G__25540;
G__24475 = G__25541;
G__24476 = G__25542;
G__24477 = G__25543;
G__24478 = G__25544;
G__24479 = G__25545;
G__24480 = G__25546;
G__24481 = G__25547;
G__24482 = G__25548;
G__24483 = G__25549;
G__24484 = G__25550;
G__24485 = G__25551;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24520 = cljs.core._nth(params,(0));
var G__24521 = cljs.core._nth(params,(1));
var G__24522 = cljs.core._nth(params,(2));
var G__24523 = cljs.core._nth(params,(3));
var G__24524 = cljs.core._nth(params,(4));
var G__24525 = cljs.core._nth(params,(5));
var G__24526 = cljs.core._nth(params,(6));
var G__24527 = cljs.core._nth(params,(7));
var G__24528 = cljs.core._nth(params,(8));
var G__24529 = cljs.core._nth(params,(9));
var G__24530 = cljs.core._nth(params,(10));
var G__24531 = cljs.core._nth(params,(11));
return (function sci$impl$fns$fun_$_arity_12(G__24508,G__24509,G__24510,G__24511,G__24512,G__24513,G__24514,G__24515,G__24516,G__24517,G__24518,G__24519){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((12),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24520,G__24508);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24521,G__24509);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24522,G__24510);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24523,G__24511);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24524,G__24512);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24525,G__24513);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24526,G__24514);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24527,G__24515);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24528,G__24516);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__24529,G__24517);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__24530,G__24518);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__24531,G__24519);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$13,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25565 = cljs.core._nth(recur_val,(0));
var G__25566 = cljs.core._nth(recur_val,(1));
var G__25567 = cljs.core._nth(recur_val,(2));
var G__25568 = cljs.core._nth(recur_val,(3));
var G__25569 = cljs.core._nth(recur_val,(4));
var G__25570 = cljs.core._nth(recur_val,(5));
var G__25571 = cljs.core._nth(recur_val,(6));
var G__25572 = cljs.core._nth(recur_val,(7));
var G__25573 = cljs.core._nth(recur_val,(8));
var G__25574 = cljs.core._nth(recur_val,(9));
var G__25575 = cljs.core._nth(recur_val,(10));
var G__25576 = cljs.core._nth(recur_val,(11));
G__24508 = G__25565;
G__24509 = G__25566;
G__24510 = G__25567;
G__24511 = G__25568;
G__24512 = G__25569;
G__24513 = G__25570;
G__24514 = G__25571;
G__24515 = G__25572;
G__24516 = G__25573;
G__24517 = G__25574;
G__24518 = G__25575;
G__24519 = G__25576;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (13):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24554 = cljs.core._nth(params,(0));
var G__24555 = cljs.core._nth(params,(1));
var G__24556 = cljs.core._nth(params,(2));
var G__24557 = cljs.core._nth(params,(3));
var G__24558 = cljs.core._nth(params,(4));
var G__24559 = cljs.core._nth(params,(5));
var G__24560 = cljs.core._nth(params,(6));
var G__24561 = cljs.core._nth(params,(7));
var G__24562 = cljs.core._nth(params,(8));
var G__24563 = cljs.core._nth(params,(9));
var G__24564 = cljs.core._nth(params,(10));
var G__24565 = cljs.core._nth(params,(11));
var G__24566 = cljs.core._nth(params,(12));
return (function sci$impl$fns$fun_$_arity_13(G__24541,G__24542,G__24543,G__24544,G__24545,G__24546,G__24547,G__24548,G__24549,G__24550,G__24551,G__24552,G__24553){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24554,G__24541);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24555,G__24542);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24556,G__24543);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24557,G__24544);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24558,G__24545);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24559,G__24546);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24560,G__24547);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24561,G__24548);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24562,G__24549);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__24563,G__24550);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__24564,G__24551);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__24565,G__24552);
var bindings__$14 = cljs.core._assoc(bindings__$13,G__24566,G__24553);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$14,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25582 = cljs.core._nth(recur_val,(0));
var G__25583 = cljs.core._nth(recur_val,(1));
var G__25584 = cljs.core._nth(recur_val,(2));
var G__25585 = cljs.core._nth(recur_val,(3));
var G__25586 = cljs.core._nth(recur_val,(4));
var G__25587 = cljs.core._nth(recur_val,(5));
var G__25588 = cljs.core._nth(recur_val,(6));
var G__25589 = cljs.core._nth(recur_val,(7));
var G__25590 = cljs.core._nth(recur_val,(8));
var G__25591 = cljs.core._nth(recur_val,(9));
var G__25592 = cljs.core._nth(recur_val,(10));
var G__25593 = cljs.core._nth(recur_val,(11));
var G__25594 = cljs.core._nth(recur_val,(12));
G__24541 = G__25582;
G__24542 = G__25583;
G__24543 = G__25584;
G__24544 = G__25585;
G__24545 = G__25586;
G__24546 = G__25587;
G__24547 = G__25588;
G__24548 = G__25589;
G__24549 = G__25590;
G__24550 = G__25591;
G__24551 = G__25592;
G__24552 = G__25593;
G__24553 = G__25594;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24587 = cljs.core._nth(params,(0));
var G__24588 = cljs.core._nth(params,(1));
var G__24589 = cljs.core._nth(params,(2));
var G__24590 = cljs.core._nth(params,(3));
var G__24591 = cljs.core._nth(params,(4));
var G__24592 = cljs.core._nth(params,(5));
var G__24593 = cljs.core._nth(params,(6));
var G__24594 = cljs.core._nth(params,(7));
var G__24595 = cljs.core._nth(params,(8));
var G__24596 = cljs.core._nth(params,(9));
var G__24597 = cljs.core._nth(params,(10));
var G__24598 = cljs.core._nth(params,(11));
var G__24599 = cljs.core._nth(params,(12));
return (function sci$impl$fns$fun_$_arity_13(G__24574,G__24575,G__24576,G__24577,G__24578,G__24579,G__24580,G__24581,G__24582,G__24583,G__24584,G__24585,G__24586){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((13),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24587,G__24574);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24588,G__24575);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24589,G__24576);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24590,G__24577);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24591,G__24578);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24592,G__24579);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24593,G__24580);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24594,G__24581);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24595,G__24582);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__24596,G__24583);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__24597,G__24584);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__24598,G__24585);
var bindings__$14 = cljs.core._assoc(bindings__$13,G__24599,G__24586);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$14,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25626 = cljs.core._nth(recur_val,(0));
var G__25627 = cljs.core._nth(recur_val,(1));
var G__25628 = cljs.core._nth(recur_val,(2));
var G__25629 = cljs.core._nth(recur_val,(3));
var G__25630 = cljs.core._nth(recur_val,(4));
var G__25631 = cljs.core._nth(recur_val,(5));
var G__25632 = cljs.core._nth(recur_val,(6));
var G__25633 = cljs.core._nth(recur_val,(7));
var G__25634 = cljs.core._nth(recur_val,(8));
var G__25635 = cljs.core._nth(recur_val,(9));
var G__25636 = cljs.core._nth(recur_val,(10));
var G__25637 = cljs.core._nth(recur_val,(11));
var G__25638 = cljs.core._nth(recur_val,(12));
G__24574 = G__25626;
G__24575 = G__25627;
G__24576 = G__25628;
G__24577 = G__25629;
G__24578 = G__25630;
G__24579 = G__25631;
G__24580 = G__25632;
G__24581 = G__25633;
G__24582 = G__25634;
G__24583 = G__25635;
G__24584 = G__25636;
G__24585 = G__25637;
G__24586 = G__25638;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (14):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24620 = cljs.core._nth(params,(0));
var G__24621 = cljs.core._nth(params,(1));
var G__24622 = cljs.core._nth(params,(2));
var G__24623 = cljs.core._nth(params,(3));
var G__24624 = cljs.core._nth(params,(4));
var G__24625 = cljs.core._nth(params,(5));
var G__24626 = cljs.core._nth(params,(6));
var G__24627 = cljs.core._nth(params,(7));
var G__24628 = cljs.core._nth(params,(8));
var G__24629 = cljs.core._nth(params,(9));
var G__24630 = cljs.core._nth(params,(10));
var G__24631 = cljs.core._nth(params,(11));
var G__24632 = cljs.core._nth(params,(12));
var G__24633 = cljs.core._nth(params,(13));
var G__24634 = cljs.core._nth(params,(14));
return (function sci$impl$fns$fun_$_arity_15(G__24605,G__24606,G__24607,G__24608,G__24609,G__24610,G__24611,G__24612,G__24613,G__24614,G__24615,G__24616,G__24617,G__24618,G__24619){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24620,G__24605);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24621,G__24606);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24622,G__24607);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24623,G__24608);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24624,G__24609);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24625,G__24610);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24626,G__24611);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24627,G__24612);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24628,G__24613);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__24629,G__24614);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__24630,G__24615);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__24631,G__24616);
var bindings__$14 = cljs.core._assoc(bindings__$13,G__24632,G__24617);
var bindings__$15 = cljs.core._assoc(bindings__$14,G__24633,G__24618);
var bindings__$16 = cljs.core._assoc(bindings__$15,G__24634,G__24619);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$16,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25658 = cljs.core._nth(recur_val,(0));
var G__25659 = cljs.core._nth(recur_val,(1));
var G__25660 = cljs.core._nth(recur_val,(2));
var G__25661 = cljs.core._nth(recur_val,(3));
var G__25662 = cljs.core._nth(recur_val,(4));
var G__25663 = cljs.core._nth(recur_val,(5));
var G__25664 = cljs.core._nth(recur_val,(6));
var G__25665 = cljs.core._nth(recur_val,(7));
var G__25666 = cljs.core._nth(recur_val,(8));
var G__25667 = cljs.core._nth(recur_val,(9));
var G__25668 = cljs.core._nth(recur_val,(10));
var G__25669 = cljs.core._nth(recur_val,(11));
var G__25670 = cljs.core._nth(recur_val,(12));
var G__25671 = cljs.core._nth(recur_val,(13));
var G__25672 = cljs.core._nth(recur_val,(14));
G__24605 = G__25658;
G__24606 = G__25659;
G__24607 = G__25660;
G__24608 = G__25661;
G__24609 = G__25662;
G__24610 = G__25663;
G__24611 = G__25664;
G__24612 = G__25665;
G__24613 = G__25666;
G__24614 = G__25667;
G__24615 = G__25668;
G__24616 = G__25669;
G__24617 = G__25670;
G__24618 = G__25671;
G__24619 = G__25672;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24656 = cljs.core._nth(params,(0));
var G__24657 = cljs.core._nth(params,(1));
var G__24658 = cljs.core._nth(params,(2));
var G__24659 = cljs.core._nth(params,(3));
var G__24660 = cljs.core._nth(params,(4));
var G__24661 = cljs.core._nth(params,(5));
var G__24662 = cljs.core._nth(params,(6));
var G__24663 = cljs.core._nth(params,(7));
var G__24664 = cljs.core._nth(params,(8));
var G__24665 = cljs.core._nth(params,(9));
var G__24666 = cljs.core._nth(params,(10));
var G__24667 = cljs.core._nth(params,(11));
var G__24668 = cljs.core._nth(params,(12));
var G__24669 = cljs.core._nth(params,(13));
var G__24670 = cljs.core._nth(params,(14));
return (function sci$impl$fns$fun_$_arity_15(G__24641,G__24642,G__24643,G__24644,G__24645,G__24646,G__24647,G__24648,G__24649,G__24650,G__24651,G__24652,G__24653,G__24654,G__24655){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((15),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24656,G__24641);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24657,G__24642);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24658,G__24643);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24659,G__24644);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24660,G__24645);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24661,G__24646);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24662,G__24647);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24663,G__24648);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24664,G__24649);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__24665,G__24650);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__24666,G__24651);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__24667,G__24652);
var bindings__$14 = cljs.core._assoc(bindings__$13,G__24668,G__24653);
var bindings__$15 = cljs.core._assoc(bindings__$14,G__24669,G__24654);
var bindings__$16 = cljs.core._assoc(bindings__$15,G__24670,G__24655);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$16,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25685 = cljs.core._nth(recur_val,(0));
var G__25686 = cljs.core._nth(recur_val,(1));
var G__25687 = cljs.core._nth(recur_val,(2));
var G__25688 = cljs.core._nth(recur_val,(3));
var G__25689 = cljs.core._nth(recur_val,(4));
var G__25690 = cljs.core._nth(recur_val,(5));
var G__25691 = cljs.core._nth(recur_val,(6));
var G__25692 = cljs.core._nth(recur_val,(7));
var G__25693 = cljs.core._nth(recur_val,(8));
var G__25694 = cljs.core._nth(recur_val,(9));
var G__25695 = cljs.core._nth(recur_val,(10));
var G__25696 = cljs.core._nth(recur_val,(11));
var G__25697 = cljs.core._nth(recur_val,(12));
var G__25698 = cljs.core._nth(recur_val,(13));
var G__25699 = cljs.core._nth(recur_val,(14));
G__24641 = G__25685;
G__24642 = G__25686;
G__24643 = G__25687;
G__24644 = G__25688;
G__24645 = G__25689;
G__24646 = G__25690;
G__24647 = G__25691;
G__24648 = G__25692;
G__24649 = G__25693;
G__24650 = G__25694;
G__24651 = G__25695;
G__24652 = G__25696;
G__24653 = G__25697;
G__24654 = G__25698;
G__24655 = G__25699;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (15):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24692 = cljs.core._nth(params,(0));
var G__24693 = cljs.core._nth(params,(1));
var G__24694 = cljs.core._nth(params,(2));
var G__24695 = cljs.core._nth(params,(3));
var G__24696 = cljs.core._nth(params,(4));
var G__24697 = cljs.core._nth(params,(5));
var G__24698 = cljs.core._nth(params,(6));
var G__24699 = cljs.core._nth(params,(7));
var G__24700 = cljs.core._nth(params,(8));
var G__24701 = cljs.core._nth(params,(9));
var G__24702 = cljs.core._nth(params,(10));
var G__24703 = cljs.core._nth(params,(11));
var G__24704 = cljs.core._nth(params,(12));
var G__24705 = cljs.core._nth(params,(13));
var G__24706 = cljs.core._nth(params,(14));
return (function sci$impl$fns$fun_$_arity_15(G__24677,G__24678,G__24679,G__24680,G__24681,G__24682,G__24683,G__24684,G__24685,G__24686,G__24687,G__24688,G__24689,G__24690,G__24691){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24692,G__24677);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24693,G__24678);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24694,G__24679);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24695,G__24680);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24696,G__24681);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24697,G__24682);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24698,G__24683);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24699,G__24684);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24700,G__24685);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__24701,G__24686);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__24702,G__24687);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__24703,G__24688);
var bindings__$14 = cljs.core._assoc(bindings__$13,G__24704,G__24689);
var bindings__$15 = cljs.core._assoc(bindings__$14,G__24705,G__24690);
var bindings__$16 = cljs.core._assoc(bindings__$15,G__24706,G__24691);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$16,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25729 = cljs.core._nth(recur_val,(0));
var G__25730 = cljs.core._nth(recur_val,(1));
var G__25731 = cljs.core._nth(recur_val,(2));
var G__25732 = cljs.core._nth(recur_val,(3));
var G__25733 = cljs.core._nth(recur_val,(4));
var G__25734 = cljs.core._nth(recur_val,(5));
var G__25735 = cljs.core._nth(recur_val,(6));
var G__25736 = cljs.core._nth(recur_val,(7));
var G__25737 = cljs.core._nth(recur_val,(8));
var G__25738 = cljs.core._nth(recur_val,(9));
var G__25739 = cljs.core._nth(recur_val,(10));
var G__25740 = cljs.core._nth(recur_val,(11));
var G__25741 = cljs.core._nth(recur_val,(12));
var G__25742 = cljs.core._nth(recur_val,(13));
var G__25743 = cljs.core._nth(recur_val,(14));
G__24677 = G__25729;
G__24678 = G__25730;
G__24679 = G__25731;
G__24680 = G__25732;
G__24681 = G__25733;
G__24682 = G__25734;
G__24683 = G__25735;
G__24684 = G__25736;
G__24685 = G__25737;
G__24686 = G__25738;
G__24687 = G__25739;
G__24688 = G__25740;
G__24689 = G__25741;
G__24690 = G__25742;
G__24691 = G__25743;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24732 = cljs.core._nth(params,(0));
var G__24733 = cljs.core._nth(params,(1));
var G__24734 = cljs.core._nth(params,(2));
var G__24735 = cljs.core._nth(params,(3));
var G__24736 = cljs.core._nth(params,(4));
var G__24737 = cljs.core._nth(params,(5));
var G__24738 = cljs.core._nth(params,(6));
var G__24739 = cljs.core._nth(params,(7));
var G__24740 = cljs.core._nth(params,(8));
var G__24741 = cljs.core._nth(params,(9));
var G__24742 = cljs.core._nth(params,(10));
var G__24743 = cljs.core._nth(params,(11));
var G__24744 = cljs.core._nth(params,(12));
var G__24745 = cljs.core._nth(params,(13));
var G__24746 = cljs.core._nth(params,(14));
return (function sci$impl$fns$fun_$_arity_15(G__24717,G__24718,G__24719,G__24720,G__24721,G__24722,G__24723,G__24724,G__24725,G__24726,G__24727,G__24728,G__24729,G__24730,G__24731){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((15),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24732,G__24717);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24733,G__24718);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24734,G__24719);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24735,G__24720);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24736,G__24721);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24737,G__24722);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24738,G__24723);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24739,G__24724);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24740,G__24725);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__24741,G__24726);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__24742,G__24727);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__24743,G__24728);
var bindings__$14 = cljs.core._assoc(bindings__$13,G__24744,G__24729);
var bindings__$15 = cljs.core._assoc(bindings__$14,G__24745,G__24730);
var bindings__$16 = cljs.core._assoc(bindings__$15,G__24746,G__24731);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$16,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25752 = cljs.core._nth(recur_val,(0));
var G__25753 = cljs.core._nth(recur_val,(1));
var G__25754 = cljs.core._nth(recur_val,(2));
var G__25755 = cljs.core._nth(recur_val,(3));
var G__25756 = cljs.core._nth(recur_val,(4));
var G__25757 = cljs.core._nth(recur_val,(5));
var G__25758 = cljs.core._nth(recur_val,(6));
var G__25759 = cljs.core._nth(recur_val,(7));
var G__25760 = cljs.core._nth(recur_val,(8));
var G__25761 = cljs.core._nth(recur_val,(9));
var G__25762 = cljs.core._nth(recur_val,(10));
var G__25763 = cljs.core._nth(recur_val,(11));
var G__25764 = cljs.core._nth(recur_val,(12));
var G__25765 = cljs.core._nth(recur_val,(13));
var G__25766 = cljs.core._nth(recur_val,(14));
G__24717 = G__25752;
G__24718 = G__25753;
G__24719 = G__25754;
G__24720 = G__25755;
G__24721 = G__25756;
G__24722 = G__25757;
G__24723 = G__25758;
G__24724 = G__25759;
G__24725 = G__25760;
G__24726 = G__25761;
G__24727 = G__25762;
G__24728 = G__25763;
G__24729 = G__25764;
G__24730 = G__25765;
G__24731 = G__25766;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (16):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24771 = cljs.core._nth(params,(0));
var G__24772 = cljs.core._nth(params,(1));
var G__24773 = cljs.core._nth(params,(2));
var G__24774 = cljs.core._nth(params,(3));
var G__24775 = cljs.core._nth(params,(4));
var G__24776 = cljs.core._nth(params,(5));
var G__24777 = cljs.core._nth(params,(6));
var G__24778 = cljs.core._nth(params,(7));
var G__24779 = cljs.core._nth(params,(8));
var G__24780 = cljs.core._nth(params,(9));
var G__24781 = cljs.core._nth(params,(10));
var G__24782 = cljs.core._nth(params,(11));
var G__24783 = cljs.core._nth(params,(12));
var G__24784 = cljs.core._nth(params,(13));
var G__24785 = cljs.core._nth(params,(14));
var G__24786 = cljs.core._nth(params,(15));
return (function sci$impl$fns$fun_$_arity_16(G__24755,G__24756,G__24757,G__24758,G__24759,G__24760,G__24761,G__24762,G__24763,G__24764,G__24765,G__24766,G__24767,G__24768,G__24769,G__24770){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24771,G__24755);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24772,G__24756);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24773,G__24757);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24774,G__24758);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24775,G__24759);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24776,G__24760);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24777,G__24761);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24778,G__24762);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24779,G__24763);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__24780,G__24764);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__24781,G__24765);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__24782,G__24766);
var bindings__$14 = cljs.core._assoc(bindings__$13,G__24783,G__24767);
var bindings__$15 = cljs.core._assoc(bindings__$14,G__24784,G__24768);
var bindings__$16 = cljs.core._assoc(bindings__$15,G__24785,G__24769);
var bindings__$17 = cljs.core._assoc(bindings__$16,G__24786,G__24770);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$17,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25782 = cljs.core._nth(recur_val,(0));
var G__25783 = cljs.core._nth(recur_val,(1));
var G__25784 = cljs.core._nth(recur_val,(2));
var G__25785 = cljs.core._nth(recur_val,(3));
var G__25786 = cljs.core._nth(recur_val,(4));
var G__25787 = cljs.core._nth(recur_val,(5));
var G__25788 = cljs.core._nth(recur_val,(6));
var G__25789 = cljs.core._nth(recur_val,(7));
var G__25790 = cljs.core._nth(recur_val,(8));
var G__25791 = cljs.core._nth(recur_val,(9));
var G__25792 = cljs.core._nth(recur_val,(10));
var G__25793 = cljs.core._nth(recur_val,(11));
var G__25794 = cljs.core._nth(recur_val,(12));
var G__25795 = cljs.core._nth(recur_val,(13));
var G__25796 = cljs.core._nth(recur_val,(14));
var G__25797 = cljs.core._nth(recur_val,(15));
G__24755 = G__25782;
G__24756 = G__25783;
G__24757 = G__25784;
G__24758 = G__25785;
G__24759 = G__25786;
G__24760 = G__25787;
G__24761 = G__25788;
G__24762 = G__25789;
G__24763 = G__25790;
G__24764 = G__25791;
G__24765 = G__25792;
G__24766 = G__25793;
G__24767 = G__25794;
G__24768 = G__25795;
G__24769 = G__25796;
G__24770 = G__25797;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24814 = cljs.core._nth(params,(0));
var G__24815 = cljs.core._nth(params,(1));
var G__24816 = cljs.core._nth(params,(2));
var G__24817 = cljs.core._nth(params,(3));
var G__24818 = cljs.core._nth(params,(4));
var G__24819 = cljs.core._nth(params,(5));
var G__24820 = cljs.core._nth(params,(6));
var G__24821 = cljs.core._nth(params,(7));
var G__24822 = cljs.core._nth(params,(8));
var G__24823 = cljs.core._nth(params,(9));
var G__24824 = cljs.core._nth(params,(10));
var G__24825 = cljs.core._nth(params,(11));
var G__24826 = cljs.core._nth(params,(12));
var G__24827 = cljs.core._nth(params,(13));
var G__24828 = cljs.core._nth(params,(14));
var G__24829 = cljs.core._nth(params,(15));
return (function sci$impl$fns$fun_$_arity_16(G__24798,G__24799,G__24800,G__24801,G__24802,G__24803,G__24804,G__24805,G__24806,G__24807,G__24808,G__24809,G__24810,G__24811,G__24812,G__24813){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((16),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24814,G__24798);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24815,G__24799);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24816,G__24800);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24817,G__24801);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24818,G__24802);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24819,G__24803);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24820,G__24804);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24821,G__24805);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24822,G__24806);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__24823,G__24807);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__24824,G__24808);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__24825,G__24809);
var bindings__$14 = cljs.core._assoc(bindings__$13,G__24826,G__24810);
var bindings__$15 = cljs.core._assoc(bindings__$14,G__24827,G__24811);
var bindings__$16 = cljs.core._assoc(bindings__$15,G__24828,G__24812);
var bindings__$17 = cljs.core._assoc(bindings__$16,G__24829,G__24813);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$17,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25801 = cljs.core._nth(recur_val,(0));
var G__25802 = cljs.core._nth(recur_val,(1));
var G__25803 = cljs.core._nth(recur_val,(2));
var G__25804 = cljs.core._nth(recur_val,(3));
var G__25805 = cljs.core._nth(recur_val,(4));
var G__25806 = cljs.core._nth(recur_val,(5));
var G__25807 = cljs.core._nth(recur_val,(6));
var G__25808 = cljs.core._nth(recur_val,(7));
var G__25809 = cljs.core._nth(recur_val,(8));
var G__25810 = cljs.core._nth(recur_val,(9));
var G__25811 = cljs.core._nth(recur_val,(10));
var G__25812 = cljs.core._nth(recur_val,(11));
var G__25813 = cljs.core._nth(recur_val,(12));
var G__25814 = cljs.core._nth(recur_val,(13));
var G__25815 = cljs.core._nth(recur_val,(14));
var G__25816 = cljs.core._nth(recur_val,(15));
G__24798 = G__25801;
G__24799 = G__25802;
G__24800 = G__25803;
G__24801 = G__25804;
G__24802 = G__25805;
G__24803 = G__25806;
G__24804 = G__25807;
G__24805 = G__25808;
G__24806 = G__25809;
G__24807 = G__25810;
G__24808 = G__25811;
G__24809 = G__25812;
G__24810 = G__25813;
G__24811 = G__25814;
G__24812 = G__25815;
G__24813 = G__25816;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (17):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24856 = cljs.core._nth(params,(0));
var G__24857 = cljs.core._nth(params,(1));
var G__24858 = cljs.core._nth(params,(2));
var G__24859 = cljs.core._nth(params,(3));
var G__24860 = cljs.core._nth(params,(4));
var G__24861 = cljs.core._nth(params,(5));
var G__24862 = cljs.core._nth(params,(6));
var G__24863 = cljs.core._nth(params,(7));
var G__24864 = cljs.core._nth(params,(8));
var G__24865 = cljs.core._nth(params,(9));
var G__24866 = cljs.core._nth(params,(10));
var G__24867 = cljs.core._nth(params,(11));
var G__24868 = cljs.core._nth(params,(12));
var G__24869 = cljs.core._nth(params,(13));
var G__24870 = cljs.core._nth(params,(14));
var G__24871 = cljs.core._nth(params,(15));
var G__24872 = cljs.core._nth(params,(16));
return (function sci$impl$fns$fun_$_arity_17(G__24839,G__24840,G__24841,G__24842,G__24843,G__24844,G__24845,G__24846,G__24847,G__24848,G__24849,G__24850,G__24851,G__24852,G__24853,G__24854,G__24855){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24856,G__24839);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24857,G__24840);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24858,G__24841);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24859,G__24842);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24860,G__24843);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24861,G__24844);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24862,G__24845);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24863,G__24846);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24864,G__24847);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__24865,G__24848);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__24866,G__24849);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__24867,G__24850);
var bindings__$14 = cljs.core._assoc(bindings__$13,G__24868,G__24851);
var bindings__$15 = cljs.core._assoc(bindings__$14,G__24869,G__24852);
var bindings__$16 = cljs.core._assoc(bindings__$15,G__24870,G__24853);
var bindings__$17 = cljs.core._assoc(bindings__$16,G__24871,G__24854);
var bindings__$18 = cljs.core._assoc(bindings__$17,G__24872,G__24855);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$18,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25827 = cljs.core._nth(recur_val,(0));
var G__25828 = cljs.core._nth(recur_val,(1));
var G__25829 = cljs.core._nth(recur_val,(2));
var G__25830 = cljs.core._nth(recur_val,(3));
var G__25831 = cljs.core._nth(recur_val,(4));
var G__25832 = cljs.core._nth(recur_val,(5));
var G__25833 = cljs.core._nth(recur_val,(6));
var G__25834 = cljs.core._nth(recur_val,(7));
var G__25835 = cljs.core._nth(recur_val,(8));
var G__25836 = cljs.core._nth(recur_val,(9));
var G__25837 = cljs.core._nth(recur_val,(10));
var G__25838 = cljs.core._nth(recur_val,(11));
var G__25839 = cljs.core._nth(recur_val,(12));
var G__25840 = cljs.core._nth(recur_val,(13));
var G__25841 = cljs.core._nth(recur_val,(14));
var G__25842 = cljs.core._nth(recur_val,(15));
var G__25843 = cljs.core._nth(recur_val,(16));
G__24839 = G__25827;
G__24840 = G__25828;
G__24841 = G__25829;
G__24842 = G__25830;
G__24843 = G__25831;
G__24844 = G__25832;
G__24845 = G__25833;
G__24846 = G__25834;
G__24847 = G__25835;
G__24848 = G__25836;
G__24849 = G__25837;
G__24850 = G__25838;
G__24851 = G__25839;
G__24852 = G__25840;
G__24853 = G__25841;
G__24854 = G__25842;
G__24855 = G__25843;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24893 = cljs.core._nth(params,(0));
var G__24894 = cljs.core._nth(params,(1));
var G__24895 = cljs.core._nth(params,(2));
var G__24896 = cljs.core._nth(params,(3));
var G__24897 = cljs.core._nth(params,(4));
var G__24898 = cljs.core._nth(params,(5));
var G__24899 = cljs.core._nth(params,(6));
var G__24900 = cljs.core._nth(params,(7));
var G__24901 = cljs.core._nth(params,(8));
var G__24902 = cljs.core._nth(params,(9));
var G__24903 = cljs.core._nth(params,(10));
var G__24904 = cljs.core._nth(params,(11));
var G__24905 = cljs.core._nth(params,(12));
var G__24906 = cljs.core._nth(params,(13));
var G__24907 = cljs.core._nth(params,(14));
var G__24908 = cljs.core._nth(params,(15));
var G__24909 = cljs.core._nth(params,(16));
return (function sci$impl$fns$fun_$_arity_17(G__24876,G__24877,G__24878,G__24879,G__24880,G__24881,G__24882,G__24883,G__24884,G__24885,G__24886,G__24887,G__24888,G__24889,G__24890,G__24891,G__24892){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((17),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24893,G__24876);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24894,G__24877);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24895,G__24878);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24896,G__24879);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24897,G__24880);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24898,G__24881);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24899,G__24882);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24900,G__24883);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24901,G__24884);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__24902,G__24885);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__24903,G__24886);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__24904,G__24887);
var bindings__$14 = cljs.core._assoc(bindings__$13,G__24905,G__24888);
var bindings__$15 = cljs.core._assoc(bindings__$14,G__24906,G__24889);
var bindings__$16 = cljs.core._assoc(bindings__$15,G__24907,G__24890);
var bindings__$17 = cljs.core._assoc(bindings__$16,G__24908,G__24891);
var bindings__$18 = cljs.core._assoc(bindings__$17,G__24909,G__24892);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$18,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25850 = cljs.core._nth(recur_val,(0));
var G__25851 = cljs.core._nth(recur_val,(1));
var G__25852 = cljs.core._nth(recur_val,(2));
var G__25853 = cljs.core._nth(recur_val,(3));
var G__25854 = cljs.core._nth(recur_val,(4));
var G__25855 = cljs.core._nth(recur_val,(5));
var G__25856 = cljs.core._nth(recur_val,(6));
var G__25857 = cljs.core._nth(recur_val,(7));
var G__25858 = cljs.core._nth(recur_val,(8));
var G__25859 = cljs.core._nth(recur_val,(9));
var G__25860 = cljs.core._nth(recur_val,(10));
var G__25861 = cljs.core._nth(recur_val,(11));
var G__25862 = cljs.core._nth(recur_val,(12));
var G__25863 = cljs.core._nth(recur_val,(13));
var G__25864 = cljs.core._nth(recur_val,(14));
var G__25865 = cljs.core._nth(recur_val,(15));
var G__25866 = cljs.core._nth(recur_val,(16));
G__24876 = G__25850;
G__24877 = G__25851;
G__24878 = G__25852;
G__24879 = G__25853;
G__24880 = G__25854;
G__24881 = G__25855;
G__24882 = G__25856;
G__24883 = G__25857;
G__24884 = G__25858;
G__24885 = G__25859;
G__24886 = G__25860;
G__24887 = G__25861;
G__24888 = G__25862;
G__24889 = G__25863;
G__24890 = G__25864;
G__24891 = G__25865;
G__24892 = G__25866;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (18):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__24947 = cljs.core._nth(params,(0));
var G__24948 = cljs.core._nth(params,(1));
var G__24949 = cljs.core._nth(params,(2));
var G__24950 = cljs.core._nth(params,(3));
var G__24951 = cljs.core._nth(params,(4));
var G__24952 = cljs.core._nth(params,(5));
var G__24953 = cljs.core._nth(params,(6));
var G__24954 = cljs.core._nth(params,(7));
var G__24955 = cljs.core._nth(params,(8));
var G__24956 = cljs.core._nth(params,(9));
var G__24957 = cljs.core._nth(params,(10));
var G__24958 = cljs.core._nth(params,(11));
var G__24959 = cljs.core._nth(params,(12));
var G__24960 = cljs.core._nth(params,(13));
var G__24961 = cljs.core._nth(params,(14));
var G__24962 = cljs.core._nth(params,(15));
var G__24963 = cljs.core._nth(params,(16));
var G__24964 = cljs.core._nth(params,(17));
return (function sci$impl$fns$fun_$_arity_18(G__24929,G__24930,G__24931,G__24932,G__24933,G__24934,G__24935,G__24936,G__24937,G__24938,G__24939,G__24940,G__24941,G__24942,G__24943,G__24944,G__24945,G__24946){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__24947,G__24929);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24948,G__24930);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24949,G__24931);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24950,G__24932);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24951,G__24933);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24952,G__24934);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24953,G__24935);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24954,G__24936);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24955,G__24937);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__24956,G__24938);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__24957,G__24939);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__24958,G__24940);
var bindings__$14 = cljs.core._assoc(bindings__$13,G__24959,G__24941);
var bindings__$15 = cljs.core._assoc(bindings__$14,G__24960,G__24942);
var bindings__$16 = cljs.core._assoc(bindings__$15,G__24961,G__24943);
var bindings__$17 = cljs.core._assoc(bindings__$16,G__24962,G__24944);
var bindings__$18 = cljs.core._assoc(bindings__$17,G__24963,G__24945);
var bindings__$19 = cljs.core._assoc(bindings__$18,G__24964,G__24946);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$19,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25877 = cljs.core._nth(recur_val,(0));
var G__25878 = cljs.core._nth(recur_val,(1));
var G__25879 = cljs.core._nth(recur_val,(2));
var G__25880 = cljs.core._nth(recur_val,(3));
var G__25881 = cljs.core._nth(recur_val,(4));
var G__25882 = cljs.core._nth(recur_val,(5));
var G__25883 = cljs.core._nth(recur_val,(6));
var G__25884 = cljs.core._nth(recur_val,(7));
var G__25885 = cljs.core._nth(recur_val,(8));
var G__25886 = cljs.core._nth(recur_val,(9));
var G__25887 = cljs.core._nth(recur_val,(10));
var G__25888 = cljs.core._nth(recur_val,(11));
var G__25889 = cljs.core._nth(recur_val,(12));
var G__25890 = cljs.core._nth(recur_val,(13));
var G__25891 = cljs.core._nth(recur_val,(14));
var G__25892 = cljs.core._nth(recur_val,(15));
var G__25893 = cljs.core._nth(recur_val,(16));
var G__25894 = cljs.core._nth(recur_val,(17));
G__24929 = G__25877;
G__24930 = G__25878;
G__24931 = G__25879;
G__24932 = G__25880;
G__24933 = G__25881;
G__24934 = G__25882;
G__24935 = G__25883;
G__24936 = G__25884;
G__24937 = G__25885;
G__24938 = G__25886;
G__24939 = G__25887;
G__24940 = G__25888;
G__24941 = G__25889;
G__24942 = G__25890;
G__24943 = G__25891;
G__24944 = G__25892;
G__24945 = G__25893;
G__24946 = G__25894;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__24991 = cljs.core._nth(params,(0));
var G__24992 = cljs.core._nth(params,(1));
var G__24993 = cljs.core._nth(params,(2));
var G__24994 = cljs.core._nth(params,(3));
var G__24995 = cljs.core._nth(params,(4));
var G__24996 = cljs.core._nth(params,(5));
var G__24997 = cljs.core._nth(params,(6));
var G__24998 = cljs.core._nth(params,(7));
var G__24999 = cljs.core._nth(params,(8));
var G__25000 = cljs.core._nth(params,(9));
var G__25001 = cljs.core._nth(params,(10));
var G__25002 = cljs.core._nth(params,(11));
var G__25003 = cljs.core._nth(params,(12));
var G__25004 = cljs.core._nth(params,(13));
var G__25005 = cljs.core._nth(params,(14));
var G__25006 = cljs.core._nth(params,(15));
var G__25007 = cljs.core._nth(params,(16));
var G__25008 = cljs.core._nth(params,(17));
return (function sci$impl$fns$fun_$_arity_18(G__24973,G__24974,G__24975,G__24976,G__24977,G__24978,G__24979,G__24980,G__24981,G__24982,G__24983,G__24984,G__24985,G__24986,G__24987,G__24988,G__24989,G__24990){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((18),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__24991,G__24973);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__24992,G__24974);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__24993,G__24975);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__24994,G__24976);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__24995,G__24977);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__24996,G__24978);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__24997,G__24979);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__24998,G__24980);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__24999,G__24981);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__25000,G__24982);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__25001,G__24983);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__25002,G__24984);
var bindings__$14 = cljs.core._assoc(bindings__$13,G__25003,G__24985);
var bindings__$15 = cljs.core._assoc(bindings__$14,G__25004,G__24986);
var bindings__$16 = cljs.core._assoc(bindings__$15,G__25005,G__24987);
var bindings__$17 = cljs.core._assoc(bindings__$16,G__25006,G__24988);
var bindings__$18 = cljs.core._assoc(bindings__$17,G__25007,G__24989);
var bindings__$19 = cljs.core._assoc(bindings__$18,G__25008,G__24990);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$19,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25903 = cljs.core._nth(recur_val,(0));
var G__25904 = cljs.core._nth(recur_val,(1));
var G__25905 = cljs.core._nth(recur_val,(2));
var G__25906 = cljs.core._nth(recur_val,(3));
var G__25907 = cljs.core._nth(recur_val,(4));
var G__25908 = cljs.core._nth(recur_val,(5));
var G__25909 = cljs.core._nth(recur_val,(6));
var G__25910 = cljs.core._nth(recur_val,(7));
var G__25911 = cljs.core._nth(recur_val,(8));
var G__25912 = cljs.core._nth(recur_val,(9));
var G__25913 = cljs.core._nth(recur_val,(10));
var G__25914 = cljs.core._nth(recur_val,(11));
var G__25915 = cljs.core._nth(recur_val,(12));
var G__25916 = cljs.core._nth(recur_val,(13));
var G__25917 = cljs.core._nth(recur_val,(14));
var G__25918 = cljs.core._nth(recur_val,(15));
var G__25919 = cljs.core._nth(recur_val,(16));
var G__25920 = cljs.core._nth(recur_val,(17));
G__24973 = G__25903;
G__24974 = G__25904;
G__24975 = G__25905;
G__24976 = G__25906;
G__24977 = G__25907;
G__24978 = G__25908;
G__24979 = G__25909;
G__24980 = G__25910;
G__24981 = G__25911;
G__24982 = G__25912;
G__24983 = G__25913;
G__24984 = G__25914;
G__24985 = G__25915;
G__24986 = G__25916;
G__24987 = G__25917;
G__24988 = G__25918;
G__24989 = G__25919;
G__24990 = G__25920;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (19):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__25033 = cljs.core._nth(params,(0));
var G__25034 = cljs.core._nth(params,(1));
var G__25035 = cljs.core._nth(params,(2));
var G__25036 = cljs.core._nth(params,(3));
var G__25037 = cljs.core._nth(params,(4));
var G__25038 = cljs.core._nth(params,(5));
var G__25039 = cljs.core._nth(params,(6));
var G__25040 = cljs.core._nth(params,(7));
var G__25041 = cljs.core._nth(params,(8));
var G__25042 = cljs.core._nth(params,(9));
var G__25043 = cljs.core._nth(params,(10));
var G__25044 = cljs.core._nth(params,(11));
var G__25045 = cljs.core._nth(params,(12));
var G__25046 = cljs.core._nth(params,(13));
var G__25047 = cljs.core._nth(params,(14));
var G__25048 = cljs.core._nth(params,(15));
var G__25049 = cljs.core._nth(params,(16));
var G__25050 = cljs.core._nth(params,(17));
var G__25051 = cljs.core._nth(params,(18));
return (function sci$impl$fns$fun_$_arity_19(G__25014,G__25015,G__25016,G__25017,G__25018,G__25019,G__25020,G__25021,G__25022,G__25023,G__25024,G__25025,G__25026,G__25027,G__25028,G__25029,G__25030,G__25031,G__25032){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__25033,G__25014);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__25034,G__25015);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__25035,G__25016);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__25036,G__25017);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__25037,G__25018);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__25038,G__25019);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__25039,G__25020);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__25040,G__25021);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__25041,G__25022);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__25042,G__25023);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__25043,G__25024);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__25044,G__25025);
var bindings__$14 = cljs.core._assoc(bindings__$13,G__25045,G__25026);
var bindings__$15 = cljs.core._assoc(bindings__$14,G__25046,G__25027);
var bindings__$16 = cljs.core._assoc(bindings__$15,G__25047,G__25028);
var bindings__$17 = cljs.core._assoc(bindings__$16,G__25048,G__25029);
var bindings__$18 = cljs.core._assoc(bindings__$17,G__25049,G__25030);
var bindings__$19 = cljs.core._assoc(bindings__$18,G__25050,G__25031);
var bindings__$20 = cljs.core._assoc(bindings__$19,G__25051,G__25032);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$20,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25932 = cljs.core._nth(recur_val,(0));
var G__25933 = cljs.core._nth(recur_val,(1));
var G__25934 = cljs.core._nth(recur_val,(2));
var G__25935 = cljs.core._nth(recur_val,(3));
var G__25936 = cljs.core._nth(recur_val,(4));
var G__25937 = cljs.core._nth(recur_val,(5));
var G__25938 = cljs.core._nth(recur_val,(6));
var G__25939 = cljs.core._nth(recur_val,(7));
var G__25940 = cljs.core._nth(recur_val,(8));
var G__25941 = cljs.core._nth(recur_val,(9));
var G__25942 = cljs.core._nth(recur_val,(10));
var G__25943 = cljs.core._nth(recur_val,(11));
var G__25944 = cljs.core._nth(recur_val,(12));
var G__25945 = cljs.core._nth(recur_val,(13));
var G__25946 = cljs.core._nth(recur_val,(14));
var G__25947 = cljs.core._nth(recur_val,(15));
var G__25948 = cljs.core._nth(recur_val,(16));
var G__25949 = cljs.core._nth(recur_val,(17));
var G__25950 = cljs.core._nth(recur_val,(18));
G__25014 = G__25932;
G__25015 = G__25933;
G__25016 = G__25934;
G__25017 = G__25935;
G__25018 = G__25936;
G__25019 = G__25937;
G__25020 = G__25938;
G__25021 = G__25939;
G__25022 = G__25940;
G__25023 = G__25941;
G__25024 = G__25942;
G__25025 = G__25943;
G__25026 = G__25944;
G__25027 = G__25945;
G__25028 = G__25946;
G__25029 = G__25947;
G__25030 = G__25948;
G__25031 = G__25949;
G__25032 = G__25950;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__25089 = cljs.core._nth(params,(0));
var G__25090 = cljs.core._nth(params,(1));
var G__25091 = cljs.core._nth(params,(2));
var G__25092 = cljs.core._nth(params,(3));
var G__25093 = cljs.core._nth(params,(4));
var G__25094 = cljs.core._nth(params,(5));
var G__25095 = cljs.core._nth(params,(6));
var G__25096 = cljs.core._nth(params,(7));
var G__25097 = cljs.core._nth(params,(8));
var G__25098 = cljs.core._nth(params,(9));
var G__25099 = cljs.core._nth(params,(10));
var G__25100 = cljs.core._nth(params,(11));
var G__25101 = cljs.core._nth(params,(12));
var G__25102 = cljs.core._nth(params,(13));
var G__25103 = cljs.core._nth(params,(14));
var G__25104 = cljs.core._nth(params,(15));
var G__25105 = cljs.core._nth(params,(16));
var G__25106 = cljs.core._nth(params,(17));
var G__25107 = cljs.core._nth(params,(18));
return (function sci$impl$fns$fun_$_arity_19(G__25067,G__25068,G__25069,G__25070,G__25071,G__25072,G__25073,G__25075,G__25076,G__25077,G__25078,G__25079,G__25080,G__25081,G__25082,G__25083,G__25085,G__25086,G__25087){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((19),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__25089,G__25067);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__25090,G__25068);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__25091,G__25069);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__25092,G__25070);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__25093,G__25071);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__25094,G__25072);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__25095,G__25073);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__25096,G__25075);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__25097,G__25076);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__25098,G__25077);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__25099,G__25078);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__25100,G__25079);
var bindings__$14 = cljs.core._assoc(bindings__$13,G__25101,G__25080);
var bindings__$15 = cljs.core._assoc(bindings__$14,G__25102,G__25081);
var bindings__$16 = cljs.core._assoc(bindings__$15,G__25103,G__25082);
var bindings__$17 = cljs.core._assoc(bindings__$16,G__25104,G__25083);
var bindings__$18 = cljs.core._assoc(bindings__$17,G__25105,G__25085);
var bindings__$19 = cljs.core._assoc(bindings__$18,G__25106,G__25086);
var bindings__$20 = cljs.core._assoc(bindings__$19,G__25107,G__25087);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$20,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__25972 = cljs.core._nth(recur_val,(0));
var G__25973 = cljs.core._nth(recur_val,(1));
var G__25974 = cljs.core._nth(recur_val,(2));
var G__25975 = cljs.core._nth(recur_val,(3));
var G__25976 = cljs.core._nth(recur_val,(4));
var G__25977 = cljs.core._nth(recur_val,(5));
var G__25978 = cljs.core._nth(recur_val,(6));
var G__25979 = cljs.core._nth(recur_val,(7));
var G__25980 = cljs.core._nth(recur_val,(8));
var G__25981 = cljs.core._nth(recur_val,(9));
var G__25982 = cljs.core._nth(recur_val,(10));
var G__25983 = cljs.core._nth(recur_val,(11));
var G__25984 = cljs.core._nth(recur_val,(12));
var G__25985 = cljs.core._nth(recur_val,(13));
var G__25986 = cljs.core._nth(recur_val,(14));
var G__25987 = cljs.core._nth(recur_val,(15));
var G__25988 = cljs.core._nth(recur_val,(16));
var G__25989 = cljs.core._nth(recur_val,(17));
var G__25990 = cljs.core._nth(recur_val,(18));
G__25067 = G__25972;
G__25068 = G__25973;
G__25069 = G__25974;
G__25070 = G__25975;
G__25071 = G__25976;
G__25072 = G__25977;
G__25073 = G__25978;
G__25075 = G__25979;
G__25076 = G__25980;
G__25077 = G__25981;
G__25078 = G__25982;
G__25079 = G__25983;
G__25080 = G__25984;
G__25081 = G__25985;
G__25082 = G__25986;
G__25083 = G__25987;
G__25085 = G__25988;
G__25086 = G__25989;
G__25087 = G__25990;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
case (20):
if(cljs.core.truth_(disable_arity_checks_QMARK_)){
var G__25193 = cljs.core._nth(params,(0));
var G__25194 = cljs.core._nth(params,(1));
var G__25195 = cljs.core._nth(params,(2));
var G__25196 = cljs.core._nth(params,(3));
var G__25197 = cljs.core._nth(params,(4));
var G__25198 = cljs.core._nth(params,(5));
var G__25199 = cljs.core._nth(params,(6));
var G__25200 = cljs.core._nth(params,(7));
var G__25201 = cljs.core._nth(params,(8));
var G__25202 = cljs.core._nth(params,(9));
var G__25203 = cljs.core._nth(params,(10));
var G__25204 = cljs.core._nth(params,(11));
var G__25205 = cljs.core._nth(params,(12));
var G__25206 = cljs.core._nth(params,(13));
var G__25207 = cljs.core._nth(params,(14));
var G__25208 = cljs.core._nth(params,(15));
var G__25209 = cljs.core._nth(params,(16));
var G__25210 = cljs.core._nth(params,(17));
var G__25211 = cljs.core._nth(params,(18));
var G__25212 = cljs.core._nth(params,(19));
return (function sci$impl$fns$fun_$_arity_20(G__25173,G__25174,G__25175,G__25176,G__25177,G__25178,G__25179,G__25180,G__25181,G__25182,G__25183,G__25184,G__25185,G__25186,G__25187,G__25188,G__25189,G__25190,G__25191,G__25192){
while(true){
var bindings__$2 = cljs.core._assoc(bindings__$1,G__25193,G__25173);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__25194,G__25174);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__25195,G__25175);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__25196,G__25176);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__25197,G__25177);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__25198,G__25178);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__25199,G__25179);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__25200,G__25180);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__25201,G__25181);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__25202,G__25182);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__25203,G__25183);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__25204,G__25184);
var bindings__$14 = cljs.core._assoc(bindings__$13,G__25205,G__25185);
var bindings__$15 = cljs.core._assoc(bindings__$14,G__25206,G__25186);
var bindings__$16 = cljs.core._assoc(bindings__$15,G__25207,G__25187);
var bindings__$17 = cljs.core._assoc(bindings__$16,G__25208,G__25188);
var bindings__$18 = cljs.core._assoc(bindings__$17,G__25209,G__25189);
var bindings__$19 = cljs.core._assoc(bindings__$18,G__25210,G__25190);
var bindings__$20 = cljs.core._assoc(bindings__$19,G__25211,G__25191);
var bindings__$21 = cljs.core._assoc(bindings__$20,G__25212,G__25192);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$21,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__26007 = cljs.core._nth(recur_val,(0));
var G__26008 = cljs.core._nth(recur_val,(1));
var G__26009 = cljs.core._nth(recur_val,(2));
var G__26010 = cljs.core._nth(recur_val,(3));
var G__26011 = cljs.core._nth(recur_val,(4));
var G__26012 = cljs.core._nth(recur_val,(5));
var G__26013 = cljs.core._nth(recur_val,(6));
var G__26014 = cljs.core._nth(recur_val,(7));
var G__26015 = cljs.core._nth(recur_val,(8));
var G__26016 = cljs.core._nth(recur_val,(9));
var G__26017 = cljs.core._nth(recur_val,(10));
var G__26018 = cljs.core._nth(recur_val,(11));
var G__26019 = cljs.core._nth(recur_val,(12));
var G__26020 = cljs.core._nth(recur_val,(13));
var G__26021 = cljs.core._nth(recur_val,(14));
var G__26022 = cljs.core._nth(recur_val,(15));
var G__26023 = cljs.core._nth(recur_val,(16));
var G__26024 = cljs.core._nth(recur_val,(17));
var G__26025 = cljs.core._nth(recur_val,(18));
var G__26026 = cljs.core._nth(recur_val,(19));
G__25173 = G__26007;
G__25174 = G__26008;
G__25175 = G__26009;
G__25176 = G__26010;
G__25177 = G__26011;
G__25178 = G__26012;
G__25179 = G__26013;
G__25180 = G__26014;
G__25181 = G__26015;
G__25182 = G__26016;
G__25183 = G__26017;
G__25184 = G__26018;
G__25185 = G__26019;
G__25186 = G__26020;
G__25187 = G__26021;
G__25188 = G__26022;
G__25189 = G__26023;
G__25190 = G__26024;
G__25191 = G__26025;
G__25192 = G__26026;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
} else {
var G__25269 = cljs.core._nth(params,(0));
var G__25270 = cljs.core._nth(params,(1));
var G__25271 = cljs.core._nth(params,(2));
var G__25272 = cljs.core._nth(params,(3));
var G__25273 = cljs.core._nth(params,(4));
var G__25274 = cljs.core._nth(params,(5));
var G__25275 = cljs.core._nth(params,(6));
var G__25276 = cljs.core._nth(params,(7));
var G__25277 = cljs.core._nth(params,(8));
var G__25278 = cljs.core._nth(params,(9));
var G__25279 = cljs.core._nth(params,(10));
var G__25280 = cljs.core._nth(params,(11));
var G__25281 = cljs.core._nth(params,(12));
var G__25282 = cljs.core._nth(params,(13));
var G__25283 = cljs.core._nth(params,(14));
var G__25284 = cljs.core._nth(params,(15));
var G__25285 = cljs.core._nth(params,(16));
var G__25286 = cljs.core._nth(params,(17));
var G__25287 = cljs.core._nth(params,(18));
var G__25288 = cljs.core._nth(params,(19));
return (function sci$impl$fns$fun_$_arity_20(G__25249,G__25250,G__25251,G__25252,G__25253,G__25254,G__25255,G__25256,G__25257,G__25258,G__25259,G__25260,G__25261,G__25262,G__25263,G__25264,G__25265,G__25266,G__25267,G__25268){
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((20),arguments.length)){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,cljs.core.vals(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(arguments)));
}

var bindings__$2 = cljs.core._assoc(bindings__$1,G__25269,G__25249);
var bindings__$3 = cljs.core._assoc(bindings__$2,G__25270,G__25250);
var bindings__$4 = cljs.core._assoc(bindings__$3,G__25271,G__25251);
var bindings__$5 = cljs.core._assoc(bindings__$4,G__25272,G__25252);
var bindings__$6 = cljs.core._assoc(bindings__$5,G__25273,G__25253);
var bindings__$7 = cljs.core._assoc(bindings__$6,G__25274,G__25254);
var bindings__$8 = cljs.core._assoc(bindings__$7,G__25275,G__25255);
var bindings__$9 = cljs.core._assoc(bindings__$8,G__25276,G__25256);
var bindings__$10 = cljs.core._assoc(bindings__$9,G__25277,G__25257);
var bindings__$11 = cljs.core._assoc(bindings__$10,G__25278,G__25258);
var bindings__$12 = cljs.core._assoc(bindings__$11,G__25279,G__25259);
var bindings__$13 = cljs.core._assoc(bindings__$12,G__25280,G__25260);
var bindings__$14 = cljs.core._assoc(bindings__$13,G__25281,G__25261);
var bindings__$15 = cljs.core._assoc(bindings__$14,G__25282,G__25262);
var bindings__$16 = cljs.core._assoc(bindings__$15,G__25283,G__25263);
var bindings__$17 = cljs.core._assoc(bindings__$16,G__25284,G__25264);
var bindings__$18 = cljs.core._assoc(bindings__$17,G__25285,G__25265);
var bindings__$19 = cljs.core._assoc(bindings__$18,G__25286,G__25266);
var bindings__$20 = cljs.core._assoc(bindings__$19,G__25287,G__25267);
var bindings__$21 = cljs.core._assoc(bindings__$20,G__25288,G__25268);
var ret__23498__auto__ = sci.impl.evaluator.eval(ctx,bindings__$21,body);
var recur_QMARK___23499__auto__ = (ret__23498__auto__ instanceof sci.impl.fns.Recur);
if(recur_QMARK___23499__auto__){
var recur_val = sci.impl.types.getVal(ret__23498__auto__);
var G__26039 = cljs.core._nth(recur_val,(0));
var G__26040 = cljs.core._nth(recur_val,(1));
var G__26041 = cljs.core._nth(recur_val,(2));
var G__26042 = cljs.core._nth(recur_val,(3));
var G__26043 = cljs.core._nth(recur_val,(4));
var G__26044 = cljs.core._nth(recur_val,(5));
var G__26045 = cljs.core._nth(recur_val,(6));
var G__26046 = cljs.core._nth(recur_val,(7));
var G__26047 = cljs.core._nth(recur_val,(8));
var G__26048 = cljs.core._nth(recur_val,(9));
var G__26049 = cljs.core._nth(recur_val,(10));
var G__26050 = cljs.core._nth(recur_val,(11));
var G__26051 = cljs.core._nth(recur_val,(12));
var G__26052 = cljs.core._nth(recur_val,(13));
var G__26053 = cljs.core._nth(recur_val,(14));
var G__26054 = cljs.core._nth(recur_val,(15));
var G__26055 = cljs.core._nth(recur_val,(16));
var G__26056 = cljs.core._nth(recur_val,(17));
var G__26057 = cljs.core._nth(recur_val,(18));
var G__26058 = cljs.core._nth(recur_val,(19));
G__25249 = G__26039;
G__25250 = G__26040;
G__25251 = G__26041;
G__25252 = G__26042;
G__25253 = G__26043;
G__25254 = G__26044;
G__25255 = G__26045;
G__25256 = G__26046;
G__25257 = G__26047;
G__25258 = G__26048;
G__25259 = G__26049;
G__25260 = G__26050;
G__25261 = G__26051;
G__25262 = G__26052;
G__25263 = G__26053;
G__25264 = G__26054;
G__25265 = G__26055;
G__25266 = G__26056;
G__25267 = G__26057;
G__25268 = G__26058;
continue;
} else {
return ret__23498__auto__;
}
break;
}
});
}

break;
default:
return (function() { 
var sci$impl$fns$fun_$_varargs__delegate = function (args){
while(true){
var bindings__$2 = (function (){var args_STAR_ = cljs.core.seq(args);
var params__$1 = cljs.core.seq(params);
var ret = bindings__$1;
while(true){
if(params__$1){
var fp = cljs.core.first(params__$1);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"&","&",-2144855648,null),fp)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(ret,cljs.core.second(params__$1),args_STAR_);
} else {
if(args_STAR_){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,args);
}

var G__26062 = cljs.core.next(args_STAR_);
var G__26063 = cljs.core.next(params__$1);
var G__26064 = cljs.core._assoc(ret,fp,cljs.core.first(args_STAR_));
args_STAR_ = G__26062;
params__$1 = G__26063;
ret = G__26064;
continue;
}
} else {
if(args_STAR_){
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,args);
} else {
}

return ret;
}
break;
}
})();
var ret = sci.impl.evaluator.eval(ctx,bindings__$2,body);
var recur_QMARK_ = (ret instanceof sci.impl.fns.Recur);
if(recur_QMARK_){
var recur_val = sci.impl.types.getVal(ret);
var min_var_args_arity = (cljs.core.truth_(var_arg_name)?fixed_arity:null);
if(cljs.core.truth_(min_var_args_arity)){
var vec__25295 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.subvec.cljs$core$IFn$_invoke$arity$3(recur_val,(0),min_var_args_arity),cljs.core.subvec.cljs$core$IFn$_invoke$arity$2(recur_val,min_var_args_arity)], null);
var fixed_args = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25295,(0),null);
var vec__25298 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25295,(1),null);
var rest_args = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25298,(0),null);
var G__26068 = cljs.core.into.cljs$core$IFn$_invoke$arity$2(fixed_args,rest_args);
args = G__26068;
continue;
} else {
var G__26069 = recur_val;
args = G__26069;
continue;
}
} else {
return ret;
}
break;
}
};
var sci$impl$fns$fun_$_varargs = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__26070__i = 0, G__26070__a = new Array(arguments.length -  0);
while (G__26070__i < G__26070__a.length) {G__26070__a[G__26070__i] = arguments[G__26070__i + 0]; ++G__26070__i;}
  args = new cljs.core.IndexedSeq(G__26070__a,0,null);
} 
return sci$impl$fns$fun_$_varargs__delegate.call(this,args);};
sci$impl$fns$fun_$_varargs.cljs$lang$maxFixedArity = 0;
sci$impl$fns$fun_$_varargs.cljs$lang$applyTo = (function (arglist__26071){
var args = cljs.core.seq(arglist__26071);
return sci$impl$fns$fun_$_varargs__delegate(args);
});
sci$impl$fns$fun_$_varargs.cljs$core$IFn$_invoke$arity$variadic = sci$impl$fns$fun_$_varargs__delegate;
return sci$impl$fns$fun_$_varargs;
})()
;

}
})():(function() { 
var sci$impl$fns$fun_$_varargs__delegate = function (args){
while(true){
var bindings__$2 = (function (){var args_STAR_ = cljs.core.seq(args);
var params__$1 = cljs.core.seq(params);
var ret = bindings__$1;
while(true){
if(params__$1){
var fp = cljs.core.first(params__$1);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"&","&",-2144855648,null),fp)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(ret,cljs.core.second(params__$1),args_STAR_);
} else {
if(args_STAR_){
} else {
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,args);
}

var G__26072 = cljs.core.next(args_STAR_);
var G__26073 = cljs.core.next(params__$1);
var G__26074 = cljs.core._assoc(ret,fp,cljs.core.first(args_STAR_));
args_STAR_ = G__26072;
params__$1 = G__26073;
ret = G__26074;
continue;
}
} else {
if(args_STAR_){
sci.impl.fns.throw_arity(ctx,nsm,fn_name,macro_QMARK_,args);
} else {
}

return ret;
}
break;
}
})();
var ret = sci.impl.evaluator.eval(ctx,bindings__$2,body);
var recur_QMARK_ = (ret instanceof sci.impl.fns.Recur);
if(recur_QMARK_){
var recur_val = sci.impl.types.getVal(ret);
var min_var_args_arity = (cljs.core.truth_(var_arg_name)?fixed_arity:null);
if(cljs.core.truth_(min_var_args_arity)){
var vec__25301 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.subvec.cljs$core$IFn$_invoke$arity$3(recur_val,(0),min_var_args_arity),cljs.core.subvec.cljs$core$IFn$_invoke$arity$2(recur_val,min_var_args_arity)], null);
var fixed_args = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25301,(0),null);
var vec__25304 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25301,(1),null);
var rest_args = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25304,(0),null);
var G__26080 = cljs.core.into.cljs$core$IFn$_invoke$arity$2(fixed_args,rest_args);
args = G__26080;
continue;
} else {
var G__26082 = recur_val;
args = G__26082;
continue;
}
} else {
return ret;
}
break;
}
};
var sci$impl$fns$fun_$_varargs = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__26083__i = 0, G__26083__a = new Array(arguments.length -  0);
while (G__26083__i < G__26083__a.length) {G__26083__a[G__26083__i] = arguments[G__26083__i + 0]; ++G__26083__i;}
  args = new cljs.core.IndexedSeq(G__26083__a,0,null);
} 
return sci$impl$fns$fun_$_varargs__delegate.call(this,args);};
sci$impl$fns$fun_$_varargs.cljs$lang$maxFixedArity = 0;
sci$impl$fns$fun_$_varargs.cljs$lang$applyTo = (function (arglist__26084){
var args = cljs.core.seq(arglist__26084);
return sci$impl$fns$fun_$_varargs__delegate(args);
});
sci$impl$fns$fun_$_varargs.cljs$core$IFn$_invoke$arity$variadic = sci$impl$fns$fun_$_varargs__delegate;
return sci$impl$fns$fun_$_varargs;
})()
);
return f;
});
sci.impl.fns.lookup_by_arity = (function sci$impl$fns$lookup_by_arity(arities,arity){
var or__5002__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(arities,arity);
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return new cljs.core.Keyword(null,"variadic","variadic",882626057).cljs$core$IFn$_invoke$arity$1(arities);
}
});
sci.impl.fns.fn_arity_map = (function sci$impl$fns$fn_arity_map(ctx,bindings,fn_name,macro_QMARK_,fn_bodies){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (arity_map,fn_body){
var f = sci.impl.fns.fun(ctx,bindings,fn_body,fn_name,macro_QMARK_);
var var_arg_QMARK_ = new cljs.core.Keyword(null,"var-arg-name","var-arg-name",-1100024887).cljs$core$IFn$_invoke$arity$1(fn_body);
var fixed_arity = new cljs.core.Keyword(null,"fixed-arity","fixed-arity",1586445869).cljs$core$IFn$_invoke$arity$1(fn_body);
if(cljs.core.truth_(var_arg_QMARK_)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(arity_map,new cljs.core.Keyword(null,"variadic","variadic",882626057),f);
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(arity_map,fixed_arity,f);
}
}),cljs.core.PersistentArrayMap.EMPTY,fn_bodies);
});
sci.impl.fns.eval_fn = (function sci$impl$fns$eval_fn(ctx,bindings,fn_name,fn_bodies,macro_QMARK_,single_arity,self_ref){
var self_ref__$1 = (cljs.core.truth_(self_ref)?cljs.core.volatile_BANG_(null):null);
var bindings__$1 = (cljs.core.truth_(self_ref__$1)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(bindings,fn_name,self_ref__$1):bindings);
var f = (cljs.core.truth_(single_arity)?sci.impl.fns.fun(ctx,bindings__$1,single_arity,fn_name,macro_QMARK_):(function (){var arities = sci.impl.fns.fn_arity_map(ctx,bindings__$1,fn_name,macro_QMARK_,fn_bodies);
return (function() { 
var G__26091__delegate = function (args){
var arg_count = cljs.core.count(args);
var temp__5802__auto__ = sci.impl.fns.lookup_by_arity(arities,arg_count);
if(cljs.core.truth_(temp__5802__auto__)){
var f = temp__5802__auto__;
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,args);
} else {
throw (new Error((function (){var actual_count = (cljs.core.truth_(macro_QMARK_)?(arg_count - (2)):arg_count);
return ["Cannot call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_name)," with ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(actual_count)," arguments"].join('');
})()));
}
};
var G__26091 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__26097__i = 0, G__26097__a = new Array(arguments.length -  0);
while (G__26097__i < G__26097__a.length) {G__26097__a[G__26097__i] = arguments[G__26097__i + 0]; ++G__26097__i;}
  args = new cljs.core.IndexedSeq(G__26097__a,0,null);
} 
return G__26091__delegate.call(this,args);};
G__26091.cljs$lang$maxFixedArity = 0;
G__26091.cljs$lang$applyTo = (function (arglist__26098){
var args = cljs.core.seq(arglist__26098);
return G__26091__delegate(args);
});
G__26091.cljs$core$IFn$_invoke$arity$variadic = G__26091__delegate;
return G__26091;
})()
;
})());
var f__$1 = (cljs.core.truth_(macro_QMARK_)?cljs.core.vary_meta.cljs$core$IFn$_invoke$arity$2(f,(function (p1__25311_SHARP_){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__25311_SHARP_,new cljs.core.Keyword("sci","macro","sci/macro",-868536151),macro_QMARK_);
})):f);
if(cljs.core.truth_(self_ref__$1)){
cljs.core.vreset_BANG_(self_ref__$1,f__$1);
} else {
}

return f__$1;
});
cljs.core.vreset_BANG_(sci.impl.utils.eval_fn,sci.impl.fns.eval_fn);

//# sourceMappingURL=sci.impl.fns.js.map
