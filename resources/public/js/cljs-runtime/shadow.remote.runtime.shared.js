goog.provide('shadow.remote.runtime.shared');
shadow.remote.runtime.shared.init_state = (function shadow$remote$runtime$shared$init_state(client_info){
return new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"ops","ops",1237330063),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"client-info","client-info",1958982504),client_info,new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218),(0),new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),cljs.core.PersistentArrayMap.EMPTY], null);
});
shadow.remote.runtime.shared.now = (function shadow$remote$runtime$shared$now(){
return Date.now();
});
shadow.remote.runtime.shared.get_client_id = (function shadow$remote$runtime$shared$get_client_id(p__22082){
var map__22083 = p__22082;
var map__22083__$1 = cljs.core.__destructure_map(map__22083);
var runtime = map__22083__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22083__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var or__5002__auto__ = new cljs.core.Keyword(null,"client-id","client-id",-464622140).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref));
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("runtime has no assigned runtime-id",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime], null));
}
});
shadow.remote.runtime.shared.relay_msg = (function shadow$remote$runtime$shared$relay_msg(runtime,msg){
var self_id_22212 = shadow.remote.runtime.shared.get_client_id(runtime);
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"to","to",192099007).cljs$core$IFn$_invoke$arity$1(msg),self_id_22212)){
shadow.remote.runtime.api.relay_msg(runtime,msg);
} else {
Promise.resolve((1)).then((function (){
var G__22086 = runtime;
var G__22087 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"from","from",1815293044),self_id_22212);
return (shadow.remote.runtime.shared.process.cljs$core$IFn$_invoke$arity$2 ? shadow.remote.runtime.shared.process.cljs$core$IFn$_invoke$arity$2(G__22086,G__22087) : shadow.remote.runtime.shared.process.call(null, G__22086,G__22087));
}));
}

return msg;
});
shadow.remote.runtime.shared.reply = (function shadow$remote$runtime$shared$reply(runtime,p__22092,res){
var map__22093 = p__22092;
var map__22093__$1 = cljs.core.__destructure_map(map__22093);
var call_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22093__$1,new cljs.core.Keyword(null,"call-id","call-id",1043012968));
var from = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22093__$1,new cljs.core.Keyword(null,"from","from",1815293044));
var res__$1 = (function (){var G__22094 = res;
var G__22094__$1 = (cljs.core.truth_(call_id)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__22094,new cljs.core.Keyword(null,"call-id","call-id",1043012968),call_id):G__22094);
if(cljs.core.truth_(from)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__22094__$1,new cljs.core.Keyword(null,"to","to",192099007),from);
} else {
return G__22094__$1;
}
})();
return shadow.remote.runtime.api.relay_msg(runtime,res__$1);
});
shadow.remote.runtime.shared.call = (function shadow$remote$runtime$shared$call(var_args){
var G__22096 = arguments.length;
switch (G__22096) {
case 3:
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3 = (function (runtime,msg,handlers){
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4(runtime,msg,handlers,(0));
}));

(shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4 = (function (p__22097,msg,handlers,timeout_after_ms){
var map__22098 = p__22097;
var map__22098__$1 = cljs.core.__destructure_map(map__22098);
var runtime = map__22098__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22098__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
if(cljs.core.map_QMARK_(msg)){
} else {
throw (new Error("Assert failed: (map? msg)"));
}

if(cljs.core.map_QMARK_(handlers)){
} else {
throw (new Error("Assert failed: (map? handlers)"));
}

if(cljs.core.nat_int_QMARK_(timeout_after_ms)){
} else {
throw (new Error("Assert failed: (nat-int? timeout-after-ms)"));
}

var call_id = new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.update,new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218),cljs.core.inc);

cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),call_id], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"handlers","handlers",79528781),handlers,new cljs.core.Keyword(null,"called-at","called-at",607081160),shadow.remote.runtime.shared.now(),new cljs.core.Keyword(null,"msg","msg",-1386103444),msg,new cljs.core.Keyword(null,"timeout","timeout",-318625318),timeout_after_ms], null));

return shadow.remote.runtime.api.relay_msg(runtime,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"call-id","call-id",1043012968),call_id));
}));

(shadow.remote.runtime.shared.call.cljs$lang$maxFixedArity = 4);

shadow.remote.runtime.shared.trigger_BANG_ = (function shadow$remote$runtime$shared$trigger_BANG_(var_args){
var args__5732__auto__ = [];
var len__5726__auto___22215 = arguments.length;
var i__5727__auto___22216 = (0);
while(true){
if((i__5727__auto___22216 < len__5726__auto___22215)){
args__5732__auto__.push((arguments[i__5727__auto___22216]));

var G__22217 = (i__5727__auto___22216 + (1));
i__5727__auto___22216 = G__22217;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((2) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((2)),(0),null)):null);
return shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__5733__auto__);
});

(shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (p__22103,ev,args){
var map__22106 = p__22103;
var map__22106__$1 = cljs.core.__destructure_map(map__22106);
var runtime = map__22106__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22106__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var seq__22107 = cljs.core.seq(cljs.core.vals(new cljs.core.Keyword(null,"extensions","extensions",-1103629196).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref))));
var chunk__22110 = null;
var count__22111 = (0);
var i__22112 = (0);
while(true){
if((i__22112 < count__22111)){
var ext = chunk__22110.cljs$core$IIndexed$_nth$arity$2(null, i__22112);
var ev_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(ext,ev);
if(cljs.core.truth_(ev_fn)){
cljs.core.apply.cljs$core$IFn$_invoke$arity$2(ev_fn,args);


var G__22218 = seq__22107;
var G__22219 = chunk__22110;
var G__22220 = count__22111;
var G__22221 = (i__22112 + (1));
seq__22107 = G__22218;
chunk__22110 = G__22219;
count__22111 = G__22220;
i__22112 = G__22221;
continue;
} else {
var G__22222 = seq__22107;
var G__22223 = chunk__22110;
var G__22224 = count__22111;
var G__22225 = (i__22112 + (1));
seq__22107 = G__22222;
chunk__22110 = G__22223;
count__22111 = G__22224;
i__22112 = G__22225;
continue;
}
} else {
var temp__5804__auto__ = cljs.core.seq(seq__22107);
if(temp__5804__auto__){
var seq__22107__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__22107__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__22107__$1);
var G__22226 = cljs.core.chunk_rest(seq__22107__$1);
var G__22227 = c__5525__auto__;
var G__22228 = cljs.core.count(c__5525__auto__);
var G__22229 = (0);
seq__22107 = G__22226;
chunk__22110 = G__22227;
count__22111 = G__22228;
i__22112 = G__22229;
continue;
} else {
var ext = cljs.core.first(seq__22107__$1);
var ev_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(ext,ev);
if(cljs.core.truth_(ev_fn)){
cljs.core.apply.cljs$core$IFn$_invoke$arity$2(ev_fn,args);


var G__22230 = cljs.core.next(seq__22107__$1);
var G__22231 = null;
var G__22232 = (0);
var G__22233 = (0);
seq__22107 = G__22230;
chunk__22110 = G__22231;
count__22111 = G__22232;
i__22112 = G__22233;
continue;
} else {
var G__22234 = cljs.core.next(seq__22107__$1);
var G__22235 = null;
var G__22236 = (0);
var G__22237 = (0);
seq__22107 = G__22234;
chunk__22110 = G__22235;
count__22111 = G__22236;
i__22112 = G__22237;
continue;
}
}
} else {
return null;
}
}
break;
}
}));

(shadow.remote.runtime.shared.trigger_BANG_.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(shadow.remote.runtime.shared.trigger_BANG_.cljs$lang$applyTo = (function (seq22099){
var G__22100 = cljs.core.first(seq22099);
var seq22099__$1 = cljs.core.next(seq22099);
var G__22101 = cljs.core.first(seq22099__$1);
var seq22099__$2 = cljs.core.next(seq22099__$1);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__22100,G__22101,seq22099__$2);
}));

shadow.remote.runtime.shared.welcome = (function shadow$remote$runtime$shared$welcome(p__22137,p__22138){
var map__22139 = p__22137;
var map__22139__$1 = cljs.core.__destructure_map(map__22139);
var runtime = map__22139__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22139__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var map__22140 = p__22138;
var map__22140__$1 = cljs.core.__destructure_map(map__22140);
var msg = map__22140__$1;
var client_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22140__$1,new cljs.core.Keyword(null,"client-id","client-id",-464622140));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.assoc,new cljs.core.Keyword(null,"client-id","client-id",-464622140),client_id);

var map__22141 = cljs.core.deref(state_ref);
var map__22141__$1 = cljs.core.__destructure_map(map__22141);
var client_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22141__$1,new cljs.core.Keyword(null,"client-info","client-info",1958982504));
var extensions = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22141__$1,new cljs.core.Keyword(null,"extensions","extensions",-1103629196));
shadow.remote.runtime.shared.relay_msg(runtime,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"hello","hello",-245025397),new cljs.core.Keyword(null,"client-info","client-info",1958982504),client_info], null));

return shadow.remote.runtime.shared.trigger_BANG_(runtime,new cljs.core.Keyword(null,"on-welcome","on-welcome",1895317125));
});
shadow.remote.runtime.shared.ping = (function shadow$remote$runtime$shared$ping(runtime,msg){
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"pong","pong",-172484958)], null));
});
shadow.remote.runtime.shared.request_supported_ops = (function shadow$remote$runtime$shared$request_supported_ops(p__22142,msg){
var map__22143 = p__22142;
var map__22143__$1 = cljs.core.__destructure_map(map__22143);
var runtime = map__22143__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22143__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"supported-ops","supported-ops",337914702),new cljs.core.Keyword(null,"ops","ops",1237330063),cljs.core.disj.cljs$core$IFn$_invoke$arity$variadic(cljs.core.set(cljs.core.keys(new cljs.core.Keyword(null,"ops","ops",1237330063).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref)))),new cljs.core.Keyword(null,"welcome","welcome",-578152123),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"unknown-relay-op","unknown-relay-op",170832753),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),new cljs.core.Keyword(null,"request-supported-ops","request-supported-ops",-1034994502),new cljs.core.Keyword(null,"tool-disconnect","tool-disconnect",189103996)], 0))], null));
});
shadow.remote.runtime.shared.unknown_relay_op = (function shadow$remote$runtime$shared$unknown_relay_op(msg){
return console.warn("unknown-relay-op",msg);
});
shadow.remote.runtime.shared.unknown_op = (function shadow$remote$runtime$shared$unknown_op(msg){
return console.warn("unknown-op",msg);
});
shadow.remote.runtime.shared.add_extension_STAR_ = (function shadow$remote$runtime$shared$add_extension_STAR_(p__22147,key,p__22148){
var map__22150 = p__22147;
var map__22150__$1 = cljs.core.__destructure_map(map__22150);
var state = map__22150__$1;
var extensions = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22150__$1,new cljs.core.Keyword(null,"extensions","extensions",-1103629196));
var map__22151 = p__22148;
var map__22151__$1 = cljs.core.__destructure_map(map__22151);
var spec = map__22151__$1;
var ops = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22151__$1,new cljs.core.Keyword(null,"ops","ops",1237330063));
var transit_write_handlers = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22151__$1,new cljs.core.Keyword(null,"transit-write-handlers","transit-write-handlers",1886308716));
if(cljs.core.contains_QMARK_(extensions,key)){
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("extension already registered",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),key,new cljs.core.Keyword(null,"spec","spec",347520401),spec], null));
} else {
}

return cljs.core.reduce_kv((function (state__$1,op_kw,op_handler){
if(cljs.core.truth_(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state__$1,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op_kw], null)))){
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("op already registered",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),key,new cljs.core.Keyword(null,"op","op",-1882987955),op_kw], null));
} else {
}

return cljs.core.assoc_in(state__$1,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op_kw], null),op_handler);
}),cljs.core.assoc_in(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),key], null),spec),ops);
});
shadow.remote.runtime.shared.add_extension = (function shadow$remote$runtime$shared$add_extension(p__22157,key,spec){
var map__22158 = p__22157;
var map__22158__$1 = cljs.core.__destructure_map(map__22158);
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22158__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,shadow.remote.runtime.shared.add_extension_STAR_,key,spec);
});
shadow.remote.runtime.shared.add_defaults = (function shadow$remote$runtime$shared$add_defaults(runtime){
return shadow.remote.runtime.shared.add_extension(runtime,new cljs.core.Keyword("shadow.remote.runtime.shared","defaults","shadow.remote.runtime.shared/defaults",-1821257543),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"welcome","welcome",-578152123),(function (p1__22159_SHARP_){
return shadow.remote.runtime.shared.welcome(runtime,p1__22159_SHARP_);
}),new cljs.core.Keyword(null,"unknown-relay-op","unknown-relay-op",170832753),(function (p1__22160_SHARP_){
return shadow.remote.runtime.shared.unknown_relay_op(p1__22160_SHARP_);
}),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),(function (p1__22161_SHARP_){
return shadow.remote.runtime.shared.unknown_op(p1__22161_SHARP_);
}),new cljs.core.Keyword(null,"ping","ping",-1670114784),(function (p1__22162_SHARP_){
return shadow.remote.runtime.shared.ping(runtime,p1__22162_SHARP_);
}),new cljs.core.Keyword(null,"request-supported-ops","request-supported-ops",-1034994502),(function (p1__22163_SHARP_){
return shadow.remote.runtime.shared.request_supported_ops(runtime,p1__22163_SHARP_);
})], null)], null));
});
shadow.remote.runtime.shared.del_extension_STAR_ = (function shadow$remote$runtime$shared$del_extension_STAR_(state,key){
var ext = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),key], null));
if(cljs.core.not(ext)){
return state;
} else {
return cljs.core.reduce_kv((function (state__$1,op_kw,op_handler){
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$4(state__$1,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063)], null),cljs.core.dissoc,op_kw);
}),cljs.core.update.cljs$core$IFn$_invoke$arity$4(state,new cljs.core.Keyword(null,"extensions","extensions",-1103629196),cljs.core.dissoc,key),new cljs.core.Keyword(null,"ops","ops",1237330063).cljs$core$IFn$_invoke$arity$1(ext));
}
});
shadow.remote.runtime.shared.del_extension = (function shadow$remote$runtime$shared$del_extension(p__22178,key){
var map__22180 = p__22178;
var map__22180__$1 = cljs.core.__destructure_map(map__22180);
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22180__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(state_ref,shadow.remote.runtime.shared.del_extension_STAR_,key);
});
shadow.remote.runtime.shared.unhandled_call_result = (function shadow$remote$runtime$shared$unhandled_call_result(call_config,msg){
return console.warn("unhandled call result",msg,call_config);
});
shadow.remote.runtime.shared.unhandled_client_not_found = (function shadow$remote$runtime$shared$unhandled_client_not_found(p__22181,msg){
var map__22182 = p__22181;
var map__22182__$1 = cljs.core.__destructure_map(map__22182);
var runtime = map__22182__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22182__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic(runtime,new cljs.core.Keyword(null,"on-client-not-found","on-client-not-found",-642452849),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([msg], 0));
});
shadow.remote.runtime.shared.reply_unknown_op = (function shadow$remote$runtime$shared$reply_unknown_op(runtime,msg){
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),new cljs.core.Keyword(null,"msg","msg",-1386103444),msg], null));
});
shadow.remote.runtime.shared.process = (function shadow$remote$runtime$shared$process(p__22183,p__22184){
var map__22185 = p__22183;
var map__22185__$1 = cljs.core.__destructure_map(map__22185);
var runtime = map__22185__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22185__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var map__22186 = p__22184;
var map__22186__$1 = cljs.core.__destructure_map(map__22186);
var msg = map__22186__$1;
var op = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22186__$1,new cljs.core.Keyword(null,"op","op",-1882987955));
var call_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22186__$1,new cljs.core.Keyword(null,"call-id","call-id",1043012968));
var state = cljs.core.deref(state_ref);
var op_handler = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op], null));
if(cljs.core.truth_(call_id)){
var cfg = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),call_id], null));
var call_handler = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(cfg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"handlers","handlers",79528781),op], null));
if(cljs.core.truth_(call_handler)){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(state_ref,cljs.core.update,new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),cljs.core.dissoc,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([call_id], 0));

return (call_handler.cljs$core$IFn$_invoke$arity$1 ? call_handler.cljs$core$IFn$_invoke$arity$1(msg) : call_handler.call(null, msg));
} else {
if(cljs.core.truth_(op_handler)){
return (op_handler.cljs$core$IFn$_invoke$arity$1 ? op_handler.cljs$core$IFn$_invoke$arity$1(msg) : op_handler.call(null, msg));
} else {
return shadow.remote.runtime.shared.unhandled_call_result(cfg,msg);

}
}
} else {
if(cljs.core.truth_(op_handler)){
return (op_handler.cljs$core$IFn$_invoke$arity$1 ? op_handler.cljs$core$IFn$_invoke$arity$1(msg) : op_handler.call(null, msg));
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-not-found","client-not-found",-1754042614),op)){
return shadow.remote.runtime.shared.unhandled_client_not_found(runtime,msg);
} else {
return shadow.remote.runtime.shared.reply_unknown_op(runtime,msg);

}
}
}
});
shadow.remote.runtime.shared.run_on_idle = (function shadow$remote$runtime$shared$run_on_idle(state_ref){
var seq__22191 = cljs.core.seq(cljs.core.vals(new cljs.core.Keyword(null,"extensions","extensions",-1103629196).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref))));
var chunk__22193 = null;
var count__22194 = (0);
var i__22195 = (0);
while(true){
if((i__22195 < count__22194)){
var map__22199 = chunk__22193.cljs$core$IIndexed$_nth$arity$2(null, i__22195);
var map__22199__$1 = cljs.core.__destructure_map(map__22199);
var on_idle = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22199__$1,new cljs.core.Keyword(null,"on-idle","on-idle",2044706602));
if(cljs.core.truth_(on_idle)){
(on_idle.cljs$core$IFn$_invoke$arity$0 ? on_idle.cljs$core$IFn$_invoke$arity$0() : on_idle.call(null, ));


var G__22268 = seq__22191;
var G__22269 = chunk__22193;
var G__22270 = count__22194;
var G__22271 = (i__22195 + (1));
seq__22191 = G__22268;
chunk__22193 = G__22269;
count__22194 = G__22270;
i__22195 = G__22271;
continue;
} else {
var G__22272 = seq__22191;
var G__22273 = chunk__22193;
var G__22274 = count__22194;
var G__22275 = (i__22195 + (1));
seq__22191 = G__22272;
chunk__22193 = G__22273;
count__22194 = G__22274;
i__22195 = G__22275;
continue;
}
} else {
var temp__5804__auto__ = cljs.core.seq(seq__22191);
if(temp__5804__auto__){
var seq__22191__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__22191__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__22191__$1);
var G__22276 = cljs.core.chunk_rest(seq__22191__$1);
var G__22277 = c__5525__auto__;
var G__22278 = cljs.core.count(c__5525__auto__);
var G__22279 = (0);
seq__22191 = G__22276;
chunk__22193 = G__22277;
count__22194 = G__22278;
i__22195 = G__22279;
continue;
} else {
var map__22206 = cljs.core.first(seq__22191__$1);
var map__22206__$1 = cljs.core.__destructure_map(map__22206);
var on_idle = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__22206__$1,new cljs.core.Keyword(null,"on-idle","on-idle",2044706602));
if(cljs.core.truth_(on_idle)){
(on_idle.cljs$core$IFn$_invoke$arity$0 ? on_idle.cljs$core$IFn$_invoke$arity$0() : on_idle.call(null, ));


var G__22280 = cljs.core.next(seq__22191__$1);
var G__22281 = null;
var G__22282 = (0);
var G__22283 = (0);
seq__22191 = G__22280;
chunk__22193 = G__22281;
count__22194 = G__22282;
i__22195 = G__22283;
continue;
} else {
var G__22284 = cljs.core.next(seq__22191__$1);
var G__22285 = null;
var G__22286 = (0);
var G__22287 = (0);
seq__22191 = G__22284;
chunk__22193 = G__22285;
count__22194 = G__22286;
i__22195 = G__22287;
continue;
}
}
} else {
return null;
}
}
break;
}
});

//# sourceMappingURL=shadow.remote.runtime.shared.js.map
