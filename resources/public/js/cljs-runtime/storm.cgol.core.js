goog.provide('storm.cgol.core');
storm.cgol.core.ui = (function storm$cgol$core$ui(){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [storm.cgol.views.grid.gol], null);
});
storm.cgol.core.dev_setup = (function storm$cgol$core$dev_setup(){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["dev mode"], 0));

return devtools.core.install_BANG_.cljs$core$IFn$_invoke$arity$0();

});
storm.cgol.core.mount_root = (function storm$cgol$core$mount_root(){
return reagent.dom.render.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [storm.cgol.core.ui], null),document.getElementById("app"));
});
storm.cgol.core.init_BANG_ = (function storm$cgol$core$init_BANG_(){
re_frame.core.dispatch_sync(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"initialize","initialize",609952913)], null));

storm.cgol.core.dev_setup();

return storm.cgol.core.mount_root();
});
goog.exportSymbol('storm.cgol.core.init_BANG_', storm.cgol.core.init_BANG_);

//# sourceMappingURL=storm.cgol.core.js.map
