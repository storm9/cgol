goog.provide('sci.impl.protocols');
sci.impl.protocols.defprotocol = (function sci$impl$protocols$defprotocol(var_args){
var args__5732__auto__ = [];
var len__5726__auto___24970 = arguments.length;
var i__5727__auto___24971 = (0);
while(true){
if((i__5727__auto___24971 < len__5726__auto___24970)){
args__5732__auto__.push((arguments[i__5727__auto___24971]));

var G__24972 = (i__5727__auto___24971 + (1));
i__5727__auto___24971 = G__24972;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((4) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((4)),(0),null)):null);
return sci.impl.protocols.defprotocol.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),argseq__5733__auto__);
});

(sci.impl.protocols.defprotocol.cljs$core$IFn$_invoke$arity$variadic = (function (_,___$1,_ctx,protocol_name,signatures){
var vec__24065 = (function (){var sig = cljs.core.first(signatures);
if(typeof sig === 'string'){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [sig,cljs.core.rest(signatures)], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,signatures], null);
}
})();
var docstring = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24065,(0),null);
var signatures__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24065,(1),null);
var vec__24068 = (function (){var opt = cljs.core.first(signatures__$1);
if((opt instanceof cljs.core.Keyword)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.PersistentArrayMap.createAsIfByAssoc([opt,cljs.core.second(signatures__$1)]),cljs.core.nnext(signatures__$1)], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,signatures__$1], null);
}
})();
var opts = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24068,(0),null);
var signatures__$2 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24068,(1),null);
var current_ns = cljs.core.str.cljs$core$IFn$_invoke$arity$1(sci.impl.vars.current_ns_name());
var fq_name = cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(current_ns,cljs.core.str.cljs$core$IFn$_invoke$arity$1(protocol_name));
var extend_meta = new cljs.core.Keyword(null,"extend-via-metadata","extend-via-metadata",-427346794).cljs$core$IFn$_invoke$arity$1(opts);
var expansion = cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol(null,"do","do",1686842252,null),null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol(null,"def","def",597100991,null),null,(1),null)),(new cljs.core.List(null,cljs.core.with_meta(protocol_name,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"doc","doc",1913296891),docstring], null)),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","cond->","cljs.core/cond->",-113941356,null),null,(1),null)),(new cljs.core.List(null,cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.array_map,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Keyword(null,"methods","methods",453930866),null,(1),null)),(new cljs.core.List(null,cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_set,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$0()))),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,new cljs.core.Keyword(null,"name","name",1843675177),null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),null,(1),null)),(new cljs.core.List(null,fq_name,null,(1),null))))),null,(1),null)),(new cljs.core.List(null,new cljs.core.Keyword(null,"ns","ns",441598760),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol("cljs.core","*ns*","cljs.core/*ns*",1155497085,null),null,(1),null))], 0))))),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,extend_meta,null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","assoc","cljs.core/assoc",322326297,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Keyword(null,"extend-via-metadata","extend-via-metadata",-427346794),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,true,null,(1),null))], 0)))),null,(1),null))], 0)))),null,(1),null))], 0)))),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__24084){
var vec__24085 = p__24084;
var seq__24086 = cljs.core.seq(vec__24085);
var first__24087 = cljs.core.first(seq__24086);
var seq__24086__$1 = cljs.core.next(seq__24086);
var method_name = first__24087;
var ___$2 = seq__24086__$1;
var fq_name__$1 = cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(current_ns,cljs.core.str.cljs$core$IFn$_invoke$arity$1(method_name));
var impls = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","defmulti","cljs.core/defmulti",723984225,null),null,(1),null)),(new cljs.core.List(null,method_name,null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,new cljs.core.Symbol("cljs.core","protocol-type-impl","cljs.core/protocol-type-impl",155177701,null),null,(1),null))], 0)))),cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","defmethod","cljs.core/defmethod",-180785162,null),null,(1),null)),(new cljs.core.List(null,method_name,null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,new cljs.core.Keyword("sci.impl.protocols","reified","sci.impl.protocols/reified",-2019939396),null,(1),null)),(new cljs.core.List(null,cljs.core.vec(cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol(null,"x__24051__auto__","x__24051__auto__",-963739809,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"&","&",-2144855648,null),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,new cljs.core.Symbol(null,"args__24052__auto__","args__24052__auto__",2120724760,null),null,(1),null))], 0))))),null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","let","cljs.core/let",-308701135,null),null,(1),null)),(new cljs.core.List(null,cljs.core.vec(cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"methods__24053__auto__","methods__24053__auto__",-2041378451,null),null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","-reified-methods","cljs.core/-reified-methods",-1833109469,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"x__24051__auto__","x__24051__auto__",-963739809,null),null,(1),null))))),null,(1),null)))))),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","apply","cljs.core/apply",1757277831,null),null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","get","cljs.core/get",-296075407,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"methods__24053__auto__","methods__24053__auto__",-2041378451,null),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),null,(1),null)),(new cljs.core.List(null,method_name,null,(1),null))))),null,(1),null))], 0)))),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,new cljs.core.Symbol(null,"x__24051__auto__","x__24051__auto__",-963739809,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"args__24052__auto__","args__24052__auto__",2120724760,null),null,(1),null))], 0)))),null,(1),null))], 0)))),null,(1),null))], 0))))], null);
var impls__$1 = (cljs.core.truth_(extend_meta)?cljs.core.conj.cljs$core$IFn$_invoke$arity$2(impls,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","defmethod","cljs.core/defmethod",-180785162,null),null,(1),null)),(new cljs.core.List(null,method_name,null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,new cljs.core.Keyword(null,"default","default",-1987822328),null,(1),null)),(new cljs.core.List(null,cljs.core.vec(cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol(null,"x__24054__auto__","x__24054__auto__",1118974543,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"&","&",-2144855648,null),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,new cljs.core.Symbol(null,"args__24055__auto__","args__24055__auto__",-1651175425,null),null,(1),null))], 0))))),null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","let","cljs.core/let",-308701135,null),null,(1),null)),(new cljs.core.List(null,cljs.core.vec(cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol(null,"meta__24056__auto__","meta__24056__auto__",-926413763,null),null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","meta","cljs.core/meta",-748218346,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"x__24054__auto__","x__24054__auto__",1118974543,null),null,(1),null))))),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,new cljs.core.Symbol(null,"method__24057__auto__","method__24057__auto__",-795435275,null),null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","get","cljs.core/get",-296075407,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"meta__24056__auto__","meta__24056__auto__",-926413763,null),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),null,(1),null)),(new cljs.core.List(null,fq_name__$1,null,(1),null))))),null,(1),null))], 0)))),null,(1),null))], 0))))),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol(null,"if","if",1181717262,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"method__24057__auto__","method__24057__auto__",-795435275,null),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","apply","cljs.core/apply",1757277831,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"method__24057__auto__","method__24057__auto__",-795435275,null),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,new cljs.core.Symbol(null,"x__24054__auto__","x__24054__auto__",1118974543,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"args__24055__auto__","args__24055__auto__",-1651175425,null),null,(1),null))], 0)))),null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"throw","throw",595905694,null),null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol(null,"new","new",-444906321,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol("js","Error","js/Error",-1692659266,null),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","str","cljs.core/str",-1971828991,null),null,(1),null)),(new cljs.core.List(null,"No implementation of method: ",null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,cljs.core.keyword.cljs$core$IFn$_invoke$arity$1(method_name),null,(1),null)),(new cljs.core.List(null," of protocol: ",null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"var","var",870848730,null),null,(1),null)),(new cljs.core.List(null,protocol_name,null,(1),null))))),null,(1),null)),(new cljs.core.List(null," found for: ",null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","protocol-type-impl","cljs.core/protocol-type-impl",155177701,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"x__24054__auto__","x__24054__auto__",1118974543,null),null,(1),null))))),null,(1),null))], 0)))),null,(1),null))], 0)))),null,(1),null))))),null,(1),null))], 0)))),null,(1),null))], 0)))),null,(1),null))], 0))))):impls);
return cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol(null,"do","do",1686842252,null),null,(1),null)),impls__$1,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol(null,"def","def",597100991,null),null,(1),null)),(new cljs.core.List(null,protocol_name,null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","update","cljs.core/update",-908565906,null),null,(1),null)),(new cljs.core.List(null,protocol_name,null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,new cljs.core.Keyword(null,"methods","methods",453930866),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol("cljs.core","conj","cljs.core/conj",-460750931,null),null,(1),null)),(new cljs.core.List(null,method_name,null,(1),null))], 0)))),null,(1),null))], 0)))),null,(1),null))], 0))));
}),signatures__$2)], 0))));
return expansion;
}));

(sci.impl.protocols.defprotocol.cljs$lang$maxFixedArity = (4));

/** @this {Function} */
(sci.impl.protocols.defprotocol.cljs$lang$applyTo = (function (seq24060){
var G__24061 = cljs.core.first(seq24060);
var seq24060__$1 = cljs.core.next(seq24060);
var G__24062 = cljs.core.first(seq24060__$1);
var seq24060__$2 = cljs.core.next(seq24060__$1);
var G__24063 = cljs.core.first(seq24060__$2);
var seq24060__$3 = cljs.core.next(seq24060__$2);
var G__24064 = cljs.core.first(seq24060__$3);
var seq24060__$4 = cljs.core.next(seq24060__$3);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__24061,G__24062,G__24063,G__24064,seq24060__$4);
}));

sci.impl.protocols.extend = (function sci$impl$protocols$extend(var_args){
var args__5732__auto__ = [];
var len__5726__auto___25055 = arguments.length;
var i__5727__auto___25056 = (0);
while(true){
if((i__5727__auto___25056 < len__5726__auto___25055)){
args__5732__auto__.push((arguments[i__5727__auto___25056]));

var G__25057 = (i__5727__auto___25056 + (1));
i__5727__auto___25056 = G__25057;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((2) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((2)),(0),null)):null);
return sci.impl.protocols.extend.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__5733__auto__);
});

(sci.impl.protocols.extend.cljs$core$IFn$_invoke$arity$variadic = (function (ctx,atype,proto_PLUS_mmaps){
var seq__24166 = cljs.core.seq(cljs.core.partition.cljs$core$IFn$_invoke$arity$2((2),proto_PLUS_mmaps));
var chunk__24168 = null;
var count__24169 = (0);
var i__24170 = (0);
while(true){
if((i__24170 < count__24169)){
var vec__24391 = chunk__24168.cljs$core$IIndexed$_nth$arity$2(null, i__24170);
var proto = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24391,(0),null);
var mmap = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24391,(1),null);
var extend_via_metadata_25059 = new cljs.core.Keyword(null,"extend-via-metadata","extend-via-metadata",-427346794).cljs$core$IFn$_invoke$arity$1(proto);
var proto_ns_25060 = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(proto);
var pns_25061 = sci.impl.vars.getName(proto_ns_25060);
var pns_str_25062 = (cljs.core.truth_(extend_via_metadata_25059)?cljs.core.str.cljs$core$IFn$_invoke$arity$1(pns_25061):null);
var seq__24394_25063 = cljs.core.seq(mmap);
var chunk__24395_25064 = null;
var count__24396_25065 = (0);
var i__24397_25066 = (0);
while(true){
if((i__24397_25066 < count__24396_25065)){
var vec__24452_25074 = chunk__24395_25064.cljs$core$IIndexed$_nth$arity$2(null, i__24397_25066);
var meth_name_25084 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24452_25074,(0),null);
var f_25088 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24452_25074,(1),null);
var meth_str_25108 = cljs.core.name(meth_name_25084);
var meth_sym_25109 = cljs.core.symbol.cljs$core$IFn$_invoke$arity$1(meth_str_25108);
var env_25110 = cljs.core.deref(new cljs.core.Keyword(null,"env","env",-1815813235).cljs$core$IFn$_invoke$arity$1(ctx));
var multi_method_var_25111 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(env_25110,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"namespaces","namespaces",-1444157469),pns_25061,meth_sym_25109], null));
var multi_method_25112 = cljs.core.deref(multi_method_var_25111);
sci.impl.multimethods.multi_fn_add_method_impl(multi_method_25112,atype,(cljs.core.truth_(extend_via_metadata_25059)?(function (){var fq = cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(pns_str_25062,meth_str_25108);
return ((function (seq__24394_25063,chunk__24395_25064,count__24396_25065,i__24397_25066,seq__24166,chunk__24168,count__24169,i__24170,fq,meth_str_25108,meth_sym_25109,env_25110,multi_method_var_25111,multi_method_25112,vec__24452_25074,meth_name_25084,f_25088,extend_via_metadata_25059,proto_ns_25060,pns_25061,pns_str_25062,vec__24391,proto,mmap){
return (function() { 
var G__25113__delegate = function (this$,args){
var temp__5802__auto__ = cljs.core.meta(this$);
if(cljs.core.truth_(temp__5802__auto__)){
var m = temp__5802__auto__;
var temp__5802__auto____$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(m,fq);
if(cljs.core.truth_(temp__5802__auto____$1)){
var meth = temp__5802__auto____$1;
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(meth,this$,args);
} else {
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(f_25088,this$,args);
}
} else {
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(f_25088,this$,args);
}
};
var G__25113 = function (this$,var_args){
var args = null;
if (arguments.length > 1) {
var G__25118__i = 0, G__25118__a = new Array(arguments.length -  1);
while (G__25118__i < G__25118__a.length) {G__25118__a[G__25118__i] = arguments[G__25118__i + 1]; ++G__25118__i;}
  args = new cljs.core.IndexedSeq(G__25118__a,0,null);
} 
return G__25113__delegate.call(this,this$,args);};
G__25113.cljs$lang$maxFixedArity = 1;
G__25113.cljs$lang$applyTo = (function (arglist__25119){
var this$ = cljs.core.first(arglist__25119);
var args = cljs.core.rest(arglist__25119);
return G__25113__delegate(this$,args);
});
G__25113.cljs$core$IFn$_invoke$arity$variadic = G__25113__delegate;
return G__25113;
})()
;
;})(seq__24394_25063,chunk__24395_25064,count__24396_25065,i__24397_25066,seq__24166,chunk__24168,count__24169,i__24170,fq,meth_str_25108,meth_sym_25109,env_25110,multi_method_var_25111,multi_method_25112,vec__24452_25074,meth_name_25084,f_25088,extend_via_metadata_25059,proto_ns_25060,pns_25061,pns_str_25062,vec__24391,proto,mmap))
})():f_25088));


var G__25120 = seq__24394_25063;
var G__25121 = chunk__24395_25064;
var G__25122 = count__24396_25065;
var G__25123 = (i__24397_25066 + (1));
seq__24394_25063 = G__25120;
chunk__24395_25064 = G__25121;
count__24396_25065 = G__25122;
i__24397_25066 = G__25123;
continue;
} else {
var temp__5804__auto___25124 = cljs.core.seq(seq__24394_25063);
if(temp__5804__auto___25124){
var seq__24394_25125__$1 = temp__5804__auto___25124;
if(cljs.core.chunked_seq_QMARK_(seq__24394_25125__$1)){
var c__5525__auto___25126 = cljs.core.chunk_first(seq__24394_25125__$1);
var G__25127 = cljs.core.chunk_rest(seq__24394_25125__$1);
var G__25128 = c__5525__auto___25126;
var G__25129 = cljs.core.count(c__5525__auto___25126);
var G__25130 = (0);
seq__24394_25063 = G__25127;
chunk__24395_25064 = G__25128;
count__24396_25065 = G__25129;
i__24397_25066 = G__25130;
continue;
} else {
var vec__24465_25131 = cljs.core.first(seq__24394_25125__$1);
var meth_name_25132 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24465_25131,(0),null);
var f_25133 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24465_25131,(1),null);
var meth_str_25134 = cljs.core.name(meth_name_25132);
var meth_sym_25135 = cljs.core.symbol.cljs$core$IFn$_invoke$arity$1(meth_str_25134);
var env_25136 = cljs.core.deref(new cljs.core.Keyword(null,"env","env",-1815813235).cljs$core$IFn$_invoke$arity$1(ctx));
var multi_method_var_25137 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(env_25136,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"namespaces","namespaces",-1444157469),pns_25061,meth_sym_25135], null));
var multi_method_25138 = cljs.core.deref(multi_method_var_25137);
sci.impl.multimethods.multi_fn_add_method_impl(multi_method_25138,atype,(cljs.core.truth_(extend_via_metadata_25059)?(function (){var fq = cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(pns_str_25062,meth_str_25134);
return ((function (seq__24394_25063,chunk__24395_25064,count__24396_25065,i__24397_25066,seq__24166,chunk__24168,count__24169,i__24170,fq,meth_str_25134,meth_sym_25135,env_25136,multi_method_var_25137,multi_method_25138,vec__24465_25131,meth_name_25132,f_25133,seq__24394_25125__$1,temp__5804__auto___25124,extend_via_metadata_25059,proto_ns_25060,pns_25061,pns_str_25062,vec__24391,proto,mmap){
return (function() { 
var G__25140__delegate = function (this$,args){
var temp__5802__auto__ = cljs.core.meta(this$);
if(cljs.core.truth_(temp__5802__auto__)){
var m = temp__5802__auto__;
var temp__5802__auto____$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(m,fq);
if(cljs.core.truth_(temp__5802__auto____$1)){
var meth = temp__5802__auto____$1;
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(meth,this$,args);
} else {
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(f_25133,this$,args);
}
} else {
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(f_25133,this$,args);
}
};
var G__25140 = function (this$,var_args){
var args = null;
if (arguments.length > 1) {
var G__25141__i = 0, G__25141__a = new Array(arguments.length -  1);
while (G__25141__i < G__25141__a.length) {G__25141__a[G__25141__i] = arguments[G__25141__i + 1]; ++G__25141__i;}
  args = new cljs.core.IndexedSeq(G__25141__a,0,null);
} 
return G__25140__delegate.call(this,this$,args);};
G__25140.cljs$lang$maxFixedArity = 1;
G__25140.cljs$lang$applyTo = (function (arglist__25142){
var this$ = cljs.core.first(arglist__25142);
var args = cljs.core.rest(arglist__25142);
return G__25140__delegate(this$,args);
});
G__25140.cljs$core$IFn$_invoke$arity$variadic = G__25140__delegate;
return G__25140;
})()
;
;})(seq__24394_25063,chunk__24395_25064,count__24396_25065,i__24397_25066,seq__24166,chunk__24168,count__24169,i__24170,fq,meth_str_25134,meth_sym_25135,env_25136,multi_method_var_25137,multi_method_25138,vec__24465_25131,meth_name_25132,f_25133,seq__24394_25125__$1,temp__5804__auto___25124,extend_via_metadata_25059,proto_ns_25060,pns_25061,pns_str_25062,vec__24391,proto,mmap))
})():f_25133));


var G__25143 = cljs.core.next(seq__24394_25125__$1);
var G__25144 = null;
var G__25145 = (0);
var G__25146 = (0);
seq__24394_25063 = G__25143;
chunk__24395_25064 = G__25144;
count__24396_25065 = G__25145;
i__24397_25066 = G__25146;
continue;
}
} else {
}
}
break;
}


var G__25147 = seq__24166;
var G__25148 = chunk__24168;
var G__25149 = count__24169;
var G__25150 = (i__24170 + (1));
seq__24166 = G__25147;
chunk__24168 = G__25148;
count__24169 = G__25149;
i__24170 = G__25150;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__24166);
if(temp__5804__auto__){
var seq__24166__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__24166__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__24166__$1);
var G__25151 = cljs.core.chunk_rest(seq__24166__$1);
var G__25152 = c__5525__auto__;
var G__25153 = cljs.core.count(c__5525__auto__);
var G__25154 = (0);
seq__24166 = G__25151;
chunk__24168 = G__25152;
count__24169 = G__25153;
i__24170 = G__25154;
continue;
} else {
var vec__24498 = cljs.core.first(seq__24166__$1);
var proto = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24498,(0),null);
var mmap = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24498,(1),null);
var extend_via_metadata_25155 = new cljs.core.Keyword(null,"extend-via-metadata","extend-via-metadata",-427346794).cljs$core$IFn$_invoke$arity$1(proto);
var proto_ns_25156 = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(proto);
var pns_25157 = sci.impl.vars.getName(proto_ns_25156);
var pns_str_25158 = (cljs.core.truth_(extend_via_metadata_25155)?cljs.core.str.cljs$core$IFn$_invoke$arity$1(pns_25157):null);
var seq__24501_25159 = cljs.core.seq(mmap);
var chunk__24502_25160 = null;
var count__24503_25161 = (0);
var i__24504_25162 = (0);
while(true){
if((i__24504_25162 < count__24503_25161)){
var vec__24569_25163 = chunk__24502_25160.cljs$core$IIndexed$_nth$arity$2(null, i__24504_25162);
var meth_name_25164 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24569_25163,(0),null);
var f_25165 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24569_25163,(1),null);
var meth_str_25166 = cljs.core.name(meth_name_25164);
var meth_sym_25167 = cljs.core.symbol.cljs$core$IFn$_invoke$arity$1(meth_str_25166);
var env_25168 = cljs.core.deref(new cljs.core.Keyword(null,"env","env",-1815813235).cljs$core$IFn$_invoke$arity$1(ctx));
var multi_method_var_25169 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(env_25168,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"namespaces","namespaces",-1444157469),pns_25157,meth_sym_25167], null));
var multi_method_25170 = cljs.core.deref(multi_method_var_25169);
sci.impl.multimethods.multi_fn_add_method_impl(multi_method_25170,atype,(cljs.core.truth_(extend_via_metadata_25155)?(function (){var fq = cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(pns_str_25158,meth_str_25166);
return ((function (seq__24501_25159,chunk__24502_25160,count__24503_25161,i__24504_25162,seq__24166,chunk__24168,count__24169,i__24170,fq,meth_str_25166,meth_sym_25167,env_25168,multi_method_var_25169,multi_method_25170,vec__24569_25163,meth_name_25164,f_25165,extend_via_metadata_25155,proto_ns_25156,pns_25157,pns_str_25158,vec__24498,proto,mmap,seq__24166__$1,temp__5804__auto__){
return (function() { 
var G__25172__delegate = function (this$,args){
var temp__5802__auto__ = cljs.core.meta(this$);
if(cljs.core.truth_(temp__5802__auto__)){
var m = temp__5802__auto__;
var temp__5802__auto____$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(m,fq);
if(cljs.core.truth_(temp__5802__auto____$1)){
var meth = temp__5802__auto____$1;
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(meth,this$,args);
} else {
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(f_25165,this$,args);
}
} else {
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(f_25165,this$,args);
}
};
var G__25172 = function (this$,var_args){
var args = null;
if (arguments.length > 1) {
var G__25213__i = 0, G__25213__a = new Array(arguments.length -  1);
while (G__25213__i < G__25213__a.length) {G__25213__a[G__25213__i] = arguments[G__25213__i + 1]; ++G__25213__i;}
  args = new cljs.core.IndexedSeq(G__25213__a,0,null);
} 
return G__25172__delegate.call(this,this$,args);};
G__25172.cljs$lang$maxFixedArity = 1;
G__25172.cljs$lang$applyTo = (function (arglist__25214){
var this$ = cljs.core.first(arglist__25214);
var args = cljs.core.rest(arglist__25214);
return G__25172__delegate(this$,args);
});
G__25172.cljs$core$IFn$_invoke$arity$variadic = G__25172__delegate;
return G__25172;
})()
;
;})(seq__24501_25159,chunk__24502_25160,count__24503_25161,i__24504_25162,seq__24166,chunk__24168,count__24169,i__24170,fq,meth_str_25166,meth_sym_25167,env_25168,multi_method_var_25169,multi_method_25170,vec__24569_25163,meth_name_25164,f_25165,extend_via_metadata_25155,proto_ns_25156,pns_25157,pns_str_25158,vec__24498,proto,mmap,seq__24166__$1,temp__5804__auto__))
})():f_25165));


var G__25215 = seq__24501_25159;
var G__25216 = chunk__24502_25160;
var G__25217 = count__24503_25161;
var G__25218 = (i__24504_25162 + (1));
seq__24501_25159 = G__25215;
chunk__24502_25160 = G__25216;
count__24503_25161 = G__25217;
i__24504_25162 = G__25218;
continue;
} else {
var temp__5804__auto___25219__$1 = cljs.core.seq(seq__24501_25159);
if(temp__5804__auto___25219__$1){
var seq__24501_25220__$1 = temp__5804__auto___25219__$1;
if(cljs.core.chunked_seq_QMARK_(seq__24501_25220__$1)){
var c__5525__auto___25221 = cljs.core.chunk_first(seq__24501_25220__$1);
var G__25222 = cljs.core.chunk_rest(seq__24501_25220__$1);
var G__25223 = c__5525__auto___25221;
var G__25224 = cljs.core.count(c__5525__auto___25221);
var G__25225 = (0);
seq__24501_25159 = G__25222;
chunk__24502_25160 = G__25223;
count__24503_25161 = G__25224;
i__24504_25162 = G__25225;
continue;
} else {
var vec__24600_25226 = cljs.core.first(seq__24501_25220__$1);
var meth_name_25227 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24600_25226,(0),null);
var f_25228 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24600_25226,(1),null);
var meth_str_25229 = cljs.core.name(meth_name_25227);
var meth_sym_25230 = cljs.core.symbol.cljs$core$IFn$_invoke$arity$1(meth_str_25229);
var env_25231 = cljs.core.deref(new cljs.core.Keyword(null,"env","env",-1815813235).cljs$core$IFn$_invoke$arity$1(ctx));
var multi_method_var_25232 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(env_25231,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"namespaces","namespaces",-1444157469),pns_25157,meth_sym_25230], null));
var multi_method_25233 = cljs.core.deref(multi_method_var_25232);
sci.impl.multimethods.multi_fn_add_method_impl(multi_method_25233,atype,(cljs.core.truth_(extend_via_metadata_25155)?(function (){var fq = cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(pns_str_25158,meth_str_25229);
return ((function (seq__24501_25159,chunk__24502_25160,count__24503_25161,i__24504_25162,seq__24166,chunk__24168,count__24169,i__24170,fq,meth_str_25229,meth_sym_25230,env_25231,multi_method_var_25232,multi_method_25233,vec__24600_25226,meth_name_25227,f_25228,seq__24501_25220__$1,temp__5804__auto___25219__$1,extend_via_metadata_25155,proto_ns_25156,pns_25157,pns_str_25158,vec__24498,proto,mmap,seq__24166__$1,temp__5804__auto__){
return (function() { 
var G__25235__delegate = function (this$,args){
var temp__5802__auto__ = cljs.core.meta(this$);
if(cljs.core.truth_(temp__5802__auto__)){
var m = temp__5802__auto__;
var temp__5802__auto____$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(m,fq);
if(cljs.core.truth_(temp__5802__auto____$1)){
var meth = temp__5802__auto____$1;
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(meth,this$,args);
} else {
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(f_25228,this$,args);
}
} else {
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(f_25228,this$,args);
}
};
var G__25235 = function (this$,var_args){
var args = null;
if (arguments.length > 1) {
var G__25239__i = 0, G__25239__a = new Array(arguments.length -  1);
while (G__25239__i < G__25239__a.length) {G__25239__a[G__25239__i] = arguments[G__25239__i + 1]; ++G__25239__i;}
  args = new cljs.core.IndexedSeq(G__25239__a,0,null);
} 
return G__25235__delegate.call(this,this$,args);};
G__25235.cljs$lang$maxFixedArity = 1;
G__25235.cljs$lang$applyTo = (function (arglist__25240){
var this$ = cljs.core.first(arglist__25240);
var args = cljs.core.rest(arglist__25240);
return G__25235__delegate(this$,args);
});
G__25235.cljs$core$IFn$_invoke$arity$variadic = G__25235__delegate;
return G__25235;
})()
;
;})(seq__24501_25159,chunk__24502_25160,count__24503_25161,i__24504_25162,seq__24166,chunk__24168,count__24169,i__24170,fq,meth_str_25229,meth_sym_25230,env_25231,multi_method_var_25232,multi_method_25233,vec__24600_25226,meth_name_25227,f_25228,seq__24501_25220__$1,temp__5804__auto___25219__$1,extend_via_metadata_25155,proto_ns_25156,pns_25157,pns_str_25158,vec__24498,proto,mmap,seq__24166__$1,temp__5804__auto__))
})():f_25228));


var G__25241 = cljs.core.next(seq__24501_25220__$1);
var G__25242 = null;
var G__25243 = (0);
var G__25244 = (0);
seq__24501_25159 = G__25241;
chunk__24502_25160 = G__25242;
count__24503_25161 = G__25243;
i__24504_25162 = G__25244;
continue;
}
} else {
}
}
break;
}


var G__25245 = cljs.core.next(seq__24166__$1);
var G__25246 = null;
var G__25247 = (0);
var G__25248 = (0);
seq__24166 = G__25245;
chunk__24168 = G__25246;
count__24169 = G__25247;
i__24170 = G__25248;
continue;
}
} else {
return null;
}
}
break;
}
}));

(sci.impl.protocols.extend.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(sci.impl.protocols.extend.cljs$lang$applyTo = (function (seq24152){
var G__24153 = cljs.core.first(seq24152);
var seq24152__$1 = cljs.core.next(seq24152);
var G__24154 = cljs.core.first(seq24152__$1);
var seq24152__$2 = cljs.core.next(seq24152__$1);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__24153,G__24154,seq24152__$2);
}));

/**
 * Processes single args+body pair for extending via metadata
 */
sci.impl.protocols.process_single_extend_meta = (function sci$impl$protocols$process_single_extend_meta(fq,p__24640){
var vec__24671 = p__24640;
var seq__24672 = cljs.core.seq(vec__24671);
var first__24673 = cljs.core.first(seq__24672);
var seq__24672__$1 = cljs.core.next(seq__24672);
var args = first__24673;
var body = seq__24672__$1;
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [args,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","let","cljs.core/let",-308701135,null),null,(1),null)),(new cljs.core.List(null,cljs.core.vec(cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"farg__24637__auto__","farg__24637__auto__",-397219096,null),null,(1),null)),(new cljs.core.List(null,cljs.core.first(args),null,(1),null)))))),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","if-let","cljs.core/if-let",1346583165,null),null,(1),null)),(new cljs.core.List(null,cljs.core.vec(cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"m__24638__auto__","m__24638__auto__",-1813517481,null),null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","meta","cljs.core/meta",-748218346,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"farg__24637__auto__","farg__24637__auto__",-397219096,null),null,(1),null))))),null,(1),null)))))),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","if-let","cljs.core/if-let",1346583165,null),null,(1),null)),(new cljs.core.List(null,cljs.core.vec(cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"meth__24639__auto__","meth__24639__auto__",-715765632,null),null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","get","cljs.core/get",-296075407,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"m__24638__auto__","m__24638__auto__",-1813517481,null),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),null,(1),null)),(new cljs.core.List(null,fq,null,(1),null))))),null,(1),null))], 0)))),null,(1),null)))))),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","apply","cljs.core/apply",1757277831,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"meth__24639__auto__","meth__24639__auto__",-715765632,null),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,args,null,(1),null))], 0)))),null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"do","do",1686842252,null),null,(1),null)),body))),null,(1),null))], 0)))),null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"do","do",1686842252,null),null,(1),null)),body))),null,(1),null))], 0)))),null,(1),null))], 0))))], null);
});
sci.impl.protocols.process_methods = (function sci$impl$protocols$process_methods(type,meths,protocol_ns,extend_via_metadata){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__24707){
var vec__24708 = p__24707;
var seq__24709 = cljs.core.seq(vec__24708);
var first__24710 = cljs.core.first(seq__24709);
var seq__24709__$1 = cljs.core.next(seq__24709);
var meth_name = first__24710;
var fn_body = seq__24709__$1;
var fq = cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(protocol_ns,cljs.core.name(meth_name));
var fn_body__$1 = (cljs.core.truth_(extend_via_metadata)?((cljs.core.vector_QMARK_(cljs.core.first(fn_body)))?sci.impl.protocols.process_single_extend_meta(fq,fn_body):cljs.core.mapcat.cljs$core$IFn$_invoke$arity$variadic((function (p1__24676_SHARP_){
return sci.impl.protocols.process_single_extend_meta(fq,p1__24676_SHARP_);
}),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([fn_body], 0))):fn_body);
return cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","defmethod","cljs.core/defmethod",-180785162,null),null,(1),null)),(new cljs.core.List(null,fq,null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,type,null,(1),null)),fn_body__$1], 0))));
}),meths);
});
sci.impl.protocols.extend_protocol = (function sci$impl$protocols$extend_protocol(var_args){
var args__5732__auto__ = [];
var len__5726__auto___25289 = arguments.length;
var i__5727__auto___25290 = (0);
while(true){
if((i__5727__auto___25290 < len__5726__auto___25289)){
args__5732__auto__.push((arguments[i__5727__auto___25290]));

var G__25291 = (i__5727__auto___25290 + (1));
i__5727__auto___25290 = G__25291;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((4) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((4)),(0),null)):null);
return sci.impl.protocols.extend_protocol.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),argseq__5733__auto__);
});

(sci.impl.protocols.extend_protocol.cljs$core$IFn$_invoke$arity$variadic = (function (_,___$1,ctx,protocol_name,impls){
var impls__$1 = sci.impl.utils.split_when((function (p1__24711_SHARP_){
return (!(cljs.core.seq_QMARK_(p1__24711_SHARP_)));
}),impls);
var protocol_var = (function (){var G__24748 = ctx;
var G__24749 = new cljs.core.Keyword(null,"bindingx","bindingx",679516896).cljs$core$IFn$_invoke$arity$1(ctx);
var G__24750 = protocol_name;
var fexpr__24747 = cljs.core.deref(sci.impl.utils.eval_resolve_state);
return (fexpr__24747.cljs$core$IFn$_invoke$arity$3 ? fexpr__24747.cljs$core$IFn$_invoke$arity$3(G__24748,G__24749,G__24750) : fexpr__24747.call(null, G__24748,G__24749,G__24750));
})();
var protocol_data = cljs.core.deref(protocol_var);
var extend_via_metadata = new cljs.core.Keyword(null,"extend-via-metadata","extend-via-metadata",-427346794).cljs$core$IFn$_invoke$arity$1(protocol_data);
var protocol_ns = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(protocol_data);
var pns = cljs.core.str.cljs$core$IFn$_invoke$arity$1(sci.impl.vars.getName(protocol_ns));
var expansion = cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"do","do",1686842252,null),null,(1),null)),cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__24751){
var vec__24752 = p__24751;
var seq__24753 = cljs.core.seq(vec__24752);
var first__24754 = cljs.core.first(seq__24753);
var seq__24753__$1 = cljs.core.next(seq__24753);
var type = first__24754;
var meths = seq__24753__$1;
return cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"do","do",1686842252,null),null,(1),null)),sci.impl.protocols.process_methods(type,meths,pns,extend_via_metadata))));
}),impls__$1))));
return expansion;
}));

(sci.impl.protocols.extend_protocol.cljs$lang$maxFixedArity = (4));

/** @this {Function} */
(sci.impl.protocols.extend_protocol.cljs$lang$applyTo = (function (seq24712){
var G__24713 = cljs.core.first(seq24712);
var seq24712__$1 = cljs.core.next(seq24712);
var G__24714 = cljs.core.first(seq24712__$1);
var seq24712__$2 = cljs.core.next(seq24712__$1);
var G__24715 = cljs.core.first(seq24712__$2);
var seq24712__$3 = cljs.core.next(seq24712__$2);
var G__24716 = cljs.core.first(seq24712__$3);
var seq24712__$4 = cljs.core.next(seq24712__$3);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__24713,G__24714,G__24715,G__24716,seq24712__$4);
}));

sci.impl.protocols.extend_type = (function sci$impl$protocols$extend_type(var_args){
var args__5732__auto__ = [];
var len__5726__auto___25292 = arguments.length;
var i__5727__auto___25293 = (0);
while(true){
if((i__5727__auto___25293 < len__5726__auto___25292)){
args__5732__auto__.push((arguments[i__5727__auto___25293]));

var G__25294 = (i__5727__auto___25293 + (1));
i__5727__auto___25293 = G__25294;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((4) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((4)),(0),null)):null);
return sci.impl.protocols.extend_type.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),argseq__5733__auto__);
});

(sci.impl.protocols.extend_type.cljs$core$IFn$_invoke$arity$variadic = (function (_,___$1,ctx,atype,proto_PLUS_meths){
var proto_PLUS_meths__$1 = sci.impl.utils.split_when((function (p1__24790_SHARP_){
return (!(cljs.core.seq_QMARK_(p1__24790_SHARP_)));
}),proto_PLUS_meths);
return cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"do","do",1686842252,null),null,(1),null)),cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__24830){
var vec__24831 = p__24830;
var seq__24832 = cljs.core.seq(vec__24831);
var first__24833 = cljs.core.first(seq__24832);
var seq__24832__$1 = cljs.core.next(seq__24832);
var proto = first__24833;
var meths = seq__24832__$1;
var protocol_var = (function (){var G__24835 = ctx;
var G__24836 = new cljs.core.Keyword(null,"bindings","bindings",1271397192).cljs$core$IFn$_invoke$arity$1(ctx);
var G__24837 = proto;
var fexpr__24834 = cljs.core.deref(sci.impl.utils.eval_resolve_state);
return (fexpr__24834.cljs$core$IFn$_invoke$arity$3 ? fexpr__24834.cljs$core$IFn$_invoke$arity$3(G__24835,G__24836,G__24837) : fexpr__24834.call(null, G__24835,G__24836,G__24837));
})();
var proto_data = cljs.core.deref(protocol_var);
var protocol_ns = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(proto_data);
var pns = cljs.core.str.cljs$core$IFn$_invoke$arity$1(sci.impl.vars.getName(protocol_ns));
var extend_via_metadata = new cljs.core.Keyword(null,"extend-via-metadata","extend-via-metadata",-427346794).cljs$core$IFn$_invoke$arity$1(proto_data);
return cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"do","do",1686842252,null),null,(1),null)),sci.impl.protocols.process_methods(atype,meths,pns,extend_via_metadata))));
}),proto_PLUS_meths__$1))));
}));

(sci.impl.protocols.extend_type.cljs$lang$maxFixedArity = (4));

/** @this {Function} */
(sci.impl.protocols.extend_type.cljs$lang$applyTo = (function (seq24791){
var G__24792 = cljs.core.first(seq24791);
var seq24791__$1 = cljs.core.next(seq24791);
var G__24793 = cljs.core.first(seq24791__$1);
var seq24791__$2 = cljs.core.next(seq24791__$1);
var G__24794 = cljs.core.first(seq24791__$2);
var seq24791__$3 = cljs.core.next(seq24791__$2);
var G__24795 = cljs.core.first(seq24791__$3);
var seq24791__$4 = cljs.core.next(seq24791__$3);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__24792,G__24793,G__24794,G__24795,seq24791__$4);
}));

sci.impl.protocols.find_matching_non_default_method = (function sci$impl$protocols$find_matching_non_default_method(protocol,obj){
return cljs.core.boolean$(cljs.core.some((function (p1__24873_SHARP_){
var temp__5804__auto__ = cljs.core.get_method(p1__24873_SHARP_,sci.impl.types.type_impl(obj));
if(cljs.core.truth_(temp__5804__auto__)){
var m = temp__5804__auto__;
var ms = cljs.core.methods$(p1__24873_SHARP_);
var default$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(ms,new cljs.core.Keyword(null,"default","default",-1987822328));
return (!((m === default$)));
} else {
return null;
}
}),new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(protocol)));
});
sci.impl.protocols.satisfies_QMARK_ = (function sci$impl$protocols$satisfies_QMARK_(protocol,obj){
if((obj instanceof sci.impl.types.Reified)){
return cljs.core.contains_QMARK_(obj.sci$impl$types$IReified$getProtocols$arity$1(null, ),protocol);
} else {
var p = new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(protocol);
var or__5002__auto__ = (function (){var and__5000__auto__ = p;
if(cljs.core.truth_(and__5000__auto__)){
var pred__24910 = cljs.core._EQ_;
var expr__24911 = p;
if(cljs.core.truth_((pred__24910.cljs$core$IFn$_invoke$arity$2 ? pred__24910.cljs$core$IFn$_invoke$arity$2(cljs.core.IDeref,expr__24911) : pred__24910.call(null, cljs.core.IDeref,expr__24911)))){
if((!((obj == null)))){
if((((obj.cljs$lang$protocol_mask$partition0$ & (32768))) || ((cljs.core.PROTOCOL_SENTINEL === obj.cljs$core$IDeref$)))){
return true;
} else {
if((!obj.cljs$lang$protocol_mask$partition0$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.IDeref,obj);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.IDeref,obj);
}
} else {
if(cljs.core.truth_((pred__24910.cljs$core$IFn$_invoke$arity$2 ? pred__24910.cljs$core$IFn$_invoke$arity$2(cljs.core.ISwap,expr__24911) : pred__24910.call(null, cljs.core.ISwap,expr__24911)))){
if((!((obj == null)))){
if((((obj.cljs$lang$protocol_mask$partition1$ & (65536))) || ((cljs.core.PROTOCOL_SENTINEL === obj.cljs$core$ISwap$)))){
return true;
} else {
if((!obj.cljs$lang$protocol_mask$partition1$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.ISwap,obj);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.ISwap,obj);
}
} else {
if(cljs.core.truth_((pred__24910.cljs$core$IFn$_invoke$arity$2 ? pred__24910.cljs$core$IFn$_invoke$arity$2(cljs.core.IReset,expr__24911) : pred__24910.call(null, cljs.core.IReset,expr__24911)))){
if((!((obj == null)))){
if((((obj.cljs$lang$protocol_mask$partition1$ & (32768))) || ((cljs.core.PROTOCOL_SENTINEL === obj.cljs$core$IReset$)))){
return true;
} else {
if((!obj.cljs$lang$protocol_mask$partition1$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.IReset,obj);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.IReset,obj);
}
} else {
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(expr__24911)].join('')));
}
}
}
} else {
return and__5000__auto__;
}
})();
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return sci.impl.protocols.find_matching_non_default_method(protocol,obj);
}
}
});
sci.impl.protocols.instance_impl = (function sci$impl$protocols$instance_impl(clazz,x){
if(cljs.core.truth_((function (){var and__5000__auto__ = (clazz instanceof cljs.core.Symbol);
if(and__5000__auto__){
var G__24924 = clazz;
var G__24924__$1 = (((G__24924 == null))?null:cljs.core.meta(G__24924));
if((G__24924__$1 == null)){
return null;
} else {
return new cljs.core.Keyword("sci.impl","record","sci.impl/record",-1939193950).cljs$core$IFn$_invoke$arity$1(G__24924__$1);
}
} else {
return and__5000__auto__;
}
})())){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(clazz,new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(cljs.core.meta(x)));
} else {
return (x instanceof clazz);

}
});
/**
 * Returns true if atype extends protocol
 */
sci.impl.protocols.extends_QMARK_ = (function sci$impl$protocols$extends_QMARK_(protocol,atype){
return cljs.core.boolean$(cljs.core.some((function (p1__24927_SHARP_){
return cljs.core.get_method(p1__24927_SHARP_,atype);
}),new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(protocol)));
});

//# sourceMappingURL=sci.impl.protocols.js.map
