goog.provide('sci.impl.evaluator');

sci.impl.evaluator.macros = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Symbol(null,"fn","fn",465265323,null),"null",new cljs.core.Symbol(null,"do","do",1686842252,null),"null",new cljs.core.Symbol(null,"defn","defn",-126010802,null),"null",new cljs.core.Symbol(null,"syntax-quote","syntax-quote",407366680,null),"null",new cljs.core.Symbol(null,"def","def",597100991,null),"null"], null), null);
/**
 * The and macro from clojure.core. Note: and is unrolled in the analyzer, this is a fallback.
 */
sci.impl.evaluator.eval_and = (function sci$impl$evaluator$eval_and(ctx,bindings,args){
var args__$1 = cljs.core.seq(args);
var args__$2 = args__$1;
while(true){
if(args__$2){
var x = cljs.core.first(args__$2);
var v = (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,x) : sci.impl.evaluator.eval.call(null, ctx,bindings,x));
if(cljs.core.truth_(v)){
var xs = cljs.core.next(args__$2);
if(xs){
var G__23453 = xs;
args__$2 = G__23453;
continue;
} else {
return v;
}
} else {
return v;
}
} else {
return true;
}
break;
}
});
/**
 * The or macro from clojure.core. Note: or is unrolled in the analyzer, this is a fallback.
 */
sci.impl.evaluator.eval_or = (function sci$impl$evaluator$eval_or(ctx,bindings,args){
var args__$1 = cljs.core.seq(args);
var args__$2 = args__$1;
while(true){
if(args__$2){
var x = cljs.core.first(args__$2);
var v = (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,x) : sci.impl.evaluator.eval.call(null, ctx,bindings,x));
if(cljs.core.truth_(v)){
return v;
} else {
var xs = cljs.core.next(args__$2);
if(xs){
var G__23456 = xs;
args__$2 = G__23456;
continue;
} else {
return v;
}
}
} else {
return null;
}
break;
}
});
/**
 * The let macro from clojure.core
 */
sci.impl.evaluator.eval_let = (function sci$impl$evaluator$eval_let(ctx,bindings,let_bindings,exprs){
var vec__22417 = (function (){var ctx__$1 = ctx;
var bindings__$1 = bindings;
var let_bindings__$1 = let_bindings;
while(true){
var let_name = cljs.core.first(let_bindings__$1);
var let_bindings__$2 = cljs.core.rest(let_bindings__$1);
var let_val = cljs.core.first(let_bindings__$2);
var rest_let_bindings = cljs.core.next(let_bindings__$2);
var v = (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx__$1,bindings__$1,let_val) : sci.impl.evaluator.eval.call(null, ctx__$1,bindings__$1,let_val));
var bindings__$2 = cljs.core._assoc(bindings__$1,let_name,v);
if(cljs.core.not(rest_let_bindings)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [ctx__$1,bindings__$2], null);
} else {
var G__23460 = ctx__$1;
var G__23461 = bindings__$2;
var G__23462 = rest_let_bindings;
ctx__$1 = G__23460;
bindings__$1 = G__23461;
let_bindings__$1 = G__23462;
continue;
}
break;
}
})();
var ctx__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__22417,(0),null);
var bindings__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__22417,(1),null);
if(cljs.core.truth_(exprs)){
var exprs__$1 = exprs;
while(true){
var e = cljs.core.first(exprs__$1);
var ret = (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx__$1,bindings__$1,e) : sci.impl.evaluator.eval.call(null, ctx__$1,bindings__$1,e));
var nexprs = cljs.core.next(exprs__$1);
if(nexprs){
var G__23463 = nexprs;
exprs__$1 = G__23463;
continue;
} else {
return ret;
}
break;
}
} else {
return null;
}
});
sci.impl.evaluator.handle_meta = (function sci$impl$evaluator$handle_meta(ctx,bindings,m){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2((function (){var temp__5802__auto__ = cljs.core.meta(m);
if(cljs.core.truth_(temp__5802__auto__)){
var mm = temp__5802__auto__;
if(cljs.core.truth_((cljs.core.truth_(mm)?mm.get(new cljs.core.Keyword("sci.impl","op","sci.impl/op",950953978)):null))){
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,m) : sci.impl.evaluator.eval.call(null, ctx,bindings,m));
} else {
return m;
}
} else {
return m;
}
})(),new cljs.core.Keyword("sci.impl","op","sci.impl/op",950953978));
});
sci.impl.evaluator.eval_map = (function sci$impl$evaluator$eval_map(ctx,bindings,expr){
var temp__5802__auto__ = cljs.core.meta(expr);
if(cljs.core.truth_(temp__5802__auto__)){
var m = temp__5802__auto__;
if(cljs.core.truth_((function (){var G__22455 = new cljs.core.Keyword(null,"eval","eval",-1103567905);
var G__22456 = new cljs.core.Keyword("sci.impl","op","sci.impl/op",950953978).cljs$core$IFn$_invoke$arity$1(m);
return (sci.impl.utils.kw_identical_QMARK_.cljs$core$IFn$_invoke$arity$2 ? sci.impl.utils.kw_identical_QMARK_.cljs$core$IFn$_invoke$arity$2(G__22455,G__22456) : sci.impl.utils.kw_identical_QMARK_.call(null, G__22455,G__22456));
})())){
return cljs.core.with_meta(cljs.core.zipmap(cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__22451_SHARP_){
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,p1__22451_SHARP_) : sci.impl.evaluator.eval.call(null, ctx,bindings,p1__22451_SHARP_));
}),cljs.core.keys(expr)),cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__22452_SHARP_){
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,p1__22452_SHARP_) : sci.impl.evaluator.eval.call(null, ctx,bindings,p1__22452_SHARP_));
}),cljs.core.vals(expr))),sci.impl.evaluator.handle_meta(ctx,bindings,m));
} else {
return expr;
}
} else {
return expr;
}
});
sci.impl.evaluator.eval_def = (function sci$impl$evaluator$eval_def(ctx,bindings,var_name,init,m){
var init__$1 = (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,init) : sci.impl.evaluator.eval.call(null, ctx,bindings,init));
var m__$1 = (function (){var or__5002__auto__ = m;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return cljs.core.meta(var_name);
}
})();
var m__$2 = sci.impl.evaluator.eval_map(ctx,bindings,m__$1);
var cnn = sci.impl.vars.getName(new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m__$2));
var assoc_in_env = (function (env){
var the_current_ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.get.cljs$core$IFn$_invoke$arity$2(env,new cljs.core.Keyword(null,"namespaces","namespaces",-1444157469)),cnn);
var prev = cljs.core.get.cljs$core$IFn$_invoke$arity$2(the_current_ns,var_name);
var prev__$1 = (((!(sci.impl.vars.var_QMARK_(prev))))?sci.impl.vars.__GT_SciVar(prev,cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(cnn),cljs.core.str.cljs$core$IFn$_invoke$arity$1(var_name)),cljs.core.meta(prev),false):prev);
var v = (cljs.core.truth_((sci.impl.utils.kw_identical_QMARK_.cljs$core$IFn$_invoke$arity$2 ? sci.impl.utils.kw_identical_QMARK_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("sci.impl","var.unbound","sci.impl/var.unbound",-1824207647),init__$1) : sci.impl.utils.kw_identical_QMARK_.call(null, new cljs.core.Keyword("sci.impl","var.unbound","sci.impl/var.unbound",-1824207647),init__$1)))?(function (){var G__22476 = prev__$1;
cljs.core.alter_meta_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__22476,cljs.core.merge,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([m__$2], 0));

return G__22476;
})():(function (){
sci.impl.vars.bindRoot(prev__$1,init__$1);

cljs.core.alter_meta_BANG_.cljs$core$IFn$_invoke$arity$variadic(prev__$1,cljs.core.merge,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([m__$2], 0));

return prev__$1;
})()
);
var the_current_ns__$1 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(the_current_ns,var_name,v);
return cljs.core.assoc_in(env,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"namespaces","namespaces",-1444157469),cnn], null),the_current_ns__$1);
});
var env = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"env","env",-1815813235).cljs$core$IFn$_invoke$arity$1(ctx),assoc_in_env);
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.get.cljs$core$IFn$_invoke$arity$2(env,new cljs.core.Keyword(null,"namespaces","namespaces",-1444157469)),cnn),var_name);
});
sci.impl.evaluator.eval_case = (function sci$impl$evaluator$eval_case(var_args){
var G__22487 = arguments.length;
switch (G__22487) {
case 4:
return sci.impl.evaluator.eval_case.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return sci.impl.evaluator.eval_case.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sci.impl.evaluator.eval_case.cljs$core$IFn$_invoke$arity$4 = (function (ctx,bindings,case_map,case_val){
var v = (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,case_val) : sci.impl.evaluator.eval.call(null, ctx,bindings,case_val));
var temp__5802__auto__ = cljs.core.find(case_map,v);
if(cljs.core.truth_(temp__5802__auto__)){
var vec__22494 = temp__5802__auto__;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__22494,(0),null);
var found = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__22494,(1),null);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,found) : sci.impl.evaluator.eval.call(null, ctx,bindings,found));
} else {
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)].join('')));
}
}));

(sci.impl.evaluator.eval_case.cljs$core$IFn$_invoke$arity$5 = (function (ctx,bindings,case_map,case_val,case_default){
var v = (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,case_val) : sci.impl.evaluator.eval.call(null, ctx,bindings,case_val));
var temp__5802__auto__ = cljs.core.find(case_map,v);
if(cljs.core.truth_(temp__5802__auto__)){
var vec__22498 = temp__5802__auto__;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__22498,(0),null);
var found = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__22498,(1),null);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,found) : sci.impl.evaluator.eval.call(null, ctx,bindings,found));
} else {
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,case_default) : sci.impl.evaluator.eval.call(null, ctx,bindings,case_default));
}
}));

(sci.impl.evaluator.eval_case.cljs$lang$maxFixedArity = 5);

sci.impl.evaluator.eval_try = (function sci$impl$evaluator$eval_try(ctx,bindings,body,catches,finally$){
try{var _STAR_in_try_STAR__orig_val__22511 = sci.impl.utils._STAR_in_try_STAR_;
var _STAR_in_try_STAR__temp_val__22512 = true;
(sci.impl.utils._STAR_in_try_STAR_ = _STAR_in_try_STAR__temp_val__22512);

try{return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,body) : sci.impl.evaluator.eval.call(null, ctx,bindings,body));
}finally {(sci.impl.utils._STAR_in_try_STAR_ = _STAR_in_try_STAR__orig_val__22511);
}}catch (e22503){if((e22503 instanceof Error)){
var e = e22503;
var temp__5802__auto__ = cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (_,c){
var clazz = new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(c);
if(cljs.core.truth_((function (){var or__5002__auto__ = (sci.impl.utils.kw_identical_QMARK_.cljs$core$IFn$_invoke$arity$2 ? sci.impl.utils.kw_identical_QMARK_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"default","default",-1987822328),clazz) : sci.impl.utils.kw_identical_QMARK_.call(null, new cljs.core.Keyword(null,"default","default",-1987822328),clazz));
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
if((clazz instanceof sci.impl.types.EvalFn)){
var c__5035__auto__ = (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,clazz) : sci.impl.evaluator.eval.call(null, ctx,bindings,clazz));
var x__5036__auto__ = e;
return (x__5036__auto__ instanceof c__5035__auto__);
} else {
return (e instanceof clazz);
}
}
})())){
return cljs.core.reduced(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("sci.impl.evaluator","try-result","sci.impl.evaluator/try-result",-1394897780),(function (){var G__22505 = ctx;
var G__22506 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(bindings,new cljs.core.Keyword(null,"binding","binding",539932593).cljs$core$IFn$_invoke$arity$1(c),e);
var G__22507 = new cljs.core.Keyword(null,"body","body",-2049205669).cljs$core$IFn$_invoke$arity$1(c);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22505,G__22506,G__22507) : sci.impl.evaluator.eval.call(null, G__22505,G__22506,G__22507));
})()], null));
} else {
return null;
}
}),null,catches);
if(cljs.core.truth_(temp__5802__auto__)){
var vec__22508 = temp__5802__auto__;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__22508,(0),null);
var r = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__22508,(1),null);
return r;
} else {
return sci.impl.utils.rethrow_with_location_of_node.cljs$core$IFn$_invoke$arity$4(ctx,bindings,e,body);
}
} else {
throw e22503;

}
}finally {(sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,finally$) : sci.impl.evaluator.eval.call(null, ctx,bindings,finally$));
}});
sci.impl.evaluator.eval_static_method_invocation = (function sci$impl$evaluator$eval_static_method_invocation(ctx,bindings,expr){
return sci.impl.interop.invoke_static_method(cljs.core.first(expr),cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__22525_SHARP_){
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,p1__22525_SHARP_) : sci.impl.evaluator.eval.call(null, ctx,bindings,p1__22525_SHARP_));
}),cljs.core.rest(expr)));
});
sci.impl.evaluator.eval_instance_method_invocation = (function sci$impl$evaluator$eval_instance_method_invocation(ctx,bindings,instance_expr,method_str,field_access,args,allowed){
var instance_meta = cljs.core.meta(instance_expr);
var tag_class = new cljs.core.Keyword(null,"tag-class","tag-class",714967874).cljs$core$IFn$_invoke$arity$1(instance_meta);
var instance_expr_STAR_ = (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,instance_expr) : sci.impl.evaluator.eval.call(null, ctx,bindings,instance_expr));
if(cljs.core.truth_((function (){var and__5000__auto__ = cljs.core.map_QMARK_(instance_expr_STAR_);
if(and__5000__auto__){
return new cljs.core.Keyword("sci.impl","record","sci.impl/record",-1939193950).cljs$core$IFn$_invoke$arity$1(cljs.core.meta(instance_expr_STAR_));
} else {
return and__5000__auto__;
}
})())){
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(instance_expr_STAR_,cljs.core.keyword.cljs$core$IFn$_invoke$arity$1(method_str));
} else {
var instance_class = (function (){var or__5002__auto__ = tag_class;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return cljs.core.type(instance_expr_STAR_);
}
})();
var class__GT_opts = new cljs.core.Keyword(null,"class->opts","class->opts",2061906477).cljs$core$IFn$_invoke$arity$1(ctx);
var allowed_QMARK_ = (function (){var or__5002__auto__ = allowed;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
var or__5002__auto____$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(class__GT_opts,new cljs.core.Keyword(null,"allow","allow",-1857325745));
if(cljs.core.truth_(or__5002__auto____$1)){
return or__5002__auto____$1;
} else {
var or__5002__auto____$2 = (function (){var instance_class_name = instance_class.name;
var instance_class_symbol = cljs.core.symbol.cljs$core$IFn$_invoke$arity$1(instance_class_name);
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(class__GT_opts,instance_class_symbol);
})();
if(cljs.core.truth_(or__5002__auto____$2)){
return or__5002__auto____$2;
} else {
return console.log(cljs.core.str.cljs$core$IFn$_invoke$arity$1(method_str));
}
}
}
})();
var target_class = (cljs.core.truth_(allowed_QMARK_)?instance_class:(function (){var temp__5804__auto__ = new cljs.core.Keyword(null,"public-class","public-class",1127293019).cljs$core$IFn$_invoke$arity$1(ctx);
if(cljs.core.truth_(temp__5804__auto__)){
var f = temp__5804__auto__;
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(instance_expr_STAR_) : f.call(null, instance_expr_STAR_));
} else {
return null;
}
})());
if(cljs.core.truth_(allowed_QMARK_)){
} else {
sci.impl.utils.throw_error_with_location.cljs$core$IFn$_invoke$arity$2(["Method ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(method_str)," on ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(instance_class)," not allowed!"].join(''),instance_expr);
}

if(cljs.core.truth_(field_access)){
return sci.impl.interop.invoke_instance_field(instance_expr_STAR_,target_class,method_str);
} else {
var args__$1 = cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__22527_SHARP_){
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,p1__22527_SHARP_) : sci.impl.evaluator.eval.call(null, ctx,bindings,p1__22527_SHARP_));
}),args);
return sci.impl.interop.invoke_instance_method(instance_expr_STAR_,target_class,method_str,args__$1);
}
}
});
sci.impl.evaluator.eval_resolve = (function sci$impl$evaluator$eval_resolve(var_args){
var G__22545 = arguments.length;
switch (G__22545) {
case 3:
return sci.impl.evaluator.eval_resolve.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return sci.impl.evaluator.eval_resolve.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sci.impl.evaluator.eval_resolve.cljs$core$IFn$_invoke$arity$3 = (function (ctx,bindings,sym){
return sci.impl.evaluator.eval_resolve.cljs$core$IFn$_invoke$arity$4(ctx,bindings,null,sym);
}));

(sci.impl.evaluator.eval_resolve.cljs$core$IFn$_invoke$arity$4 = (function (ctx,bindings,env,sym){
if(((cljs.core.not(env)) || ((!(cljs.core.contains_QMARK_(env,sym)))))){
var sym__$1 = (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,sym) : sci.impl.evaluator.eval.call(null, ctx,bindings,sym));
var res = cljs.core.second((function (){var fexpr__22562 = cljs.core.deref(sci.impl.utils.lookup);
return (fexpr__22562.cljs$core$IFn$_invoke$arity$3 ? fexpr__22562.cljs$core$IFn$_invoke$arity$3(ctx,sym__$1,false) : fexpr__22562.call(null, ctx,sym__$1,false));
})());
if((res instanceof sci.impl.types.EvalFn)){
return null;
} else {
return res;
}
} else {
return null;
}
}));

(sci.impl.evaluator.eval_resolve.cljs$lang$maxFixedArity = 4);

cljs.core.vreset_BANG_(sci.impl.utils.eval_resolve_state,sci.impl.evaluator.eval_resolve);
sci.impl.evaluator.eval_import = (function sci$impl$evaluator$eval_import(var_args){
var args__5732__auto__ = [];
var len__5726__auto___23487 = arguments.length;
var i__5727__auto___23488 = (0);
while(true){
if((i__5727__auto___23488 < len__5726__auto___23487)){
args__5732__auto__.push((arguments[i__5727__auto___23488]));

var G__23489 = (i__5727__auto___23488 + (1));
i__5727__auto___23488 = G__23489;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((1) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((1)),(0),null)):null);
return sci.impl.evaluator.eval_import.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__5733__auto__);
});

(sci.impl.evaluator.eval_import.cljs$core$IFn$_invoke$arity$variadic = (function (ctx,import_symbols_or_lists){
var specs = cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__22563_SHARP_){
if(((cljs.core.seq_QMARK_(p1__22563_SHARP_)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first(p1__22563_SHARP_))))){
return cljs.core.second(p1__22563_SHARP_);
} else {
return p1__22563_SHARP_;
}
}),import_symbols_or_lists);
var env = new cljs.core.Keyword(null,"env","env",-1815813235).cljs$core$IFn$_invoke$arity$1(ctx);
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (_,spec){
var vec__22579 = (((spec instanceof cljs.core.Symbol))?(function (){var s = cljs.core.str.cljs$core$IFn$_invoke$arity$1(spec);
var last_dot = clojure.string.last_index_of.cljs$core$IFn$_invoke$arity$2(s,".");
var package_PLUS_class_name = (cljs.core.truth_(last_dot)?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.symbol.cljs$core$IFn$_invoke$arity$1(cljs.core.subs.cljs$core$IFn$_invoke$arity$3(s,(0),last_dot)),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.symbol.cljs$core$IFn$_invoke$arity$1(cljs.core.subs.cljs$core$IFn$_invoke$arity$3(s,(last_dot + (1)),((s).length)))], null)], null):new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec], null)], null));
return package_PLUS_class_name;
})():(function (){var p = cljs.core.first(spec);
var cs = cljs.core.rest(spec);
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p,cs], null);
})());
var package$ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__22579,(0),null);
var classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__22579,(1),null);
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (___$1,class$){
var fq_class_name = cljs.core.symbol.cljs$core$IFn$_invoke$arity$1((cljs.core.truth_(package$)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(package$),".",cljs.core.str.cljs$core$IFn$_invoke$arity$1(class$)].join(''):class$));
var temp__5802__auto__ = sci.impl.interop.resolve_class(ctx,fq_class_name);
if(cljs.core.truth_(temp__5802__auto__)){
var clazz = temp__5802__auto__;
var cnn = sci.impl.vars.current_ns_name();
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(env,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"namespaces","namespaces",-1444157469),cnn,new cljs.core.Keyword(null,"imports","imports",-1249933394),class$], null),fq_class_name);

return clazz;
} else {
var temp__5802__auto____$1 = sci.impl.records.resolve_record_or_protocol_class.cljs$core$IFn$_invoke$arity$3(ctx,package$,class$);
if(cljs.core.truth_(temp__5802__auto____$1)){
var rec = temp__5802__auto____$1;
var cnn = sci.impl.vars.current_ns_name();
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(env,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"namespaces","namespaces",-1444157469),cnn,class$], null),rec);

return rec;
} else {
throw (new Error(["Unable to resolve classname: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fq_class_name)].join('')));
}
}
}),null,classes);
}),null,specs);
}));

(sci.impl.evaluator.eval_import.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(sci.impl.evaluator.eval_import.cljs$lang$applyTo = (function (seq22565){
var G__22566 = cljs.core.first(seq22565);
var seq22565__$1 = cljs.core.next(seq22565);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__22566,seq22565__$1);
}));

/**
 * Note: various arities of do have already been unrolled in the analyzer.
 */
sci.impl.evaluator.eval_do = (function sci$impl$evaluator$eval_do(ctx,bindings,exprs){
var exprs__$1 = cljs.core.seq(exprs);
var exprs__$2 = exprs__$1;
while(true){
if(exprs__$2){
var ret = (function (){var G__22596 = ctx;
var G__22597 = bindings;
var G__22598 = cljs.core.first(exprs__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22596,G__22597,G__22598) : sci.impl.evaluator.eval.call(null, G__22596,G__22597,G__22598));
})();
var temp__5802__auto__ = cljs.core.next(exprs__$2);
if(temp__5802__auto__){
var exprs__$3 = temp__5802__auto__;
var G__23495 = exprs__$3;
exprs__$2 = G__23495;
continue;
} else {
return ret;
}
} else {
return null;
}
break;
}
});
cljs.core.vreset_BANG_(sci.impl.utils.eval_do_STAR_,sci.impl.evaluator.eval_do);
sci.impl.evaluator.fn_call = (function sci$impl$evaluator$fn_call(ctx,bindings,f,args){
var G__22809 = cljs.core.count(args);
switch (G__22809) {
case (0):
return (f.cljs$core$IFn$_invoke$arity$0 ? f.cljs$core$IFn$_invoke$arity$0() : f.call(null, ));

break;
case (1):
var arg22617 = (function (){var G__22813 = ctx;
var G__22814 = bindings;
var G__22815 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22813,G__22814,G__22815) : sci.impl.evaluator.eval.call(null, G__22813,G__22814,G__22815));
})();
var args__$1 = cljs.core.rest(args);
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(arg22617) : f.call(null, arg22617));

break;
case (2):
var arg22618 = (function (){var G__22816 = ctx;
var G__22817 = bindings;
var G__22818 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22816,G__22817,G__22818) : sci.impl.evaluator.eval.call(null, G__22816,G__22817,G__22818));
})();
var args__$1 = cljs.core.rest(args);
var arg22619 = (function (){var G__22819 = ctx;
var G__22820 = bindings;
var G__22821 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22819,G__22820,G__22821) : sci.impl.evaluator.eval.call(null, G__22819,G__22820,G__22821));
})();
var args__$2 = cljs.core.rest(args__$1);
return (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(arg22618,arg22619) : f.call(null, arg22618,arg22619));

break;
case (3):
var arg22620 = (function (){var G__22822 = ctx;
var G__22823 = bindings;
var G__22824 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22822,G__22823,G__22824) : sci.impl.evaluator.eval.call(null, G__22822,G__22823,G__22824));
})();
var args__$1 = cljs.core.rest(args);
var arg22621 = (function (){var G__22825 = ctx;
var G__22826 = bindings;
var G__22827 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22825,G__22826,G__22827) : sci.impl.evaluator.eval.call(null, G__22825,G__22826,G__22827));
})();
var args__$2 = cljs.core.rest(args__$1);
var arg22622 = (function (){var G__22828 = ctx;
var G__22829 = bindings;
var G__22830 = cljs.core.first(args__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22828,G__22829,G__22830) : sci.impl.evaluator.eval.call(null, G__22828,G__22829,G__22830));
})();
var args__$3 = cljs.core.rest(args__$2);
return (f.cljs$core$IFn$_invoke$arity$3 ? f.cljs$core$IFn$_invoke$arity$3(arg22620,arg22621,arg22622) : f.call(null, arg22620,arg22621,arg22622));

break;
case (4):
var arg22623 = (function (){var G__22831 = ctx;
var G__22832 = bindings;
var G__22833 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22831,G__22832,G__22833) : sci.impl.evaluator.eval.call(null, G__22831,G__22832,G__22833));
})();
var args__$1 = cljs.core.rest(args);
var arg22624 = (function (){var G__22834 = ctx;
var G__22835 = bindings;
var G__22836 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22834,G__22835,G__22836) : sci.impl.evaluator.eval.call(null, G__22834,G__22835,G__22836));
})();
var args__$2 = cljs.core.rest(args__$1);
var arg22625 = (function (){var G__22837 = ctx;
var G__22838 = bindings;
var G__22839 = cljs.core.first(args__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22837,G__22838,G__22839) : sci.impl.evaluator.eval.call(null, G__22837,G__22838,G__22839));
})();
var args__$3 = cljs.core.rest(args__$2);
var arg22626 = (function (){var G__22840 = ctx;
var G__22841 = bindings;
var G__22842 = cljs.core.first(args__$3);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22840,G__22841,G__22842) : sci.impl.evaluator.eval.call(null, G__22840,G__22841,G__22842));
})();
var args__$4 = cljs.core.rest(args__$3);
return (f.cljs$core$IFn$_invoke$arity$4 ? f.cljs$core$IFn$_invoke$arity$4(arg22623,arg22624,arg22625,arg22626) : f.call(null, arg22623,arg22624,arg22625,arg22626));

break;
case (5):
var arg22627 = (function (){var G__22843 = ctx;
var G__22844 = bindings;
var G__22845 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22843,G__22844,G__22845) : sci.impl.evaluator.eval.call(null, G__22843,G__22844,G__22845));
})();
var args__$1 = cljs.core.rest(args);
var arg22628 = (function (){var G__22846 = ctx;
var G__22847 = bindings;
var G__22848 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22846,G__22847,G__22848) : sci.impl.evaluator.eval.call(null, G__22846,G__22847,G__22848));
})();
var args__$2 = cljs.core.rest(args__$1);
var arg22629 = (function (){var G__22849 = ctx;
var G__22850 = bindings;
var G__22851 = cljs.core.first(args__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22849,G__22850,G__22851) : sci.impl.evaluator.eval.call(null, G__22849,G__22850,G__22851));
})();
var args__$3 = cljs.core.rest(args__$2);
var arg22630 = (function (){var G__22852 = ctx;
var G__22853 = bindings;
var G__22854 = cljs.core.first(args__$3);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22852,G__22853,G__22854) : sci.impl.evaluator.eval.call(null, G__22852,G__22853,G__22854));
})();
var args__$4 = cljs.core.rest(args__$3);
var arg22631 = (function (){var G__22856 = ctx;
var G__22857 = bindings;
var G__22858 = cljs.core.first(args__$4);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22856,G__22857,G__22858) : sci.impl.evaluator.eval.call(null, G__22856,G__22857,G__22858));
})();
var args__$5 = cljs.core.rest(args__$4);
return (f.cljs$core$IFn$_invoke$arity$5 ? f.cljs$core$IFn$_invoke$arity$5(arg22627,arg22628,arg22629,arg22630,arg22631) : f.call(null, arg22627,arg22628,arg22629,arg22630,arg22631));

break;
case (6):
var arg22632 = (function (){var G__22859 = ctx;
var G__22860 = bindings;
var G__22861 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22859,G__22860,G__22861) : sci.impl.evaluator.eval.call(null, G__22859,G__22860,G__22861));
})();
var args__$1 = cljs.core.rest(args);
var arg22633 = (function (){var G__22862 = ctx;
var G__22863 = bindings;
var G__22864 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22862,G__22863,G__22864) : sci.impl.evaluator.eval.call(null, G__22862,G__22863,G__22864));
})();
var args__$2 = cljs.core.rest(args__$1);
var arg22634 = (function (){var G__22865 = ctx;
var G__22866 = bindings;
var G__22867 = cljs.core.first(args__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22865,G__22866,G__22867) : sci.impl.evaluator.eval.call(null, G__22865,G__22866,G__22867));
})();
var args__$3 = cljs.core.rest(args__$2);
var arg22635 = (function (){var G__22868 = ctx;
var G__22869 = bindings;
var G__22870 = cljs.core.first(args__$3);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22868,G__22869,G__22870) : sci.impl.evaluator.eval.call(null, G__22868,G__22869,G__22870));
})();
var args__$4 = cljs.core.rest(args__$3);
var arg22636 = (function (){var G__22872 = ctx;
var G__22873 = bindings;
var G__22874 = cljs.core.first(args__$4);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22872,G__22873,G__22874) : sci.impl.evaluator.eval.call(null, G__22872,G__22873,G__22874));
})();
var args__$5 = cljs.core.rest(args__$4);
var arg22637 = (function (){var G__22875 = ctx;
var G__22876 = bindings;
var G__22877 = cljs.core.first(args__$5);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22875,G__22876,G__22877) : sci.impl.evaluator.eval.call(null, G__22875,G__22876,G__22877));
})();
var args__$6 = cljs.core.rest(args__$5);
return (f.cljs$core$IFn$_invoke$arity$6 ? f.cljs$core$IFn$_invoke$arity$6(arg22632,arg22633,arg22634,arg22635,arg22636,arg22637) : f.call(null, arg22632,arg22633,arg22634,arg22635,arg22636,arg22637));

break;
case (7):
var arg22638 = (function (){var G__22878 = ctx;
var G__22879 = bindings;
var G__22880 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22878,G__22879,G__22880) : sci.impl.evaluator.eval.call(null, G__22878,G__22879,G__22880));
})();
var args__$1 = cljs.core.rest(args);
var arg22639 = (function (){var G__22881 = ctx;
var G__22882 = bindings;
var G__22883 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22881,G__22882,G__22883) : sci.impl.evaluator.eval.call(null, G__22881,G__22882,G__22883));
})();
var args__$2 = cljs.core.rest(args__$1);
var arg22640 = (function (){var G__22884 = ctx;
var G__22885 = bindings;
var G__22886 = cljs.core.first(args__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22884,G__22885,G__22886) : sci.impl.evaluator.eval.call(null, G__22884,G__22885,G__22886));
})();
var args__$3 = cljs.core.rest(args__$2);
var arg22641 = (function (){var G__22887 = ctx;
var G__22888 = bindings;
var G__22889 = cljs.core.first(args__$3);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22887,G__22888,G__22889) : sci.impl.evaluator.eval.call(null, G__22887,G__22888,G__22889));
})();
var args__$4 = cljs.core.rest(args__$3);
var arg22642 = (function (){var G__22890 = ctx;
var G__22891 = bindings;
var G__22892 = cljs.core.first(args__$4);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22890,G__22891,G__22892) : sci.impl.evaluator.eval.call(null, G__22890,G__22891,G__22892));
})();
var args__$5 = cljs.core.rest(args__$4);
var arg22643 = (function (){var G__22894 = ctx;
var G__22895 = bindings;
var G__22896 = cljs.core.first(args__$5);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22894,G__22895,G__22896) : sci.impl.evaluator.eval.call(null, G__22894,G__22895,G__22896));
})();
var args__$6 = cljs.core.rest(args__$5);
var arg22644 = (function (){var G__22899 = ctx;
var G__22900 = bindings;
var G__22901 = cljs.core.first(args__$6);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22899,G__22900,G__22901) : sci.impl.evaluator.eval.call(null, G__22899,G__22900,G__22901));
})();
var args__$7 = cljs.core.rest(args__$6);
return (f.cljs$core$IFn$_invoke$arity$7 ? f.cljs$core$IFn$_invoke$arity$7(arg22638,arg22639,arg22640,arg22641,arg22642,arg22643,arg22644) : f.call(null, arg22638,arg22639,arg22640,arg22641,arg22642,arg22643,arg22644));

break;
case (8):
var arg22645 = (function (){var G__22902 = ctx;
var G__22903 = bindings;
var G__22904 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22902,G__22903,G__22904) : sci.impl.evaluator.eval.call(null, G__22902,G__22903,G__22904));
})();
var args__$1 = cljs.core.rest(args);
var arg22646 = (function (){var G__22905 = ctx;
var G__22906 = bindings;
var G__22907 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22905,G__22906,G__22907) : sci.impl.evaluator.eval.call(null, G__22905,G__22906,G__22907));
})();
var args__$2 = cljs.core.rest(args__$1);
var arg22647 = (function (){var G__22908 = ctx;
var G__22909 = bindings;
var G__22910 = cljs.core.first(args__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22908,G__22909,G__22910) : sci.impl.evaluator.eval.call(null, G__22908,G__22909,G__22910));
})();
var args__$3 = cljs.core.rest(args__$2);
var arg22648 = (function (){var G__22911 = ctx;
var G__22912 = bindings;
var G__22913 = cljs.core.first(args__$3);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22911,G__22912,G__22913) : sci.impl.evaluator.eval.call(null, G__22911,G__22912,G__22913));
})();
var args__$4 = cljs.core.rest(args__$3);
var arg22649 = (function (){var G__22917 = ctx;
var G__22918 = bindings;
var G__22919 = cljs.core.first(args__$4);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22917,G__22918,G__22919) : sci.impl.evaluator.eval.call(null, G__22917,G__22918,G__22919));
})();
var args__$5 = cljs.core.rest(args__$4);
var arg22650 = (function (){var G__22920 = ctx;
var G__22921 = bindings;
var G__22922 = cljs.core.first(args__$5);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22920,G__22921,G__22922) : sci.impl.evaluator.eval.call(null, G__22920,G__22921,G__22922));
})();
var args__$6 = cljs.core.rest(args__$5);
var arg22651 = (function (){var G__22923 = ctx;
var G__22924 = bindings;
var G__22925 = cljs.core.first(args__$6);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22923,G__22924,G__22925) : sci.impl.evaluator.eval.call(null, G__22923,G__22924,G__22925));
})();
var args__$7 = cljs.core.rest(args__$6);
var arg22652 = (function (){var G__22926 = ctx;
var G__22927 = bindings;
var G__22928 = cljs.core.first(args__$7);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22926,G__22927,G__22928) : sci.impl.evaluator.eval.call(null, G__22926,G__22927,G__22928));
})();
var args__$8 = cljs.core.rest(args__$7);
return (f.cljs$core$IFn$_invoke$arity$8 ? f.cljs$core$IFn$_invoke$arity$8(arg22645,arg22646,arg22647,arg22648,arg22649,arg22650,arg22651,arg22652) : f.call(null, arg22645,arg22646,arg22647,arg22648,arg22649,arg22650,arg22651,arg22652));

break;
case (9):
var arg22653 = (function (){var G__22931 = ctx;
var G__22932 = bindings;
var G__22933 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22931,G__22932,G__22933) : sci.impl.evaluator.eval.call(null, G__22931,G__22932,G__22933));
})();
var args__$1 = cljs.core.rest(args);
var arg22654 = (function (){var G__22935 = ctx;
var G__22936 = bindings;
var G__22937 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22935,G__22936,G__22937) : sci.impl.evaluator.eval.call(null, G__22935,G__22936,G__22937));
})();
var args__$2 = cljs.core.rest(args__$1);
var arg22655 = (function (){var G__22938 = ctx;
var G__22939 = bindings;
var G__22940 = cljs.core.first(args__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22938,G__22939,G__22940) : sci.impl.evaluator.eval.call(null, G__22938,G__22939,G__22940));
})();
var args__$3 = cljs.core.rest(args__$2);
var arg22656 = (function (){var G__22941 = ctx;
var G__22942 = bindings;
var G__22943 = cljs.core.first(args__$3);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22941,G__22942,G__22943) : sci.impl.evaluator.eval.call(null, G__22941,G__22942,G__22943));
})();
var args__$4 = cljs.core.rest(args__$3);
var arg22657 = (function (){var G__22944 = ctx;
var G__22945 = bindings;
var G__22946 = cljs.core.first(args__$4);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22944,G__22945,G__22946) : sci.impl.evaluator.eval.call(null, G__22944,G__22945,G__22946));
})();
var args__$5 = cljs.core.rest(args__$4);
var arg22658 = (function (){var G__22947 = ctx;
var G__22948 = bindings;
var G__22949 = cljs.core.first(args__$5);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22947,G__22948,G__22949) : sci.impl.evaluator.eval.call(null, G__22947,G__22948,G__22949));
})();
var args__$6 = cljs.core.rest(args__$5);
var arg22659 = (function (){var G__22951 = ctx;
var G__22952 = bindings;
var G__22953 = cljs.core.first(args__$6);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22951,G__22952,G__22953) : sci.impl.evaluator.eval.call(null, G__22951,G__22952,G__22953));
})();
var args__$7 = cljs.core.rest(args__$6);
var arg22660 = (function (){var G__22956 = ctx;
var G__22957 = bindings;
var G__22958 = cljs.core.first(args__$7);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22956,G__22957,G__22958) : sci.impl.evaluator.eval.call(null, G__22956,G__22957,G__22958));
})();
var args__$8 = cljs.core.rest(args__$7);
var arg22661 = (function (){var G__22959 = ctx;
var G__22960 = bindings;
var G__22961 = cljs.core.first(args__$8);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22959,G__22960,G__22961) : sci.impl.evaluator.eval.call(null, G__22959,G__22960,G__22961));
})();
var args__$9 = cljs.core.rest(args__$8);
return (f.cljs$core$IFn$_invoke$arity$9 ? f.cljs$core$IFn$_invoke$arity$9(arg22653,arg22654,arg22655,arg22656,arg22657,arg22658,arg22659,arg22660,arg22661) : f.call(null, arg22653,arg22654,arg22655,arg22656,arg22657,arg22658,arg22659,arg22660,arg22661));

break;
case (10):
var arg22662 = (function (){var G__22962 = ctx;
var G__22963 = bindings;
var G__22964 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22962,G__22963,G__22964) : sci.impl.evaluator.eval.call(null, G__22962,G__22963,G__22964));
})();
var args__$1 = cljs.core.rest(args);
var arg22663 = (function (){var G__22965 = ctx;
var G__22966 = bindings;
var G__22967 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22965,G__22966,G__22967) : sci.impl.evaluator.eval.call(null, G__22965,G__22966,G__22967));
})();
var args__$2 = cljs.core.rest(args__$1);
var arg22664 = (function (){var G__22971 = ctx;
var G__22972 = bindings;
var G__22973 = cljs.core.first(args__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22971,G__22972,G__22973) : sci.impl.evaluator.eval.call(null, G__22971,G__22972,G__22973));
})();
var args__$3 = cljs.core.rest(args__$2);
var arg22665 = (function (){var G__22974 = ctx;
var G__22975 = bindings;
var G__22976 = cljs.core.first(args__$3);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22974,G__22975,G__22976) : sci.impl.evaluator.eval.call(null, G__22974,G__22975,G__22976));
})();
var args__$4 = cljs.core.rest(args__$3);
var arg22666 = (function (){var G__22977 = ctx;
var G__22978 = bindings;
var G__22979 = cljs.core.first(args__$4);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22977,G__22978,G__22979) : sci.impl.evaluator.eval.call(null, G__22977,G__22978,G__22979));
})();
var args__$5 = cljs.core.rest(args__$4);
var arg22667 = (function (){var G__22980 = ctx;
var G__22981 = bindings;
var G__22982 = cljs.core.first(args__$5);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22980,G__22981,G__22982) : sci.impl.evaluator.eval.call(null, G__22980,G__22981,G__22982));
})();
var args__$6 = cljs.core.rest(args__$5);
var arg22668 = (function (){var G__22983 = ctx;
var G__22984 = bindings;
var G__22985 = cljs.core.first(args__$6);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22983,G__22984,G__22985) : sci.impl.evaluator.eval.call(null, G__22983,G__22984,G__22985));
})();
var args__$7 = cljs.core.rest(args__$6);
var arg22669 = (function (){var G__22988 = ctx;
var G__22989 = bindings;
var G__22990 = cljs.core.first(args__$7);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22988,G__22989,G__22990) : sci.impl.evaluator.eval.call(null, G__22988,G__22989,G__22990));
})();
var args__$8 = cljs.core.rest(args__$7);
var arg22670 = (function (){var G__22992 = ctx;
var G__22993 = bindings;
var G__22994 = cljs.core.first(args__$8);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22992,G__22993,G__22994) : sci.impl.evaluator.eval.call(null, G__22992,G__22993,G__22994));
})();
var args__$9 = cljs.core.rest(args__$8);
var arg22671 = (function (){var G__22995 = ctx;
var G__22996 = bindings;
var G__22997 = cljs.core.first(args__$9);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22995,G__22996,G__22997) : sci.impl.evaluator.eval.call(null, G__22995,G__22996,G__22997));
})();
var args__$10 = cljs.core.rest(args__$9);
return (f.cljs$core$IFn$_invoke$arity$10 ? f.cljs$core$IFn$_invoke$arity$10(arg22662,arg22663,arg22664,arg22665,arg22666,arg22667,arg22668,arg22669,arg22670,arg22671) : f.call(null, arg22662,arg22663,arg22664,arg22665,arg22666,arg22667,arg22668,arg22669,arg22670,arg22671));

break;
case (11):
var arg22672 = (function (){var G__22999 = ctx;
var G__23000 = bindings;
var G__23001 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__22999,G__23000,G__23001) : sci.impl.evaluator.eval.call(null, G__22999,G__23000,G__23001));
})();
var args__$1 = cljs.core.rest(args);
var arg22673 = (function (){var G__23004 = ctx;
var G__23005 = bindings;
var G__23006 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23004,G__23005,G__23006) : sci.impl.evaluator.eval.call(null, G__23004,G__23005,G__23006));
})();
var args__$2 = cljs.core.rest(args__$1);
var arg22674 = (function (){var G__23007 = ctx;
var G__23008 = bindings;
var G__23009 = cljs.core.first(args__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23007,G__23008,G__23009) : sci.impl.evaluator.eval.call(null, G__23007,G__23008,G__23009));
})();
var args__$3 = cljs.core.rest(args__$2);
var arg22675 = (function (){var G__23010 = ctx;
var G__23011 = bindings;
var G__23012 = cljs.core.first(args__$3);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23010,G__23011,G__23012) : sci.impl.evaluator.eval.call(null, G__23010,G__23011,G__23012));
})();
var args__$4 = cljs.core.rest(args__$3);
var arg22676 = (function (){var G__23013 = ctx;
var G__23014 = bindings;
var G__23015 = cljs.core.first(args__$4);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23013,G__23014,G__23015) : sci.impl.evaluator.eval.call(null, G__23013,G__23014,G__23015));
})();
var args__$5 = cljs.core.rest(args__$4);
var arg22677 = (function (){var G__23017 = ctx;
var G__23018 = bindings;
var G__23019 = cljs.core.first(args__$5);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23017,G__23018,G__23019) : sci.impl.evaluator.eval.call(null, G__23017,G__23018,G__23019));
})();
var args__$6 = cljs.core.rest(args__$5);
var arg22678 = (function (){var G__23022 = ctx;
var G__23023 = bindings;
var G__23024 = cljs.core.first(args__$6);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23022,G__23023,G__23024) : sci.impl.evaluator.eval.call(null, G__23022,G__23023,G__23024));
})();
var args__$7 = cljs.core.rest(args__$6);
var arg22679 = (function (){var G__23025 = ctx;
var G__23026 = bindings;
var G__23027 = cljs.core.first(args__$7);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23025,G__23026,G__23027) : sci.impl.evaluator.eval.call(null, G__23025,G__23026,G__23027));
})();
var args__$8 = cljs.core.rest(args__$7);
var arg22680 = (function (){var G__23028 = ctx;
var G__23029 = bindings;
var G__23030 = cljs.core.first(args__$8);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23028,G__23029,G__23030) : sci.impl.evaluator.eval.call(null, G__23028,G__23029,G__23030));
})();
var args__$9 = cljs.core.rest(args__$8);
var arg22681 = (function (){var G__23032 = ctx;
var G__23033 = bindings;
var G__23034 = cljs.core.first(args__$9);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23032,G__23033,G__23034) : sci.impl.evaluator.eval.call(null, G__23032,G__23033,G__23034));
})();
var args__$10 = cljs.core.rest(args__$9);
var arg22682 = (function (){var G__23037 = ctx;
var G__23038 = bindings;
var G__23039 = cljs.core.first(args__$10);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23037,G__23038,G__23039) : sci.impl.evaluator.eval.call(null, G__23037,G__23038,G__23039));
})();
var args__$11 = cljs.core.rest(args__$10);
return (f.cljs$core$IFn$_invoke$arity$11 ? f.cljs$core$IFn$_invoke$arity$11(arg22672,arg22673,arg22674,arg22675,arg22676,arg22677,arg22678,arg22679,arg22680,arg22681,arg22682) : f.call(null, arg22672,arg22673,arg22674,arg22675,arg22676,arg22677,arg22678,arg22679,arg22680,arg22681,arg22682));

break;
case (12):
var arg22683 = (function (){var G__23040 = ctx;
var G__23041 = bindings;
var G__23042 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23040,G__23041,G__23042) : sci.impl.evaluator.eval.call(null, G__23040,G__23041,G__23042));
})();
var args__$1 = cljs.core.rest(args);
var arg22684 = (function (){var G__23043 = ctx;
var G__23044 = bindings;
var G__23045 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23043,G__23044,G__23045) : sci.impl.evaluator.eval.call(null, G__23043,G__23044,G__23045));
})();
var args__$2 = cljs.core.rest(args__$1);
var arg22685 = (function (){var G__23046 = ctx;
var G__23047 = bindings;
var G__23048 = cljs.core.first(args__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23046,G__23047,G__23048) : sci.impl.evaluator.eval.call(null, G__23046,G__23047,G__23048));
})();
var args__$3 = cljs.core.rest(args__$2);
var arg22686 = (function (){var G__23049 = ctx;
var G__23050 = bindings;
var G__23051 = cljs.core.first(args__$3);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23049,G__23050,G__23051) : sci.impl.evaluator.eval.call(null, G__23049,G__23050,G__23051));
})();
var args__$4 = cljs.core.rest(args__$3);
var arg22687 = (function (){var G__23052 = ctx;
var G__23053 = bindings;
var G__23054 = cljs.core.first(args__$4);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23052,G__23053,G__23054) : sci.impl.evaluator.eval.call(null, G__23052,G__23053,G__23054));
})();
var args__$5 = cljs.core.rest(args__$4);
var arg22688 = (function (){var G__23055 = ctx;
var G__23056 = bindings;
var G__23057 = cljs.core.first(args__$5);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23055,G__23056,G__23057) : sci.impl.evaluator.eval.call(null, G__23055,G__23056,G__23057));
})();
var args__$6 = cljs.core.rest(args__$5);
var arg22689 = (function (){var G__23058 = ctx;
var G__23059 = bindings;
var G__23060 = cljs.core.first(args__$6);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23058,G__23059,G__23060) : sci.impl.evaluator.eval.call(null, G__23058,G__23059,G__23060));
})();
var args__$7 = cljs.core.rest(args__$6);
var arg22690 = (function (){var G__23061 = ctx;
var G__23062 = bindings;
var G__23063 = cljs.core.first(args__$7);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23061,G__23062,G__23063) : sci.impl.evaluator.eval.call(null, G__23061,G__23062,G__23063));
})();
var args__$8 = cljs.core.rest(args__$7);
var arg22691 = (function (){var G__23064 = ctx;
var G__23065 = bindings;
var G__23066 = cljs.core.first(args__$8);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23064,G__23065,G__23066) : sci.impl.evaluator.eval.call(null, G__23064,G__23065,G__23066));
})();
var args__$9 = cljs.core.rest(args__$8);
var arg22692 = (function (){var G__23067 = ctx;
var G__23068 = bindings;
var G__23069 = cljs.core.first(args__$9);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23067,G__23068,G__23069) : sci.impl.evaluator.eval.call(null, G__23067,G__23068,G__23069));
})();
var args__$10 = cljs.core.rest(args__$9);
var arg22693 = (function (){var G__23070 = ctx;
var G__23071 = bindings;
var G__23072 = cljs.core.first(args__$10);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23070,G__23071,G__23072) : sci.impl.evaluator.eval.call(null, G__23070,G__23071,G__23072));
})();
var args__$11 = cljs.core.rest(args__$10);
var arg22694 = (function (){var G__23073 = ctx;
var G__23074 = bindings;
var G__23075 = cljs.core.first(args__$11);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23073,G__23074,G__23075) : sci.impl.evaluator.eval.call(null, G__23073,G__23074,G__23075));
})();
var args__$12 = cljs.core.rest(args__$11);
return (f.cljs$core$IFn$_invoke$arity$12 ? f.cljs$core$IFn$_invoke$arity$12(arg22683,arg22684,arg22685,arg22686,arg22687,arg22688,arg22689,arg22690,arg22691,arg22692,arg22693,arg22694) : f.call(null, arg22683,arg22684,arg22685,arg22686,arg22687,arg22688,arg22689,arg22690,arg22691,arg22692,arg22693,arg22694));

break;
case (13):
var arg22695 = (function (){var G__23076 = ctx;
var G__23077 = bindings;
var G__23078 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23076,G__23077,G__23078) : sci.impl.evaluator.eval.call(null, G__23076,G__23077,G__23078));
})();
var args__$1 = cljs.core.rest(args);
var arg22696 = (function (){var G__23079 = ctx;
var G__23080 = bindings;
var G__23081 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23079,G__23080,G__23081) : sci.impl.evaluator.eval.call(null, G__23079,G__23080,G__23081));
})();
var args__$2 = cljs.core.rest(args__$1);
var arg22697 = (function (){var G__23083 = ctx;
var G__23084 = bindings;
var G__23085 = cljs.core.first(args__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23083,G__23084,G__23085) : sci.impl.evaluator.eval.call(null, G__23083,G__23084,G__23085));
})();
var args__$3 = cljs.core.rest(args__$2);
var arg22698 = (function (){var G__23088 = ctx;
var G__23089 = bindings;
var G__23090 = cljs.core.first(args__$3);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23088,G__23089,G__23090) : sci.impl.evaluator.eval.call(null, G__23088,G__23089,G__23090));
})();
var args__$4 = cljs.core.rest(args__$3);
var arg22699 = (function (){var G__23092 = ctx;
var G__23093 = bindings;
var G__23094 = cljs.core.first(args__$4);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23092,G__23093,G__23094) : sci.impl.evaluator.eval.call(null, G__23092,G__23093,G__23094));
})();
var args__$5 = cljs.core.rest(args__$4);
var arg22700 = (function (){var G__23096 = ctx;
var G__23097 = bindings;
var G__23098 = cljs.core.first(args__$5);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23096,G__23097,G__23098) : sci.impl.evaluator.eval.call(null, G__23096,G__23097,G__23098));
})();
var args__$6 = cljs.core.rest(args__$5);
var arg22701 = (function (){var G__23100 = ctx;
var G__23101 = bindings;
var G__23102 = cljs.core.first(args__$6);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23100,G__23101,G__23102) : sci.impl.evaluator.eval.call(null, G__23100,G__23101,G__23102));
})();
var args__$7 = cljs.core.rest(args__$6);
var arg22702 = (function (){var G__23103 = ctx;
var G__23104 = bindings;
var G__23105 = cljs.core.first(args__$7);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23103,G__23104,G__23105) : sci.impl.evaluator.eval.call(null, G__23103,G__23104,G__23105));
})();
var args__$8 = cljs.core.rest(args__$7);
var arg22703 = (function (){var G__23107 = ctx;
var G__23108 = bindings;
var G__23109 = cljs.core.first(args__$8);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23107,G__23108,G__23109) : sci.impl.evaluator.eval.call(null, G__23107,G__23108,G__23109));
})();
var args__$9 = cljs.core.rest(args__$8);
var arg22704 = (function (){var G__23110 = ctx;
var G__23112 = bindings;
var G__23113 = cljs.core.first(args__$9);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23110,G__23112,G__23113) : sci.impl.evaluator.eval.call(null, G__23110,G__23112,G__23113));
})();
var args__$10 = cljs.core.rest(args__$9);
var arg22705 = (function (){var G__23114 = ctx;
var G__23115 = bindings;
var G__23116 = cljs.core.first(args__$10);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23114,G__23115,G__23116) : sci.impl.evaluator.eval.call(null, G__23114,G__23115,G__23116));
})();
var args__$11 = cljs.core.rest(args__$10);
var arg22706 = (function (){var G__23121 = ctx;
var G__23122 = bindings;
var G__23123 = cljs.core.first(args__$11);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23121,G__23122,G__23123) : sci.impl.evaluator.eval.call(null, G__23121,G__23122,G__23123));
})();
var args__$12 = cljs.core.rest(args__$11);
var arg22707 = (function (){var G__23124 = ctx;
var G__23125 = bindings;
var G__23126 = cljs.core.first(args__$12);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23124,G__23125,G__23126) : sci.impl.evaluator.eval.call(null, G__23124,G__23125,G__23126));
})();
var args__$13 = cljs.core.rest(args__$12);
return (f.cljs$core$IFn$_invoke$arity$13 ? f.cljs$core$IFn$_invoke$arity$13(arg22695,arg22696,arg22697,arg22698,arg22699,arg22700,arg22701,arg22702,arg22703,arg22704,arg22705,arg22706,arg22707) : f.call(null, arg22695,arg22696,arg22697,arg22698,arg22699,arg22700,arg22701,arg22702,arg22703,arg22704,arg22705,arg22706,arg22707));

break;
case (14):
var arg22708 = (function (){var G__23132 = ctx;
var G__23133 = bindings;
var G__23134 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23132,G__23133,G__23134) : sci.impl.evaluator.eval.call(null, G__23132,G__23133,G__23134));
})();
var args__$1 = cljs.core.rest(args);
var arg22709 = (function (){var G__23135 = ctx;
var G__23136 = bindings;
var G__23137 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23135,G__23136,G__23137) : sci.impl.evaluator.eval.call(null, G__23135,G__23136,G__23137));
})();
var args__$2 = cljs.core.rest(args__$1);
var arg22710 = (function (){var G__23138 = ctx;
var G__23139 = bindings;
var G__23140 = cljs.core.first(args__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23138,G__23139,G__23140) : sci.impl.evaluator.eval.call(null, G__23138,G__23139,G__23140));
})();
var args__$3 = cljs.core.rest(args__$2);
var arg22711 = (function (){var G__23141 = ctx;
var G__23142 = bindings;
var G__23143 = cljs.core.first(args__$3);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23141,G__23142,G__23143) : sci.impl.evaluator.eval.call(null, G__23141,G__23142,G__23143));
})();
var args__$4 = cljs.core.rest(args__$3);
var arg22712 = (function (){var G__23144 = ctx;
var G__23145 = bindings;
var G__23146 = cljs.core.first(args__$4);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23144,G__23145,G__23146) : sci.impl.evaluator.eval.call(null, G__23144,G__23145,G__23146));
})();
var args__$5 = cljs.core.rest(args__$4);
var arg22713 = (function (){var G__23148 = ctx;
var G__23149 = bindings;
var G__23150 = cljs.core.first(args__$5);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23148,G__23149,G__23150) : sci.impl.evaluator.eval.call(null, G__23148,G__23149,G__23150));
})();
var args__$6 = cljs.core.rest(args__$5);
var arg22714 = (function (){var G__23151 = ctx;
var G__23152 = bindings;
var G__23153 = cljs.core.first(args__$6);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23151,G__23152,G__23153) : sci.impl.evaluator.eval.call(null, G__23151,G__23152,G__23153));
})();
var args__$7 = cljs.core.rest(args__$6);
var arg22715 = (function (){var G__23155 = ctx;
var G__23156 = bindings;
var G__23157 = cljs.core.first(args__$7);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23155,G__23156,G__23157) : sci.impl.evaluator.eval.call(null, G__23155,G__23156,G__23157));
})();
var args__$8 = cljs.core.rest(args__$7);
var arg22716 = (function (){var G__23158 = ctx;
var G__23159 = bindings;
var G__23160 = cljs.core.first(args__$8);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23158,G__23159,G__23160) : sci.impl.evaluator.eval.call(null, G__23158,G__23159,G__23160));
})();
var args__$9 = cljs.core.rest(args__$8);
var arg22717 = (function (){var G__23161 = ctx;
var G__23162 = bindings;
var G__23163 = cljs.core.first(args__$9);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23161,G__23162,G__23163) : sci.impl.evaluator.eval.call(null, G__23161,G__23162,G__23163));
})();
var args__$10 = cljs.core.rest(args__$9);
var arg22718 = (function (){var G__23164 = ctx;
var G__23165 = bindings;
var G__23166 = cljs.core.first(args__$10);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23164,G__23165,G__23166) : sci.impl.evaluator.eval.call(null, G__23164,G__23165,G__23166));
})();
var args__$11 = cljs.core.rest(args__$10);
var arg22719 = (function (){var G__23170 = ctx;
var G__23171 = bindings;
var G__23172 = cljs.core.first(args__$11);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23170,G__23171,G__23172) : sci.impl.evaluator.eval.call(null, G__23170,G__23171,G__23172));
})();
var args__$12 = cljs.core.rest(args__$11);
var arg22720 = (function (){var G__23173 = ctx;
var G__23174 = bindings;
var G__23175 = cljs.core.first(args__$12);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23173,G__23174,G__23175) : sci.impl.evaluator.eval.call(null, G__23173,G__23174,G__23175));
})();
var args__$13 = cljs.core.rest(args__$12);
var arg22721 = (function (){var G__23176 = ctx;
var G__23177 = bindings;
var G__23178 = cljs.core.first(args__$13);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23176,G__23177,G__23178) : sci.impl.evaluator.eval.call(null, G__23176,G__23177,G__23178));
})();
var args__$14 = cljs.core.rest(args__$13);
return (f.cljs$core$IFn$_invoke$arity$14 ? f.cljs$core$IFn$_invoke$arity$14(arg22708,arg22709,arg22710,arg22711,arg22712,arg22713,arg22714,arg22715,arg22716,arg22717,arg22718,arg22719,arg22720,arg22721) : f.call(null, arg22708,arg22709,arg22710,arg22711,arg22712,arg22713,arg22714,arg22715,arg22716,arg22717,arg22718,arg22719,arg22720,arg22721));

break;
case (15):
var arg22722 = (function (){var G__23179 = ctx;
var G__23180 = bindings;
var G__23181 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23179,G__23180,G__23181) : sci.impl.evaluator.eval.call(null, G__23179,G__23180,G__23181));
})();
var args__$1 = cljs.core.rest(args);
var arg22723 = (function (){var G__23182 = ctx;
var G__23183 = bindings;
var G__23184 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23182,G__23183,G__23184) : sci.impl.evaluator.eval.call(null, G__23182,G__23183,G__23184));
})();
var args__$2 = cljs.core.rest(args__$1);
var arg22724 = (function (){var G__23185 = ctx;
var G__23186 = bindings;
var G__23187 = cljs.core.first(args__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23185,G__23186,G__23187) : sci.impl.evaluator.eval.call(null, G__23185,G__23186,G__23187));
})();
var args__$3 = cljs.core.rest(args__$2);
var arg22725 = (function (){var G__23188 = ctx;
var G__23189 = bindings;
var G__23190 = cljs.core.first(args__$3);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23188,G__23189,G__23190) : sci.impl.evaluator.eval.call(null, G__23188,G__23189,G__23190));
})();
var args__$4 = cljs.core.rest(args__$3);
var arg22726 = (function (){var G__23191 = ctx;
var G__23192 = bindings;
var G__23193 = cljs.core.first(args__$4);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23191,G__23192,G__23193) : sci.impl.evaluator.eval.call(null, G__23191,G__23192,G__23193));
})();
var args__$5 = cljs.core.rest(args__$4);
var arg22727 = (function (){var G__23194 = ctx;
var G__23195 = bindings;
var G__23196 = cljs.core.first(args__$5);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23194,G__23195,G__23196) : sci.impl.evaluator.eval.call(null, G__23194,G__23195,G__23196));
})();
var args__$6 = cljs.core.rest(args__$5);
var arg22728 = (function (){var G__23197 = ctx;
var G__23198 = bindings;
var G__23199 = cljs.core.first(args__$6);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23197,G__23198,G__23199) : sci.impl.evaluator.eval.call(null, G__23197,G__23198,G__23199));
})();
var args__$7 = cljs.core.rest(args__$6);
var arg22729 = (function (){var G__23200 = ctx;
var G__23201 = bindings;
var G__23202 = cljs.core.first(args__$7);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23200,G__23201,G__23202) : sci.impl.evaluator.eval.call(null, G__23200,G__23201,G__23202));
})();
var args__$8 = cljs.core.rest(args__$7);
var arg22730 = (function (){var G__23203 = ctx;
var G__23204 = bindings;
var G__23205 = cljs.core.first(args__$8);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23203,G__23204,G__23205) : sci.impl.evaluator.eval.call(null, G__23203,G__23204,G__23205));
})();
var args__$9 = cljs.core.rest(args__$8);
var arg22731 = (function (){var G__23206 = ctx;
var G__23207 = bindings;
var G__23208 = cljs.core.first(args__$9);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23206,G__23207,G__23208) : sci.impl.evaluator.eval.call(null, G__23206,G__23207,G__23208));
})();
var args__$10 = cljs.core.rest(args__$9);
var arg22732 = (function (){var G__23209 = ctx;
var G__23210 = bindings;
var G__23211 = cljs.core.first(args__$10);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23209,G__23210,G__23211) : sci.impl.evaluator.eval.call(null, G__23209,G__23210,G__23211));
})();
var args__$11 = cljs.core.rest(args__$10);
var arg22733 = (function (){var G__23212 = ctx;
var G__23213 = bindings;
var G__23214 = cljs.core.first(args__$11);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23212,G__23213,G__23214) : sci.impl.evaluator.eval.call(null, G__23212,G__23213,G__23214));
})();
var args__$12 = cljs.core.rest(args__$11);
var arg22734 = (function (){var G__23215 = ctx;
var G__23216 = bindings;
var G__23217 = cljs.core.first(args__$12);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23215,G__23216,G__23217) : sci.impl.evaluator.eval.call(null, G__23215,G__23216,G__23217));
})();
var args__$13 = cljs.core.rest(args__$12);
var arg22735 = (function (){var G__23218 = ctx;
var G__23219 = bindings;
var G__23220 = cljs.core.first(args__$13);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23218,G__23219,G__23220) : sci.impl.evaluator.eval.call(null, G__23218,G__23219,G__23220));
})();
var args__$14 = cljs.core.rest(args__$13);
var arg22736 = (function (){var G__23221 = ctx;
var G__23222 = bindings;
var G__23223 = cljs.core.first(args__$14);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23221,G__23222,G__23223) : sci.impl.evaluator.eval.call(null, G__23221,G__23222,G__23223));
})();
var args__$15 = cljs.core.rest(args__$14);
return (f.cljs$core$IFn$_invoke$arity$15 ? f.cljs$core$IFn$_invoke$arity$15(arg22722,arg22723,arg22724,arg22725,arg22726,arg22727,arg22728,arg22729,arg22730,arg22731,arg22732,arg22733,arg22734,arg22735,arg22736) : f.call(null, arg22722,arg22723,arg22724,arg22725,arg22726,arg22727,arg22728,arg22729,arg22730,arg22731,arg22732,arg22733,arg22734,arg22735,arg22736));

break;
case (16):
var arg22737 = (function (){var G__23224 = ctx;
var G__23225 = bindings;
var G__23226 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23224,G__23225,G__23226) : sci.impl.evaluator.eval.call(null, G__23224,G__23225,G__23226));
})();
var args__$1 = cljs.core.rest(args);
var arg22738 = (function (){var G__23227 = ctx;
var G__23228 = bindings;
var G__23229 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23227,G__23228,G__23229) : sci.impl.evaluator.eval.call(null, G__23227,G__23228,G__23229));
})();
var args__$2 = cljs.core.rest(args__$1);
var arg22739 = (function (){var G__23230 = ctx;
var G__23231 = bindings;
var G__23232 = cljs.core.first(args__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23230,G__23231,G__23232) : sci.impl.evaluator.eval.call(null, G__23230,G__23231,G__23232));
})();
var args__$3 = cljs.core.rest(args__$2);
var arg22740 = (function (){var G__23233 = ctx;
var G__23234 = bindings;
var G__23235 = cljs.core.first(args__$3);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23233,G__23234,G__23235) : sci.impl.evaluator.eval.call(null, G__23233,G__23234,G__23235));
})();
var args__$4 = cljs.core.rest(args__$3);
var arg22741 = (function (){var G__23236 = ctx;
var G__23237 = bindings;
var G__23238 = cljs.core.first(args__$4);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23236,G__23237,G__23238) : sci.impl.evaluator.eval.call(null, G__23236,G__23237,G__23238));
})();
var args__$5 = cljs.core.rest(args__$4);
var arg22742 = (function (){var G__23240 = ctx;
var G__23241 = bindings;
var G__23242 = cljs.core.first(args__$5);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23240,G__23241,G__23242) : sci.impl.evaluator.eval.call(null, G__23240,G__23241,G__23242));
})();
var args__$6 = cljs.core.rest(args__$5);
var arg22743 = (function (){var G__23247 = ctx;
var G__23248 = bindings;
var G__23249 = cljs.core.first(args__$6);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23247,G__23248,G__23249) : sci.impl.evaluator.eval.call(null, G__23247,G__23248,G__23249));
})();
var args__$7 = cljs.core.rest(args__$6);
var arg22744 = (function (){var G__23250 = ctx;
var G__23251 = bindings;
var G__23252 = cljs.core.first(args__$7);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23250,G__23251,G__23252) : sci.impl.evaluator.eval.call(null, G__23250,G__23251,G__23252));
})();
var args__$8 = cljs.core.rest(args__$7);
var arg22745 = (function (){var G__23253 = ctx;
var G__23254 = bindings;
var G__23255 = cljs.core.first(args__$8);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23253,G__23254,G__23255) : sci.impl.evaluator.eval.call(null, G__23253,G__23254,G__23255));
})();
var args__$9 = cljs.core.rest(args__$8);
var arg22746 = (function (){var G__23256 = ctx;
var G__23257 = bindings;
var G__23258 = cljs.core.first(args__$9);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23256,G__23257,G__23258) : sci.impl.evaluator.eval.call(null, G__23256,G__23257,G__23258));
})();
var args__$10 = cljs.core.rest(args__$9);
var arg22747 = (function (){var G__23260 = ctx;
var G__23261 = bindings;
var G__23262 = cljs.core.first(args__$10);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23260,G__23261,G__23262) : sci.impl.evaluator.eval.call(null, G__23260,G__23261,G__23262));
})();
var args__$11 = cljs.core.rest(args__$10);
var arg22748 = (function (){var G__23263 = ctx;
var G__23264 = bindings;
var G__23265 = cljs.core.first(args__$11);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23263,G__23264,G__23265) : sci.impl.evaluator.eval.call(null, G__23263,G__23264,G__23265));
})();
var args__$12 = cljs.core.rest(args__$11);
var arg22749 = (function (){var G__23266 = ctx;
var G__23267 = bindings;
var G__23268 = cljs.core.first(args__$12);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23266,G__23267,G__23268) : sci.impl.evaluator.eval.call(null, G__23266,G__23267,G__23268));
})();
var args__$13 = cljs.core.rest(args__$12);
var arg22750 = (function (){var G__23269 = ctx;
var G__23270 = bindings;
var G__23271 = cljs.core.first(args__$13);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23269,G__23270,G__23271) : sci.impl.evaluator.eval.call(null, G__23269,G__23270,G__23271));
})();
var args__$14 = cljs.core.rest(args__$13);
var arg22751 = (function (){var G__23272 = ctx;
var G__23273 = bindings;
var G__23274 = cljs.core.first(args__$14);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23272,G__23273,G__23274) : sci.impl.evaluator.eval.call(null, G__23272,G__23273,G__23274));
})();
var args__$15 = cljs.core.rest(args__$14);
var arg22752 = (function (){var G__23275 = ctx;
var G__23276 = bindings;
var G__23277 = cljs.core.first(args__$15);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23275,G__23276,G__23277) : sci.impl.evaluator.eval.call(null, G__23275,G__23276,G__23277));
})();
var args__$16 = cljs.core.rest(args__$15);
return (f.cljs$core$IFn$_invoke$arity$16 ? f.cljs$core$IFn$_invoke$arity$16(arg22737,arg22738,arg22739,arg22740,arg22741,arg22742,arg22743,arg22744,arg22745,arg22746,arg22747,arg22748,arg22749,arg22750,arg22751,arg22752) : f.call(null, arg22737,arg22738,arg22739,arg22740,arg22741,arg22742,arg22743,arg22744,arg22745,arg22746,arg22747,arg22748,arg22749,arg22750,arg22751,arg22752));

break;
case (17):
var arg22753 = (function (){var G__23278 = ctx;
var G__23279 = bindings;
var G__23280 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23278,G__23279,G__23280) : sci.impl.evaluator.eval.call(null, G__23278,G__23279,G__23280));
})();
var args__$1 = cljs.core.rest(args);
var arg22754 = (function (){var G__23281 = ctx;
var G__23282 = bindings;
var G__23283 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23281,G__23282,G__23283) : sci.impl.evaluator.eval.call(null, G__23281,G__23282,G__23283));
})();
var args__$2 = cljs.core.rest(args__$1);
var arg22755 = (function (){var G__23284 = ctx;
var G__23285 = bindings;
var G__23286 = cljs.core.first(args__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23284,G__23285,G__23286) : sci.impl.evaluator.eval.call(null, G__23284,G__23285,G__23286));
})();
var args__$3 = cljs.core.rest(args__$2);
var arg22756 = (function (){var G__23287 = ctx;
var G__23288 = bindings;
var G__23289 = cljs.core.first(args__$3);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23287,G__23288,G__23289) : sci.impl.evaluator.eval.call(null, G__23287,G__23288,G__23289));
})();
var args__$4 = cljs.core.rest(args__$3);
var arg22757 = (function (){var G__23293 = ctx;
var G__23294 = bindings;
var G__23295 = cljs.core.first(args__$4);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23293,G__23294,G__23295) : sci.impl.evaluator.eval.call(null, G__23293,G__23294,G__23295));
})();
var args__$5 = cljs.core.rest(args__$4);
var arg22758 = (function (){var G__23296 = ctx;
var G__23297 = bindings;
var G__23298 = cljs.core.first(args__$5);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23296,G__23297,G__23298) : sci.impl.evaluator.eval.call(null, G__23296,G__23297,G__23298));
})();
var args__$6 = cljs.core.rest(args__$5);
var arg22759 = (function (){var G__23299 = ctx;
var G__23300 = bindings;
var G__23301 = cljs.core.first(args__$6);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23299,G__23300,G__23301) : sci.impl.evaluator.eval.call(null, G__23299,G__23300,G__23301));
})();
var args__$7 = cljs.core.rest(args__$6);
var arg22760 = (function (){var G__23302 = ctx;
var G__23303 = bindings;
var G__23304 = cljs.core.first(args__$7);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23302,G__23303,G__23304) : sci.impl.evaluator.eval.call(null, G__23302,G__23303,G__23304));
})();
var args__$8 = cljs.core.rest(args__$7);
var arg22761 = (function (){var G__23305 = ctx;
var G__23306 = bindings;
var G__23307 = cljs.core.first(args__$8);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23305,G__23306,G__23307) : sci.impl.evaluator.eval.call(null, G__23305,G__23306,G__23307));
})();
var args__$9 = cljs.core.rest(args__$8);
var arg22762 = (function (){var G__23310 = ctx;
var G__23311 = bindings;
var G__23312 = cljs.core.first(args__$9);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23310,G__23311,G__23312) : sci.impl.evaluator.eval.call(null, G__23310,G__23311,G__23312));
})();
var args__$10 = cljs.core.rest(args__$9);
var arg22763 = (function (){var G__23314 = ctx;
var G__23315 = bindings;
var G__23316 = cljs.core.first(args__$10);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23314,G__23315,G__23316) : sci.impl.evaluator.eval.call(null, G__23314,G__23315,G__23316));
})();
var args__$11 = cljs.core.rest(args__$10);
var arg22764 = (function (){var G__23317 = ctx;
var G__23318 = bindings;
var G__23319 = cljs.core.first(args__$11);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23317,G__23318,G__23319) : sci.impl.evaluator.eval.call(null, G__23317,G__23318,G__23319));
})();
var args__$12 = cljs.core.rest(args__$11);
var arg22765 = (function (){var G__23320 = ctx;
var G__23321 = bindings;
var G__23322 = cljs.core.first(args__$12);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23320,G__23321,G__23322) : sci.impl.evaluator.eval.call(null, G__23320,G__23321,G__23322));
})();
var args__$13 = cljs.core.rest(args__$12);
var arg22766 = (function (){var G__23323 = ctx;
var G__23324 = bindings;
var G__23325 = cljs.core.first(args__$13);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23323,G__23324,G__23325) : sci.impl.evaluator.eval.call(null, G__23323,G__23324,G__23325));
})();
var args__$14 = cljs.core.rest(args__$13);
var arg22767 = (function (){var G__23326 = ctx;
var G__23327 = bindings;
var G__23328 = cljs.core.first(args__$14);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23326,G__23327,G__23328) : sci.impl.evaluator.eval.call(null, G__23326,G__23327,G__23328));
})();
var args__$15 = cljs.core.rest(args__$14);
var arg22768 = (function (){var G__23329 = ctx;
var G__23330 = bindings;
var G__23331 = cljs.core.first(args__$15);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23329,G__23330,G__23331) : sci.impl.evaluator.eval.call(null, G__23329,G__23330,G__23331));
})();
var args__$16 = cljs.core.rest(args__$15);
var arg22769 = (function (){var G__23332 = ctx;
var G__23333 = bindings;
var G__23334 = cljs.core.first(args__$16);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23332,G__23333,G__23334) : sci.impl.evaluator.eval.call(null, G__23332,G__23333,G__23334));
})();
var args__$17 = cljs.core.rest(args__$16);
return (f.cljs$core$IFn$_invoke$arity$17 ? f.cljs$core$IFn$_invoke$arity$17(arg22753,arg22754,arg22755,arg22756,arg22757,arg22758,arg22759,arg22760,arg22761,arg22762,arg22763,arg22764,arg22765,arg22766,arg22767,arg22768,arg22769) : f.call(null, arg22753,arg22754,arg22755,arg22756,arg22757,arg22758,arg22759,arg22760,arg22761,arg22762,arg22763,arg22764,arg22765,arg22766,arg22767,arg22768,arg22769));

break;
case (18):
var arg22770 = (function (){var G__23335 = ctx;
var G__23336 = bindings;
var G__23337 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23335,G__23336,G__23337) : sci.impl.evaluator.eval.call(null, G__23335,G__23336,G__23337));
})();
var args__$1 = cljs.core.rest(args);
var arg22771 = (function (){var G__23338 = ctx;
var G__23339 = bindings;
var G__23340 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23338,G__23339,G__23340) : sci.impl.evaluator.eval.call(null, G__23338,G__23339,G__23340));
})();
var args__$2 = cljs.core.rest(args__$1);
var arg22772 = (function (){var G__23341 = ctx;
var G__23342 = bindings;
var G__23343 = cljs.core.first(args__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23341,G__23342,G__23343) : sci.impl.evaluator.eval.call(null, G__23341,G__23342,G__23343));
})();
var args__$3 = cljs.core.rest(args__$2);
var arg22773 = (function (){var G__23344 = ctx;
var G__23345 = bindings;
var G__23346 = cljs.core.first(args__$3);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23344,G__23345,G__23346) : sci.impl.evaluator.eval.call(null, G__23344,G__23345,G__23346));
})();
var args__$4 = cljs.core.rest(args__$3);
var arg22774 = (function (){var G__23347 = ctx;
var G__23348 = bindings;
var G__23349 = cljs.core.first(args__$4);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23347,G__23348,G__23349) : sci.impl.evaluator.eval.call(null, G__23347,G__23348,G__23349));
})();
var args__$5 = cljs.core.rest(args__$4);
var arg22775 = (function (){var G__23350 = ctx;
var G__23351 = bindings;
var G__23352 = cljs.core.first(args__$5);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23350,G__23351,G__23352) : sci.impl.evaluator.eval.call(null, G__23350,G__23351,G__23352));
})();
var args__$6 = cljs.core.rest(args__$5);
var arg22776 = (function (){var G__23353 = ctx;
var G__23354 = bindings;
var G__23355 = cljs.core.first(args__$6);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23353,G__23354,G__23355) : sci.impl.evaluator.eval.call(null, G__23353,G__23354,G__23355));
})();
var args__$7 = cljs.core.rest(args__$6);
var arg22777 = (function (){var G__23356 = ctx;
var G__23357 = bindings;
var G__23358 = cljs.core.first(args__$7);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23356,G__23357,G__23358) : sci.impl.evaluator.eval.call(null, G__23356,G__23357,G__23358));
})();
var args__$8 = cljs.core.rest(args__$7);
var arg22778 = (function (){var G__23359 = ctx;
var G__23360 = bindings;
var G__23361 = cljs.core.first(args__$8);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23359,G__23360,G__23361) : sci.impl.evaluator.eval.call(null, G__23359,G__23360,G__23361));
})();
var args__$9 = cljs.core.rest(args__$8);
var arg22779 = (function (){var G__23362 = ctx;
var G__23363 = bindings;
var G__23364 = cljs.core.first(args__$9);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23362,G__23363,G__23364) : sci.impl.evaluator.eval.call(null, G__23362,G__23363,G__23364));
})();
var args__$10 = cljs.core.rest(args__$9);
var arg22780 = (function (){var G__23368 = ctx;
var G__23369 = bindings;
var G__23370 = cljs.core.first(args__$10);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23368,G__23369,G__23370) : sci.impl.evaluator.eval.call(null, G__23368,G__23369,G__23370));
})();
var args__$11 = cljs.core.rest(args__$10);
var arg22781 = (function (){var G__23371 = ctx;
var G__23372 = bindings;
var G__23373 = cljs.core.first(args__$11);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23371,G__23372,G__23373) : sci.impl.evaluator.eval.call(null, G__23371,G__23372,G__23373));
})();
var args__$12 = cljs.core.rest(args__$11);
var arg22782 = (function (){var G__23374 = ctx;
var G__23375 = bindings;
var G__23376 = cljs.core.first(args__$12);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23374,G__23375,G__23376) : sci.impl.evaluator.eval.call(null, G__23374,G__23375,G__23376));
})();
var args__$13 = cljs.core.rest(args__$12);
var arg22783 = (function (){var G__23377 = ctx;
var G__23378 = bindings;
var G__23379 = cljs.core.first(args__$13);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23377,G__23378,G__23379) : sci.impl.evaluator.eval.call(null, G__23377,G__23378,G__23379));
})();
var args__$14 = cljs.core.rest(args__$13);
var arg22784 = (function (){var G__23380 = ctx;
var G__23381 = bindings;
var G__23382 = cljs.core.first(args__$14);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23380,G__23381,G__23382) : sci.impl.evaluator.eval.call(null, G__23380,G__23381,G__23382));
})();
var args__$15 = cljs.core.rest(args__$14);
var arg22785 = (function (){var G__23383 = ctx;
var G__23384 = bindings;
var G__23385 = cljs.core.first(args__$15);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23383,G__23384,G__23385) : sci.impl.evaluator.eval.call(null, G__23383,G__23384,G__23385));
})();
var args__$16 = cljs.core.rest(args__$15);
var arg22786 = (function (){var G__23386 = ctx;
var G__23387 = bindings;
var G__23388 = cljs.core.first(args__$16);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23386,G__23387,G__23388) : sci.impl.evaluator.eval.call(null, G__23386,G__23387,G__23388));
})();
var args__$17 = cljs.core.rest(args__$16);
var arg22787 = (function (){var G__23389 = ctx;
var G__23390 = bindings;
var G__23391 = cljs.core.first(args__$17);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23389,G__23390,G__23391) : sci.impl.evaluator.eval.call(null, G__23389,G__23390,G__23391));
})();
var args__$18 = cljs.core.rest(args__$17);
return (f.cljs$core$IFn$_invoke$arity$18 ? f.cljs$core$IFn$_invoke$arity$18(arg22770,arg22771,arg22772,arg22773,arg22774,arg22775,arg22776,arg22777,arg22778,arg22779,arg22780,arg22781,arg22782,arg22783,arg22784,arg22785,arg22786,arg22787) : f.call(null, arg22770,arg22771,arg22772,arg22773,arg22774,arg22775,arg22776,arg22777,arg22778,arg22779,arg22780,arg22781,arg22782,arg22783,arg22784,arg22785,arg22786,arg22787));

break;
case (19):
var arg22788 = (function (){var G__23392 = ctx;
var G__23393 = bindings;
var G__23394 = cljs.core.first(args);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23392,G__23393,G__23394) : sci.impl.evaluator.eval.call(null, G__23392,G__23393,G__23394));
})();
var args__$1 = cljs.core.rest(args);
var arg22789 = (function (){var G__23395 = ctx;
var G__23396 = bindings;
var G__23397 = cljs.core.first(args__$1);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23395,G__23396,G__23397) : sci.impl.evaluator.eval.call(null, G__23395,G__23396,G__23397));
})();
var args__$2 = cljs.core.rest(args__$1);
var arg22790 = (function (){var G__23398 = ctx;
var G__23399 = bindings;
var G__23400 = cljs.core.first(args__$2);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23398,G__23399,G__23400) : sci.impl.evaluator.eval.call(null, G__23398,G__23399,G__23400));
})();
var args__$3 = cljs.core.rest(args__$2);
var arg22791 = (function (){var G__23401 = ctx;
var G__23402 = bindings;
var G__23403 = cljs.core.first(args__$3);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23401,G__23402,G__23403) : sci.impl.evaluator.eval.call(null, G__23401,G__23402,G__23403));
})();
var args__$4 = cljs.core.rest(args__$3);
var arg22792 = (function (){var G__23404 = ctx;
var G__23405 = bindings;
var G__23406 = cljs.core.first(args__$4);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23404,G__23405,G__23406) : sci.impl.evaluator.eval.call(null, G__23404,G__23405,G__23406));
})();
var args__$5 = cljs.core.rest(args__$4);
var arg22793 = (function (){var G__23407 = ctx;
var G__23408 = bindings;
var G__23409 = cljs.core.first(args__$5);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23407,G__23408,G__23409) : sci.impl.evaluator.eval.call(null, G__23407,G__23408,G__23409));
})();
var args__$6 = cljs.core.rest(args__$5);
var arg22794 = (function (){var G__23410 = ctx;
var G__23411 = bindings;
var G__23412 = cljs.core.first(args__$6);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23410,G__23411,G__23412) : sci.impl.evaluator.eval.call(null, G__23410,G__23411,G__23412));
})();
var args__$7 = cljs.core.rest(args__$6);
var arg22795 = (function (){var G__23413 = ctx;
var G__23414 = bindings;
var G__23415 = cljs.core.first(args__$7);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23413,G__23414,G__23415) : sci.impl.evaluator.eval.call(null, G__23413,G__23414,G__23415));
})();
var args__$8 = cljs.core.rest(args__$7);
var arg22796 = (function (){var G__23416 = ctx;
var G__23417 = bindings;
var G__23418 = cljs.core.first(args__$8);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23416,G__23417,G__23418) : sci.impl.evaluator.eval.call(null, G__23416,G__23417,G__23418));
})();
var args__$9 = cljs.core.rest(args__$8);
var arg22797 = (function (){var G__23419 = ctx;
var G__23420 = bindings;
var G__23421 = cljs.core.first(args__$9);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23419,G__23420,G__23421) : sci.impl.evaluator.eval.call(null, G__23419,G__23420,G__23421));
})();
var args__$10 = cljs.core.rest(args__$9);
var arg22798 = (function (){var G__23422 = ctx;
var G__23423 = bindings;
var G__23424 = cljs.core.first(args__$10);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23422,G__23423,G__23424) : sci.impl.evaluator.eval.call(null, G__23422,G__23423,G__23424));
})();
var args__$11 = cljs.core.rest(args__$10);
var arg22799 = (function (){var G__23425 = ctx;
var G__23426 = bindings;
var G__23427 = cljs.core.first(args__$11);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23425,G__23426,G__23427) : sci.impl.evaluator.eval.call(null, G__23425,G__23426,G__23427));
})();
var args__$12 = cljs.core.rest(args__$11);
var arg22800 = (function (){var G__23428 = ctx;
var G__23429 = bindings;
var G__23430 = cljs.core.first(args__$12);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23428,G__23429,G__23430) : sci.impl.evaluator.eval.call(null, G__23428,G__23429,G__23430));
})();
var args__$13 = cljs.core.rest(args__$12);
var arg22801 = (function (){var G__23431 = ctx;
var G__23432 = bindings;
var G__23433 = cljs.core.first(args__$13);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23431,G__23432,G__23433) : sci.impl.evaluator.eval.call(null, G__23431,G__23432,G__23433));
})();
var args__$14 = cljs.core.rest(args__$13);
var arg22802 = (function (){var G__23434 = ctx;
var G__23435 = bindings;
var G__23436 = cljs.core.first(args__$14);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23434,G__23435,G__23436) : sci.impl.evaluator.eval.call(null, G__23434,G__23435,G__23436));
})();
var args__$15 = cljs.core.rest(args__$14);
var arg22803 = (function (){var G__23437 = ctx;
var G__23438 = bindings;
var G__23439 = cljs.core.first(args__$15);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23437,G__23438,G__23439) : sci.impl.evaluator.eval.call(null, G__23437,G__23438,G__23439));
})();
var args__$16 = cljs.core.rest(args__$15);
var arg22804 = (function (){var G__23440 = ctx;
var G__23441 = bindings;
var G__23442 = cljs.core.first(args__$16);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23440,G__23441,G__23442) : sci.impl.evaluator.eval.call(null, G__23440,G__23441,G__23442));
})();
var args__$17 = cljs.core.rest(args__$16);
var arg22805 = (function (){var G__23443 = ctx;
var G__23444 = bindings;
var G__23445 = cljs.core.first(args__$17);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23443,G__23444,G__23445) : sci.impl.evaluator.eval.call(null, G__23443,G__23444,G__23445));
})();
var args__$18 = cljs.core.rest(args__$17);
var arg22806 = (function (){var G__23446 = ctx;
var G__23447 = bindings;
var G__23448 = cljs.core.first(args__$18);
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(G__23446,G__23447,G__23448) : sci.impl.evaluator.eval.call(null, G__23446,G__23447,G__23448));
})();
var args__$19 = cljs.core.rest(args__$18);
return (f.cljs$core$IFn$_invoke$arity$19 ? f.cljs$core$IFn$_invoke$arity$19(arg22788,arg22789,arg22790,arg22791,arg22792,arg22793,arg22794,arg22795,arg22796,arg22797,arg22798,arg22799,arg22800,arg22801,arg22802,arg22803,arg22804,arg22805,arg22806) : f.call(null, arg22788,arg22789,arg22790,arg22791,arg22792,arg22793,arg22794,arg22795,arg22796,arg22797,arg22798,arg22799,arg22800,arg22801,arg22802,arg22803,arg22804,arg22805,arg22806));

break;
default:
var args__$1 = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2((function (p1__22183_SHARP_){
return (sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3 ? sci.impl.evaluator.eval.cljs$core$IFn$_invoke$arity$3(ctx,bindings,p1__22183_SHARP_) : sci.impl.evaluator.eval.call(null, ctx,bindings,p1__22183_SHARP_));
}),args);
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,args__$1);

}
});
sci.impl.evaluator.eval = (function sci$impl$evaluator$eval(ctx,bindings,expr){
try{if((expr instanceof sci.impl.types.EvalFn)){
var f = expr.f;
return (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(ctx,bindings) : f.call(null, ctx,bindings));
} else {
if((expr instanceof sci.impl.types.EvalVar)){
var v = expr.v;
return cljs.core._deref(v);
} else {
if((((expr == null))?false:(((!((expr == null))))?(((((expr.cljs$lang$protocol_mask$partition0$ & (1024))) || ((cljs.core.PROTOCOL_SENTINEL === expr.cljs$core$IMap$))))?true:(((!expr.cljs$lang$protocol_mask$partition0$))?cljs.core.native_satisfies_QMARK_(cljs.core.IMap,expr):false)):cljs.core.native_satisfies_QMARK_(cljs.core.IMap,expr)))){
return sci.impl.evaluator.eval_map(ctx,bindings,expr);
} else {
return expr;

}
}
}
}catch (e23449){if((e23449 instanceof Error)){
var e = e23449;
return sci.impl.utils.rethrow_with_location_of_node.cljs$core$IFn$_invoke$arity$4(ctx,bindings,e,expr);
} else {
throw e23449;

}
}});
cljs.core.vreset_BANG_(sci.impl.utils.eval_STAR_,sci.impl.evaluator.eval);

//# sourceMappingURL=sci.impl.evaluator.js.map
