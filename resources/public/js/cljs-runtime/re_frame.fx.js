goog.provide('re_frame.fx');
re_frame.fx.kind = new cljs.core.Keyword(null,"fx","fx",-1237829572);
if(cljs.core.truth_((re_frame.registrar.kinds.cljs$core$IFn$_invoke$arity$1 ? re_frame.registrar.kinds.cljs$core$IFn$_invoke$arity$1(re_frame.fx.kind) : re_frame.registrar.kinds.call(null, re_frame.fx.kind)))){
} else {
throw (new Error("Assert failed: (re-frame.registrar/kinds kind)"));
}
re_frame.fx.reg_fx = (function re_frame$fx$reg_fx(id,handler){
return re_frame.registrar.register_handler(re_frame.fx.kind,id,handler);
});
/**
 * An interceptor whose `:after` actions the contents of `:effects`. As a result,
 *   this interceptor is Domino 3.
 * 
 *   This interceptor is silently added (by reg-event-db etc) to the front of
 *   interceptor chains for all events.
 * 
 *   For each key in `:effects` (a map), it calls the registered `effects handler`
 *   (see `reg-fx` for registration of effect handlers).
 * 
 *   So, if `:effects` was:
 *    {:dispatch  [:hello 42]
 *     :db        {...}
 *     :undo      "set flag"}
 * 
 *   it will call the registered effect handlers for each of the map's keys:
 *   `:dispatch`, `:undo` and `:db`. When calling each handler, provides the map
 *   value for that key - so in the example above the effect handler for :dispatch
 *   will be given one arg `[:hello 42]`.
 * 
 *   You cannot rely on the ordering in which effects are executed, other than that
 *   `:db` is guaranteed to be executed first.
 */
re_frame.fx.do_fx = re_frame.interceptor.__GT_interceptor.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"do-fx","do-fx",1194163050),new cljs.core.Keyword(null,"after","after",594996914),(function re_frame$fx$do_fx_after(context){
if(re_frame.trace.is_trace_enabled_QMARK_()){
var _STAR_current_trace_STAR__orig_val__14802 = re_frame.trace._STAR_current_trace_STAR_;
var _STAR_current_trace_STAR__temp_val__14803 = re_frame.trace.start_trace(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"op-type","op-type",-1636141668),new cljs.core.Keyword("event","do-fx","event/do-fx",1357330452)], null));
(re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR__temp_val__14803);

try{try{var effects = new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context);
var effects_without_db = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(effects,new cljs.core.Keyword(null,"db","db",993250759));
var temp__5804__auto___14943 = new cljs.core.Keyword(null,"db","db",993250759).cljs$core$IFn$_invoke$arity$1(effects);
if(cljs.core.truth_(temp__5804__auto___14943)){
var new_db_14944 = temp__5804__auto___14943;
var fexpr__14809_14945 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,new cljs.core.Keyword(null,"db","db",993250759),false);
(fexpr__14809_14945.cljs$core$IFn$_invoke$arity$1 ? fexpr__14809_14945.cljs$core$IFn$_invoke$arity$1(new_db_14944) : fexpr__14809_14945.call(null, new_db_14944));
} else {
}

var seq__14811 = cljs.core.seq(effects_without_db);
var chunk__14812 = null;
var count__14813 = (0);
var i__14814 = (0);
while(true){
if((i__14814 < count__14813)){
var vec__14835 = chunk__14812.cljs$core$IIndexed$_nth$arity$2(null, i__14814);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__14835,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__14835,(1),null);
var temp__5802__auto___14946 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5802__auto___14946)){
var effect_fn_14947 = temp__5802__auto___14946;
(effect_fn_14947.cljs$core$IFn$_invoke$arity$1 ? effect_fn_14947.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_14947.call(null, effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring.",((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"event","event",301435442),effect_key))?["You may be trying to return a coeffect map from an event-fx handler. ","See https://day8.github.io/re-frame/use-cofx-as-fx/"].join(''):null)], 0));
}


var G__14948 = seq__14811;
var G__14949 = chunk__14812;
var G__14950 = count__14813;
var G__14951 = (i__14814 + (1));
seq__14811 = G__14948;
chunk__14812 = G__14949;
count__14813 = G__14950;
i__14814 = G__14951;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__14811);
if(temp__5804__auto__){
var seq__14811__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__14811__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__14811__$1);
var G__14952 = cljs.core.chunk_rest(seq__14811__$1);
var G__14953 = c__5525__auto__;
var G__14954 = cljs.core.count(c__5525__auto__);
var G__14955 = (0);
seq__14811 = G__14952;
chunk__14812 = G__14953;
count__14813 = G__14954;
i__14814 = G__14955;
continue;
} else {
var vec__14857 = cljs.core.first(seq__14811__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__14857,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__14857,(1),null);
var temp__5802__auto___14956 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5802__auto___14956)){
var effect_fn_14958 = temp__5802__auto___14956;
(effect_fn_14958.cljs$core$IFn$_invoke$arity$1 ? effect_fn_14958.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_14958.call(null, effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring.",((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"event","event",301435442),effect_key))?["You may be trying to return a coeffect map from an event-fx handler. ","See https://day8.github.io/re-frame/use-cofx-as-fx/"].join(''):null)], 0));
}


var G__14959 = cljs.core.next(seq__14811__$1);
var G__14960 = null;
var G__14961 = (0);
var G__14962 = (0);
seq__14811 = G__14959;
chunk__14812 = G__14960;
count__14813 = G__14961;
i__14814 = G__14962;
continue;
}
} else {
return null;
}
}
break;
}
}finally {if(re_frame.trace.is_trace_enabled_QMARK_()){
var end__14299__auto___14963 = re_frame.interop.now();
var duration__14300__auto___14964 = (end__14299__auto___14963 - new cljs.core.Keyword(null,"start","start",-355208981).cljs$core$IFn$_invoke$arity$1(re_frame.trace._STAR_current_trace_STAR_));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(re_frame.trace.traces,cljs.core.conj,cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(re_frame.trace._STAR_current_trace_STAR_,new cljs.core.Keyword(null,"duration","duration",1444101068),duration__14300__auto___14964,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"end","end",-268185958),re_frame.interop.now()], 0)));

re_frame.trace.run_tracing_callbacks_BANG_(end__14299__auto___14963);
} else {
}
}}finally {(re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR__orig_val__14802);
}} else {
var effects = new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context);
var effects_without_db = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(effects,new cljs.core.Keyword(null,"db","db",993250759));
var temp__5804__auto___14965 = new cljs.core.Keyword(null,"db","db",993250759).cljs$core$IFn$_invoke$arity$1(effects);
if(cljs.core.truth_(temp__5804__auto___14965)){
var new_db_14966 = temp__5804__auto___14965;
var fexpr__14873_14967 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,new cljs.core.Keyword(null,"db","db",993250759),false);
(fexpr__14873_14967.cljs$core$IFn$_invoke$arity$1 ? fexpr__14873_14967.cljs$core$IFn$_invoke$arity$1(new_db_14966) : fexpr__14873_14967.call(null, new_db_14966));
} else {
}

var seq__14876 = cljs.core.seq(effects_without_db);
var chunk__14877 = null;
var count__14878 = (0);
var i__14879 = (0);
while(true){
if((i__14879 < count__14878)){
var vec__14888 = chunk__14877.cljs$core$IIndexed$_nth$arity$2(null, i__14879);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__14888,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__14888,(1),null);
var temp__5802__auto___14968 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5802__auto___14968)){
var effect_fn_14969 = temp__5802__auto___14968;
(effect_fn_14969.cljs$core$IFn$_invoke$arity$1 ? effect_fn_14969.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_14969.call(null, effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring.",((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"event","event",301435442),effect_key))?["You may be trying to return a coeffect map from an event-fx handler. ","See https://day8.github.io/re-frame/use-cofx-as-fx/"].join(''):null)], 0));
}


var G__14970 = seq__14876;
var G__14971 = chunk__14877;
var G__14972 = count__14878;
var G__14973 = (i__14879 + (1));
seq__14876 = G__14970;
chunk__14877 = G__14971;
count__14878 = G__14972;
i__14879 = G__14973;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__14876);
if(temp__5804__auto__){
var seq__14876__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__14876__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__14876__$1);
var G__14974 = cljs.core.chunk_rest(seq__14876__$1);
var G__14975 = c__5525__auto__;
var G__14976 = cljs.core.count(c__5525__auto__);
var G__14977 = (0);
seq__14876 = G__14974;
chunk__14877 = G__14975;
count__14878 = G__14976;
i__14879 = G__14977;
continue;
} else {
var vec__14891 = cljs.core.first(seq__14876__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__14891,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__14891,(1),null);
var temp__5802__auto___14979 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5802__auto___14979)){
var effect_fn_14981 = temp__5802__auto___14979;
(effect_fn_14981.cljs$core$IFn$_invoke$arity$1 ? effect_fn_14981.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_14981.call(null, effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring.",((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"event","event",301435442),effect_key))?["You may be trying to return a coeffect map from an event-fx handler. ","See https://day8.github.io/re-frame/use-cofx-as-fx/"].join(''):null)], 0));
}


var G__14982 = cljs.core.next(seq__14876__$1);
var G__14983 = null;
var G__14984 = (0);
var G__14985 = (0);
seq__14876 = G__14982;
chunk__14877 = G__14983;
count__14878 = G__14984;
i__14879 = G__14985;
continue;
}
} else {
return null;
}
}
break;
}
}
})], 0));
re_frame.fx.dispatch_later = (function re_frame$fx$dispatch_later(p__14895){
var map__14896 = p__14895;
var map__14896__$1 = cljs.core.__destructure_map(map__14896);
var effect = map__14896__$1;
var ms = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__14896__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__14896__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if(((cljs.core.empty_QMARK_(dispatch)) || ((!(typeof ms === 'number'))))){
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch-later value:",effect], 0));
} else {
return re_frame.interop.set_timeout_BANG_((function (){
return re_frame.router.dispatch(dispatch);
}),ms);
}
});
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch-later","dispatch-later",291951390),(function (value){
if(cljs.core.map_QMARK_(value)){
return re_frame.fx.dispatch_later(value);
} else {
var seq__14901 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,value));
var chunk__14902 = null;
var count__14903 = (0);
var i__14904 = (0);
while(true){
if((i__14904 < count__14903)){
var effect = chunk__14902.cljs$core$IIndexed$_nth$arity$2(null, i__14904);
re_frame.fx.dispatch_later(effect);


var G__14986 = seq__14901;
var G__14987 = chunk__14902;
var G__14988 = count__14903;
var G__14989 = (i__14904 + (1));
seq__14901 = G__14986;
chunk__14902 = G__14987;
count__14903 = G__14988;
i__14904 = G__14989;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__14901);
if(temp__5804__auto__){
var seq__14901__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__14901__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__14901__$1);
var G__14990 = cljs.core.chunk_rest(seq__14901__$1);
var G__14991 = c__5525__auto__;
var G__14992 = cljs.core.count(c__5525__auto__);
var G__14993 = (0);
seq__14901 = G__14990;
chunk__14902 = G__14991;
count__14903 = G__14992;
i__14904 = G__14993;
continue;
} else {
var effect = cljs.core.first(seq__14901__$1);
re_frame.fx.dispatch_later(effect);


var G__14994 = cljs.core.next(seq__14901__$1);
var G__14995 = null;
var G__14996 = (0);
var G__14997 = (0);
seq__14901 = G__14994;
chunk__14902 = G__14995;
count__14903 = G__14996;
i__14904 = G__14997;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"fx","fx",-1237829572),(function (seq_of_effects){
if((!(cljs.core.sequential_QMARK_(seq_of_effects)))){
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: \":fx\" effect expects a seq, but was given ",cljs.core.type(seq_of_effects)], 0));
} else {
var seq__14906 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,seq_of_effects));
var chunk__14907 = null;
var count__14908 = (0);
var i__14909 = (0);
while(true){
if((i__14909 < count__14908)){
var vec__14918 = chunk__14907.cljs$core$IIndexed$_nth$arity$2(null, i__14909);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__14918,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__14918,(1),null);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"db","db",993250759),effect_key)){
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: \":fx\" effect should not contain a :db effect"], 0));
} else {
}

var temp__5802__auto___14998 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5802__auto___14998)){
var effect_fn_14999 = temp__5802__auto___14998;
(effect_fn_14999.cljs$core$IFn$_invoke$arity$1 ? effect_fn_14999.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_14999.call(null, effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: in \":fx\" effect found ",effect_key," which has no associated handler. Ignoring."], 0));
}


var G__15000 = seq__14906;
var G__15001 = chunk__14907;
var G__15002 = count__14908;
var G__15003 = (i__14909 + (1));
seq__14906 = G__15000;
chunk__14907 = G__15001;
count__14908 = G__15002;
i__14909 = G__15003;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__14906);
if(temp__5804__auto__){
var seq__14906__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__14906__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__14906__$1);
var G__15004 = cljs.core.chunk_rest(seq__14906__$1);
var G__15005 = c__5525__auto__;
var G__15006 = cljs.core.count(c__5525__auto__);
var G__15007 = (0);
seq__14906 = G__15004;
chunk__14907 = G__15005;
count__14908 = G__15006;
i__14909 = G__15007;
continue;
} else {
var vec__14922 = cljs.core.first(seq__14906__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__14922,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__14922,(1),null);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"db","db",993250759),effect_key)){
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: \":fx\" effect should not contain a :db effect"], 0));
} else {
}

var temp__5802__auto___15008 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5802__auto___15008)){
var effect_fn_15009 = temp__5802__auto___15008;
(effect_fn_15009.cljs$core$IFn$_invoke$arity$1 ? effect_fn_15009.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_15009.call(null, effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: in \":fx\" effect found ",effect_key," which has no associated handler. Ignoring."], 0));
}


var G__15010 = cljs.core.next(seq__14906__$1);
var G__15011 = null;
var G__15012 = (0);
var G__15013 = (0);
seq__14906 = G__15010;
chunk__14907 = G__15011;
count__14908 = G__15012;
i__14909 = G__15013;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch","dispatch",1319337009),(function (value){
if((!(cljs.core.vector_QMARK_(value)))){
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch value. Expected a vector, but got:",value], 0));
} else {
return re_frame.router.dispatch(value);
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch-n","dispatch-n",-504469236),(function (value){
if((!(cljs.core.sequential_QMARK_(value)))){
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch-n value. Expected a collection, but got:",value], 0));
} else {
var seq__14925 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,value));
var chunk__14926 = null;
var count__14927 = (0);
var i__14928 = (0);
while(true){
if((i__14928 < count__14927)){
var event = chunk__14926.cljs$core$IIndexed$_nth$arity$2(null, i__14928);
re_frame.router.dispatch(event);


var G__15017 = seq__14925;
var G__15018 = chunk__14926;
var G__15019 = count__14927;
var G__15020 = (i__14928 + (1));
seq__14925 = G__15017;
chunk__14926 = G__15018;
count__14927 = G__15019;
i__14928 = G__15020;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__14925);
if(temp__5804__auto__){
var seq__14925__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__14925__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__14925__$1);
var G__15021 = cljs.core.chunk_rest(seq__14925__$1);
var G__15022 = c__5525__auto__;
var G__15023 = cljs.core.count(c__5525__auto__);
var G__15024 = (0);
seq__14925 = G__15021;
chunk__14926 = G__15022;
count__14927 = G__15023;
i__14928 = G__15024;
continue;
} else {
var event = cljs.core.first(seq__14925__$1);
re_frame.router.dispatch(event);


var G__15025 = cljs.core.next(seq__14925__$1);
var G__15026 = null;
var G__15027 = (0);
var G__15028 = (0);
seq__14925 = G__15025;
chunk__14926 = G__15026;
count__14927 = G__15027;
i__14928 = G__15028;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"deregister-event-handler","deregister-event-handler",-1096518994),(function (value){
var clear_event = cljs.core.partial.cljs$core$IFn$_invoke$arity$2(re_frame.registrar.clear_handlers,re_frame.events.kind);
if(cljs.core.sequential_QMARK_(value)){
var seq__14937 = cljs.core.seq(value);
var chunk__14938 = null;
var count__14939 = (0);
var i__14940 = (0);
while(true){
if((i__14940 < count__14939)){
var event = chunk__14938.cljs$core$IIndexed$_nth$arity$2(null, i__14940);
clear_event(event);


var G__15029 = seq__14937;
var G__15030 = chunk__14938;
var G__15031 = count__14939;
var G__15032 = (i__14940 + (1));
seq__14937 = G__15029;
chunk__14938 = G__15030;
count__14939 = G__15031;
i__14940 = G__15032;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__14937);
if(temp__5804__auto__){
var seq__14937__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__14937__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__14937__$1);
var G__15033 = cljs.core.chunk_rest(seq__14937__$1);
var G__15034 = c__5525__auto__;
var G__15035 = cljs.core.count(c__5525__auto__);
var G__15036 = (0);
seq__14937 = G__15033;
chunk__14938 = G__15034;
count__14939 = G__15035;
i__14940 = G__15036;
continue;
} else {
var event = cljs.core.first(seq__14937__$1);
clear_event(event);


var G__15037 = cljs.core.next(seq__14937__$1);
var G__15038 = null;
var G__15039 = (0);
var G__15040 = (0);
seq__14937 = G__15037;
chunk__14938 = G__15038;
count__14939 = G__15039;
i__14940 = G__15040;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return clear_event(value);
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"db","db",993250759),(function (value){
if((!((cljs.core.deref(re_frame.db.app_db) === value)))){
return cljs.core.reset_BANG_(re_frame.db.app_db,value);
} else {
if(re_frame.trace.is_trace_enabled_QMARK_()){
var _STAR_current_trace_STAR__orig_val__14941 = re_frame.trace._STAR_current_trace_STAR_;
var _STAR_current_trace_STAR__temp_val__14942 = re_frame.trace.start_trace(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"op-type","op-type",-1636141668),new cljs.core.Keyword("reagent","quiescent","reagent/quiescent",-16138681)], null));
(re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR__temp_val__14942);

try{try{return null;
}finally {if(re_frame.trace.is_trace_enabled_QMARK_()){
var end__14299__auto___15041 = re_frame.interop.now();
var duration__14300__auto___15042 = (end__14299__auto___15041 - new cljs.core.Keyword(null,"start","start",-355208981).cljs$core$IFn$_invoke$arity$1(re_frame.trace._STAR_current_trace_STAR_));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(re_frame.trace.traces,cljs.core.conj,cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(re_frame.trace._STAR_current_trace_STAR_,new cljs.core.Keyword(null,"duration","duration",1444101068),duration__14300__auto___15042,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"end","end",-268185958),re_frame.interop.now()], 0)));

re_frame.trace.run_tracing_callbacks_BANG_(end__14299__auto___15041);
} else {
}
}}finally {(re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR__orig_val__14941);
}} else {
return null;
}
}
}));

//# sourceMappingURL=re_frame.fx.js.map
