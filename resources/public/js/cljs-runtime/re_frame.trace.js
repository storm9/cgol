goog.provide('re_frame.trace');
re_frame.trace.id = cljs.core.atom.cljs$core$IFn$_invoke$arity$1((0));
re_frame.trace._STAR_current_trace_STAR_ = null;
re_frame.trace.reset_tracing_BANG_ = (function re_frame$trace$reset_tracing_BANG_(){
return cljs.core.reset_BANG_(re_frame.trace.id,(0));
});
/**
 * @define {boolean}
 */
re_frame.trace.trace_enabled_QMARK_ = goog.define("re_frame.trace.trace_enabled_QMARK_",false);
/**
 * See https://groups.google.com/d/msg/clojurescript/jk43kmYiMhA/IHglVr_TPdgJ for more details
 */
re_frame.trace.is_trace_enabled_QMARK_ = (function re_frame$trace$is_trace_enabled_QMARK_(){
return re_frame.trace.trace_enabled_QMARK_;
});
re_frame.trace.trace_cbs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
if((typeof re_frame !== 'undefined') && (typeof re_frame.trace !== 'undefined') && (typeof re_frame.trace.traces !== 'undefined')){
} else {
re_frame.trace.traces = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentVector.EMPTY);
}
if((typeof re_frame !== 'undefined') && (typeof re_frame.trace !== 'undefined') && (typeof re_frame.trace.next_delivery !== 'undefined')){
} else {
re_frame.trace.next_delivery = cljs.core.atom.cljs$core$IFn$_invoke$arity$1((0));
}
/**
 * Registers a tracing callback function which will receive a collection of one or more traces.
 *   Will replace an existing callback function if it shares the same key.
 */
re_frame.trace.register_trace_cb = (function re_frame$trace$register_trace_cb(key,f){
if(re_frame.trace.trace_enabled_QMARK_){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(re_frame.trace.trace_cbs,cljs.core.assoc,key,f);
} else {
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Tracing is not enabled. Please set {\"re_frame.trace.trace_enabled_QMARK_\" true} in :closure-defines. See: https://github.com/day8/re-frame-10x#installation."], 0));
}
});
re_frame.trace.remove_trace_cb = (function re_frame$trace$remove_trace_cb(key){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(re_frame.trace.trace_cbs,cljs.core.dissoc,key);

return null;
});
re_frame.trace.next_id = (function re_frame$trace$next_id(){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(re_frame.trace.id,cljs.core.inc);
});
re_frame.trace.start_trace = (function re_frame$trace$start_trace(p__14325){
var map__14326 = p__14325;
var map__14326__$1 = cljs.core.__destructure_map(map__14326);
var operation = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__14326__$1,new cljs.core.Keyword(null,"operation","operation",-1267664310));
var op_type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__14326__$1,new cljs.core.Keyword(null,"op-type","op-type",-1636141668));
var tags = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__14326__$1,new cljs.core.Keyword(null,"tags","tags",1771418977));
var child_of = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__14326__$1,new cljs.core.Keyword(null,"child-of","child-of",-903376662));
return new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"id","id",-1388402092),re_frame.trace.next_id(),new cljs.core.Keyword(null,"operation","operation",-1267664310),operation,new cljs.core.Keyword(null,"op-type","op-type",-1636141668),op_type,new cljs.core.Keyword(null,"tags","tags",1771418977),tags,new cljs.core.Keyword(null,"child-of","child-of",-903376662),(function (){var or__5002__auto__ = child_of;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(re_frame.trace._STAR_current_trace_STAR_);
}
})(),new cljs.core.Keyword(null,"start","start",-355208981),re_frame.interop.now()], null);
});
re_frame.trace.debounce_time = (50);
re_frame.trace.debounce = (function re_frame$trace$debounce(f,interval){
return goog.functions.debounce(f,interval);
});
re_frame.trace.schedule_debounce = re_frame.trace.debounce((function re_frame$trace$tracing_cb_debounced(){
var seq__14331_14364 = cljs.core.seq(cljs.core.deref(re_frame.trace.trace_cbs));
var chunk__14332_14365 = null;
var count__14333_14366 = (0);
var i__14334_14367 = (0);
while(true){
if((i__14334_14367 < count__14333_14366)){
var vec__14348_14370 = chunk__14332_14365.cljs$core$IIndexed$_nth$arity$2(null, i__14334_14367);
var k_14371 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__14348_14370,(0),null);
var cb_14372 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__14348_14370,(1),null);
try{var G__14352_14374 = cljs.core.deref(re_frame.trace.traces);
(cb_14372.cljs$core$IFn$_invoke$arity$1 ? cb_14372.cljs$core$IFn$_invoke$arity$1(G__14352_14374) : cb_14372.call(null, G__14352_14374));
}catch (e14351){var e_14375 = e14351;
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Error thrown from trace cb",k_14371,"while storing",cljs.core.deref(re_frame.trace.traces),e_14375], 0));
}

var G__14376 = seq__14331_14364;
var G__14377 = chunk__14332_14365;
var G__14378 = count__14333_14366;
var G__14379 = (i__14334_14367 + (1));
seq__14331_14364 = G__14376;
chunk__14332_14365 = G__14377;
count__14333_14366 = G__14378;
i__14334_14367 = G__14379;
continue;
} else {
var temp__5804__auto___14380 = cljs.core.seq(seq__14331_14364);
if(temp__5804__auto___14380){
var seq__14331_14381__$1 = temp__5804__auto___14380;
if(cljs.core.chunked_seq_QMARK_(seq__14331_14381__$1)){
var c__5525__auto___14382 = cljs.core.chunk_first(seq__14331_14381__$1);
var G__14383 = cljs.core.chunk_rest(seq__14331_14381__$1);
var G__14384 = c__5525__auto___14382;
var G__14385 = cljs.core.count(c__5525__auto___14382);
var G__14386 = (0);
seq__14331_14364 = G__14383;
chunk__14332_14365 = G__14384;
count__14333_14366 = G__14385;
i__14334_14367 = G__14386;
continue;
} else {
var vec__14354_14387 = cljs.core.first(seq__14331_14381__$1);
var k_14388 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__14354_14387,(0),null);
var cb_14389 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__14354_14387,(1),null);
try{var G__14358_14390 = cljs.core.deref(re_frame.trace.traces);
(cb_14389.cljs$core$IFn$_invoke$arity$1 ? cb_14389.cljs$core$IFn$_invoke$arity$1(G__14358_14390) : cb_14389.call(null, G__14358_14390));
}catch (e14357){var e_14391 = e14357;
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Error thrown from trace cb",k_14388,"while storing",cljs.core.deref(re_frame.trace.traces),e_14391], 0));
}

var G__14393 = cljs.core.next(seq__14331_14381__$1);
var G__14394 = null;
var G__14395 = (0);
var G__14396 = (0);
seq__14331_14364 = G__14393;
chunk__14332_14365 = G__14394;
count__14333_14366 = G__14395;
i__14334_14367 = G__14396;
continue;
}
} else {
}
}
break;
}

return cljs.core.reset_BANG_(re_frame.trace.traces,cljs.core.PersistentVector.EMPTY);
}),re_frame.trace.debounce_time);
re_frame.trace.run_tracing_callbacks_BANG_ = (function re_frame$trace$run_tracing_callbacks_BANG_(now){
if(((cljs.core.deref(re_frame.trace.next_delivery) - (25)) < now)){
(re_frame.trace.schedule_debounce.cljs$core$IFn$_invoke$arity$0 ? re_frame.trace.schedule_debounce.cljs$core$IFn$_invoke$arity$0() : re_frame.trace.schedule_debounce.call(null, ));

return cljs.core.reset_BANG_(re_frame.trace.next_delivery,(now + re_frame.trace.debounce_time));
} else {
return null;
}
});

//# sourceMappingURL=re_frame.trace.js.map
