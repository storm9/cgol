goog.provide('reagent.dom');
var module$node_modules$react_dom$index=shadow.js.require("module$node_modules$react_dom$index", {});
if((typeof reagent !== 'undefined') && (typeof reagent.dom !== 'undefined') && (typeof reagent.dom.roots !== 'undefined')){
} else {
reagent.dom.roots = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
}
reagent.dom.unmount_comp = (function reagent$dom$unmount_comp(container){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(reagent.dom.roots,cljs.core.dissoc,container);

return module$node_modules$react_dom$index.unmountComponentAtNode(container);
});
reagent.dom.render_comp = (function reagent$dom$render_comp(comp,container,callback){
var _STAR_always_update_STAR__orig_val__27695 = reagent.impl.util._STAR_always_update_STAR_;
var _STAR_always_update_STAR__temp_val__27696 = true;
(reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR__temp_val__27696);

try{return module$node_modules$react_dom$index.render((comp.cljs$core$IFn$_invoke$arity$0 ? comp.cljs$core$IFn$_invoke$arity$0() : comp.call(null, )),container,(function (){
var _STAR_always_update_STAR__orig_val__27697 = reagent.impl.util._STAR_always_update_STAR_;
var _STAR_always_update_STAR__temp_val__27698 = false;
(reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR__temp_val__27698);

try{cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(reagent.dom.roots,cljs.core.assoc,container,comp);

reagent.impl.batching.flush_after_render();

if((!((callback == null)))){
return (callback.cljs$core$IFn$_invoke$arity$0 ? callback.cljs$core$IFn$_invoke$arity$0() : callback.call(null, ));
} else {
return null;
}
}finally {(reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR__orig_val__27697);
}}));
}finally {(reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR__orig_val__27695);
}});
reagent.dom.re_render_component = (function reagent$dom$re_render_component(comp,container){
return reagent.dom.render_comp(comp,container,null);
});
/**
 * Render a Reagent component into the DOM. The first argument may be
 *   either a vector (using Reagent's Hiccup syntax), or a React element.
 *   The second argument should be a DOM node.
 * 
 *   Optionally takes a callback that is called when the component is in place.
 * 
 *   Returns the mounted component instance.
 */
reagent.dom.render = (function reagent$dom$render(var_args){
var G__27700 = arguments.length;
switch (G__27700) {
case 2:
return reagent.dom.render.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return reagent.dom.render.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(reagent.dom.render.cljs$core$IFn$_invoke$arity$2 = (function (comp,container){
return reagent.dom.render.cljs$core$IFn$_invoke$arity$3(comp,container,reagent.impl.template.default_compiler);
}));

(reagent.dom.render.cljs$core$IFn$_invoke$arity$3 = (function (comp,container,callback_or_compiler){
reagent.ratom.flush_BANG_();

var vec__27701 = ((cljs.core.fn_QMARK_(callback_or_compiler))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [reagent.impl.template.default_compiler,callback_or_compiler], null):new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [callback_or_compiler,new cljs.core.Keyword(null,"callback","callback",-705136228).cljs$core$IFn$_invoke$arity$1(callback_or_compiler)], null));
var compiler = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__27701,(0),null);
var callback = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__27701,(1),null);
var f = (function (){
return reagent.impl.protocols.as_element(compiler,((cljs.core.fn_QMARK_(comp))?(comp.cljs$core$IFn$_invoke$arity$0 ? comp.cljs$core$IFn$_invoke$arity$0() : comp.call(null, )):comp));
});
return reagent.dom.render_comp(f,container,callback);
}));

(reagent.dom.render.cljs$lang$maxFixedArity = 3);

/**
 * Remove a component from the given DOM node.
 */
reagent.dom.unmount_component_at_node = (function reagent$dom$unmount_component_at_node(container){
return reagent.dom.unmount_comp(container);
});
/**
 * Returns the root DOM node of a mounted component.
 */
reagent.dom.dom_node = (function reagent$dom$dom_node(this$){
return module$node_modules$react_dom$index.findDOMNode(this$);
});
/**
 * Force re-rendering of all mounted Reagent components. This is
 *   probably only useful in a development environment, when you want to
 *   update components in response to some dynamic changes to code.
 * 
 *   Note that force-update-all may not update root components. This
 *   happens if a component 'foo' is mounted with `(render [foo])` (since
 *   functions are passed by value, and not by reference, in
 *   ClojureScript). To get around this you'll have to introduce a layer
 *   of indirection, for example by using `(render [#'foo])` instead.
 */
reagent.dom.force_update_all = (function reagent$dom$force_update_all(){
reagent.ratom.flush_BANG_();

var seq__27704_27727 = cljs.core.seq(cljs.core.deref(reagent.dom.roots));
var chunk__27705_27728 = null;
var count__27706_27729 = (0);
var i__27707_27730 = (0);
while(true){
if((i__27707_27730 < count__27706_27729)){
var vec__27714_27731 = chunk__27705_27728.cljs$core$IIndexed$_nth$arity$2(null, i__27707_27730);
var container_27732 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__27714_27731,(0),null);
var comp_27733 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__27714_27731,(1),null);
reagent.dom.re_render_component(comp_27733,container_27732);


var G__27734 = seq__27704_27727;
var G__27735 = chunk__27705_27728;
var G__27736 = count__27706_27729;
var G__27737 = (i__27707_27730 + (1));
seq__27704_27727 = G__27734;
chunk__27705_27728 = G__27735;
count__27706_27729 = G__27736;
i__27707_27730 = G__27737;
continue;
} else {
var temp__5804__auto___27738 = cljs.core.seq(seq__27704_27727);
if(temp__5804__auto___27738){
var seq__27704_27739__$1 = temp__5804__auto___27738;
if(cljs.core.chunked_seq_QMARK_(seq__27704_27739__$1)){
var c__5525__auto___27740 = cljs.core.chunk_first(seq__27704_27739__$1);
var G__27741 = cljs.core.chunk_rest(seq__27704_27739__$1);
var G__27742 = c__5525__auto___27740;
var G__27743 = cljs.core.count(c__5525__auto___27740);
var G__27744 = (0);
seq__27704_27727 = G__27741;
chunk__27705_27728 = G__27742;
count__27706_27729 = G__27743;
i__27707_27730 = G__27744;
continue;
} else {
var vec__27723_27745 = cljs.core.first(seq__27704_27739__$1);
var container_27746 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__27723_27745,(0),null);
var comp_27747 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__27723_27745,(1),null);
reagent.dom.re_render_component(comp_27747,container_27746);


var G__27748 = cljs.core.next(seq__27704_27739__$1);
var G__27749 = null;
var G__27750 = (0);
var G__27751 = (0);
seq__27704_27727 = G__27748;
chunk__27705_27728 = G__27749;
count__27706_27729 = G__27750;
i__27707_27730 = G__27751;
continue;
}
} else {
}
}
break;
}

return reagent.impl.batching.flush_after_render();
});

//# sourceMappingURL=reagent.dom.js.map
