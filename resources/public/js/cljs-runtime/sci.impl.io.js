goog.provide('sci.impl.io');
/**
 * create a dynamic var with clojure.core :ns meta
 */
sci.impl.io.core_dynamic_var = (function sci$impl$io$core_dynamic_var(var_args){
var G__22393 = arguments.length;
switch (G__22393) {
case 1:
return sci.impl.io.core_dynamic_var.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sci.impl.io.core_dynamic_var.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(sci.impl.io.core_dynamic_var.cljs$core$IFn$_invoke$arity$1 = (function (name){
return sci.impl.io.core_dynamic_var.cljs$core$IFn$_invoke$arity$2(name,null);
}));

(sci.impl.io.core_dynamic_var.cljs$core$IFn$_invoke$arity$2 = (function (name,init_val){
return sci.impl.vars.dynamic_var.cljs$core$IFn$_invoke$arity$3(name,init_val,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"ns","ns",441598760),sci.impl.vars.clojure_core_ns], null));
}));

(sci.impl.io.core_dynamic_var.cljs$lang$maxFixedArity = 2);

sci.impl.io.in$ = (function (){var _STAR_unrestricted_STAR__orig_val__22399 = sci.impl.unrestrict._STAR_unrestricted_STAR_;
var _STAR_unrestricted_STAR__temp_val__22400 = true;
(sci.impl.unrestrict._STAR_unrestricted_STAR_ = _STAR_unrestricted_STAR__temp_val__22400);

try{var G__22401 = sci.impl.io.core_dynamic_var.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"*in*","*in*",1130010229,null));
sci.impl.vars.unbind(G__22401);

return G__22401;
}finally {(sci.impl.unrestrict._STAR_unrestricted_STAR_ = _STAR_unrestricted_STAR__orig_val__22399);
}})();
sci.impl.io.out = (function (){var _STAR_unrestricted_STAR__orig_val__22406 = sci.impl.unrestrict._STAR_unrestricted_STAR_;
var _STAR_unrestricted_STAR__temp_val__22407 = true;
(sci.impl.unrestrict._STAR_unrestricted_STAR_ = _STAR_unrestricted_STAR__temp_val__22407);

try{var G__22412 = sci.impl.io.core_dynamic_var.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"*out*","*out*",1277591796,null));
sci.impl.vars.unbind(G__22412);

return G__22412;
}finally {(sci.impl.unrestrict._STAR_unrestricted_STAR_ = _STAR_unrestricted_STAR__orig_val__22406);
}})();
sci.impl.io.err = (function (){var _STAR_unrestricted_STAR__orig_val__22414 = sci.impl.unrestrict._STAR_unrestricted_STAR_;
var _STAR_unrestricted_STAR__temp_val__22415 = true;
(sci.impl.unrestrict._STAR_unrestricted_STAR_ = _STAR_unrestricted_STAR__temp_val__22415);

try{var G__22423 = sci.impl.io.core_dynamic_var.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"*err*","*err*",2070937226,null));
sci.impl.vars.unbind(G__22423);

return G__22423;
}finally {(sci.impl.unrestrict._STAR_unrestricted_STAR_ = _STAR_unrestricted_STAR__orig_val__22414);
}})();
sci.impl.io.print_fn = (function (){var _STAR_unrestricted_STAR__orig_val__22424 = sci.impl.unrestrict._STAR_unrestricted_STAR_;
var _STAR_unrestricted_STAR__temp_val__22425 = true;
(sci.impl.unrestrict._STAR_unrestricted_STAR_ = _STAR_unrestricted_STAR__temp_val__22425);

try{var G__22426 = sci.impl.io.core_dynamic_var.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"*print-fn*","*print-fn*",138509853,null));
sci.impl.vars.unbind(G__22426);

return G__22426;
}finally {(sci.impl.unrestrict._STAR_unrestricted_STAR_ = _STAR_unrestricted_STAR__orig_val__22424);
}})();
sci.impl.io.print_meta = sci.impl.io.core_dynamic_var.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"*print-meta*","*print-meta*",-919406644,null),false);
sci.impl.io.print_length = sci.impl.io.core_dynamic_var.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"*print-length*","*print-length*",-687693654,null));
sci.impl.io.print_level = sci.impl.io.core_dynamic_var.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"*print-level*","*print-level*",-634488505,null));
sci.impl.io.print_namespace_maps = sci.impl.io.core_dynamic_var.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"*print-namespace-maps*","*print-namespace-maps*",-1759108415,null),true);
sci.impl.io.flush_on_newline = sci.impl.io.core_dynamic_var.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"*flush-on-newline*","*flush-on-newline*",-737526501,null),cljs.core._STAR_flush_on_newline_STAR_);
sci.impl.io.print_readably = sci.impl.io.core_dynamic_var.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"*print-readably*","*print-readably*",-761361221,null),cljs.core._STAR_print_readably_STAR_);
sci.impl.io.print_newline = sci.impl.io.core_dynamic_var.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"*print-newline*","*print-newline*",1478078956,null),cljs.core._STAR_print_newline_STAR_);
sci.impl.io.string_print = (function sci$impl$io$string_print(x){
var _STAR_print_fn_STAR__orig_val__22453 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_fn_STAR__temp_val__22454 = cljs.core.deref(sci.impl.io.print_fn);
(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__22454);

try{return cljs.core.string_print(x);
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__22453);
}});
sci.impl.io.pr = (function sci$impl$io$pr(var_args){
var args__5732__auto__ = [];
var len__5726__auto___22893 = arguments.length;
var i__5727__auto___22897 = (0);
while(true){
if((i__5727__auto___22897 < len__5726__auto___22893)){
args__5732__auto__.push((arguments[i__5727__auto___22897]));

var G__22898 = (i__5727__auto___22897 + (1));
i__5727__auto___22897 = G__22898;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((0) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((0)),(0),null)):null);
return sci.impl.io.pr.cljs$core$IFn$_invoke$arity$variadic(argseq__5733__auto__);
});

(sci.impl.io.pr.cljs$core$IFn$_invoke$arity$variadic = (function (objs){
var _STAR_print_fn_STAR__orig_val__22462 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_length_STAR__orig_val__22463 = cljs.core._STAR_print_length_STAR_;
var _STAR_print_level_STAR__orig_val__22464 = cljs.core._STAR_print_level_STAR_;
var _STAR_print_meta_STAR__orig_val__22465 = cljs.core._STAR_print_meta_STAR_;
var _STAR_print_namespace_maps_STAR__orig_val__22466 = cljs.core._STAR_print_namespace_maps_STAR_;
var _STAR_print_readably_STAR__orig_val__22467 = cljs.core._STAR_print_readably_STAR_;
var _STAR_print_newline_STAR__orig_val__22468 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__temp_val__22469 = cljs.core.deref(sci.impl.io.print_fn);
var _STAR_print_length_STAR__temp_val__22470 = cljs.core.deref(sci.impl.io.print_length);
var _STAR_print_level_STAR__temp_val__22471 = cljs.core.deref(sci.impl.io.print_level);
var _STAR_print_meta_STAR__temp_val__22472 = cljs.core.deref(sci.impl.io.print_meta);
var _STAR_print_namespace_maps_STAR__temp_val__22473 = cljs.core.deref(sci.impl.io.print_namespace_maps);
var _STAR_print_readably_STAR__temp_val__22474 = cljs.core.deref(sci.impl.io.print_readably);
var _STAR_print_newline_STAR__temp_val__22475 = cljs.core.deref(sci.impl.io.print_newline);
(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__22469);

(cljs.core._STAR_print_length_STAR_ = _STAR_print_length_STAR__temp_val__22470);

(cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR__temp_val__22471);

(cljs.core._STAR_print_meta_STAR_ = _STAR_print_meta_STAR__temp_val__22472);

(cljs.core._STAR_print_namespace_maps_STAR_ = _STAR_print_namespace_maps_STAR__temp_val__22473);

(cljs.core._STAR_print_readably_STAR_ = _STAR_print_readably_STAR__temp_val__22474);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__22475);

try{return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.pr,objs);
}finally {(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__22468);

(cljs.core._STAR_print_readably_STAR_ = _STAR_print_readably_STAR__orig_val__22467);

(cljs.core._STAR_print_namespace_maps_STAR_ = _STAR_print_namespace_maps_STAR__orig_val__22466);

(cljs.core._STAR_print_meta_STAR_ = _STAR_print_meta_STAR__orig_val__22465);

(cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR__orig_val__22464);

(cljs.core._STAR_print_length_STAR_ = _STAR_print_length_STAR__orig_val__22463);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__22462);
}}));

(sci.impl.io.pr.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(sci.impl.io.pr.cljs$lang$applyTo = (function (seq22459){
var self__5712__auto__ = this;
return self__5712__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq22459));
}));

sci.impl.io.flush = (function sci$impl$io$flush(){
return null;
});
sci.impl.io.newline = (function sci$impl$io$newline(){
var _STAR_print_fn_STAR__orig_val__22501 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_fn_STAR__temp_val__22502 = cljs.core.deref(sci.impl.io.print_fn);
(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__22502);

try{return cljs.core.newline.cljs$core$IFn$_invoke$arity$0();
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__22501);
}});
/**
 * pr to a string, returning it
 */
sci.impl.io.pr_str = (function sci$impl$io$pr_str(var_args){
var args__5732__auto__ = [];
var len__5726__auto___22914 = arguments.length;
var i__5727__auto___22915 = (0);
while(true){
if((i__5727__auto___22915 < len__5726__auto___22914)){
args__5732__auto__.push((arguments[i__5727__auto___22915]));

var G__22916 = (i__5727__auto___22915 + (1));
i__5727__auto___22915 = G__22916;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((0) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((0)),(0),null)):null);
return sci.impl.io.pr_str.cljs$core$IFn$_invoke$arity$variadic(argseq__5733__auto__);
});

(sci.impl.io.pr_str.cljs$core$IFn$_invoke$arity$variadic = (function (objs){
var _STAR_print_length_STAR__orig_val__22513 = cljs.core._STAR_print_length_STAR_;
var _STAR_print_level_STAR__orig_val__22514 = cljs.core._STAR_print_level_STAR_;
var _STAR_print_meta_STAR__orig_val__22515 = cljs.core._STAR_print_meta_STAR_;
var _STAR_print_namespace_maps_STAR__orig_val__22516 = cljs.core._STAR_print_namespace_maps_STAR_;
var _STAR_print_readably_STAR__orig_val__22517 = cljs.core._STAR_print_readably_STAR_;
var _STAR_print_newline_STAR__orig_val__22518 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_length_STAR__temp_val__22519 = cljs.core.deref(sci.impl.io.print_length);
var _STAR_print_level_STAR__temp_val__22520 = cljs.core.deref(sci.impl.io.print_level);
var _STAR_print_meta_STAR__temp_val__22521 = cljs.core.deref(sci.impl.io.print_meta);
var _STAR_print_namespace_maps_STAR__temp_val__22522 = cljs.core.deref(sci.impl.io.print_namespace_maps);
var _STAR_print_readably_STAR__temp_val__22523 = cljs.core.deref(sci.impl.io.print_readably);
var _STAR_print_newline_STAR__temp_val__22524 = cljs.core.deref(sci.impl.io.print_newline);
(cljs.core._STAR_print_length_STAR_ = _STAR_print_length_STAR__temp_val__22519);

(cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR__temp_val__22520);

(cljs.core._STAR_print_meta_STAR_ = _STAR_print_meta_STAR__temp_val__22521);

(cljs.core._STAR_print_namespace_maps_STAR_ = _STAR_print_namespace_maps_STAR__temp_val__22522);

(cljs.core._STAR_print_readably_STAR_ = _STAR_print_readably_STAR__temp_val__22523);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__22524);

try{return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.pr_str,objs);
}finally {(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__22518);

(cljs.core._STAR_print_readably_STAR_ = _STAR_print_readably_STAR__orig_val__22517);

(cljs.core._STAR_print_namespace_maps_STAR_ = _STAR_print_namespace_maps_STAR__orig_val__22516);

(cljs.core._STAR_print_meta_STAR_ = _STAR_print_meta_STAR__orig_val__22515);

(cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR__orig_val__22514);

(cljs.core._STAR_print_length_STAR_ = _STAR_print_length_STAR__orig_val__22513);
}}));

(sci.impl.io.pr_str.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(sci.impl.io.pr_str.cljs$lang$applyTo = (function (seq22504){
var self__5712__auto__ = this;
return self__5712__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq22504));
}));

sci.impl.io.prn = (function sci$impl$io$prn(var_args){
var args__5732__auto__ = [];
var len__5726__auto___22929 = arguments.length;
var i__5727__auto___22930 = (0);
while(true){
if((i__5727__auto___22930 < len__5726__auto___22929)){
args__5732__auto__.push((arguments[i__5727__auto___22930]));

var G__22934 = (i__5727__auto___22930 + (1));
i__5727__auto___22930 = G__22934;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((0) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((0)),(0),null)):null);
return sci.impl.io.prn.cljs$core$IFn$_invoke$arity$variadic(argseq__5733__auto__);
});

(sci.impl.io.prn.cljs$core$IFn$_invoke$arity$variadic = (function (objs){
var _STAR_print_fn_STAR__orig_val__22529 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_length_STAR__orig_val__22530 = cljs.core._STAR_print_length_STAR_;
var _STAR_print_level_STAR__orig_val__22531 = cljs.core._STAR_print_level_STAR_;
var _STAR_print_meta_STAR__orig_val__22532 = cljs.core._STAR_print_meta_STAR_;
var _STAR_print_namespace_maps_STAR__orig_val__22533 = cljs.core._STAR_print_namespace_maps_STAR_;
var _STAR_print_readably_STAR__orig_val__22534 = cljs.core._STAR_print_readably_STAR_;
var _STAR_print_newline_STAR__orig_val__22535 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__temp_val__22536 = cljs.core.deref(sci.impl.io.print_fn);
var _STAR_print_length_STAR__temp_val__22537 = cljs.core.deref(sci.impl.io.print_length);
var _STAR_print_level_STAR__temp_val__22538 = cljs.core.deref(sci.impl.io.print_level);
var _STAR_print_meta_STAR__temp_val__22539 = cljs.core.deref(sci.impl.io.print_meta);
var _STAR_print_namespace_maps_STAR__temp_val__22540 = cljs.core.deref(sci.impl.io.print_namespace_maps);
var _STAR_print_readably_STAR__temp_val__22541 = cljs.core.deref(sci.impl.io.print_readably);
var _STAR_print_newline_STAR__temp_val__22542 = cljs.core.deref(sci.impl.io.print_newline);
(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__22536);

(cljs.core._STAR_print_length_STAR_ = _STAR_print_length_STAR__temp_val__22537);

(cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR__temp_val__22538);

(cljs.core._STAR_print_meta_STAR_ = _STAR_print_meta_STAR__temp_val__22539);

(cljs.core._STAR_print_namespace_maps_STAR_ = _STAR_print_namespace_maps_STAR__temp_val__22540);

(cljs.core._STAR_print_readably_STAR_ = _STAR_print_readably_STAR__temp_val__22541);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__22542);

try{return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.prn,objs);
}finally {(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__22535);

(cljs.core._STAR_print_readably_STAR_ = _STAR_print_readably_STAR__orig_val__22534);

(cljs.core._STAR_print_namespace_maps_STAR_ = _STAR_print_namespace_maps_STAR__orig_val__22533);

(cljs.core._STAR_print_meta_STAR_ = _STAR_print_meta_STAR__orig_val__22532);

(cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR__orig_val__22531);

(cljs.core._STAR_print_length_STAR_ = _STAR_print_length_STAR__orig_val__22530);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__22529);
}}));

(sci.impl.io.prn.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(sci.impl.io.prn.cljs$lang$applyTo = (function (seq22528){
var self__5712__auto__ = this;
return self__5712__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq22528));
}));

/**
 * prn to a string, returning it
 */
sci.impl.io.prn_str = (function sci$impl$io$prn_str(var_args){
var args__5732__auto__ = [];
var len__5726__auto___22950 = arguments.length;
var i__5727__auto___22954 = (0);
while(true){
if((i__5727__auto___22954 < len__5726__auto___22950)){
args__5732__auto__.push((arguments[i__5727__auto___22954]));

var G__22955 = (i__5727__auto___22954 + (1));
i__5727__auto___22954 = G__22955;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((0) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((0)),(0),null)):null);
return sci.impl.io.prn_str.cljs$core$IFn$_invoke$arity$variadic(argseq__5733__auto__);
});

(sci.impl.io.prn_str.cljs$core$IFn$_invoke$arity$variadic = (function (objs){
var _STAR_print_length_STAR__orig_val__22547 = cljs.core._STAR_print_length_STAR_;
var _STAR_print_level_STAR__orig_val__22548 = cljs.core._STAR_print_level_STAR_;
var _STAR_print_meta_STAR__orig_val__22549 = cljs.core._STAR_print_meta_STAR_;
var _STAR_print_namespace_maps_STAR__orig_val__22550 = cljs.core._STAR_print_namespace_maps_STAR_;
var _STAR_print_readably_STAR__orig_val__22551 = cljs.core._STAR_print_readably_STAR_;
var _STAR_print_newline_STAR__orig_val__22552 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_length_STAR__temp_val__22553 = cljs.core.deref(sci.impl.io.print_length);
var _STAR_print_level_STAR__temp_val__22554 = cljs.core.deref(sci.impl.io.print_level);
var _STAR_print_meta_STAR__temp_val__22555 = cljs.core.deref(sci.impl.io.print_meta);
var _STAR_print_namespace_maps_STAR__temp_val__22556 = cljs.core.deref(sci.impl.io.print_namespace_maps);
var _STAR_print_readably_STAR__temp_val__22557 = cljs.core.deref(sci.impl.io.print_readably);
var _STAR_print_newline_STAR__temp_val__22558 = cljs.core.deref(sci.impl.io.print_newline);
(cljs.core._STAR_print_length_STAR_ = _STAR_print_length_STAR__temp_val__22553);

(cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR__temp_val__22554);

(cljs.core._STAR_print_meta_STAR_ = _STAR_print_meta_STAR__temp_val__22555);

(cljs.core._STAR_print_namespace_maps_STAR_ = _STAR_print_namespace_maps_STAR__temp_val__22556);

(cljs.core._STAR_print_readably_STAR_ = _STAR_print_readably_STAR__temp_val__22557);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__22558);

try{return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.prn_str,objs);
}finally {(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__22552);

(cljs.core._STAR_print_readably_STAR_ = _STAR_print_readably_STAR__orig_val__22551);

(cljs.core._STAR_print_namespace_maps_STAR_ = _STAR_print_namespace_maps_STAR__orig_val__22550);

(cljs.core._STAR_print_meta_STAR_ = _STAR_print_meta_STAR__orig_val__22549);

(cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR__orig_val__22548);

(cljs.core._STAR_print_length_STAR_ = _STAR_print_length_STAR__orig_val__22547);
}}));

(sci.impl.io.prn_str.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(sci.impl.io.prn_str.cljs$lang$applyTo = (function (seq22543){
var self__5712__auto__ = this;
return self__5712__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq22543));
}));

sci.impl.io.print = (function sci$impl$io$print(var_args){
var args__5732__auto__ = [];
var len__5726__auto___22968 = arguments.length;
var i__5727__auto___22969 = (0);
while(true){
if((i__5727__auto___22969 < len__5726__auto___22968)){
args__5732__auto__.push((arguments[i__5727__auto___22969]));

var G__22970 = (i__5727__auto___22969 + (1));
i__5727__auto___22969 = G__22970;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((0) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((0)),(0),null)):null);
return sci.impl.io.print.cljs$core$IFn$_invoke$arity$variadic(argseq__5733__auto__);
});

(sci.impl.io.print.cljs$core$IFn$_invoke$arity$variadic = (function (objs){
var _STAR_print_fn_STAR__orig_val__22567 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_length_STAR__orig_val__22568 = cljs.core._STAR_print_length_STAR_;
var _STAR_print_level_STAR__orig_val__22569 = cljs.core._STAR_print_level_STAR_;
var _STAR_print_namespace_maps_STAR__orig_val__22570 = cljs.core._STAR_print_namespace_maps_STAR_;
var _STAR_print_readably_STAR__orig_val__22571 = cljs.core._STAR_print_readably_STAR_;
var _STAR_print_newline_STAR__orig_val__22572 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__temp_val__22573 = cljs.core.deref(sci.impl.io.print_fn);
var _STAR_print_length_STAR__temp_val__22574 = cljs.core.deref(sci.impl.io.print_length);
var _STAR_print_level_STAR__temp_val__22575 = cljs.core.deref(sci.impl.io.print_level);
var _STAR_print_namespace_maps_STAR__temp_val__22576 = cljs.core.deref(sci.impl.io.print_namespace_maps);
var _STAR_print_readably_STAR__temp_val__22577 = null;
var _STAR_print_newline_STAR__temp_val__22578 = cljs.core.deref(sci.impl.io.print_newline);
(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__22573);

(cljs.core._STAR_print_length_STAR_ = _STAR_print_length_STAR__temp_val__22574);

(cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR__temp_val__22575);

(cljs.core._STAR_print_namespace_maps_STAR_ = _STAR_print_namespace_maps_STAR__temp_val__22576);

(cljs.core._STAR_print_readably_STAR_ = _STAR_print_readably_STAR__temp_val__22577);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__22578);

try{return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.print,objs);
}finally {(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__22572);

(cljs.core._STAR_print_readably_STAR_ = _STAR_print_readably_STAR__orig_val__22571);

(cljs.core._STAR_print_namespace_maps_STAR_ = _STAR_print_namespace_maps_STAR__orig_val__22570);

(cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR__orig_val__22569);

(cljs.core._STAR_print_length_STAR_ = _STAR_print_length_STAR__orig_val__22568);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__22567);
}}));

(sci.impl.io.print.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(sci.impl.io.print.cljs$lang$applyTo = (function (seq22564){
var self__5712__auto__ = this;
return self__5712__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq22564));
}));

/**
 * print to a string, returning it
 */
sci.impl.io.print_str = (function sci$impl$io$print_str(var_args){
var args__5732__auto__ = [];
var len__5726__auto___22986 = arguments.length;
var i__5727__auto___22987 = (0);
while(true){
if((i__5727__auto___22987 < len__5726__auto___22986)){
args__5732__auto__.push((arguments[i__5727__auto___22987]));

var G__22991 = (i__5727__auto___22987 + (1));
i__5727__auto___22987 = G__22991;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((0) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((0)),(0),null)):null);
return sci.impl.io.print_str.cljs$core$IFn$_invoke$arity$variadic(argseq__5733__auto__);
});

(sci.impl.io.print_str.cljs$core$IFn$_invoke$arity$variadic = (function (objs){
var _STAR_print_length_STAR__orig_val__22583 = cljs.core._STAR_print_length_STAR_;
var _STAR_print_level_STAR__orig_val__22584 = cljs.core._STAR_print_level_STAR_;
var _STAR_print_meta_STAR__orig_val__22585 = cljs.core._STAR_print_meta_STAR_;
var _STAR_print_namespace_maps_STAR__orig_val__22586 = cljs.core._STAR_print_namespace_maps_STAR_;
var _STAR_print_readably_STAR__orig_val__22587 = cljs.core._STAR_print_readably_STAR_;
var _STAR_print_newline_STAR__orig_val__22588 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_length_STAR__temp_val__22589 = cljs.core.deref(sci.impl.io.print_length);
var _STAR_print_level_STAR__temp_val__22590 = cljs.core.deref(sci.impl.io.print_level);
var _STAR_print_meta_STAR__temp_val__22591 = cljs.core.deref(sci.impl.io.print_meta);
var _STAR_print_namespace_maps_STAR__temp_val__22592 = cljs.core.deref(sci.impl.io.print_namespace_maps);
var _STAR_print_readably_STAR__temp_val__22593 = cljs.core.deref(sci.impl.io.print_readably);
var _STAR_print_newline_STAR__temp_val__22594 = cljs.core.deref(sci.impl.io.print_newline);
(cljs.core._STAR_print_length_STAR_ = _STAR_print_length_STAR__temp_val__22589);

(cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR__temp_val__22590);

(cljs.core._STAR_print_meta_STAR_ = _STAR_print_meta_STAR__temp_val__22591);

(cljs.core._STAR_print_namespace_maps_STAR_ = _STAR_print_namespace_maps_STAR__temp_val__22592);

(cljs.core._STAR_print_readably_STAR_ = _STAR_print_readably_STAR__temp_val__22593);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__22594);

try{return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.print_str,objs);
}finally {(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__22588);

(cljs.core._STAR_print_readably_STAR_ = _STAR_print_readably_STAR__orig_val__22587);

(cljs.core._STAR_print_namespace_maps_STAR_ = _STAR_print_namespace_maps_STAR__orig_val__22586);

(cljs.core._STAR_print_meta_STAR_ = _STAR_print_meta_STAR__orig_val__22585);

(cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR__orig_val__22584);

(cljs.core._STAR_print_length_STAR_ = _STAR_print_length_STAR__orig_val__22583);
}}));

(sci.impl.io.print_str.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(sci.impl.io.print_str.cljs$lang$applyTo = (function (seq22582){
var self__5712__auto__ = this;
return self__5712__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq22582));
}));

sci.impl.io.println = (function sci$impl$io$println(var_args){
var args__5732__auto__ = [];
var len__5726__auto___22998 = arguments.length;
var i__5727__auto___23002 = (0);
while(true){
if((i__5727__auto___23002 < len__5726__auto___22998)){
args__5732__auto__.push((arguments[i__5727__auto___23002]));

var G__23003 = (i__5727__auto___23002 + (1));
i__5727__auto___23002 = G__23003;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((0) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((0)),(0),null)):null);
return sci.impl.io.println.cljs$core$IFn$_invoke$arity$variadic(argseq__5733__auto__);
});

(sci.impl.io.println.cljs$core$IFn$_invoke$arity$variadic = (function (objs){
var _STAR_print_fn_STAR__orig_val__22599 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_length_STAR__orig_val__22600 = cljs.core._STAR_print_length_STAR_;
var _STAR_print_level_STAR__orig_val__22601 = cljs.core._STAR_print_level_STAR_;
var _STAR_print_meta_STAR__orig_val__22602 = cljs.core._STAR_print_meta_STAR_;
var _STAR_print_namespace_maps_STAR__orig_val__22603 = cljs.core._STAR_print_namespace_maps_STAR_;
var _STAR_print_readably_STAR__orig_val__22604 = cljs.core._STAR_print_readably_STAR_;
var _STAR_print_newline_STAR__orig_val__22605 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__temp_val__22606 = cljs.core.deref(sci.impl.io.print_fn);
var _STAR_print_length_STAR__temp_val__22607 = cljs.core.deref(sci.impl.io.print_length);
var _STAR_print_level_STAR__temp_val__22608 = cljs.core.deref(sci.impl.io.print_level);
var _STAR_print_meta_STAR__temp_val__22609 = cljs.core.deref(sci.impl.io.print_meta);
var _STAR_print_namespace_maps_STAR__temp_val__22610 = cljs.core.deref(sci.impl.io.print_namespace_maps);
var _STAR_print_readably_STAR__temp_val__22611 = cljs.core.deref(sci.impl.io.print_readably);
var _STAR_print_newline_STAR__temp_val__22612 = cljs.core.deref(sci.impl.io.print_newline);
(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__22606);

(cljs.core._STAR_print_length_STAR_ = _STAR_print_length_STAR__temp_val__22607);

(cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR__temp_val__22608);

(cljs.core._STAR_print_meta_STAR_ = _STAR_print_meta_STAR__temp_val__22609);

(cljs.core._STAR_print_namespace_maps_STAR_ = _STAR_print_namespace_maps_STAR__temp_val__22610);

(cljs.core._STAR_print_readably_STAR_ = _STAR_print_readably_STAR__temp_val__22611);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__22612);

try{return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.println,objs);
}finally {(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__22605);

(cljs.core._STAR_print_readably_STAR_ = _STAR_print_readably_STAR__orig_val__22604);

(cljs.core._STAR_print_namespace_maps_STAR_ = _STAR_print_namespace_maps_STAR__orig_val__22603);

(cljs.core._STAR_print_meta_STAR_ = _STAR_print_meta_STAR__orig_val__22602);

(cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR__orig_val__22601);

(cljs.core._STAR_print_length_STAR_ = _STAR_print_length_STAR__orig_val__22600);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__22599);
}}));

(sci.impl.io.println.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(sci.impl.io.println.cljs$lang$applyTo = (function (seq22595){
var self__5712__auto__ = this;
return self__5712__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq22595));
}));

sci.impl.io.with_out_str = (function sci$impl$io$with_out_str(var_args){
var args__5732__auto__ = [];
var len__5726__auto___23016 = arguments.length;
var i__5727__auto___23020 = (0);
while(true){
if((i__5727__auto___23020 < len__5726__auto___23016)){
args__5732__auto__.push((arguments[i__5727__auto___23020]));

var G__23021 = (i__5727__auto___23020 + (1));
i__5727__auto___23020 = G__23021;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((2) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((2)),(0),null)):null);
return sci.impl.io.with_out_str.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__5733__auto__);
});

(sci.impl.io.with_out_str.cljs$core$IFn$_invoke$arity$variadic = (function (_,___$1,body){
return cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","let","cljs.core/let",-308701135,null),null,(1),null)),(new cljs.core.List(null,cljs.core.vec(cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"s__22807__auto__","s__22807__auto__",-753645720,null),null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol(null,"new","new",-444906321,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"goog.string.StringBuffer","goog.string.StringBuffer",-1220229842,null),null,(1),null))))),null,(1),null)))))),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","binding","cljs.core/binding",2050379843,null),null,(1),null)),(new cljs.core.List(null,cljs.core.vec(cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","*print-newline*","cljs.core/*print-newline*",6231625,null),null,(1),null)),(new cljs.core.List(null,true,null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,new cljs.core.Symbol("cljs.core","*print-fn*","cljs.core/*print-fn*",1342365176,null),null,(1),null)),(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),null,(1),null)),(new cljs.core.List(null,cljs.core.vec(cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$1((new cljs.core.List(null,new cljs.core.Symbol(null,"x__22808__auto__","x__22808__auto__",-1126162499,null),null,(1),null)))))),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic((new cljs.core.List(null,new cljs.core.Symbol(null,".",".",1975675962,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"s__22807__auto__","s__22807__auto__",-753645720,null),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(new cljs.core.List(null,sci.impl.utils.allowed_append,null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"x__22808__auto__","x__22808__auto__",-1126162499,null),null,(1),null))], 0)))),null,(1),null))], 0)))),null,(1),null))], 0))))),null,(1),null)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([body,(new cljs.core.List(null,cljs.core.sequence.cljs$core$IFn$_invoke$arity$1(cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2((new cljs.core.List(null,new cljs.core.Symbol("cljs.core","str","cljs.core/str",-1971828991,null),null,(1),null)),(new cljs.core.List(null,new cljs.core.Symbol(null,"s__22807__auto__","s__22807__auto__",-753645720,null),null,(1),null))))),null,(1),null))], 0)))),null,(1),null))], 0))));
}));

(sci.impl.io.with_out_str.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(sci.impl.io.with_out_str.cljs$lang$applyTo = (function (seq22810){
var G__22811 = cljs.core.first(seq22810);
var seq22810__$1 = cljs.core.next(seq22810);
var G__22812 = cljs.core.first(seq22810__$1);
var seq22810__$2 = cljs.core.next(seq22810__$1);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__22811,G__22812,seq22810__$2);
}));


//# sourceMappingURL=sci.impl.io.js.map
