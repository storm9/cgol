goog.provide('day8.re_frame_10x.navigation.subs');
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword("day8.re-frame-10x.navigation.subs","global","day8.re-frame-10x.navigation.subs/global",-347934985),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function (p__15906,_){
var map__15908 = p__15906;
var map__15908__$1 = cljs.core.__destructure_map(map__15908);
var global__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__15908__$1,new cljs.core.Keyword(null,"global","global",93595047));
return global__$1;
})], 0));
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword("day8.re-frame-10x.navigation.subs","unloading?","day8.re-frame-10x.navigation.subs/unloading?",2074795558),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function (p__15910,_){
var map__15911 = p__15910;
var map__15911__$1 = cljs.core.__destructure_map(map__15911);
var unloading_QMARK_ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__15911__$1,new cljs.core.Keyword(null,"unloading?","unloading?",621163286));
return unloading_QMARK_;
})], 0));
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword("day8.re-frame-10x.navigation.subs","errors","day8.re-frame-10x.navigation.subs/errors",1803528594),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function (p__15914,_){
var map__15915 = p__15914;
var map__15915__$1 = cljs.core.__destructure_map(map__15915);
var errors = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__15915__$1,new cljs.core.Keyword(null,"errors","errors",-908790718));
return errors;
})], 0));
day8.re_frame_10x.inlined_deps.re_frame.v1v1v2.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword("day8.re-frame-10x.navigation.subs","popup-failed?","day8.re-frame-10x.navigation.subs/popup-failed?",1236377870),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"<-","<-",760412998),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("day8.re-frame-10x.navigation.subs","errors","day8.re-frame-10x.navigation.subs/errors",1803528594)], null),(function (p__15919,_){
var map__15920 = p__15919;
var map__15920__$1 = cljs.core.__destructure_map(map__15920);
var popup_failed_QMARK_ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__15920__$1,new cljs.core.Keyword(null,"popup-failed?","popup-failed?",-345183682));
return popup_failed_QMARK_;
})], 0));

//# sourceMappingURL=day8.re_frame_10x.navigation.subs.js.map
