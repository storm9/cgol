goog.provide('day8.re_frame_10x.tools.string');
/**
 * Return a pluralized phrase, appending an s to the singular form if no plural is provided.
 *   For example:
 *   (pluralize 5 "month") => "5 months"
 *   (pluralize 1 "month") => "1 month"
 *   (pluralize 1 "radius" "radii") => "1 radius"
 *   (pluralize 9 "radius" "radii") => "9 radii"
 *   From https://github.com/flatland/useful/blob/194950/src/flatland/useful/string.clj#L25-L33
 */
day8.re_frame_10x.tools.string.pluralize = (function day8$re_frame_10x$tools$string$pluralize(var_args){
var args__5732__auto__ = [];
var len__5726__auto___35763 = arguments.length;
var i__5727__auto___35764 = (0);
while(true){
if((i__5727__auto___35764 < len__5726__auto___35763)){
args__5732__auto__.push((arguments[i__5727__auto___35764]));

var G__35765 = (i__5727__auto___35764 + (1));
i__5727__auto___35764 = G__35765;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((2) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((2)),(0),null)):null);
return day8.re_frame_10x.tools.string.pluralize.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__5733__auto__);
});

(day8.re_frame_10x.tools.string.pluralize.cljs$core$IFn$_invoke$arity$variadic = (function (num,singular,p__35745){
var vec__35746 = p__35745;
var plural = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35746,(0),null);
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(num)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((1),num))?singular:(function (){var or__5002__auto__ = plural;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(singular),"s"].join('');
}
})()))].join('');
}));

(day8.re_frame_10x.tools.string.pluralize.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(day8.re_frame_10x.tools.string.pluralize.cljs$lang$applyTo = (function (seq35741){
var G__35742 = cljs.core.first(seq35741);
var seq35741__$1 = cljs.core.next(seq35741);
var G__35743 = cljs.core.first(seq35741__$1);
var seq35741__$2 = cljs.core.next(seq35741__$1);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__35742,G__35743,seq35741__$2);
}));

/**
 * Same as pluralize, but doesn't prepend the number to the pluralized string.
 */
day8.re_frame_10x.tools.string.pluralize_ = (function day8$re_frame_10x$tools$string$pluralize_(var_args){
var args__5732__auto__ = [];
var len__5726__auto___35766 = arguments.length;
var i__5727__auto___35767 = (0);
while(true){
if((i__5727__auto___35767 < len__5726__auto___35766)){
args__5732__auto__.push((arguments[i__5727__auto___35767]));

var G__35768 = (i__5727__auto___35767 + (1));
i__5727__auto___35767 = G__35768;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((2) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((2)),(0),null)):null);
return day8.re_frame_10x.tools.string.pluralize_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__5733__auto__);
});

(day8.re_frame_10x.tools.string.pluralize_.cljs$core$IFn$_invoke$arity$variadic = (function (num,singular,p__35759){
var vec__35760 = p__35759;
var plural = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35760,(0),null);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((1),num)){
return singular;
} else {
var or__5002__auto__ = plural;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(singular),"s"].join('');
}
}
}));

(day8.re_frame_10x.tools.string.pluralize_.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(day8.re_frame_10x.tools.string.pluralize_.cljs$lang$applyTo = (function (seq35755){
var G__35756 = cljs.core.first(seq35755);
var seq35755__$1 = cljs.core.next(seq35755);
var G__35757 = cljs.core.first(seq35755__$1);
var seq35755__$2 = cljs.core.next(seq35755__$1);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__35756,G__35757,seq35755__$2);
}));


//# sourceMappingURL=day8.re_frame_10x.tools.string.js.map
