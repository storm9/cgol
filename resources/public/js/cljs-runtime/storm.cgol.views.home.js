goog.provide('storm.cgol.views.home');
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"say-hello-change","say-hello-change",630773659),(function (_db,p__30300){
var vec__30301 = p__30300;
var _event = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30301,(0),null);
var name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30301,(1),null);
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"say-hello","say-hello",-1427060073),name], null);
}));
re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"say-hello","say-hello",-1427060073),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function (db,_){
return new cljs.core.Keyword(null,"say-hello","say-hello",-1427060073).cljs$core$IFn$_invoke$arity$1(db);
})], 0));
storm.cgol.views.home.hello_input = (function storm$cgol$views$home$hello_input(){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.color-input","div.color-input",-879914246),"Say hello to: ",new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"text",new cljs.core.Keyword(null,"value","value",305978217),cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"say-hello","say-hello",-1427060073)], null))),new cljs.core.Keyword(null,"on-change","on-change",-732046149),(function (p1__30304_SHARP_){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"say-hello-change","say-hello-change",630773659),p1__30304_SHARP_.target.value], null));
})], null)], null)], null);
});
storm.cgol.views.home.hello_component = (function storm$cgol$views$home$hello_component(){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),["hello ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"say-hello","say-hello",-1427060073)], null))))].join('')], null);
});
storm.cgol.views.home.home_page = (function storm$cgol$views$home$home_page(){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),"This is the Home Page.",new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [storm.cgol.views.home.hello_input], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [storm.cgol.views.home.hello_component], null)], null)], null);
});

//# sourceMappingURL=storm.cgol.views.home.js.map
