goog.provide('shadow.dom');
shadow.dom.transition_supported_QMARK_ = (((typeof window !== 'undefined'))?goog.style.transition.isSupported():null);

/**
 * @interface
 */
shadow.dom.IElement = function(){};

var shadow$dom$IElement$_to_dom$dyn_25440 = (function (this$){
var x__5350__auto__ = (((this$ == null))?null:this$);
var m__5351__auto__ = (shadow.dom._to_dom[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__5351__auto__.call(null, this$));
} else {
var m__5349__auto__ = (shadow.dom._to_dom["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__5349__auto__.call(null, this$));
} else {
throw cljs.core.missing_protocol("IElement.-to-dom",this$);
}
}
});
shadow.dom._to_dom = (function shadow$dom$_to_dom(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$IElement$_to_dom$arity$1 == null)))))){
return this$.shadow$dom$IElement$_to_dom$arity$1(this$);
} else {
return shadow$dom$IElement$_to_dom$dyn_25440(this$);
}
});


/**
 * @interface
 */
shadow.dom.SVGElement = function(){};

var shadow$dom$SVGElement$_to_svg$dyn_25442 = (function (this$){
var x__5350__auto__ = (((this$ == null))?null:this$);
var m__5351__auto__ = (shadow.dom._to_svg[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__5351__auto__.call(null, this$));
} else {
var m__5349__auto__ = (shadow.dom._to_svg["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__5349__auto__.call(null, this$));
} else {
throw cljs.core.missing_protocol("SVGElement.-to-svg",this$);
}
}
});
shadow.dom._to_svg = (function shadow$dom$_to_svg(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$SVGElement$_to_svg$arity$1 == null)))))){
return this$.shadow$dom$SVGElement$_to_svg$arity$1(this$);
} else {
return shadow$dom$SVGElement$_to_svg$dyn_25442(this$);
}
});

shadow.dom.lazy_native_coll_seq = (function shadow$dom$lazy_native_coll_seq(coll,idx){
if((idx < coll.length)){
return (new cljs.core.LazySeq(null,(function (){
return cljs.core.cons((coll[idx]),(function (){var G__24282 = coll;
var G__24283 = (idx + (1));
return (shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2 ? shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2(G__24282,G__24283) : shadow.dom.lazy_native_coll_seq.call(null, G__24282,G__24283));
})());
}),null,null));
} else {
return null;
}
});

/**
* @constructor
 * @implements {cljs.core.IIndexed}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IDeref}
 * @implements {shadow.dom.IElement}
*/
shadow.dom.NativeColl = (function (coll){
this.coll = coll;
this.cljs$lang$protocol_mask$partition0$ = 8421394;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(shadow.dom.NativeColl.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (this$,n){
var self__ = this;
var this$__$1 = this;
return (self__.coll[n]);
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (this$,n,not_found){
var self__ = this;
var this$__$1 = this;
var or__5002__auto__ = (self__.coll[n]);
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return not_found;
}
}));

(shadow.dom.NativeColl.prototype.cljs$core$ICounted$_count$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll.length;
}));

(shadow.dom.NativeColl.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return shadow.dom.lazy_native_coll_seq(self__.coll,(0));
}));

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"coll","coll",-1006698606,null)], null);
}));

(shadow.dom.NativeColl.cljs$lang$type = true);

(shadow.dom.NativeColl.cljs$lang$ctorStr = "shadow.dom/NativeColl");

(shadow.dom.NativeColl.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"shadow.dom/NativeColl");
}));

/**
 * Positional factory function for shadow.dom/NativeColl.
 */
shadow.dom.__GT_NativeColl = (function shadow$dom$__GT_NativeColl(coll){
return (new shadow.dom.NativeColl(coll));
});

shadow.dom.native_coll = (function shadow$dom$native_coll(coll){
return (new shadow.dom.NativeColl(coll));
});
shadow.dom.dom_node = (function shadow$dom$dom_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$IElement$))))?true:false):false)){
return el.shadow$dom$IElement$_to_dom$arity$1(null, );
} else {
if(typeof el === 'string'){
return document.createTextNode(el);
} else {
if(typeof el === 'number'){
return document.createTextNode(cljs.core.str.cljs$core$IFn$_invoke$arity$1(el));
} else {
return el;

}
}
}
}
});
shadow.dom.query_one = (function shadow$dom$query_one(var_args){
var G__24336 = arguments.length;
switch (G__24336) {
case 1:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return document.querySelector(sel);
}));

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return shadow.dom.dom_node(root).querySelector(sel);
}));

(shadow.dom.query_one.cljs$lang$maxFixedArity = 2);

shadow.dom.query = (function shadow$dom$query(var_args){
var G__24351 = arguments.length;
switch (G__24351) {
case 1:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return (new shadow.dom.NativeColl(document.querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(root).querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$lang$maxFixedArity = 2);

shadow.dom.by_id = (function shadow$dom$by_id(var_args){
var G__24398 = arguments.length;
switch (G__24398) {
case 2:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2 = (function (id,el){
return shadow.dom.dom_node(el).getElementById(id);
}));

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1 = (function (id){
return document.getElementById(id);
}));

(shadow.dom.by_id.cljs$lang$maxFixedArity = 2);

shadow.dom.build = shadow.dom.dom_node;
shadow.dom.ev_stop = (function shadow$dom$ev_stop(var_args){
var G__24413 = arguments.length;
switch (G__24413) {
case 1:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1 = (function (e){
if(cljs.core.truth_(e.stopPropagation)){
e.stopPropagation();

e.preventDefault();
} else {
(e.cancelBubble = true);

(e.returnValue = false);
}

return e;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2 = (function (e,el){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4 = (function (e,el,scope,owner){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$lang$maxFixedArity = 4);

/**
 * check wether a parent node (or the document) contains the child
 */
shadow.dom.contains_QMARK_ = (function shadow$dom$contains_QMARK_(var_args){
var G__24429 = arguments.length;
switch (G__24429) {
case 1:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1 = (function (el){
return goog.dom.contains(document,shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2 = (function (parent,el){
return goog.dom.contains(shadow.dom.dom_node(parent),shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$lang$maxFixedArity = 2);

shadow.dom.add_class = (function shadow$dom$add_class(el,cls){
return goog.dom.classlist.add(shadow.dom.dom_node(el),cls);
});
shadow.dom.remove_class = (function shadow$dom$remove_class(el,cls){
return goog.dom.classlist.remove(shadow.dom.dom_node(el),cls);
});
shadow.dom.toggle_class = (function shadow$dom$toggle_class(var_args){
var G__24454 = arguments.length;
switch (G__24454) {
case 2:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2 = (function (el,cls){
return goog.dom.classlist.toggle(shadow.dom.dom_node(el),cls);
}));

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3 = (function (el,cls,v){
if(cljs.core.truth_(v)){
return shadow.dom.add_class(el,cls);
} else {
return shadow.dom.remove_class(el,cls);
}
}));

(shadow.dom.toggle_class.cljs$lang$maxFixedArity = 3);

shadow.dom.dom_listen = (cljs.core.truth_((function (){var or__5002__auto__ = (!((typeof document !== 'undefined')));
if(or__5002__auto__){
return or__5002__auto__;
} else {
return document.addEventListener;
}
})())?(function shadow$dom$dom_listen_good(el,ev,handler){
return el.addEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_ie(el,ev,handler){
try{return el.attachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),(function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null, e,el));
}));
}catch (e24467){if((e24467 instanceof Object)){
var e = e24467;
return console.log("didnt support attachEvent",el,e);
} else {
throw e24467;

}
}}));
shadow.dom.dom_listen_remove = (cljs.core.truth_((function (){var or__5002__auto__ = (!((typeof document !== 'undefined')));
if(or__5002__auto__){
return or__5002__auto__;
} else {
return document.removeEventListener;
}
})())?(function shadow$dom$dom_listen_remove_good(el,ev,handler){
return el.removeEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_remove_ie(el,ev,handler){
return el.detachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),handler);
}));
shadow.dom.on_query = (function shadow$dom$on_query(root_el,ev,selector,handler){
var seq__24476 = cljs.core.seq(shadow.dom.query.cljs$core$IFn$_invoke$arity$2(selector,root_el));
var chunk__24477 = null;
var count__24478 = (0);
var i__24479 = (0);
while(true){
if((i__24479 < count__24478)){
var el = chunk__24477.cljs$core$IIndexed$_nth$arity$2(null, i__24479);
var handler_25464__$1 = ((function (seq__24476,chunk__24477,count__24478,i__24479,el){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null, e,el));
});})(seq__24476,chunk__24477,count__24478,i__24479,el))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_25464__$1);


var G__25465 = seq__24476;
var G__25466 = chunk__24477;
var G__25467 = count__24478;
var G__25468 = (i__24479 + (1));
seq__24476 = G__25465;
chunk__24477 = G__25466;
count__24478 = G__25467;
i__24479 = G__25468;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__24476);
if(temp__5804__auto__){
var seq__24476__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__24476__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__24476__$1);
var G__25477 = cljs.core.chunk_rest(seq__24476__$1);
var G__25478 = c__5525__auto__;
var G__25479 = cljs.core.count(c__5525__auto__);
var G__25480 = (0);
seq__24476 = G__25477;
chunk__24477 = G__25478;
count__24478 = G__25479;
i__24479 = G__25480;
continue;
} else {
var el = cljs.core.first(seq__24476__$1);
var handler_25482__$1 = ((function (seq__24476,chunk__24477,count__24478,i__24479,el,seq__24476__$1,temp__5804__auto__){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null, e,el));
});})(seq__24476,chunk__24477,count__24478,i__24479,el,seq__24476__$1,temp__5804__auto__))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_25482__$1);


var G__25483 = cljs.core.next(seq__24476__$1);
var G__25484 = null;
var G__25485 = (0);
var G__25486 = (0);
seq__24476 = G__25483;
chunk__24477 = G__25484;
count__24478 = G__25485;
i__24479 = G__25486;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.on = (function shadow$dom$on(var_args){
var G__24499 = arguments.length;
switch (G__24499) {
case 3:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.on.cljs$core$IFn$_invoke$arity$3 = (function (el,ev,handler){
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4(el,ev,handler,false);
}));

(shadow.dom.on.cljs$core$IFn$_invoke$arity$4 = (function (el,ev,handler,capture){
if(cljs.core.vector_QMARK_(ev)){
return shadow.dom.on_query(el,cljs.core.first(ev),cljs.core.second(ev),handler);
} else {
var handler__$1 = (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null, e,el));
});
return shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(ev),handler__$1);
}
}));

(shadow.dom.on.cljs$lang$maxFixedArity = 4);

shadow.dom.remove_event_handler = (function shadow$dom$remove_event_handler(el,ev,handler){
return shadow.dom.dom_listen_remove(shadow.dom.dom_node(el),cljs.core.name(ev),handler);
});
shadow.dom.add_event_listeners = (function shadow$dom$add_event_listeners(el,events){
var seq__24511 = cljs.core.seq(events);
var chunk__24513 = null;
var count__24514 = (0);
var i__24515 = (0);
while(true){
if((i__24515 < count__24514)){
var vec__24527 = chunk__24513.cljs$core$IIndexed$_nth$arity$2(null, i__24515);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24527,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24527,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__25490 = seq__24511;
var G__25491 = chunk__24513;
var G__25492 = count__24514;
var G__25493 = (i__24515 + (1));
seq__24511 = G__25490;
chunk__24513 = G__25491;
count__24514 = G__25492;
i__24515 = G__25493;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__24511);
if(temp__5804__auto__){
var seq__24511__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__24511__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__24511__$1);
var G__25494 = cljs.core.chunk_rest(seq__24511__$1);
var G__25495 = c__5525__auto__;
var G__25496 = cljs.core.count(c__5525__auto__);
var G__25497 = (0);
seq__24511 = G__25494;
chunk__24513 = G__25495;
count__24514 = G__25496;
i__24515 = G__25497;
continue;
} else {
var vec__24533 = cljs.core.first(seq__24511__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24533,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24533,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__25501 = cljs.core.next(seq__24511__$1);
var G__25502 = null;
var G__25503 = (0);
var G__25504 = (0);
seq__24511 = G__25501;
chunk__24513 = G__25502;
count__24514 = G__25503;
i__24515 = G__25504;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_style = (function shadow$dom$set_style(el,styles){
var dom = shadow.dom.dom_node(el);
var seq__24539 = cljs.core.seq(styles);
var chunk__24540 = null;
var count__24541 = (0);
var i__24542 = (0);
while(true){
if((i__24542 < count__24541)){
var vec__24556 = chunk__24540.cljs$core$IIndexed$_nth$arity$2(null, i__24542);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24556,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24556,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__25515 = seq__24539;
var G__25516 = chunk__24540;
var G__25517 = count__24541;
var G__25518 = (i__24542 + (1));
seq__24539 = G__25515;
chunk__24540 = G__25516;
count__24541 = G__25517;
i__24542 = G__25518;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__24539);
if(temp__5804__auto__){
var seq__24539__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__24539__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__24539__$1);
var G__25519 = cljs.core.chunk_rest(seq__24539__$1);
var G__25520 = c__5525__auto__;
var G__25521 = cljs.core.count(c__5525__auto__);
var G__25522 = (0);
seq__24539 = G__25519;
chunk__24540 = G__25520;
count__24541 = G__25521;
i__24542 = G__25522;
continue;
} else {
var vec__24564 = cljs.core.first(seq__24539__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24564,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24564,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__25523 = cljs.core.next(seq__24539__$1);
var G__25524 = null;
var G__25525 = (0);
var G__25526 = (0);
seq__24539 = G__25523;
chunk__24540 = G__25524;
count__24541 = G__25525;
i__24542 = G__25526;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_attr_STAR_ = (function shadow$dom$set_attr_STAR_(el,key,value){
var G__24575_25527 = key;
var G__24575_25528__$1 = (((G__24575_25527 instanceof cljs.core.Keyword))?G__24575_25527.fqn:null);
switch (G__24575_25528__$1) {
case "id":
(el.id = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "class":
(el.className = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "for":
(el.htmlFor = value);

break;
case "cellpadding":
el.setAttribute("cellPadding",value);

break;
case "cellspacing":
el.setAttribute("cellSpacing",value);

break;
case "colspan":
el.setAttribute("colSpan",value);

break;
case "frameborder":
el.setAttribute("frameBorder",value);

break;
case "height":
el.setAttribute("height",value);

break;
case "maxlength":
el.setAttribute("maxLength",value);

break;
case "role":
el.setAttribute("role",value);

break;
case "rowspan":
el.setAttribute("rowSpan",value);

break;
case "type":
el.setAttribute("type",value);

break;
case "usemap":
el.setAttribute("useMap",value);

break;
case "valign":
el.setAttribute("vAlign",value);

break;
case "width":
el.setAttribute("width",value);

break;
case "on":
shadow.dom.add_event_listeners(el,value);

break;
case "style":
if((value == null)){
} else {
if(typeof value === 'string'){
el.setAttribute("style",value);
} else {
if(cljs.core.map_QMARK_(value)){
shadow.dom.set_style(el,value);
} else {
goog.style.setStyle(el,value);

}
}
}

break;
default:
var ks_25532 = cljs.core.name(key);
if(cljs.core.truth_((function (){var or__5002__auto__ = goog.string.startsWith(ks_25532,"data-");
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return goog.string.startsWith(ks_25532,"aria-");
}
})())){
el.setAttribute(ks_25532,value);
} else {
(el[ks_25532] = value);
}

}

return el;
});
shadow.dom.set_attrs = (function shadow$dom$set_attrs(el,attrs){
return cljs.core.reduce_kv((function (el__$1,key,value){
shadow.dom.set_attr_STAR_(el__$1,key,value);

return el__$1;
}),shadow.dom.dom_node(el),attrs);
});
shadow.dom.set_attr = (function shadow$dom$set_attr(el,key,value){
return shadow.dom.set_attr_STAR_(shadow.dom.dom_node(el),key,value);
});
shadow.dom.has_class_QMARK_ = (function shadow$dom$has_class_QMARK_(el,cls){
return goog.dom.classlist.contains(shadow.dom.dom_node(el),cls);
});
shadow.dom.merge_class_string = (function shadow$dom$merge_class_string(current,extra_class){
if(cljs.core.seq(current)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(current)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(extra_class)].join('');
} else {
return extra_class;
}
});
shadow.dom.parse_tag = (function shadow$dom$parse_tag(spec){
var spec__$1 = cljs.core.name(spec);
var fdot = spec__$1.indexOf(".");
var fhash = spec__$1.indexOf("#");
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1,null,null], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fdot),null,clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1))),null], null);
} else {
if((fhash > fdot)){
throw ["cant have id after class?",spec__$1].join('');
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1)),fdot),clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);

}
}
}
}
});
shadow.dom.create_dom_node = (function shadow$dom$create_dom_node(tag_def,p__24599){
var map__24600 = p__24599;
var map__24600__$1 = cljs.core.__destructure_map(map__24600);
var props = map__24600__$1;
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__24600__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var tag_props = ({});
var vec__24606 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24606,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24606,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24606,(2),null);
if(cljs.core.truth_(tag_id)){
(tag_props["id"] = tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
(tag_props["class"] = shadow.dom.merge_class_string(class$,tag_classes));
} else {
}

var G__24616 = goog.dom.createDom(tag_name,tag_props);
shadow.dom.set_attrs(G__24616,cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(props,new cljs.core.Keyword(null,"class","class",-2030961996)));

return G__24616;
});
shadow.dom.append = (function shadow$dom$append(var_args){
var G__24621 = arguments.length;
switch (G__24621) {
case 1:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.append.cljs$core$IFn$_invoke$arity$1 = (function (node){
if(cljs.core.truth_(node)){
var temp__5804__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5804__auto__)){
var n = temp__5804__auto__;
document.body.appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$core$IFn$_invoke$arity$2 = (function (el,node){
if(cljs.core.truth_(node)){
var temp__5804__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5804__auto__)){
var n = temp__5804__auto__;
shadow.dom.dom_node(el).appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$lang$maxFixedArity = 2);

shadow.dom.destructure_node = (function shadow$dom$destructure_node(create_fn,p__24629){
var vec__24630 = p__24629;
var seq__24631 = cljs.core.seq(vec__24630);
var first__24632 = cljs.core.first(seq__24631);
var seq__24631__$1 = cljs.core.next(seq__24631);
var nn = first__24632;
var first__24632__$1 = cljs.core.first(seq__24631__$1);
var seq__24631__$2 = cljs.core.next(seq__24631__$1);
var np = first__24632__$1;
var nc = seq__24631__$2;
var node = vec__24630;
if((nn instanceof cljs.core.Keyword)){
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("invalid dom node",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"node","node",581201198),node], null));
}

if((((np == null)) && ((nc == null)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__24637 = nn;
var G__24638 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__24637,G__24638) : create_fn.call(null, G__24637,G__24638));
})(),cljs.core.List.EMPTY], null);
} else {
if(cljs.core.map_QMARK_(np)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(nn,np) : create_fn.call(null, nn,np)),nc], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__24640 = nn;
var G__24641 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__24640,G__24641) : create_fn.call(null, G__24640,G__24641));
})(),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(nc,np)], null);

}
}
});
shadow.dom.make_dom_node = (function shadow$dom$make_dom_node(structure){
var vec__24646 = shadow.dom.destructure_node(shadow.dom.create_dom_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24646,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24646,(1),null);
var seq__24650_25546 = cljs.core.seq(node_children);
var chunk__24651_25547 = null;
var count__24652_25548 = (0);
var i__24653_25549 = (0);
while(true){
if((i__24653_25549 < count__24652_25548)){
var child_struct_25550 = chunk__24651_25547.cljs$core$IIndexed$_nth$arity$2(null, i__24653_25549);
var children_25551 = shadow.dom.dom_node(child_struct_25550);
if(cljs.core.seq_QMARK_(children_25551)){
var seq__24698_25552 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_25551));
var chunk__24700_25553 = null;
var count__24701_25554 = (0);
var i__24702_25555 = (0);
while(true){
if((i__24702_25555 < count__24701_25554)){
var child_25558 = chunk__24700_25553.cljs$core$IIndexed$_nth$arity$2(null, i__24702_25555);
if(cljs.core.truth_(child_25558)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_25558);


var G__25559 = seq__24698_25552;
var G__25560 = chunk__24700_25553;
var G__25561 = count__24701_25554;
var G__25562 = (i__24702_25555 + (1));
seq__24698_25552 = G__25559;
chunk__24700_25553 = G__25560;
count__24701_25554 = G__25561;
i__24702_25555 = G__25562;
continue;
} else {
var G__25563 = seq__24698_25552;
var G__25564 = chunk__24700_25553;
var G__25565 = count__24701_25554;
var G__25566 = (i__24702_25555 + (1));
seq__24698_25552 = G__25563;
chunk__24700_25553 = G__25564;
count__24701_25554 = G__25565;
i__24702_25555 = G__25566;
continue;
}
} else {
var temp__5804__auto___25567 = cljs.core.seq(seq__24698_25552);
if(temp__5804__auto___25567){
var seq__24698_25568__$1 = temp__5804__auto___25567;
if(cljs.core.chunked_seq_QMARK_(seq__24698_25568__$1)){
var c__5525__auto___25569 = cljs.core.chunk_first(seq__24698_25568__$1);
var G__25570 = cljs.core.chunk_rest(seq__24698_25568__$1);
var G__25571 = c__5525__auto___25569;
var G__25572 = cljs.core.count(c__5525__auto___25569);
var G__25573 = (0);
seq__24698_25552 = G__25570;
chunk__24700_25553 = G__25571;
count__24701_25554 = G__25572;
i__24702_25555 = G__25573;
continue;
} else {
var child_25574 = cljs.core.first(seq__24698_25568__$1);
if(cljs.core.truth_(child_25574)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_25574);


var G__25575 = cljs.core.next(seq__24698_25568__$1);
var G__25576 = null;
var G__25577 = (0);
var G__25578 = (0);
seq__24698_25552 = G__25575;
chunk__24700_25553 = G__25576;
count__24701_25554 = G__25577;
i__24702_25555 = G__25578;
continue;
} else {
var G__25579 = cljs.core.next(seq__24698_25568__$1);
var G__25580 = null;
var G__25581 = (0);
var G__25582 = (0);
seq__24698_25552 = G__25579;
chunk__24700_25553 = G__25580;
count__24701_25554 = G__25581;
i__24702_25555 = G__25582;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_25551);
}


var G__25583 = seq__24650_25546;
var G__25584 = chunk__24651_25547;
var G__25585 = count__24652_25548;
var G__25586 = (i__24653_25549 + (1));
seq__24650_25546 = G__25583;
chunk__24651_25547 = G__25584;
count__24652_25548 = G__25585;
i__24653_25549 = G__25586;
continue;
} else {
var temp__5804__auto___25587 = cljs.core.seq(seq__24650_25546);
if(temp__5804__auto___25587){
var seq__24650_25588__$1 = temp__5804__auto___25587;
if(cljs.core.chunked_seq_QMARK_(seq__24650_25588__$1)){
var c__5525__auto___25589 = cljs.core.chunk_first(seq__24650_25588__$1);
var G__25590 = cljs.core.chunk_rest(seq__24650_25588__$1);
var G__25591 = c__5525__auto___25589;
var G__25592 = cljs.core.count(c__5525__auto___25589);
var G__25593 = (0);
seq__24650_25546 = G__25590;
chunk__24651_25547 = G__25591;
count__24652_25548 = G__25592;
i__24653_25549 = G__25593;
continue;
} else {
var child_struct_25595 = cljs.core.first(seq__24650_25588__$1);
var children_25597 = shadow.dom.dom_node(child_struct_25595);
if(cljs.core.seq_QMARK_(children_25597)){
var seq__24712_25598 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_25597));
var chunk__24714_25599 = null;
var count__24715_25600 = (0);
var i__24716_25601 = (0);
while(true){
if((i__24716_25601 < count__24715_25600)){
var child_25602 = chunk__24714_25599.cljs$core$IIndexed$_nth$arity$2(null, i__24716_25601);
if(cljs.core.truth_(child_25602)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_25602);


var G__25603 = seq__24712_25598;
var G__25604 = chunk__24714_25599;
var G__25605 = count__24715_25600;
var G__25606 = (i__24716_25601 + (1));
seq__24712_25598 = G__25603;
chunk__24714_25599 = G__25604;
count__24715_25600 = G__25605;
i__24716_25601 = G__25606;
continue;
} else {
var G__25607 = seq__24712_25598;
var G__25608 = chunk__24714_25599;
var G__25609 = count__24715_25600;
var G__25610 = (i__24716_25601 + (1));
seq__24712_25598 = G__25607;
chunk__24714_25599 = G__25608;
count__24715_25600 = G__25609;
i__24716_25601 = G__25610;
continue;
}
} else {
var temp__5804__auto___25611__$1 = cljs.core.seq(seq__24712_25598);
if(temp__5804__auto___25611__$1){
var seq__24712_25612__$1 = temp__5804__auto___25611__$1;
if(cljs.core.chunked_seq_QMARK_(seq__24712_25612__$1)){
var c__5525__auto___25613 = cljs.core.chunk_first(seq__24712_25612__$1);
var G__25614 = cljs.core.chunk_rest(seq__24712_25612__$1);
var G__25615 = c__5525__auto___25613;
var G__25616 = cljs.core.count(c__5525__auto___25613);
var G__25617 = (0);
seq__24712_25598 = G__25614;
chunk__24714_25599 = G__25615;
count__24715_25600 = G__25616;
i__24716_25601 = G__25617;
continue;
} else {
var child_25618 = cljs.core.first(seq__24712_25612__$1);
if(cljs.core.truth_(child_25618)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_25618);


var G__25619 = cljs.core.next(seq__24712_25612__$1);
var G__25620 = null;
var G__25621 = (0);
var G__25622 = (0);
seq__24712_25598 = G__25619;
chunk__24714_25599 = G__25620;
count__24715_25600 = G__25621;
i__24716_25601 = G__25622;
continue;
} else {
var G__25624 = cljs.core.next(seq__24712_25612__$1);
var G__25626 = null;
var G__25627 = (0);
var G__25628 = (0);
seq__24712_25598 = G__25624;
chunk__24714_25599 = G__25626;
count__24715_25600 = G__25627;
i__24716_25601 = G__25628;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_25597);
}


var G__25630 = cljs.core.next(seq__24650_25588__$1);
var G__25631 = null;
var G__25632 = (0);
var G__25633 = (0);
seq__24650_25546 = G__25630;
chunk__24651_25547 = G__25631;
count__24652_25548 = G__25632;
i__24653_25549 = G__25633;
continue;
}
} else {
}
}
break;
}

return node;
});
(cljs.core.Keyword.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.Keyword.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$__$1], null));
}));

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_dom,this$__$1);
}));
if(cljs.core.truth_(((typeof HTMLElement) != 'undefined'))){
(HTMLElement.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(HTMLElement.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
if(cljs.core.truth_(((typeof DocumentFragment) != 'undefined'))){
(DocumentFragment.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(DocumentFragment.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
/**
 * clear node children
 */
shadow.dom.reset = (function shadow$dom$reset(node){
return goog.dom.removeChildren(shadow.dom.dom_node(node));
});
shadow.dom.remove = (function shadow$dom$remove(node){
if((((!((node == null))))?(((((node.cljs$lang$protocol_mask$partition0$ & (8388608))) || ((cljs.core.PROTOCOL_SENTINEL === node.cljs$core$ISeqable$))))?true:false):false)){
var seq__24742 = cljs.core.seq(node);
var chunk__24743 = null;
var count__24744 = (0);
var i__24745 = (0);
while(true){
if((i__24745 < count__24744)){
var n = chunk__24743.cljs$core$IIndexed$_nth$arity$2(null, i__24745);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null, n));


var G__25638 = seq__24742;
var G__25639 = chunk__24743;
var G__25640 = count__24744;
var G__25641 = (i__24745 + (1));
seq__24742 = G__25638;
chunk__24743 = G__25639;
count__24744 = G__25640;
i__24745 = G__25641;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__24742);
if(temp__5804__auto__){
var seq__24742__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__24742__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__24742__$1);
var G__25642 = cljs.core.chunk_rest(seq__24742__$1);
var G__25643 = c__5525__auto__;
var G__25644 = cljs.core.count(c__5525__auto__);
var G__25645 = (0);
seq__24742 = G__25642;
chunk__24743 = G__25643;
count__24744 = G__25644;
i__24745 = G__25645;
continue;
} else {
var n = cljs.core.first(seq__24742__$1);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null, n));


var G__25646 = cljs.core.next(seq__24742__$1);
var G__25647 = null;
var G__25648 = (0);
var G__25649 = (0);
seq__24742 = G__25646;
chunk__24743 = G__25647;
count__24744 = G__25648;
i__24745 = G__25649;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return goog.dom.removeNode(node);
}
});
shadow.dom.replace_node = (function shadow$dom$replace_node(old,new$){
return goog.dom.replaceNode(shadow.dom.dom_node(new$),shadow.dom.dom_node(old));
});
shadow.dom.text = (function shadow$dom$text(var_args){
var G__24756 = arguments.length;
switch (G__24756) {
case 2:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.text.cljs$core$IFn$_invoke$arity$2 = (function (el,new_text){
return (shadow.dom.dom_node(el).innerText = new_text);
}));

(shadow.dom.text.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.dom_node(el).innerText;
}));

(shadow.dom.text.cljs$lang$maxFixedArity = 2);

shadow.dom.check = (function shadow$dom$check(var_args){
var G__24769 = arguments.length;
switch (G__24769) {
case 1:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.check.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2(el,true);
}));

(shadow.dom.check.cljs$core$IFn$_invoke$arity$2 = (function (el,checked){
return (shadow.dom.dom_node(el).checked = checked);
}));

(shadow.dom.check.cljs$lang$maxFixedArity = 2);

shadow.dom.checked_QMARK_ = (function shadow$dom$checked_QMARK_(el){
return shadow.dom.dom_node(el).checked;
});
shadow.dom.form_elements = (function shadow$dom$form_elements(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).elements));
});
shadow.dom.children = (function shadow$dom$children(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).children));
});
shadow.dom.child_nodes = (function shadow$dom$child_nodes(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).childNodes));
});
shadow.dom.attr = (function shadow$dom$attr(var_args){
var G__24785 = arguments.length;
switch (G__24785) {
case 2:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$2 = (function (el,key){
return shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
}));

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$3 = (function (el,key,default$){
var or__5002__auto__ = shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return default$;
}
}));

(shadow.dom.attr.cljs$lang$maxFixedArity = 3);

shadow.dom.del_attr = (function shadow$dom$del_attr(el,key){
return shadow.dom.dom_node(el).removeAttribute(cljs.core.name(key));
});
shadow.dom.data = (function shadow$dom$data(el,key){
return shadow.dom.dom_node(el).getAttribute(["data-",cljs.core.name(key)].join(''));
});
shadow.dom.set_data = (function shadow$dom$set_data(el,key,value){
return shadow.dom.dom_node(el).setAttribute(["data-",cljs.core.name(key)].join(''),cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));
});
shadow.dom.set_html = (function shadow$dom$set_html(node,text){
return (shadow.dom.dom_node(node).innerHTML = text);
});
shadow.dom.get_html = (function shadow$dom$get_html(node){
return shadow.dom.dom_node(node).innerHTML;
});
shadow.dom.fragment = (function shadow$dom$fragment(var_args){
var args__5732__auto__ = [];
var len__5726__auto___25656 = arguments.length;
var i__5727__auto___25657 = (0);
while(true){
if((i__5727__auto___25657 < len__5726__auto___25656)){
args__5732__auto__.push((arguments[i__5727__auto___25657]));

var G__25658 = (i__5727__auto___25657 + (1));
i__5727__auto___25657 = G__25658;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((0) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((0)),(0),null)):null);
return shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic(argseq__5733__auto__);
});

(shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic = (function (nodes){
var fragment = document.createDocumentFragment();
var seq__24809_25659 = cljs.core.seq(nodes);
var chunk__24810_25660 = null;
var count__24811_25661 = (0);
var i__24812_25662 = (0);
while(true){
if((i__24812_25662 < count__24811_25661)){
var node_25663 = chunk__24810_25660.cljs$core$IIndexed$_nth$arity$2(null, i__24812_25662);
fragment.appendChild(shadow.dom._to_dom(node_25663));


var G__25664 = seq__24809_25659;
var G__25665 = chunk__24810_25660;
var G__25666 = count__24811_25661;
var G__25667 = (i__24812_25662 + (1));
seq__24809_25659 = G__25664;
chunk__24810_25660 = G__25665;
count__24811_25661 = G__25666;
i__24812_25662 = G__25667;
continue;
} else {
var temp__5804__auto___25668 = cljs.core.seq(seq__24809_25659);
if(temp__5804__auto___25668){
var seq__24809_25669__$1 = temp__5804__auto___25668;
if(cljs.core.chunked_seq_QMARK_(seq__24809_25669__$1)){
var c__5525__auto___25670 = cljs.core.chunk_first(seq__24809_25669__$1);
var G__25671 = cljs.core.chunk_rest(seq__24809_25669__$1);
var G__25672 = c__5525__auto___25670;
var G__25673 = cljs.core.count(c__5525__auto___25670);
var G__25674 = (0);
seq__24809_25659 = G__25671;
chunk__24810_25660 = G__25672;
count__24811_25661 = G__25673;
i__24812_25662 = G__25674;
continue;
} else {
var node_25675 = cljs.core.first(seq__24809_25669__$1);
fragment.appendChild(shadow.dom._to_dom(node_25675));


var G__25676 = cljs.core.next(seq__24809_25669__$1);
var G__25677 = null;
var G__25678 = (0);
var G__25679 = (0);
seq__24809_25659 = G__25676;
chunk__24810_25660 = G__25677;
count__24811_25661 = G__25678;
i__24812_25662 = G__25679;
continue;
}
} else {
}
}
break;
}

return (new shadow.dom.NativeColl(fragment));
}));

(shadow.dom.fragment.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(shadow.dom.fragment.cljs$lang$applyTo = (function (seq24800){
var self__5712__auto__ = this;
return self__5712__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq24800));
}));

/**
 * given a html string, eval all <script> tags and return the html without the scripts
 * don't do this for everything, only content you trust.
 */
shadow.dom.eval_scripts = (function shadow$dom$eval_scripts(s){
var scripts = cljs.core.re_seq(/<script[^>]*?>(.+?)<\/script>/,s);
var seq__24825_25682 = cljs.core.seq(scripts);
var chunk__24826_25683 = null;
var count__24827_25684 = (0);
var i__24828_25685 = (0);
while(true){
if((i__24828_25685 < count__24827_25684)){
var vec__24849_25688 = chunk__24826_25683.cljs$core$IIndexed$_nth$arity$2(null, i__24828_25685);
var script_tag_25689 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24849_25688,(0),null);
var script_body_25690 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24849_25688,(1),null);
eval(script_body_25690);


var G__25691 = seq__24825_25682;
var G__25692 = chunk__24826_25683;
var G__25693 = count__24827_25684;
var G__25694 = (i__24828_25685 + (1));
seq__24825_25682 = G__25691;
chunk__24826_25683 = G__25692;
count__24827_25684 = G__25693;
i__24828_25685 = G__25694;
continue;
} else {
var temp__5804__auto___25695 = cljs.core.seq(seq__24825_25682);
if(temp__5804__auto___25695){
var seq__24825_25696__$1 = temp__5804__auto___25695;
if(cljs.core.chunked_seq_QMARK_(seq__24825_25696__$1)){
var c__5525__auto___25697 = cljs.core.chunk_first(seq__24825_25696__$1);
var G__25698 = cljs.core.chunk_rest(seq__24825_25696__$1);
var G__25699 = c__5525__auto___25697;
var G__25700 = cljs.core.count(c__5525__auto___25697);
var G__25701 = (0);
seq__24825_25682 = G__25698;
chunk__24826_25683 = G__25699;
count__24827_25684 = G__25700;
i__24828_25685 = G__25701;
continue;
} else {
var vec__24855_25703 = cljs.core.first(seq__24825_25696__$1);
var script_tag_25704 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24855_25703,(0),null);
var script_body_25705 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24855_25703,(1),null);
eval(script_body_25705);


var G__25706 = cljs.core.next(seq__24825_25696__$1);
var G__25707 = null;
var G__25708 = (0);
var G__25709 = (0);
seq__24825_25682 = G__25706;
chunk__24826_25683 = G__25707;
count__24827_25684 = G__25708;
i__24828_25685 = G__25709;
continue;
}
} else {
}
}
break;
}

return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (s__$1,p__24860){
var vec__24865 = p__24860;
var script_tag = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24865,(0),null);
var script_body = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24865,(1),null);
return clojure.string.replace(s__$1,script_tag,"");
}),s,scripts);
});
shadow.dom.str__GT_fragment = (function shadow$dom$str__GT_fragment(s){
var el = document.createElement("div");
(el.innerHTML = s);

return (new shadow.dom.NativeColl(goog.dom.childrenToNode_(document,el)));
});
shadow.dom.node_name = (function shadow$dom$node_name(el){
return shadow.dom.dom_node(el).nodeName;
});
shadow.dom.ancestor_by_class = (function shadow$dom$ancestor_by_class(el,cls){
return goog.dom.getAncestorByClass(shadow.dom.dom_node(el),cls);
});
shadow.dom.ancestor_by_tag = (function shadow$dom$ancestor_by_tag(var_args){
var G__24881 = arguments.length;
switch (G__24881) {
case 2:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2 = (function (el,tag){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag));
}));

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3 = (function (el,tag,cls){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag),cljs.core.name(cls));
}));

(shadow.dom.ancestor_by_tag.cljs$lang$maxFixedArity = 3);

shadow.dom.get_value = (function shadow$dom$get_value(dom){
return goog.dom.forms.getValue(shadow.dom.dom_node(dom));
});
shadow.dom.set_value = (function shadow$dom$set_value(dom,value){
return goog.dom.forms.setValue(shadow.dom.dom_node(dom),value);
});
shadow.dom.px = (function shadow$dom$px(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1((value | (0))),"px"].join('');
});
shadow.dom.pct = (function shadow$dom$pct(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(value),"%"].join('');
});
shadow.dom.remove_style_STAR_ = (function shadow$dom$remove_style_STAR_(el,style){
return el.style.removeProperty(cljs.core.name(style));
});
shadow.dom.remove_style = (function shadow$dom$remove_style(el,style){
var el__$1 = shadow.dom.dom_node(el);
return shadow.dom.remove_style_STAR_(el__$1,style);
});
shadow.dom.remove_styles = (function shadow$dom$remove_styles(el,style_keys){
var el__$1 = shadow.dom.dom_node(el);
var seq__24891 = cljs.core.seq(style_keys);
var chunk__24892 = null;
var count__24893 = (0);
var i__24894 = (0);
while(true){
if((i__24894 < count__24893)){
var it = chunk__24892.cljs$core$IIndexed$_nth$arity$2(null, i__24894);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__25723 = seq__24891;
var G__25724 = chunk__24892;
var G__25725 = count__24893;
var G__25726 = (i__24894 + (1));
seq__24891 = G__25723;
chunk__24892 = G__25724;
count__24893 = G__25725;
i__24894 = G__25726;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__24891);
if(temp__5804__auto__){
var seq__24891__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__24891__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__24891__$1);
var G__25728 = cljs.core.chunk_rest(seq__24891__$1);
var G__25729 = c__5525__auto__;
var G__25730 = cljs.core.count(c__5525__auto__);
var G__25731 = (0);
seq__24891 = G__25728;
chunk__24892 = G__25729;
count__24893 = G__25730;
i__24894 = G__25731;
continue;
} else {
var it = cljs.core.first(seq__24891__$1);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__25732 = cljs.core.next(seq__24891__$1);
var G__25733 = null;
var G__25734 = (0);
var G__25735 = (0);
seq__24891 = G__25732;
chunk__24892 = G__25733;
count__24893 = G__25734;
i__24894 = G__25735;
continue;
}
} else {
return null;
}
}
break;
}
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Coordinate = (function (x,y,__meta,__extmap,__hash){
this.x = x;
this.y = y;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__5300__auto__,k__5301__auto__){
var self__ = this;
var this__5300__auto____$1 = this;
return this__5300__auto____$1.cljs$core$ILookup$_lookup$arity$3(null, k__5301__auto__,null);
}));

(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__5302__auto__,k24898,else__5303__auto__){
var self__ = this;
var this__5302__auto____$1 = this;
var G__24929 = k24898;
var G__24929__$1 = (((G__24929 instanceof cljs.core.Keyword))?G__24929.fqn:null);
switch (G__24929__$1) {
case "x":
return self__.x;

break;
case "y":
return self__.y;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k24898,else__5303__auto__);

}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__5320__auto__,f__5321__auto__,init__5322__auto__){
var self__ = this;
var this__5320__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__5323__auto__,p__24940){
var vec__24941 = p__24940;
var k__5324__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24941,(0),null);
var v__5325__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24941,(1),null);
return (f__5321__auto__.cljs$core$IFn$_invoke$arity$3 ? f__5321__auto__.cljs$core$IFn$_invoke$arity$3(ret__5323__auto__,k__5324__auto__,v__5325__auto__) : f__5321__auto__.call(null, ret__5323__auto__,k__5324__auto__,v__5325__auto__));
}),init__5322__auto__,this__5320__auto____$1);
}));

(shadow.dom.Coordinate.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__5315__auto__,writer__5316__auto__,opts__5317__auto__){
var self__ = this;
var this__5315__auto____$1 = this;
var pr_pair__5318__auto__ = (function (keyval__5319__auto__){
return cljs.core.pr_sequential_writer(writer__5316__auto__,cljs.core.pr_writer,""," ","",opts__5317__auto__,keyval__5319__auto__);
});
return cljs.core.pr_sequential_writer(writer__5316__auto__,pr_pair__5318__auto__,"#shadow.dom.Coordinate{",", ","}",opts__5317__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"x","x",2099068185),self__.x],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"y","y",-1757859776),self__.y],null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__24897){
var self__ = this;
var G__24897__$1 = this;
return (new cljs.core.RecordIter((0),G__24897__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"x","x",2099068185),new cljs.core.Keyword(null,"y","y",-1757859776)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__5298__auto__){
var self__ = this;
var this__5298__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__5295__auto__){
var self__ = this;
var this__5295__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__5304__auto__){
var self__ = this;
var this__5304__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__5296__auto__){
var self__ = this;
var this__5296__auto____$1 = this;
var h__5111__auto__ = self__.__hash;
if((!((h__5111__auto__ == null)))){
return h__5111__auto__;
} else {
var h__5111__auto____$1 = (function (coll__5297__auto__){
return (145542109 ^ cljs.core.hash_unordered_coll(coll__5297__auto__));
})(this__5296__auto____$1);
(self__.__hash = h__5111__auto____$1);

return h__5111__auto____$1;
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this24899,other24900){
var self__ = this;
var this24899__$1 = this;
return (((!((other24900 == null)))) && ((((this24899__$1.constructor === other24900.constructor)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this24899__$1.x,other24900.x)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this24899__$1.y,other24900.y)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this24899__$1.__extmap,other24900.__extmap)))))))));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__5310__auto__,k__5311__auto__){
var self__ = this;
var this__5310__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"y","y",-1757859776),null,new cljs.core.Keyword(null,"x","x",2099068185),null], null), null),k__5311__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__5310__auto____$1),self__.__meta),k__5311__auto__);
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__5311__auto__)),null));
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_contains_key_QMARK_$arity$2 = (function (this__5307__auto__,k24898){
var self__ = this;
var this__5307__auto____$1 = this;
var G__25008 = k24898;
var G__25008__$1 = (((G__25008 instanceof cljs.core.Keyword))?G__25008.fqn:null);
switch (G__25008__$1) {
case "x":
case "y":
return true;

break;
default:
return cljs.core.contains_QMARK_(self__.__extmap,k24898);

}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__5308__auto__,k__5309__auto__,G__24897){
var self__ = this;
var this__5308__auto____$1 = this;
var pred__25016 = cljs.core.keyword_identical_QMARK_;
var expr__25017 = k__5309__auto__;
if(cljs.core.truth_((pred__25016.cljs$core$IFn$_invoke$arity$2 ? pred__25016.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"x","x",2099068185),expr__25017) : pred__25016.call(null, new cljs.core.Keyword(null,"x","x",2099068185),expr__25017)))){
return (new shadow.dom.Coordinate(G__24897,self__.y,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__25016.cljs$core$IFn$_invoke$arity$2 ? pred__25016.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"y","y",-1757859776),expr__25017) : pred__25016.call(null, new cljs.core.Keyword(null,"y","y",-1757859776),expr__25017)))){
return (new shadow.dom.Coordinate(self__.x,G__24897,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__5309__auto__,G__24897),null));
}
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__5313__auto__){
var self__ = this;
var this__5313__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"x","x",2099068185),self__.x,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"y","y",-1757859776),self__.y,null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__5299__auto__,G__24897){
var self__ = this;
var this__5299__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,G__24897,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__5305__auto__,entry__5306__auto__){
var self__ = this;
var this__5305__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__5306__auto__)){
return this__5305__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null, cljs.core._nth(entry__5306__auto__,(0)),cljs.core._nth(entry__5306__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__5305__auto____$1,entry__5306__auto__);
}
}));

(shadow.dom.Coordinate.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"x","x",-555367584,null),new cljs.core.Symbol(null,"y","y",-117328249,null)], null);
}));

(shadow.dom.Coordinate.cljs$lang$type = true);

(shadow.dom.Coordinate.cljs$lang$ctorPrSeq = (function (this__5346__auto__){
return (new cljs.core.List(null,"shadow.dom/Coordinate",null,(1),null));
}));

(shadow.dom.Coordinate.cljs$lang$ctorPrWriter = (function (this__5346__auto__,writer__5347__auto__){
return cljs.core._write(writer__5347__auto__,"shadow.dom/Coordinate");
}));

/**
 * Positional factory function for shadow.dom/Coordinate.
 */
shadow.dom.__GT_Coordinate = (function shadow$dom$__GT_Coordinate(x,y){
return (new shadow.dom.Coordinate(x,y,null,null,null));
});

/**
 * Factory function for shadow.dom/Coordinate, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Coordinate = (function shadow$dom$map__GT_Coordinate(G__24903){
var extmap__5342__auto__ = (function (){var G__25040 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__24903,new cljs.core.Keyword(null,"x","x",2099068185),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"y","y",-1757859776)], 0));
if(cljs.core.record_QMARK_(G__24903)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__25040);
} else {
return G__25040;
}
})();
return (new shadow.dom.Coordinate(new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(G__24903),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(G__24903),null,cljs.core.not_empty(extmap__5342__auto__),null));
});

shadow.dom.get_position = (function shadow$dom$get_position(el){
var pos = goog.style.getPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_client_position = (function shadow$dom$get_client_position(el){
var pos = goog.style.getClientPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_page_offset = (function shadow$dom$get_page_offset(el){
var pos = goog.style.getPageOffset(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Size = (function (w,h,__meta,__extmap,__hash){
this.w = w;
this.h = h;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__5300__auto__,k__5301__auto__){
var self__ = this;
var this__5300__auto____$1 = this;
return this__5300__auto____$1.cljs$core$ILookup$_lookup$arity$3(null, k__5301__auto__,null);
}));

(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__5302__auto__,k25083,else__5303__auto__){
var self__ = this;
var this__5302__auto____$1 = this;
var G__25097 = k25083;
var G__25097__$1 = (((G__25097 instanceof cljs.core.Keyword))?G__25097.fqn:null);
switch (G__25097__$1) {
case "w":
return self__.w;

break;
case "h":
return self__.h;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k25083,else__5303__auto__);

}
}));

(shadow.dom.Size.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__5320__auto__,f__5321__auto__,init__5322__auto__){
var self__ = this;
var this__5320__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__5323__auto__,p__25104){
var vec__25106 = p__25104;
var k__5324__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25106,(0),null);
var v__5325__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25106,(1),null);
return (f__5321__auto__.cljs$core$IFn$_invoke$arity$3 ? f__5321__auto__.cljs$core$IFn$_invoke$arity$3(ret__5323__auto__,k__5324__auto__,v__5325__auto__) : f__5321__auto__.call(null, ret__5323__auto__,k__5324__auto__,v__5325__auto__));
}),init__5322__auto__,this__5320__auto____$1);
}));

(shadow.dom.Size.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__5315__auto__,writer__5316__auto__,opts__5317__auto__){
var self__ = this;
var this__5315__auto____$1 = this;
var pr_pair__5318__auto__ = (function (keyval__5319__auto__){
return cljs.core.pr_sequential_writer(writer__5316__auto__,cljs.core.pr_writer,""," ","",opts__5317__auto__,keyval__5319__auto__);
});
return cljs.core.pr_sequential_writer(writer__5316__auto__,pr_pair__5318__auto__,"#shadow.dom.Size{",", ","}",opts__5317__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"w","w",354169001),self__.w],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"h","h",1109658740),self__.h],null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__25082){
var self__ = this;
var G__25082__$1 = this;
return (new cljs.core.RecordIter((0),G__25082__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"w","w",354169001),new cljs.core.Keyword(null,"h","h",1109658740)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Size.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__5298__auto__){
var self__ = this;
var this__5298__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Size.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__5295__auto__){
var self__ = this;
var this__5295__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__5304__auto__){
var self__ = this;
var this__5304__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__5296__auto__){
var self__ = this;
var this__5296__auto____$1 = this;
var h__5111__auto__ = self__.__hash;
if((!((h__5111__auto__ == null)))){
return h__5111__auto__;
} else {
var h__5111__auto____$1 = (function (coll__5297__auto__){
return (-1228019642 ^ cljs.core.hash_unordered_coll(coll__5297__auto__));
})(this__5296__auto____$1);
(self__.__hash = h__5111__auto____$1);

return h__5111__auto____$1;
}
}));

(shadow.dom.Size.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this25084,other25085){
var self__ = this;
var this25084__$1 = this;
return (((!((other25085 == null)))) && ((((this25084__$1.constructor === other25085.constructor)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this25084__$1.w,other25085.w)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this25084__$1.h,other25085.h)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this25084__$1.__extmap,other25085.__extmap)))))))));
}));

(shadow.dom.Size.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__5310__auto__,k__5311__auto__){
var self__ = this;
var this__5310__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"w","w",354169001),null,new cljs.core.Keyword(null,"h","h",1109658740),null], null), null),k__5311__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__5310__auto____$1),self__.__meta),k__5311__auto__);
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__5311__auto__)),null));
}
}));

(shadow.dom.Size.prototype.cljs$core$IAssociative$_contains_key_QMARK_$arity$2 = (function (this__5307__auto__,k25083){
var self__ = this;
var this__5307__auto____$1 = this;
var G__25163 = k25083;
var G__25163__$1 = (((G__25163 instanceof cljs.core.Keyword))?G__25163.fqn:null);
switch (G__25163__$1) {
case "w":
case "h":
return true;

break;
default:
return cljs.core.contains_QMARK_(self__.__extmap,k25083);

}
}));

(shadow.dom.Size.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__5308__auto__,k__5309__auto__,G__25082){
var self__ = this;
var this__5308__auto____$1 = this;
var pred__25168 = cljs.core.keyword_identical_QMARK_;
var expr__25169 = k__5309__auto__;
if(cljs.core.truth_((pred__25168.cljs$core$IFn$_invoke$arity$2 ? pred__25168.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"w","w",354169001),expr__25169) : pred__25168.call(null, new cljs.core.Keyword(null,"w","w",354169001),expr__25169)))){
return (new shadow.dom.Size(G__25082,self__.h,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__25168.cljs$core$IFn$_invoke$arity$2 ? pred__25168.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"h","h",1109658740),expr__25169) : pred__25168.call(null, new cljs.core.Keyword(null,"h","h",1109658740),expr__25169)))){
return (new shadow.dom.Size(self__.w,G__25082,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__5309__auto__,G__25082),null));
}
}
}));

(shadow.dom.Size.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__5313__auto__){
var self__ = this;
var this__5313__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"w","w",354169001),self__.w,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"h","h",1109658740),self__.h,null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__5299__auto__,G__25082){
var self__ = this;
var this__5299__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,G__25082,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__5305__auto__,entry__5306__auto__){
var self__ = this;
var this__5305__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__5306__auto__)){
return this__5305__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null, cljs.core._nth(entry__5306__auto__,(0)),cljs.core._nth(entry__5306__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__5305__auto____$1,entry__5306__auto__);
}
}));

(shadow.dom.Size.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"w","w",1994700528,null),new cljs.core.Symbol(null,"h","h",-1544777029,null)], null);
}));

(shadow.dom.Size.cljs$lang$type = true);

(shadow.dom.Size.cljs$lang$ctorPrSeq = (function (this__5346__auto__){
return (new cljs.core.List(null,"shadow.dom/Size",null,(1),null));
}));

(shadow.dom.Size.cljs$lang$ctorPrWriter = (function (this__5346__auto__,writer__5347__auto__){
return cljs.core._write(writer__5347__auto__,"shadow.dom/Size");
}));

/**
 * Positional factory function for shadow.dom/Size.
 */
shadow.dom.__GT_Size = (function shadow$dom$__GT_Size(w,h){
return (new shadow.dom.Size(w,h,null,null,null));
});

/**
 * Factory function for shadow.dom/Size, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Size = (function shadow$dom$map__GT_Size(G__25088){
var extmap__5342__auto__ = (function (){var G__25182 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__25088,new cljs.core.Keyword(null,"w","w",354169001),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"h","h",1109658740)], 0));
if(cljs.core.record_QMARK_(G__25088)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__25182);
} else {
return G__25182;
}
})();
return (new shadow.dom.Size(new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(G__25088),new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(G__25088),null,cljs.core.not_empty(extmap__5342__auto__),null));
});

shadow.dom.size__GT_clj = (function shadow$dom$size__GT_clj(size){
return (new shadow.dom.Size(size.width,size.height,null,null,null));
});
shadow.dom.get_size = (function shadow$dom$get_size(el){
return shadow.dom.size__GT_clj(goog.style.getSize(shadow.dom.dom_node(el)));
});
shadow.dom.get_height = (function shadow$dom$get_height(el){
return shadow.dom.get_size(el).h;
});
shadow.dom.get_viewport_size = (function shadow$dom$get_viewport_size(){
return shadow.dom.size__GT_clj(goog.dom.getViewportSize());
});
shadow.dom.first_child = (function shadow$dom$first_child(el){
return (shadow.dom.dom_node(el).children[(0)]);
});
shadow.dom.select_option_values = (function shadow$dom$select_option_values(el){
var native$ = shadow.dom.dom_node(el);
var opts = (native$["options"]);
var a__5590__auto__ = opts;
var l__5591__auto__ = a__5590__auto__.length;
var i = (0);
var ret = cljs.core.PersistentVector.EMPTY;
while(true){
if((i < l__5591__auto__)){
var G__25782 = (i + (1));
var G__25783 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,(opts[i]["value"]));
i = G__25782;
ret = G__25783;
continue;
} else {
return ret;
}
break;
}
});
shadow.dom.build_url = (function shadow$dom$build_url(path,query_params){
if(cljs.core.empty_QMARK_(query_params)){
return path;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(path),"?",clojure.string.join.cljs$core$IFn$_invoke$arity$2("&",cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__25200){
var vec__25201 = p__25200;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25201,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25201,(1),null);
return [cljs.core.name(k),"=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(encodeURIComponent(cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)))].join('');
}),query_params))].join('');
}
});
shadow.dom.redirect = (function shadow$dom$redirect(var_args){
var G__25227 = arguments.length;
switch (G__25227) {
case 1:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1 = (function (path){
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2(path,cljs.core.PersistentArrayMap.EMPTY);
}));

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2 = (function (path,query_params){
return (document["location"]["href"] = shadow.dom.build_url(path,query_params));
}));

(shadow.dom.redirect.cljs$lang$maxFixedArity = 2);

shadow.dom.reload_BANG_ = (function shadow$dom$reload_BANG_(){
return (document.location.href = document.location.href);
});
shadow.dom.tag_name = (function shadow$dom$tag_name(el){
var dom = shadow.dom.dom_node(el);
return dom.tagName;
});
shadow.dom.insert_after = (function shadow$dom$insert_after(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingAfter(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_before = (function shadow$dom$insert_before(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingBefore(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_first = (function shadow$dom$insert_first(ref,new$){
var temp__5802__auto__ = shadow.dom.dom_node(ref).firstChild;
if(cljs.core.truth_(temp__5802__auto__)){
var child = temp__5802__auto__;
return shadow.dom.insert_before(child,new$);
} else {
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2(ref,new$);
}
});
shadow.dom.index_of = (function shadow$dom$index_of(el){
var el__$1 = shadow.dom.dom_node(el);
var i = (0);
while(true){
var ps = el__$1.previousSibling;
if((ps == null)){
return i;
} else {
var G__25788 = ps;
var G__25789 = (i + (1));
el__$1 = G__25788;
i = G__25789;
continue;
}
break;
}
});
shadow.dom.get_parent = (function shadow$dom$get_parent(el){
return goog.dom.getParentElement(shadow.dom.dom_node(el));
});
shadow.dom.parents = (function shadow$dom$parents(el){
var parent = shadow.dom.get_parent(el);
if(cljs.core.truth_(parent)){
return cljs.core.cons(parent,(new cljs.core.LazySeq(null,(function (){
return (shadow.dom.parents.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.parents.cljs$core$IFn$_invoke$arity$1(parent) : shadow.dom.parents.call(null, parent));
}),null,null)));
} else {
return null;
}
});
shadow.dom.matches = (function shadow$dom$matches(el,sel){
return shadow.dom.dom_node(el).matches(sel);
});
shadow.dom.get_next_sibling = (function shadow$dom$get_next_sibling(el){
return goog.dom.getNextElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.get_previous_sibling = (function shadow$dom$get_previous_sibling(el){
return goog.dom.getPreviousElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.xmlns = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, ["svg","http://www.w3.org/2000/svg","xlink","http://www.w3.org/1999/xlink"], null));
shadow.dom.create_svg_node = (function shadow$dom$create_svg_node(tag_def,props){
var vec__25284 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25284,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25284,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25284,(2),null);
var el = document.createElementNS("http://www.w3.org/2000/svg",tag_name);
if(cljs.core.truth_(tag_id)){
el.setAttribute("id",tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
el.setAttribute("class",shadow.dom.merge_class_string(new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(props),tag_classes));
} else {
}

var seq__25287_25790 = cljs.core.seq(props);
var chunk__25288_25791 = null;
var count__25289_25792 = (0);
var i__25290_25793 = (0);
while(true){
if((i__25290_25793 < count__25289_25792)){
var vec__25306_25794 = chunk__25288_25791.cljs$core$IIndexed$_nth$arity$2(null, i__25290_25793);
var k_25795 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25306_25794,(0),null);
var v_25796 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25306_25794,(1),null);
el.setAttributeNS((function (){var temp__5804__auto__ = cljs.core.namespace(k_25795);
if(cljs.core.truth_(temp__5804__auto__)){
var ns = temp__5804__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_25795),v_25796);


var G__25798 = seq__25287_25790;
var G__25799 = chunk__25288_25791;
var G__25800 = count__25289_25792;
var G__25801 = (i__25290_25793 + (1));
seq__25287_25790 = G__25798;
chunk__25288_25791 = G__25799;
count__25289_25792 = G__25800;
i__25290_25793 = G__25801;
continue;
} else {
var temp__5804__auto___25802 = cljs.core.seq(seq__25287_25790);
if(temp__5804__auto___25802){
var seq__25287_25803__$1 = temp__5804__auto___25802;
if(cljs.core.chunked_seq_QMARK_(seq__25287_25803__$1)){
var c__5525__auto___25804 = cljs.core.chunk_first(seq__25287_25803__$1);
var G__25805 = cljs.core.chunk_rest(seq__25287_25803__$1);
var G__25806 = c__5525__auto___25804;
var G__25807 = cljs.core.count(c__5525__auto___25804);
var G__25808 = (0);
seq__25287_25790 = G__25805;
chunk__25288_25791 = G__25806;
count__25289_25792 = G__25807;
i__25290_25793 = G__25808;
continue;
} else {
var vec__25313_25809 = cljs.core.first(seq__25287_25803__$1);
var k_25810 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25313_25809,(0),null);
var v_25811 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25313_25809,(1),null);
el.setAttributeNS((function (){var temp__5804__auto____$1 = cljs.core.namespace(k_25810);
if(cljs.core.truth_(temp__5804__auto____$1)){
var ns = temp__5804__auto____$1;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_25810),v_25811);


var G__25812 = cljs.core.next(seq__25287_25803__$1);
var G__25813 = null;
var G__25814 = (0);
var G__25815 = (0);
seq__25287_25790 = G__25812;
chunk__25288_25791 = G__25813;
count__25289_25792 = G__25814;
i__25290_25793 = G__25815;
continue;
}
} else {
}
}
break;
}

return el;
});
shadow.dom.svg_node = (function shadow$dom$svg_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$SVGElement$))))?true:false):false)){
return el.shadow$dom$SVGElement$_to_svg$arity$1(null, );
} else {
return el;

}
}
});
shadow.dom.make_svg_node = (function shadow$dom$make_svg_node(structure){
var vec__25318 = shadow.dom.destructure_node(shadow.dom.create_svg_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25318,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25318,(1),null);
var seq__25322_25816 = cljs.core.seq(node_children);
var chunk__25324_25817 = null;
var count__25325_25818 = (0);
var i__25326_25819 = (0);
while(true){
if((i__25326_25819 < count__25325_25818)){
var child_struct_25820 = chunk__25324_25817.cljs$core$IIndexed$_nth$arity$2(null, i__25326_25819);
if((!((child_struct_25820 == null)))){
if(typeof child_struct_25820 === 'string'){
var text_25821 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_25821),child_struct_25820].join(''));
} else {
var children_25822 = shadow.dom.svg_node(child_struct_25820);
if(cljs.core.seq_QMARK_(children_25822)){
var seq__25347_25823 = cljs.core.seq(children_25822);
var chunk__25349_25824 = null;
var count__25350_25825 = (0);
var i__25351_25826 = (0);
while(true){
if((i__25351_25826 < count__25350_25825)){
var child_25827 = chunk__25349_25824.cljs$core$IIndexed$_nth$arity$2(null, i__25351_25826);
if(cljs.core.truth_(child_25827)){
node.appendChild(child_25827);


var G__25829 = seq__25347_25823;
var G__25830 = chunk__25349_25824;
var G__25831 = count__25350_25825;
var G__25832 = (i__25351_25826 + (1));
seq__25347_25823 = G__25829;
chunk__25349_25824 = G__25830;
count__25350_25825 = G__25831;
i__25351_25826 = G__25832;
continue;
} else {
var G__25833 = seq__25347_25823;
var G__25834 = chunk__25349_25824;
var G__25835 = count__25350_25825;
var G__25836 = (i__25351_25826 + (1));
seq__25347_25823 = G__25833;
chunk__25349_25824 = G__25834;
count__25350_25825 = G__25835;
i__25351_25826 = G__25836;
continue;
}
} else {
var temp__5804__auto___25837 = cljs.core.seq(seq__25347_25823);
if(temp__5804__auto___25837){
var seq__25347_25838__$1 = temp__5804__auto___25837;
if(cljs.core.chunked_seq_QMARK_(seq__25347_25838__$1)){
var c__5525__auto___25839 = cljs.core.chunk_first(seq__25347_25838__$1);
var G__25840 = cljs.core.chunk_rest(seq__25347_25838__$1);
var G__25841 = c__5525__auto___25839;
var G__25842 = cljs.core.count(c__5525__auto___25839);
var G__25843 = (0);
seq__25347_25823 = G__25840;
chunk__25349_25824 = G__25841;
count__25350_25825 = G__25842;
i__25351_25826 = G__25843;
continue;
} else {
var child_25844 = cljs.core.first(seq__25347_25838__$1);
if(cljs.core.truth_(child_25844)){
node.appendChild(child_25844);


var G__25845 = cljs.core.next(seq__25347_25838__$1);
var G__25846 = null;
var G__25847 = (0);
var G__25848 = (0);
seq__25347_25823 = G__25845;
chunk__25349_25824 = G__25846;
count__25350_25825 = G__25847;
i__25351_25826 = G__25848;
continue;
} else {
var G__25849 = cljs.core.next(seq__25347_25838__$1);
var G__25850 = null;
var G__25851 = (0);
var G__25852 = (0);
seq__25347_25823 = G__25849;
chunk__25349_25824 = G__25850;
count__25350_25825 = G__25851;
i__25351_25826 = G__25852;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_25822);
}
}


var G__25853 = seq__25322_25816;
var G__25854 = chunk__25324_25817;
var G__25855 = count__25325_25818;
var G__25856 = (i__25326_25819 + (1));
seq__25322_25816 = G__25853;
chunk__25324_25817 = G__25854;
count__25325_25818 = G__25855;
i__25326_25819 = G__25856;
continue;
} else {
var G__25857 = seq__25322_25816;
var G__25858 = chunk__25324_25817;
var G__25859 = count__25325_25818;
var G__25860 = (i__25326_25819 + (1));
seq__25322_25816 = G__25857;
chunk__25324_25817 = G__25858;
count__25325_25818 = G__25859;
i__25326_25819 = G__25860;
continue;
}
} else {
var temp__5804__auto___25861 = cljs.core.seq(seq__25322_25816);
if(temp__5804__auto___25861){
var seq__25322_25862__$1 = temp__5804__auto___25861;
if(cljs.core.chunked_seq_QMARK_(seq__25322_25862__$1)){
var c__5525__auto___25863 = cljs.core.chunk_first(seq__25322_25862__$1);
var G__25864 = cljs.core.chunk_rest(seq__25322_25862__$1);
var G__25865 = c__5525__auto___25863;
var G__25866 = cljs.core.count(c__5525__auto___25863);
var G__25867 = (0);
seq__25322_25816 = G__25864;
chunk__25324_25817 = G__25865;
count__25325_25818 = G__25866;
i__25326_25819 = G__25867;
continue;
} else {
var child_struct_25868 = cljs.core.first(seq__25322_25862__$1);
if((!((child_struct_25868 == null)))){
if(typeof child_struct_25868 === 'string'){
var text_25869 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_25869),child_struct_25868].join(''));
} else {
var children_25871 = shadow.dom.svg_node(child_struct_25868);
if(cljs.core.seq_QMARK_(children_25871)){
var seq__25360_25872 = cljs.core.seq(children_25871);
var chunk__25362_25873 = null;
var count__25363_25874 = (0);
var i__25364_25875 = (0);
while(true){
if((i__25364_25875 < count__25363_25874)){
var child_25876 = chunk__25362_25873.cljs$core$IIndexed$_nth$arity$2(null, i__25364_25875);
if(cljs.core.truth_(child_25876)){
node.appendChild(child_25876);


var G__25877 = seq__25360_25872;
var G__25878 = chunk__25362_25873;
var G__25879 = count__25363_25874;
var G__25880 = (i__25364_25875 + (1));
seq__25360_25872 = G__25877;
chunk__25362_25873 = G__25878;
count__25363_25874 = G__25879;
i__25364_25875 = G__25880;
continue;
} else {
var G__25881 = seq__25360_25872;
var G__25882 = chunk__25362_25873;
var G__25883 = count__25363_25874;
var G__25884 = (i__25364_25875 + (1));
seq__25360_25872 = G__25881;
chunk__25362_25873 = G__25882;
count__25363_25874 = G__25883;
i__25364_25875 = G__25884;
continue;
}
} else {
var temp__5804__auto___25885__$1 = cljs.core.seq(seq__25360_25872);
if(temp__5804__auto___25885__$1){
var seq__25360_25886__$1 = temp__5804__auto___25885__$1;
if(cljs.core.chunked_seq_QMARK_(seq__25360_25886__$1)){
var c__5525__auto___25887 = cljs.core.chunk_first(seq__25360_25886__$1);
var G__25888 = cljs.core.chunk_rest(seq__25360_25886__$1);
var G__25889 = c__5525__auto___25887;
var G__25890 = cljs.core.count(c__5525__auto___25887);
var G__25891 = (0);
seq__25360_25872 = G__25888;
chunk__25362_25873 = G__25889;
count__25363_25874 = G__25890;
i__25364_25875 = G__25891;
continue;
} else {
var child_25892 = cljs.core.first(seq__25360_25886__$1);
if(cljs.core.truth_(child_25892)){
node.appendChild(child_25892);


var G__25893 = cljs.core.next(seq__25360_25886__$1);
var G__25894 = null;
var G__25895 = (0);
var G__25896 = (0);
seq__25360_25872 = G__25893;
chunk__25362_25873 = G__25894;
count__25363_25874 = G__25895;
i__25364_25875 = G__25896;
continue;
} else {
var G__25897 = cljs.core.next(seq__25360_25886__$1);
var G__25898 = null;
var G__25899 = (0);
var G__25900 = (0);
seq__25360_25872 = G__25897;
chunk__25362_25873 = G__25898;
count__25363_25874 = G__25899;
i__25364_25875 = G__25900;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_25871);
}
}


var G__25901 = cljs.core.next(seq__25322_25862__$1);
var G__25902 = null;
var G__25903 = (0);
var G__25904 = (0);
seq__25322_25816 = G__25901;
chunk__25324_25817 = G__25902;
count__25325_25818 = G__25903;
i__25326_25819 = G__25904;
continue;
} else {
var G__25905 = cljs.core.next(seq__25322_25862__$1);
var G__25906 = null;
var G__25907 = (0);
var G__25908 = (0);
seq__25322_25816 = G__25905;
chunk__25324_25817 = G__25906;
count__25325_25818 = G__25907;
i__25326_25819 = G__25908;
continue;
}
}
} else {
}
}
break;
}

return node;
});
(shadow.dom.SVGElement["string"] = true);

(shadow.dom._to_svg["string"] = (function (this$){
if((this$ instanceof cljs.core.Keyword)){
return shadow.dom.make_svg_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$], null));
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("strings cannot be in svgs",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"this","this",-611633625),this$], null));
}
}));

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_svg_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_svg,this$__$1);
}));

(shadow.dom.SVGElement["null"] = true);

(shadow.dom._to_svg["null"] = (function (_){
return null;
}));
shadow.dom.svg = (function shadow$dom$svg(var_args){
var args__5732__auto__ = [];
var len__5726__auto___25912 = arguments.length;
var i__5727__auto___25913 = (0);
while(true){
if((i__5727__auto___25913 < len__5726__auto___25912)){
args__5732__auto__.push((arguments[i__5727__auto___25913]));

var G__25914 = (i__5727__auto___25913 + (1));
i__5727__auto___25913 = G__25914;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((1) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((1)),(0),null)):null);
return shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__5733__auto__);
});

(shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic = (function (attrs,children){
return shadow.dom._to_svg(cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),attrs], null),children)));
}));

(shadow.dom.svg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.dom.svg.cljs$lang$applyTo = (function (seq25388){
var G__25389 = cljs.core.first(seq25388);
var seq25388__$1 = cljs.core.next(seq25388);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__25389,seq25388__$1);
}));

/**
 * returns a channel for events on el
 * transform-fn should be a (fn [e el] some-val) where some-val will be put on the chan
 * once-or-cleanup handles the removal of the event handler
 * - true: remove after one event
 * - false: never removed
 * - chan: remove on msg/close
 */
shadow.dom.event_chan = (function shadow$dom$event_chan(var_args){
var G__25398 = arguments.length;
switch (G__25398) {
case 2:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2 = (function (el,event){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,null,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3 = (function (el,event,xf){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,xf,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4 = (function (el,event,xf,once_or_cleanup){
var buf = cljs.core.async.sliding_buffer((1));
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2(buf,xf);
var event_fn = (function shadow$dom$event_fn(e){
cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,e);

if(once_or_cleanup === true){
shadow.dom.remove_event_handler(el,event,shadow$dom$event_fn);

return cljs.core.async.close_BANG_(chan);
} else {
return null;
}
});
shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(event),event_fn);

if(cljs.core.truth_((function (){var and__5000__auto__ = once_or_cleanup;
if(cljs.core.truth_(and__5000__auto__)){
return (!(once_or_cleanup === true));
} else {
return and__5000__auto__;
}
})())){
var c__21913__auto___25926 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_25405){
var state_val_25406 = (state_25405[(1)]);
if((state_val_25406 === (1))){
var state_25405__$1 = state_25405;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_25405__$1,(2),once_or_cleanup);
} else {
if((state_val_25406 === (2))){
var inst_25402 = (state_25405[(2)]);
var inst_25403 = shadow.dom.remove_event_handler(el,event,event_fn);
var state_25405__$1 = (function (){var statearr_25408 = state_25405;
(statearr_25408[(7)] = inst_25402);

return statearr_25408;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_25405__$1,inst_25403);
} else {
return null;
}
}
});
return (function() {
var shadow$dom$state_machine__21631__auto__ = null;
var shadow$dom$state_machine__21631__auto____0 = (function (){
var statearr_25410 = [null,null,null,null,null,null,null,null];
(statearr_25410[(0)] = shadow$dom$state_machine__21631__auto__);

(statearr_25410[(1)] = (1));

return statearr_25410;
});
var shadow$dom$state_machine__21631__auto____1 = (function (state_25405){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_25405);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e25411){var ex__21634__auto__ = e25411;
var statearr_25413_25931 = state_25405;
(statearr_25413_25931[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_25405[(4)]))){
var statearr_25414_25932 = state_25405;
(statearr_25414_25932[(1)] = cljs.core.first((state_25405[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__25933 = state_25405;
state_25405 = G__25933;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
shadow$dom$state_machine__21631__auto__ = function(state_25405){
switch(arguments.length){
case 0:
return shadow$dom$state_machine__21631__auto____0.call(this);
case 1:
return shadow$dom$state_machine__21631__auto____1.call(this,state_25405);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
shadow$dom$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = shadow$dom$state_machine__21631__auto____0;
shadow$dom$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = shadow$dom$state_machine__21631__auto____1;
return shadow$dom$state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_25419 = f__21914__auto__();
(statearr_25419[(6)] = c__21913__auto___25926);

return statearr_25419;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));

} else {
}

return chan;
}));

(shadow.dom.event_chan.cljs$lang$maxFixedArity = 4);


//# sourceMappingURL=shadow.dom.js.map
