goog.provide('shadow.cljs.devtools.client.browser');
shadow.cljs.devtools.client.browser.devtools_msg = (function shadow$cljs$devtools$client$browser$devtools_msg(var_args){
var args__5732__auto__ = [];
var len__5726__auto___27351 = arguments.length;
var i__5727__auto___27352 = (0);
while(true){
if((i__5727__auto___27352 < len__5726__auto___27351)){
args__5732__auto__.push((arguments[i__5727__auto___27352]));

var G__27353 = (i__5727__auto___27352 + (1));
i__5727__auto___27352 = G__27353;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((1) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((1)),(0),null)):null);
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__5733__auto__);
});

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic = (function (msg,args){
if(shadow.cljs.devtools.client.env.log){
if(cljs.core.seq(shadow.cljs.devtools.client.env.log_style)){
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [["%cshadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join(''),shadow.cljs.devtools.client.env.log_style], null),args)));
} else {
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [["shadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join('')], null),args)));
}
} else {
return null;
}
}));

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$applyTo = (function (seq26920){
var G__26921 = cljs.core.first(seq26920);
var seq26920__$1 = cljs.core.next(seq26920);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__26921,seq26920__$1);
}));

shadow.cljs.devtools.client.browser.script_eval = (function shadow$cljs$devtools$client$browser$script_eval(code){
return goog.globalEval(code);
});
shadow.cljs.devtools.client.browser.do_js_load = (function shadow$cljs$devtools$client$browser$do_js_load(sources){
var seq__26926 = cljs.core.seq(sources);
var chunk__26927 = null;
var count__26928 = (0);
var i__26929 = (0);
while(true){
if((i__26929 < count__26928)){
var map__26939 = chunk__26927.cljs$core$IIndexed$_nth$arity$2(null, i__26929);
var map__26939__$1 = cljs.core.__destructure_map(map__26939);
var src = map__26939__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26939__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26939__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26939__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26939__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e26940){var e_27355 = e26940;
if(shadow.cljs.devtools.client.env.log){
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_27355);
} else {
}

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_27355.message)].join('')));
}

var G__27356 = seq__26926;
var G__27357 = chunk__26927;
var G__27358 = count__26928;
var G__27359 = (i__26929 + (1));
seq__26926 = G__27356;
chunk__26927 = G__27357;
count__26928 = G__27358;
i__26929 = G__27359;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__26926);
if(temp__5804__auto__){
var seq__26926__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__26926__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__26926__$1);
var G__27360 = cljs.core.chunk_rest(seq__26926__$1);
var G__27361 = c__5525__auto__;
var G__27362 = cljs.core.count(c__5525__auto__);
var G__27363 = (0);
seq__26926 = G__27360;
chunk__26927 = G__27361;
count__26928 = G__27362;
i__26929 = G__27363;
continue;
} else {
var map__26947 = cljs.core.first(seq__26926__$1);
var map__26947__$1 = cljs.core.__destructure_map(map__26947);
var src = map__26947__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26947__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26947__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26947__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26947__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e26948){var e_27364 = e26948;
if(shadow.cljs.devtools.client.env.log){
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_27364);
} else {
}

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_27364.message)].join('')));
}

var G__27365 = cljs.core.next(seq__26926__$1);
var G__27366 = null;
var G__27367 = (0);
var G__27368 = (0);
seq__26926 = G__27365;
chunk__26927 = G__27366;
count__26928 = G__27367;
i__26929 = G__27368;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.do_js_reload = (function shadow$cljs$devtools$client$browser$do_js_reload(msg,sources,complete_fn,failure_fn){
return shadow.cljs.devtools.client.env.do_js_reload.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(msg,new cljs.core.Keyword(null,"log-missing-fn","log-missing-fn",732676765),(function (fn_sym){
return null;
}),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"log-call-async","log-call-async",183826192),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call async ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),new cljs.core.Keyword(null,"log-call","log-call",412404391),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
})], 0)),(function (){
return shadow.cljs.devtools.client.browser.do_js_load(sources);
}),complete_fn,failure_fn);
});
/**
 * when (require '["some-str" :as x]) is done at the REPL we need to manually call the shadow.js.require for it
 * since the file only adds the shadow$provide. only need to do this for shadow-js.
 */
shadow.cljs.devtools.client.browser.do_js_requires = (function shadow$cljs$devtools$client$browser$do_js_requires(js_requires){
var seq__26949 = cljs.core.seq(js_requires);
var chunk__26950 = null;
var count__26951 = (0);
var i__26952 = (0);
while(true){
if((i__26952 < count__26951)){
var js_ns = chunk__26950.cljs$core$IIndexed$_nth$arity$2(null, i__26952);
var require_str_27372 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_27372);


var G__27373 = seq__26949;
var G__27374 = chunk__26950;
var G__27375 = count__26951;
var G__27376 = (i__26952 + (1));
seq__26949 = G__27373;
chunk__26950 = G__27374;
count__26951 = G__27375;
i__26952 = G__27376;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__26949);
if(temp__5804__auto__){
var seq__26949__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__26949__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__26949__$1);
var G__27377 = cljs.core.chunk_rest(seq__26949__$1);
var G__27378 = c__5525__auto__;
var G__27379 = cljs.core.count(c__5525__auto__);
var G__27380 = (0);
seq__26949 = G__27377;
chunk__26950 = G__27378;
count__26951 = G__27379;
i__26952 = G__27380;
continue;
} else {
var js_ns = cljs.core.first(seq__26949__$1);
var require_str_27381 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_27381);


var G__27382 = cljs.core.next(seq__26949__$1);
var G__27383 = null;
var G__27384 = (0);
var G__27385 = (0);
seq__26949 = G__27382;
chunk__26950 = G__27383;
count__26951 = G__27384;
i__26952 = G__27385;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.handle_build_complete = (function shadow$cljs$devtools$client$browser$handle_build_complete(runtime,p__26960){
var map__26961 = p__26960;
var map__26961__$1 = cljs.core.__destructure_map(map__26961);
var msg = map__26961__$1;
var info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26961__$1,new cljs.core.Keyword(null,"info","info",-317069002));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26961__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var warnings = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.distinct.cljs$core$IFn$_invoke$arity$1((function (){var iter__5480__auto__ = (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__26962(s__26963){
return (new cljs.core.LazySeq(null,(function (){
var s__26963__$1 = s__26963;
while(true){
var temp__5804__auto__ = cljs.core.seq(s__26963__$1);
if(temp__5804__auto__){
var xs__6360__auto__ = temp__5804__auto__;
var map__26968 = cljs.core.first(xs__6360__auto__);
var map__26968__$1 = cljs.core.__destructure_map(map__26968);
var src = map__26968__$1;
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26968__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var warnings = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26968__$1,new cljs.core.Keyword(null,"warnings","warnings",-735437651));
if(cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))){
var iterys__5476__auto__ = ((function (s__26963__$1,map__26968,map__26968__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__26961,map__26961__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__26962_$_iter__26964(s__26965){
return (new cljs.core.LazySeq(null,((function (s__26963__$1,map__26968,map__26968__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__26961,map__26961__$1,msg,info,reload_info){
return (function (){
var s__26965__$1 = s__26965;
while(true){
var temp__5804__auto____$1 = cljs.core.seq(s__26965__$1);
if(temp__5804__auto____$1){
var s__26965__$2 = temp__5804__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__26965__$2)){
var c__5478__auto__ = cljs.core.chunk_first(s__26965__$2);
var size__5479__auto__ = cljs.core.count(c__5478__auto__);
var b__26967 = cljs.core.chunk_buffer(size__5479__auto__);
if((function (){var i__26966 = (0);
while(true){
if((i__26966 < size__5479__auto__)){
var warning = cljs.core._nth(c__5478__auto__,i__26966);
cljs.core.chunk_append(b__26967,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name));

var G__27387 = (i__26966 + (1));
i__26966 = G__27387;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__26967),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__26962_$_iter__26964(cljs.core.chunk_rest(s__26965__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__26967),null);
}
} else {
var warning = cljs.core.first(s__26965__$2);
return cljs.core.cons(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__26962_$_iter__26964(cljs.core.rest(s__26965__$2)));
}
} else {
return null;
}
break;
}
});})(s__26963__$1,map__26968,map__26968__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__26961,map__26961__$1,msg,info,reload_info))
,null,null));
});})(s__26963__$1,map__26968,map__26968__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__26961,map__26961__$1,msg,info,reload_info))
;
var fs__5477__auto__ = cljs.core.seq(iterys__5476__auto__(warnings));
if(fs__5477__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__5477__auto__,shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__26962(cljs.core.rest(s__26963__$1)));
} else {
var G__27388 = cljs.core.rest(s__26963__$1);
s__26963__$1 = G__27388;
continue;
}
} else {
var G__27389 = cljs.core.rest(s__26963__$1);
s__26963__$1 = G__27389;
continue;
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__5480__auto__(new cljs.core.Keyword(null,"sources","sources",-321166424).cljs$core$IFn$_invoke$arity$1(info));
})()));
if(shadow.cljs.devtools.client.env.log){
var seq__26969_27390 = cljs.core.seq(warnings);
var chunk__26970_27391 = null;
var count__26971_27392 = (0);
var i__26972_27393 = (0);
while(true){
if((i__26972_27393 < count__26971_27392)){
var map__26975_27394 = chunk__26970_27391.cljs$core$IIndexed$_nth$arity$2(null, i__26972_27393);
var map__26975_27395__$1 = cljs.core.__destructure_map(map__26975_27394);
var w_27396 = map__26975_27395__$1;
var msg_27397__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26975_27395__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_27398 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26975_27395__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_27399 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26975_27395__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_27400 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26975_27395__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_27400)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_27398),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_27399),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_27397__$1)].join(''));


var G__27401 = seq__26969_27390;
var G__27402 = chunk__26970_27391;
var G__27403 = count__26971_27392;
var G__27404 = (i__26972_27393 + (1));
seq__26969_27390 = G__27401;
chunk__26970_27391 = G__27402;
count__26971_27392 = G__27403;
i__26972_27393 = G__27404;
continue;
} else {
var temp__5804__auto___27405 = cljs.core.seq(seq__26969_27390);
if(temp__5804__auto___27405){
var seq__26969_27406__$1 = temp__5804__auto___27405;
if(cljs.core.chunked_seq_QMARK_(seq__26969_27406__$1)){
var c__5525__auto___27407 = cljs.core.chunk_first(seq__26969_27406__$1);
var G__27408 = cljs.core.chunk_rest(seq__26969_27406__$1);
var G__27409 = c__5525__auto___27407;
var G__27410 = cljs.core.count(c__5525__auto___27407);
var G__27411 = (0);
seq__26969_27390 = G__27408;
chunk__26970_27391 = G__27409;
count__26971_27392 = G__27410;
i__26972_27393 = G__27411;
continue;
} else {
var map__26976_27412 = cljs.core.first(seq__26969_27406__$1);
var map__26976_27413__$1 = cljs.core.__destructure_map(map__26976_27412);
var w_27414 = map__26976_27413__$1;
var msg_27415__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26976_27413__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_27416 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26976_27413__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_27417 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26976_27413__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_27418 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26976_27413__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_27418)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_27416),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_27417),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_27415__$1)].join(''));


var G__27419 = cljs.core.next(seq__26969_27406__$1);
var G__27420 = null;
var G__27421 = (0);
var G__27422 = (0);
seq__26969_27390 = G__27419;
chunk__26970_27391 = G__27420;
count__26971_27392 = G__27421;
i__26972_27393 = G__27422;
continue;
}
} else {
}
}
break;
}
} else {
}

if((!(shadow.cljs.devtools.client.env.autoload))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(((cljs.core.empty_QMARK_(warnings)) || (shadow.cljs.devtools.client.env.ignore_warnings))){
var sources_to_get = shadow.cljs.devtools.client.env.filter_reload_sources(info,reload_info);
if(cljs.core.not(cljs.core.seq(sources_to_get))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"after-load","after-load",-1278503285)], null)))){
} else {
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("reloading code but no :after-load hooks are configured!",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["https://shadow-cljs.github.io/docs/UsersGuide.html#_lifecycle_hooks"], 0));
}

return shadow.cljs.devtools.client.shared.load_sources(runtime,sources_to_get,(function (p1__26959_SHARP_){
return shadow.cljs.devtools.client.browser.do_js_reload(msg,p1__26959_SHARP_,shadow.cljs.devtools.client.hud.load_end_success,shadow.cljs.devtools.client.hud.load_failure);
}));
}
} else {
return null;
}
}
});
shadow.cljs.devtools.client.browser.page_load_uri = (cljs.core.truth_(goog.global.document)?goog.Uri.parse(document.location.href):null);
shadow.cljs.devtools.client.browser.match_paths = (function shadow$cljs$devtools$client$browser$match_paths(old,new$){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("file",shadow.cljs.devtools.client.browser.page_load_uri.getScheme())){
var rel_new = cljs.core.subs.cljs$core$IFn$_invoke$arity$2(new$,(1));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(old,rel_new)) || (clojure.string.starts_with_QMARK_(old,[rel_new,"?"].join(''))))){
return rel_new;
} else {
return null;
}
} else {
var node_uri = goog.Uri.parse(old);
var node_uri_resolved = shadow.cljs.devtools.client.browser.page_load_uri.resolve(node_uri);
var node_abs = node_uri_resolved.getPath();
var and__5000__auto__ = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.browser.page_load_uri.hasSameDomainAs(node_uri))) || (cljs.core.not(node_uri.hasDomain())));
if(and__5000__auto__){
var and__5000__auto____$1 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(node_abs,new$);
if(and__5000__auto____$1){
return new$;
} else {
return and__5000__auto____$1;
}
} else {
return and__5000__auto__;
}
}
});
shadow.cljs.devtools.client.browser.handle_asset_update = (function shadow$cljs$devtools$client$browser$handle_asset_update(p__26977){
var map__26978 = p__26977;
var map__26978__$1 = cljs.core.__destructure_map(map__26978);
var msg = map__26978__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26978__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26978__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var seq__26979 = cljs.core.seq(updates);
var chunk__26981 = null;
var count__26982 = (0);
var i__26983 = (0);
while(true){
if((i__26983 < count__26982)){
var path = chunk__26981.cljs$core$IIndexed$_nth$arity$2(null, i__26983);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__27197_27424 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__27201_27425 = null;
var count__27202_27426 = (0);
var i__27203_27427 = (0);
while(true){
if((i__27203_27427 < count__27202_27426)){
var node_27428 = chunk__27201_27425.cljs$core$IIndexed$_nth$arity$2(null, i__27203_27427);
if(cljs.core.not(node_27428.shadow$old)){
var path_match_27429 = shadow.cljs.devtools.client.browser.match_paths(node_27428.getAttribute("href"),path);
if(cljs.core.truth_(path_match_27429)){
var new_link_27431 = (function (){var G__27231 = node_27428.cloneNode(true);
G__27231.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_27429),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__27231;
})();
(node_27428.shadow$old = true);

(new_link_27431.onload = ((function (seq__27197_27424,chunk__27201_27425,count__27202_27426,i__27203_27427,seq__26979,chunk__26981,count__26982,i__26983,new_link_27431,path_match_27429,node_27428,path,map__26978,map__26978__$1,msg,updates,reload_info){
return (function (e){
var seq__27232_27432 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__27234_27433 = null;
var count__27235_27434 = (0);
var i__27236_27435 = (0);
while(true){
if((i__27236_27435 < count__27235_27434)){
var map__27240_27436 = chunk__27234_27433.cljs$core$IIndexed$_nth$arity$2(null, i__27236_27435);
var map__27240_27437__$1 = cljs.core.__destructure_map(map__27240_27436);
var task_27438 = map__27240_27437__$1;
var fn_str_27439 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27240_27437__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_27440 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27240_27437__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_27441 = goog.getObjectByName(fn_str_27439,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_27440)].join(''));

(fn_obj_27441.cljs$core$IFn$_invoke$arity$2 ? fn_obj_27441.cljs$core$IFn$_invoke$arity$2(path,new_link_27431) : fn_obj_27441.call(null, path,new_link_27431));


var G__27442 = seq__27232_27432;
var G__27443 = chunk__27234_27433;
var G__27444 = count__27235_27434;
var G__27445 = (i__27236_27435 + (1));
seq__27232_27432 = G__27442;
chunk__27234_27433 = G__27443;
count__27235_27434 = G__27444;
i__27236_27435 = G__27445;
continue;
} else {
var temp__5804__auto___27446 = cljs.core.seq(seq__27232_27432);
if(temp__5804__auto___27446){
var seq__27232_27447__$1 = temp__5804__auto___27446;
if(cljs.core.chunked_seq_QMARK_(seq__27232_27447__$1)){
var c__5525__auto___27448 = cljs.core.chunk_first(seq__27232_27447__$1);
var G__27449 = cljs.core.chunk_rest(seq__27232_27447__$1);
var G__27450 = c__5525__auto___27448;
var G__27451 = cljs.core.count(c__5525__auto___27448);
var G__27452 = (0);
seq__27232_27432 = G__27449;
chunk__27234_27433 = G__27450;
count__27235_27434 = G__27451;
i__27236_27435 = G__27452;
continue;
} else {
var map__27241_27453 = cljs.core.first(seq__27232_27447__$1);
var map__27241_27454__$1 = cljs.core.__destructure_map(map__27241_27453);
var task_27455 = map__27241_27454__$1;
var fn_str_27456 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27241_27454__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_27457 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27241_27454__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_27458 = goog.getObjectByName(fn_str_27456,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_27457)].join(''));

(fn_obj_27458.cljs$core$IFn$_invoke$arity$2 ? fn_obj_27458.cljs$core$IFn$_invoke$arity$2(path,new_link_27431) : fn_obj_27458.call(null, path,new_link_27431));


var G__27459 = cljs.core.next(seq__27232_27447__$1);
var G__27460 = null;
var G__27461 = (0);
var G__27462 = (0);
seq__27232_27432 = G__27459;
chunk__27234_27433 = G__27460;
count__27235_27434 = G__27461;
i__27236_27435 = G__27462;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_27428);
});})(seq__27197_27424,chunk__27201_27425,count__27202_27426,i__27203_27427,seq__26979,chunk__26981,count__26982,i__26983,new_link_27431,path_match_27429,node_27428,path,map__26978,map__26978__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_27429], 0));

goog.dom.insertSiblingAfter(new_link_27431,node_27428);


var G__27463 = seq__27197_27424;
var G__27464 = chunk__27201_27425;
var G__27465 = count__27202_27426;
var G__27466 = (i__27203_27427 + (1));
seq__27197_27424 = G__27463;
chunk__27201_27425 = G__27464;
count__27202_27426 = G__27465;
i__27203_27427 = G__27466;
continue;
} else {
var G__27467 = seq__27197_27424;
var G__27468 = chunk__27201_27425;
var G__27469 = count__27202_27426;
var G__27470 = (i__27203_27427 + (1));
seq__27197_27424 = G__27467;
chunk__27201_27425 = G__27468;
count__27202_27426 = G__27469;
i__27203_27427 = G__27470;
continue;
}
} else {
var G__27471 = seq__27197_27424;
var G__27472 = chunk__27201_27425;
var G__27473 = count__27202_27426;
var G__27474 = (i__27203_27427 + (1));
seq__27197_27424 = G__27471;
chunk__27201_27425 = G__27472;
count__27202_27426 = G__27473;
i__27203_27427 = G__27474;
continue;
}
} else {
var temp__5804__auto___27475 = cljs.core.seq(seq__27197_27424);
if(temp__5804__auto___27475){
var seq__27197_27476__$1 = temp__5804__auto___27475;
if(cljs.core.chunked_seq_QMARK_(seq__27197_27476__$1)){
var c__5525__auto___27480 = cljs.core.chunk_first(seq__27197_27476__$1);
var G__27481 = cljs.core.chunk_rest(seq__27197_27476__$1);
var G__27482 = c__5525__auto___27480;
var G__27483 = cljs.core.count(c__5525__auto___27480);
var G__27484 = (0);
seq__27197_27424 = G__27481;
chunk__27201_27425 = G__27482;
count__27202_27426 = G__27483;
i__27203_27427 = G__27484;
continue;
} else {
var node_27485 = cljs.core.first(seq__27197_27476__$1);
if(cljs.core.not(node_27485.shadow$old)){
var path_match_27486 = shadow.cljs.devtools.client.browser.match_paths(node_27485.getAttribute("href"),path);
if(cljs.core.truth_(path_match_27486)){
var new_link_27487 = (function (){var G__27244 = node_27485.cloneNode(true);
G__27244.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_27486),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__27244;
})();
(node_27485.shadow$old = true);

(new_link_27487.onload = ((function (seq__27197_27424,chunk__27201_27425,count__27202_27426,i__27203_27427,seq__26979,chunk__26981,count__26982,i__26983,new_link_27487,path_match_27486,node_27485,seq__27197_27476__$1,temp__5804__auto___27475,path,map__26978,map__26978__$1,msg,updates,reload_info){
return (function (e){
var seq__27245_27488 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__27247_27489 = null;
var count__27248_27490 = (0);
var i__27249_27491 = (0);
while(true){
if((i__27249_27491 < count__27248_27490)){
var map__27258_27492 = chunk__27247_27489.cljs$core$IIndexed$_nth$arity$2(null, i__27249_27491);
var map__27258_27493__$1 = cljs.core.__destructure_map(map__27258_27492);
var task_27494 = map__27258_27493__$1;
var fn_str_27495 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27258_27493__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_27496 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27258_27493__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_27497 = goog.getObjectByName(fn_str_27495,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_27496)].join(''));

(fn_obj_27497.cljs$core$IFn$_invoke$arity$2 ? fn_obj_27497.cljs$core$IFn$_invoke$arity$2(path,new_link_27487) : fn_obj_27497.call(null, path,new_link_27487));


var G__27498 = seq__27245_27488;
var G__27499 = chunk__27247_27489;
var G__27500 = count__27248_27490;
var G__27501 = (i__27249_27491 + (1));
seq__27245_27488 = G__27498;
chunk__27247_27489 = G__27499;
count__27248_27490 = G__27500;
i__27249_27491 = G__27501;
continue;
} else {
var temp__5804__auto___27502__$1 = cljs.core.seq(seq__27245_27488);
if(temp__5804__auto___27502__$1){
var seq__27245_27503__$1 = temp__5804__auto___27502__$1;
if(cljs.core.chunked_seq_QMARK_(seq__27245_27503__$1)){
var c__5525__auto___27504 = cljs.core.chunk_first(seq__27245_27503__$1);
var G__27505 = cljs.core.chunk_rest(seq__27245_27503__$1);
var G__27506 = c__5525__auto___27504;
var G__27507 = cljs.core.count(c__5525__auto___27504);
var G__27508 = (0);
seq__27245_27488 = G__27505;
chunk__27247_27489 = G__27506;
count__27248_27490 = G__27507;
i__27249_27491 = G__27508;
continue;
} else {
var map__27259_27509 = cljs.core.first(seq__27245_27503__$1);
var map__27259_27510__$1 = cljs.core.__destructure_map(map__27259_27509);
var task_27511 = map__27259_27510__$1;
var fn_str_27512 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27259_27510__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_27513 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27259_27510__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_27514 = goog.getObjectByName(fn_str_27512,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_27513)].join(''));

(fn_obj_27514.cljs$core$IFn$_invoke$arity$2 ? fn_obj_27514.cljs$core$IFn$_invoke$arity$2(path,new_link_27487) : fn_obj_27514.call(null, path,new_link_27487));


var G__27515 = cljs.core.next(seq__27245_27503__$1);
var G__27516 = null;
var G__27517 = (0);
var G__27518 = (0);
seq__27245_27488 = G__27515;
chunk__27247_27489 = G__27516;
count__27248_27490 = G__27517;
i__27249_27491 = G__27518;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_27485);
});})(seq__27197_27424,chunk__27201_27425,count__27202_27426,i__27203_27427,seq__26979,chunk__26981,count__26982,i__26983,new_link_27487,path_match_27486,node_27485,seq__27197_27476__$1,temp__5804__auto___27475,path,map__26978,map__26978__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_27486], 0));

goog.dom.insertSiblingAfter(new_link_27487,node_27485);


var G__27519 = cljs.core.next(seq__27197_27476__$1);
var G__27520 = null;
var G__27521 = (0);
var G__27522 = (0);
seq__27197_27424 = G__27519;
chunk__27201_27425 = G__27520;
count__27202_27426 = G__27521;
i__27203_27427 = G__27522;
continue;
} else {
var G__27523 = cljs.core.next(seq__27197_27476__$1);
var G__27524 = null;
var G__27525 = (0);
var G__27526 = (0);
seq__27197_27424 = G__27523;
chunk__27201_27425 = G__27524;
count__27202_27426 = G__27525;
i__27203_27427 = G__27526;
continue;
}
} else {
var G__27527 = cljs.core.next(seq__27197_27476__$1);
var G__27528 = null;
var G__27529 = (0);
var G__27530 = (0);
seq__27197_27424 = G__27527;
chunk__27201_27425 = G__27528;
count__27202_27426 = G__27529;
i__27203_27427 = G__27530;
continue;
}
}
} else {
}
}
break;
}


var G__27531 = seq__26979;
var G__27532 = chunk__26981;
var G__27533 = count__26982;
var G__27534 = (i__26983 + (1));
seq__26979 = G__27531;
chunk__26981 = G__27532;
count__26982 = G__27533;
i__26983 = G__27534;
continue;
} else {
var G__27535 = seq__26979;
var G__27536 = chunk__26981;
var G__27537 = count__26982;
var G__27538 = (i__26983 + (1));
seq__26979 = G__27535;
chunk__26981 = G__27536;
count__26982 = G__27537;
i__26983 = G__27538;
continue;
}
} else {
var temp__5804__auto__ = cljs.core.seq(seq__26979);
if(temp__5804__auto__){
var seq__26979__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__26979__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__26979__$1);
var G__27539 = cljs.core.chunk_rest(seq__26979__$1);
var G__27540 = c__5525__auto__;
var G__27541 = cljs.core.count(c__5525__auto__);
var G__27542 = (0);
seq__26979 = G__27539;
chunk__26981 = G__27540;
count__26982 = G__27541;
i__26983 = G__27542;
continue;
} else {
var path = cljs.core.first(seq__26979__$1);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__27260_27543 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__27264_27544 = null;
var count__27265_27545 = (0);
var i__27266_27546 = (0);
while(true){
if((i__27266_27546 < count__27265_27545)){
var node_27547 = chunk__27264_27544.cljs$core$IIndexed$_nth$arity$2(null, i__27266_27546);
if(cljs.core.not(node_27547.shadow$old)){
var path_match_27548 = shadow.cljs.devtools.client.browser.match_paths(node_27547.getAttribute("href"),path);
if(cljs.core.truth_(path_match_27548)){
var new_link_27549 = (function (){var G__27303 = node_27547.cloneNode(true);
G__27303.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_27548),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__27303;
})();
(node_27547.shadow$old = true);

(new_link_27549.onload = ((function (seq__27260_27543,chunk__27264_27544,count__27265_27545,i__27266_27546,seq__26979,chunk__26981,count__26982,i__26983,new_link_27549,path_match_27548,node_27547,path,seq__26979__$1,temp__5804__auto__,map__26978,map__26978__$1,msg,updates,reload_info){
return (function (e){
var seq__27304_27550 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__27306_27551 = null;
var count__27307_27552 = (0);
var i__27308_27553 = (0);
while(true){
if((i__27308_27553 < count__27307_27552)){
var map__27313_27554 = chunk__27306_27551.cljs$core$IIndexed$_nth$arity$2(null, i__27308_27553);
var map__27313_27555__$1 = cljs.core.__destructure_map(map__27313_27554);
var task_27556 = map__27313_27555__$1;
var fn_str_27557 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27313_27555__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_27558 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27313_27555__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_27559 = goog.getObjectByName(fn_str_27557,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_27558)].join(''));

(fn_obj_27559.cljs$core$IFn$_invoke$arity$2 ? fn_obj_27559.cljs$core$IFn$_invoke$arity$2(path,new_link_27549) : fn_obj_27559.call(null, path,new_link_27549));


var G__27560 = seq__27304_27550;
var G__27561 = chunk__27306_27551;
var G__27562 = count__27307_27552;
var G__27563 = (i__27308_27553 + (1));
seq__27304_27550 = G__27560;
chunk__27306_27551 = G__27561;
count__27307_27552 = G__27562;
i__27308_27553 = G__27563;
continue;
} else {
var temp__5804__auto___27564__$1 = cljs.core.seq(seq__27304_27550);
if(temp__5804__auto___27564__$1){
var seq__27304_27565__$1 = temp__5804__auto___27564__$1;
if(cljs.core.chunked_seq_QMARK_(seq__27304_27565__$1)){
var c__5525__auto___27566 = cljs.core.chunk_first(seq__27304_27565__$1);
var G__27567 = cljs.core.chunk_rest(seq__27304_27565__$1);
var G__27568 = c__5525__auto___27566;
var G__27569 = cljs.core.count(c__5525__auto___27566);
var G__27570 = (0);
seq__27304_27550 = G__27567;
chunk__27306_27551 = G__27568;
count__27307_27552 = G__27569;
i__27308_27553 = G__27570;
continue;
} else {
var map__27314_27571 = cljs.core.first(seq__27304_27565__$1);
var map__27314_27572__$1 = cljs.core.__destructure_map(map__27314_27571);
var task_27573 = map__27314_27572__$1;
var fn_str_27574 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27314_27572__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_27575 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27314_27572__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_27576 = goog.getObjectByName(fn_str_27574,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_27575)].join(''));

(fn_obj_27576.cljs$core$IFn$_invoke$arity$2 ? fn_obj_27576.cljs$core$IFn$_invoke$arity$2(path,new_link_27549) : fn_obj_27576.call(null, path,new_link_27549));


var G__27577 = cljs.core.next(seq__27304_27565__$1);
var G__27578 = null;
var G__27579 = (0);
var G__27580 = (0);
seq__27304_27550 = G__27577;
chunk__27306_27551 = G__27578;
count__27307_27552 = G__27579;
i__27308_27553 = G__27580;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_27547);
});})(seq__27260_27543,chunk__27264_27544,count__27265_27545,i__27266_27546,seq__26979,chunk__26981,count__26982,i__26983,new_link_27549,path_match_27548,node_27547,path,seq__26979__$1,temp__5804__auto__,map__26978,map__26978__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_27548], 0));

goog.dom.insertSiblingAfter(new_link_27549,node_27547);


var G__27581 = seq__27260_27543;
var G__27582 = chunk__27264_27544;
var G__27583 = count__27265_27545;
var G__27584 = (i__27266_27546 + (1));
seq__27260_27543 = G__27581;
chunk__27264_27544 = G__27582;
count__27265_27545 = G__27583;
i__27266_27546 = G__27584;
continue;
} else {
var G__27585 = seq__27260_27543;
var G__27586 = chunk__27264_27544;
var G__27587 = count__27265_27545;
var G__27588 = (i__27266_27546 + (1));
seq__27260_27543 = G__27585;
chunk__27264_27544 = G__27586;
count__27265_27545 = G__27587;
i__27266_27546 = G__27588;
continue;
}
} else {
var G__27589 = seq__27260_27543;
var G__27590 = chunk__27264_27544;
var G__27591 = count__27265_27545;
var G__27592 = (i__27266_27546 + (1));
seq__27260_27543 = G__27589;
chunk__27264_27544 = G__27590;
count__27265_27545 = G__27591;
i__27266_27546 = G__27592;
continue;
}
} else {
var temp__5804__auto___27593__$1 = cljs.core.seq(seq__27260_27543);
if(temp__5804__auto___27593__$1){
var seq__27260_27594__$1 = temp__5804__auto___27593__$1;
if(cljs.core.chunked_seq_QMARK_(seq__27260_27594__$1)){
var c__5525__auto___27595 = cljs.core.chunk_first(seq__27260_27594__$1);
var G__27596 = cljs.core.chunk_rest(seq__27260_27594__$1);
var G__27597 = c__5525__auto___27595;
var G__27598 = cljs.core.count(c__5525__auto___27595);
var G__27599 = (0);
seq__27260_27543 = G__27596;
chunk__27264_27544 = G__27597;
count__27265_27545 = G__27598;
i__27266_27546 = G__27599;
continue;
} else {
var node_27600 = cljs.core.first(seq__27260_27594__$1);
if(cljs.core.not(node_27600.shadow$old)){
var path_match_27601 = shadow.cljs.devtools.client.browser.match_paths(node_27600.getAttribute("href"),path);
if(cljs.core.truth_(path_match_27601)){
var new_link_27602 = (function (){var G__27315 = node_27600.cloneNode(true);
G__27315.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_27601),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__27315;
})();
(node_27600.shadow$old = true);

(new_link_27602.onload = ((function (seq__27260_27543,chunk__27264_27544,count__27265_27545,i__27266_27546,seq__26979,chunk__26981,count__26982,i__26983,new_link_27602,path_match_27601,node_27600,seq__27260_27594__$1,temp__5804__auto___27593__$1,path,seq__26979__$1,temp__5804__auto__,map__26978,map__26978__$1,msg,updates,reload_info){
return (function (e){
var seq__27316_27603 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__27318_27604 = null;
var count__27319_27605 = (0);
var i__27320_27606 = (0);
while(true){
if((i__27320_27606 < count__27319_27605)){
var map__27325_27607 = chunk__27318_27604.cljs$core$IIndexed$_nth$arity$2(null, i__27320_27606);
var map__27325_27608__$1 = cljs.core.__destructure_map(map__27325_27607);
var task_27609 = map__27325_27608__$1;
var fn_str_27610 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27325_27608__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_27611 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27325_27608__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_27612 = goog.getObjectByName(fn_str_27610,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_27611)].join(''));

(fn_obj_27612.cljs$core$IFn$_invoke$arity$2 ? fn_obj_27612.cljs$core$IFn$_invoke$arity$2(path,new_link_27602) : fn_obj_27612.call(null, path,new_link_27602));


var G__27613 = seq__27316_27603;
var G__27614 = chunk__27318_27604;
var G__27615 = count__27319_27605;
var G__27616 = (i__27320_27606 + (1));
seq__27316_27603 = G__27613;
chunk__27318_27604 = G__27614;
count__27319_27605 = G__27615;
i__27320_27606 = G__27616;
continue;
} else {
var temp__5804__auto___27617__$2 = cljs.core.seq(seq__27316_27603);
if(temp__5804__auto___27617__$2){
var seq__27316_27618__$1 = temp__5804__auto___27617__$2;
if(cljs.core.chunked_seq_QMARK_(seq__27316_27618__$1)){
var c__5525__auto___27619 = cljs.core.chunk_first(seq__27316_27618__$1);
var G__27620 = cljs.core.chunk_rest(seq__27316_27618__$1);
var G__27621 = c__5525__auto___27619;
var G__27622 = cljs.core.count(c__5525__auto___27619);
var G__27623 = (0);
seq__27316_27603 = G__27620;
chunk__27318_27604 = G__27621;
count__27319_27605 = G__27622;
i__27320_27606 = G__27623;
continue;
} else {
var map__27326_27624 = cljs.core.first(seq__27316_27618__$1);
var map__27326_27625__$1 = cljs.core.__destructure_map(map__27326_27624);
var task_27626 = map__27326_27625__$1;
var fn_str_27627 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27326_27625__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_27628 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27326_27625__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_27629 = goog.getObjectByName(fn_str_27627,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_27628)].join(''));

(fn_obj_27629.cljs$core$IFn$_invoke$arity$2 ? fn_obj_27629.cljs$core$IFn$_invoke$arity$2(path,new_link_27602) : fn_obj_27629.call(null, path,new_link_27602));


var G__27630 = cljs.core.next(seq__27316_27618__$1);
var G__27631 = null;
var G__27632 = (0);
var G__27633 = (0);
seq__27316_27603 = G__27630;
chunk__27318_27604 = G__27631;
count__27319_27605 = G__27632;
i__27320_27606 = G__27633;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_27600);
});})(seq__27260_27543,chunk__27264_27544,count__27265_27545,i__27266_27546,seq__26979,chunk__26981,count__26982,i__26983,new_link_27602,path_match_27601,node_27600,seq__27260_27594__$1,temp__5804__auto___27593__$1,path,seq__26979__$1,temp__5804__auto__,map__26978,map__26978__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_27601], 0));

goog.dom.insertSiblingAfter(new_link_27602,node_27600);


var G__27634 = cljs.core.next(seq__27260_27594__$1);
var G__27635 = null;
var G__27636 = (0);
var G__27637 = (0);
seq__27260_27543 = G__27634;
chunk__27264_27544 = G__27635;
count__27265_27545 = G__27636;
i__27266_27546 = G__27637;
continue;
} else {
var G__27638 = cljs.core.next(seq__27260_27594__$1);
var G__27639 = null;
var G__27640 = (0);
var G__27641 = (0);
seq__27260_27543 = G__27638;
chunk__27264_27544 = G__27639;
count__27265_27545 = G__27640;
i__27266_27546 = G__27641;
continue;
}
} else {
var G__27642 = cljs.core.next(seq__27260_27594__$1);
var G__27643 = null;
var G__27644 = (0);
var G__27645 = (0);
seq__27260_27543 = G__27642;
chunk__27264_27544 = G__27643;
count__27265_27545 = G__27644;
i__27266_27546 = G__27645;
continue;
}
}
} else {
}
}
break;
}


var G__27646 = cljs.core.next(seq__26979__$1);
var G__27647 = null;
var G__27648 = (0);
var G__27649 = (0);
seq__26979 = G__27646;
chunk__26981 = G__27647;
count__26982 = G__27648;
i__26983 = G__27649;
continue;
} else {
var G__27650 = cljs.core.next(seq__26979__$1);
var G__27651 = null;
var G__27652 = (0);
var G__27653 = (0);
seq__26979 = G__27650;
chunk__26981 = G__27651;
count__26982 = G__27652;
i__26983 = G__27653;
continue;
}
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.global_eval = (function shadow$cljs$devtools$client$browser$global_eval(js){
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2("undefined",typeof(module))){
return eval(js);
} else {
return (0,eval)(js);;
}
});
shadow.cljs.devtools.client.browser.runtime_info = (((typeof SHADOW_CONFIG !== 'undefined'))?shadow.json.to_clj.cljs$core$IFn$_invoke$arity$1(SHADOW_CONFIG):null);
shadow.cljs.devtools.client.browser.client_info = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([shadow.cljs.devtools.client.browser.runtime_info,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"host","host",-1558485167),(cljs.core.truth_(goog.global.document)?new cljs.core.Keyword(null,"browser","browser",828191719):new cljs.core.Keyword(null,"browser-worker","browser-worker",1638998282)),new cljs.core.Keyword(null,"user-agent","user-agent",1220426212),[(cljs.core.truth_(goog.userAgent.OPERA)?"Opera":(cljs.core.truth_(goog.userAgent.product.CHROME)?"Chrome":(cljs.core.truth_(goog.userAgent.IE)?"MSIE":(cljs.core.truth_(goog.userAgent.EDGE)?"Edge":(cljs.core.truth_(goog.userAgent.GECKO)?"Firefox":(cljs.core.truth_(goog.userAgent.SAFARI)?"Safari":(cljs.core.truth_(goog.userAgent.WEBKIT)?"Webkit":null)))))))," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.VERSION)," [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.PLATFORM),"]"].join(''),new cljs.core.Keyword(null,"dom","dom",-1236537922),(!((goog.global.document == null)))], null)], 0));
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.ws_was_welcome_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.ws_was_welcome_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(false);
}
if(((shadow.cljs.devtools.client.env.enabled) && ((shadow.cljs.devtools.client.env.worker_client_id > (0))))){
(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$_js_eval$arity$2 = (function (this$,code){
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(code);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_invoke$arity$3 = (function (this$,ns,p__27333){
var map__27334 = p__27333;
var map__27334__$1 = cljs.core.__destructure_map(map__27334);
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27334__$1,new cljs.core.Keyword(null,"js","js",1768080579));
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(js);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_init$arity$4 = (function (runtime,p__27335,done,error){
var map__27336 = p__27335;
var map__27336__$1 = cljs.core.__destructure_map(map__27336);
var repl_sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27336__$1,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535));
var runtime__$1 = this;
return shadow.cljs.devtools.client.shared.load_sources(runtime__$1,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.env.src_is_loaded_QMARK_,repl_sources)),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null, ));
}));
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_require$arity$4 = (function (runtime,p__27337,done,error){
var map__27338 = p__27337;
var map__27338__$1 = cljs.core.__destructure_map(map__27338);
var msg = map__27338__$1;
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27338__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var reload_namespaces = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27338__$1,new cljs.core.Keyword(null,"reload-namespaces","reload-namespaces",250210134));
var js_requires = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27338__$1,new cljs.core.Keyword(null,"js-requires","js-requires",-1311472051));
var runtime__$1 = this;
var sources_to_load = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p__27339){
var map__27340 = p__27339;
var map__27340__$1 = cljs.core.__destructure_map(map__27340);
var src = map__27340__$1;
var provides = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27340__$1,new cljs.core.Keyword(null,"provides","provides",-1634397992));
var and__5000__auto__ = shadow.cljs.devtools.client.env.src_is_loaded_QMARK_(src);
if(cljs.core.truth_(and__5000__auto__)){
return cljs.core.not(cljs.core.some(reload_namespaces,provides));
} else {
return and__5000__auto__;
}
}),sources));
if(cljs.core.not(cljs.core.seq(sources_to_load))){
var G__27341 = cljs.core.PersistentVector.EMPTY;
return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(G__27341) : done.call(null, G__27341));
} else {
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3(runtime__$1,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"cljs-load-sources","cljs-load-sources",-1458295962),new cljs.core.Keyword(null,"to","to",192099007),shadow.cljs.devtools.client.env.worker_client_id,new cljs.core.Keyword(null,"sources","sources",-321166424),cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582)),sources_to_load)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cljs-sources","cljs-sources",31121610),(function (p__27342){
var map__27343 = p__27342;
var map__27343__$1 = cljs.core.__destructure_map(map__27343);
var msg__$1 = map__27343__$1;
var sources__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27343__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
try{shadow.cljs.devtools.client.browser.do_js_load(sources__$1);

if(cljs.core.seq(js_requires)){
shadow.cljs.devtools.client.browser.do_js_requires(js_requires);
} else {
}

return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(sources_to_load) : done.call(null, sources_to_load));
}catch (e27344){var ex = e27344;
return (error.cljs$core$IFn$_invoke$arity$1 ? error.cljs$core$IFn$_invoke$arity$1(ex) : error.call(null, ex));
}})], null));
}
}));

shadow.cljs.devtools.client.shared.add_plugin_BANG_(new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),cljs.core.PersistentHashSet.EMPTY,(function (p__27345){
var map__27346 = p__27345;
var map__27346__$1 = cljs.core.__destructure_map(map__27346);
var env = map__27346__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27346__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
var svc = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime], null);
shadow.remote.runtime.api.add_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"on-welcome","on-welcome",1895317125),(function (){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,true);

shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

shadow.cljs.devtools.client.env.patch_goog_BANG_();

return shadow.cljs.devtools.client.browser.devtools_msg(["#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"client-id","client-id",-464622140).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(new cljs.core.Keyword(null,"state-ref","state-ref",2127874952).cljs$core$IFn$_invoke$arity$1(runtime))))," ready!"].join(''));
}),new cljs.core.Keyword(null,"on-disconnect","on-disconnect",-809021814),(function (e){
if(cljs.core.truth_(cljs.core.deref(shadow.cljs.devtools.client.browser.ws_was_welcome_ref))){
shadow.cljs.devtools.client.hud.connection_error("The Websocket connection was closed!");

return cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);
} else {
return null;
}
}),new cljs.core.Keyword(null,"on-reconnect","on-reconnect",1239988702),(function (e){
return shadow.cljs.devtools.client.hud.connection_error("Reconnecting ...");
}),new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword(null,"access-denied","access-denied",959449406),(function (msg){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);

return shadow.cljs.devtools.client.hud.connection_error(["Stale Output! Your loaded JS was not produced by the running shadow-cljs instance."," Is the watch for this build running?"].join(''));
}),new cljs.core.Keyword(null,"cljs-asset-update","cljs-asset-update",1224093028),(function (msg){
return shadow.cljs.devtools.client.browser.handle_asset_update(msg);
}),new cljs.core.Keyword(null,"cljs-build-configure","cljs-build-configure",-2089891268),(function (msg){
return null;
}),new cljs.core.Keyword(null,"cljs-build-start","cljs-build-start",-725781241),(function (msg){
shadow.cljs.devtools.client.hud.hud_hide();

shadow.cljs.devtools.client.hud.load_start();

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-start","build-start",-959649480)));
}),new cljs.core.Keyword(null,"cljs-build-complete","cljs-build-complete",273626153),(function (msg){
var msg__$1 = shadow.cljs.devtools.client.env.add_warnings_to_info(msg);
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

shadow.cljs.devtools.client.hud.hud_warnings(msg__$1);

shadow.cljs.devtools.client.browser.handle_build_complete(runtime,msg__$1);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg__$1,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-complete","build-complete",-501868472)));
}),new cljs.core.Keyword(null,"cljs-build-failure","cljs-build-failure",1718154990),(function (msg){
shadow.cljs.devtools.client.hud.load_end();

shadow.cljs.devtools.client.hud.hud_error(msg);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-failure","build-failure",-2107487466)));
}),new cljs.core.Keyword("shadow.cljs.devtools.client.env","worker-notify","shadow.cljs.devtools.client.env/worker-notify",-1456820670),(function (p__27347){
var map__27348 = p__27347;
var map__27348__$1 = cljs.core.__destructure_map(map__27348);
var event_op = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27348__$1,new cljs.core.Keyword(null,"event-op","event-op",200358057));
var client_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27348__$1,new cljs.core.Keyword(null,"client-id","client-id",-464622140));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-disconnect","client-disconnect",640227957),event_op)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(client_id,shadow.cljs.devtools.client.env.worker_client_id)))){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was stopped!");
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-connect","client-connect",-1113973888),event_op)){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was restarted. Reload required!");
} else {
return null;
}
}
})], null)], null));

return svc;
}),(function (p__27349){
var map__27350 = p__27349;
var map__27350__$1 = cljs.core.__destructure_map(map__27350);
var svc = map__27350__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27350__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
return shadow.remote.runtime.api.del_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282));
}));

shadow.cljs.devtools.client.shared.init_runtime_BANG_(shadow.cljs.devtools.client.browser.client_info,shadow.cljs.devtools.client.websocket.start,shadow.cljs.devtools.client.websocket.send,shadow.cljs.devtools.client.websocket.stop);
} else {
}

//# sourceMappingURL=shadow.cljs.devtools.client.browser.js.map
