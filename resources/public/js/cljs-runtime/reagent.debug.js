goog.provide('reagent.debug');
reagent.debug.has_console = (typeof console !== 'undefined');
reagent.debug.tracking = false;
if((typeof reagent !== 'undefined') && (typeof reagent.debug !== 'undefined') && (typeof reagent.debug.warnings !== 'undefined')){
} else {
reagent.debug.warnings = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
}
if((typeof reagent !== 'undefined') && (typeof reagent.debug !== 'undefined') && (typeof reagent.debug.track_console !== 'undefined')){
} else {
reagent.debug.track_console = (function (){var o = ({});
(o.warn = (function() { 
var G__26184__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"warn","warn",-436710552)], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__26184 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__26185__i = 0, G__26185__a = new Array(arguments.length -  0);
while (G__26185__i < G__26185__a.length) {G__26185__a[G__26185__i] = arguments[G__26185__i + 0]; ++G__26185__i;}
  args = new cljs.core.IndexedSeq(G__26185__a,0,null);
} 
return G__26184__delegate.call(this,args);};
G__26184.cljs$lang$maxFixedArity = 0;
G__26184.cljs$lang$applyTo = (function (arglist__26186){
var args = cljs.core.seq(arglist__26186);
return G__26184__delegate(args);
});
G__26184.cljs$core$IFn$_invoke$arity$variadic = G__26184__delegate;
return G__26184;
})()
);

(o.error = (function() { 
var G__26187__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"error","error",-978969032)], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__26187 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__26189__i = 0, G__26189__a = new Array(arguments.length -  0);
while (G__26189__i < G__26189__a.length) {G__26189__a[G__26189__i] = arguments[G__26189__i + 0]; ++G__26189__i;}
  args = new cljs.core.IndexedSeq(G__26189__a,0,null);
} 
return G__26187__delegate.call(this,args);};
G__26187.cljs$lang$maxFixedArity = 0;
G__26187.cljs$lang$applyTo = (function (arglist__26190){
var args = cljs.core.seq(arglist__26190);
return G__26187__delegate(args);
});
G__26187.cljs$core$IFn$_invoke$arity$variadic = G__26187__delegate;
return G__26187;
})()
);

return o;
})();
}
reagent.debug.track_warnings = (function reagent$debug$track_warnings(f){
(reagent.debug.tracking = true);

cljs.core.reset_BANG_(reagent.debug.warnings,null);

(f.cljs$core$IFn$_invoke$arity$0 ? f.cljs$core$IFn$_invoke$arity$0() : f.call(null, ));

var warns = cljs.core.deref(reagent.debug.warnings);
cljs.core.reset_BANG_(reagent.debug.warnings,null);

(reagent.debug.tracking = false);

return warns;
});

//# sourceMappingURL=reagent.debug.js.map
