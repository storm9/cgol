goog.provide('cljs.core.async');
goog.scope(function(){
  cljs.core.async.goog$module$goog$array = goog.module.get('goog.array');
});

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async22047 = (function (f,blockable,meta22048){
this.f = f;
this.blockable = blockable;
this.meta22048 = meta22048;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async22047.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_22049,meta22048__$1){
var self__ = this;
var _22049__$1 = this;
return (new cljs.core.async.t_cljs$core$async22047(self__.f,self__.blockable,meta22048__$1));
}));

(cljs.core.async.t_cljs$core$async22047.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_22049){
var self__ = this;
var _22049__$1 = this;
return self__.meta22048;
}));

(cljs.core.async.t_cljs$core$async22047.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async22047.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async22047.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
}));

(cljs.core.async.t_cljs$core$async22047.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
}));

(cljs.core.async.t_cljs$core$async22047.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta22048","meta22048",1279991808,null)], null);
}));

(cljs.core.async.t_cljs$core$async22047.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async22047.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async22047");

(cljs.core.async.t_cljs$core$async22047.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async22047");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async22047.
 */
cljs.core.async.__GT_t_cljs$core$async22047 = (function cljs$core$async$__GT_t_cljs$core$async22047(f,blockable,meta22048){
return (new cljs.core.async.t_cljs$core$async22047(f,blockable,meta22048));
});


cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__22046 = arguments.length;
switch (G__22046) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(f,true);
}));

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
return (new cljs.core.async.t_cljs$core$async22047(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
}));

(cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2);

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer(n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if((!((buff == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$)))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__22057 = arguments.length;
switch (G__22057) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,null,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,xform,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.cljs$core$IFn$_invoke$arity$3(((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer(buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
}));

(cljs.core.async.chan.cljs$lang$maxFixedArity = 3);

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__22059 = arguments.length;
switch (G__22059) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2(xform,null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(cljs.core.async.impl.buffers.promise_buffer(),xform,ex_handler);
}));

(cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout(msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__22061 = arguments.length;
switch (G__22061) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3(port,fn1,true);
}));

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(ret)){
var val_24166 = cljs.core.deref(ret);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_24166) : fn1.call(null, val_24166));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_24166) : fn1.call(null, val_24166));
}));
}
} else {
}

return null;
}));

(cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3);

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn1 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn1 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__22063 = arguments.length;
switch (G__22063) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5802__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5802__auto__)){
var ret = temp__5802__auto__;
return cljs.core.deref(ret);
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4(port,val,fn1,true);
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5802__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(temp__5802__auto__)){
var retb = temp__5802__auto__;
var ret = cljs.core.deref(retb);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null, ret));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null, ret));
}));
}

return ret;
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4);

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_(port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__5593__auto___24169 = n;
var x_24170 = (0);
while(true){
if((x_24170 < n__5593__auto___24169)){
(a[x_24170] = x_24170);

var G__24171 = (x_24170 + (1));
x_24170 = G__24171;
continue;
} else {
}
break;
}

cljs.core.async.goog$module$goog$array.shuffle(a);

return a;
});

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async22064 = (function (flag,meta22065){
this.flag = flag;
this.meta22065 = meta22065;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async22064.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_22066,meta22065__$1){
var self__ = this;
var _22066__$1 = this;
return (new cljs.core.async.t_cljs$core$async22064(self__.flag,meta22065__$1));
}));

(cljs.core.async.t_cljs$core$async22064.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_22066){
var self__ = this;
var _22066__$1 = this;
return self__.meta22065;
}));

(cljs.core.async.t_cljs$core$async22064.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async22064.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref(self__.flag);
}));

(cljs.core.async.t_cljs$core$async22064.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async22064.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.flag,null);

return true;
}));

(cljs.core.async.t_cljs$core$async22064.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta22065","meta22065",1089277571,null)], null);
}));

(cljs.core.async.t_cljs$core$async22064.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async22064.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async22064");

(cljs.core.async.t_cljs$core$async22064.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async22064");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async22064.
 */
cljs.core.async.__GT_t_cljs$core$async22064 = (function cljs$core$async$__GT_t_cljs$core$async22064(flag,meta22065){
return (new cljs.core.async.t_cljs$core$async22064(flag,meta22065));
});


cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(true);
return (new cljs.core.async.t_cljs$core$async22064(flag,cljs.core.PersistentArrayMap.EMPTY));
});

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async22069 = (function (flag,cb,meta22070){
this.flag = flag;
this.cb = cb;
this.meta22070 = meta22070;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async22069.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_22071,meta22070__$1){
var self__ = this;
var _22071__$1 = this;
return (new cljs.core.async.t_cljs$core$async22069(self__.flag,self__.cb,meta22070__$1));
}));

(cljs.core.async.t_cljs$core$async22069.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_22071){
var self__ = this;
var _22071__$1 = this;
return self__.meta22070;
}));

(cljs.core.async.t_cljs$core$async22069.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async22069.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.flag);
}));

(cljs.core.async.t_cljs$core$async22069.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async22069.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit(self__.flag);

return self__.cb;
}));

(cljs.core.async.t_cljs$core$async22069.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta22070","meta22070",2113085364,null)], null);
}));

(cljs.core.async.t_cljs$core$async22069.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async22069.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async22069");

(cljs.core.async.t_cljs$core$async22069.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async22069");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async22069.
 */
cljs.core.async.__GT_t_cljs$core$async22069 = (function cljs$core$async$__GT_t_cljs$core$async22069(flag,cb,meta22070){
return (new cljs.core.async.t_cljs$core$async22069(flag,cb,meta22070));
});


cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
return (new cljs.core.async.t_cljs$core$async22069(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
if((cljs.core.count(ports) > (0))){
} else {
throw (new Error(["Assert failed: ","alts must have at least one channel operation","\n","(pos? (count ports))"].join('')));
}

var flag = cljs.core.async.alt_flag();
var n = cljs.core.count(ports);
var idxs = cljs.core.async.random_array(n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(ports,idx);
var wport = ((cljs.core.vector_QMARK_(port))?(port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((0)) : port.call(null, (0))):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = (port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((1)) : port.call(null, (1)));
return cljs.core.async.impl.protocols.put_BANG_(wport,val,cljs.core.async.alt_handler(flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__22074_SHARP_){
var G__22077 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__22074_SHARP_,wport], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__22077) : fret.call(null, G__22077));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.alt_handler(flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__22075_SHARP_){
var G__22078 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__22075_SHARP_,port], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__22078) : fret.call(null, G__22078));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref(vbox),(function (){var or__5002__auto__ = wport;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return port;
}
})()], null));
} else {
var G__24188 = (i + (1));
i = G__24188;
continue;
}
} else {
return null;
}
break;
}
})();
var or__5002__auto__ = ret;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
if(cljs.core.contains_QMARK_(opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5804__auto__ = (function (){var and__5000__auto__ = flag.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1(null, );
if(cljs.core.truth_(and__5000__auto__)){
return flag.cljs$core$async$impl$protocols$Handler$commit$arity$1(null, );
} else {
return and__5000__auto__;
}
})();
if(cljs.core.truth_(temp__5804__auto__)){
var got = temp__5804__auto__;
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__5732__auto__ = [];
var len__5726__auto___24190 = arguments.length;
var i__5727__auto___24191 = (0);
while(true){
if((i__5727__auto___24191 < len__5726__auto___24190)){
args__5732__auto__.push((arguments[i__5727__auto___24191]));

var G__24192 = (i__5727__auto___24191 + (1));
i__5727__auto___24191 = G__24192;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((1) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__5733__auto__);
});

(cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__22084){
var map__22085 = p__22084;
var map__22085__$1 = cljs.core.__destructure_map(map__22085);
var opts = map__22085__$1;
throw (new Error("alts! used not in (go ...) block"));
}));

(cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq22080){
var G__22081 = cljs.core.first(seq22080);
var seq22080__$1 = cljs.core.next(seq22080);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__22081,seq22080__$1);
}));

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__22090 = arguments.length;
switch (G__22090) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3(from,to,true);
}));

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__21913__auto___24198 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_22144){
var state_val_22145 = (state_22144[(1)]);
if((state_val_22145 === (7))){
var inst_22132 = (state_22144[(2)]);
var state_22144__$1 = state_22144;
var statearr_22146_24200 = state_22144__$1;
(statearr_22146_24200[(2)] = inst_22132);

(statearr_22146_24200[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22145 === (1))){
var state_22144__$1 = state_22144;
var statearr_22149_24202 = state_22144__$1;
(statearr_22149_24202[(2)] = null);

(statearr_22149_24202[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22145 === (4))){
var inst_22105 = (state_22144[(7)]);
var inst_22105__$1 = (state_22144[(2)]);
var inst_22115 = (inst_22105__$1 == null);
var state_22144__$1 = (function (){var statearr_22152 = state_22144;
(statearr_22152[(7)] = inst_22105__$1);

return statearr_22152;
})();
if(cljs.core.truth_(inst_22115)){
var statearr_22153_24203 = state_22144__$1;
(statearr_22153_24203[(1)] = (5));

} else {
var statearr_22154_24205 = state_22144__$1;
(statearr_22154_24205[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22145 === (13))){
var state_22144__$1 = state_22144;
var statearr_22156_24207 = state_22144__$1;
(statearr_22156_24207[(2)] = null);

(statearr_22156_24207[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22145 === (6))){
var inst_22105 = (state_22144[(7)]);
var state_22144__$1 = state_22144;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_22144__$1,(11),to,inst_22105);
} else {
if((state_val_22145 === (3))){
var inst_22135 = (state_22144[(2)]);
var state_22144__$1 = state_22144;
return cljs.core.async.impl.ioc_helpers.return_chan(state_22144__$1,inst_22135);
} else {
if((state_val_22145 === (12))){
var state_22144__$1 = state_22144;
var statearr_22164_24209 = state_22144__$1;
(statearr_22164_24209[(2)] = null);

(statearr_22164_24209[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22145 === (2))){
var state_22144__$1 = state_22144;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_22144__$1,(4),from);
} else {
if((state_val_22145 === (11))){
var inst_22124 = (state_22144[(2)]);
var state_22144__$1 = state_22144;
if(cljs.core.truth_(inst_22124)){
var statearr_22165_24213 = state_22144__$1;
(statearr_22165_24213[(1)] = (12));

} else {
var statearr_22166_24214 = state_22144__$1;
(statearr_22166_24214[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22145 === (9))){
var state_22144__$1 = state_22144;
var statearr_22167_24215 = state_22144__$1;
(statearr_22167_24215[(2)] = null);

(statearr_22167_24215[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22145 === (5))){
var state_22144__$1 = state_22144;
if(cljs.core.truth_(close_QMARK_)){
var statearr_22168_24216 = state_22144__$1;
(statearr_22168_24216[(1)] = (8));

} else {
var statearr_22169_24217 = state_22144__$1;
(statearr_22169_24217[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22145 === (14))){
var inst_22130 = (state_22144[(2)]);
var state_22144__$1 = state_22144;
var statearr_22170_24218 = state_22144__$1;
(statearr_22170_24218[(2)] = inst_22130);

(statearr_22170_24218[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22145 === (10))){
var inst_22121 = (state_22144[(2)]);
var state_22144__$1 = state_22144;
var statearr_22171_24221 = state_22144__$1;
(statearr_22171_24221[(2)] = inst_22121);

(statearr_22171_24221[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22145 === (8))){
var inst_22118 = cljs.core.async.close_BANG_(to);
var state_22144__$1 = state_22144;
var statearr_22172_24222 = state_22144__$1;
(statearr_22172_24222[(2)] = inst_22118);

(statearr_22172_24222[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__21631__auto__ = null;
var cljs$core$async$state_machine__21631__auto____0 = (function (){
var statearr_22173 = [null,null,null,null,null,null,null,null];
(statearr_22173[(0)] = cljs$core$async$state_machine__21631__auto__);

(statearr_22173[(1)] = (1));

return statearr_22173;
});
var cljs$core$async$state_machine__21631__auto____1 = (function (state_22144){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_22144);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e22174){var ex__21634__auto__ = e22174;
var statearr_22175_24224 = state_22144;
(statearr_22175_24224[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_22144[(4)]))){
var statearr_22176_24226 = state_22144;
(statearr_22176_24226[(1)] = cljs.core.first((state_22144[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24227 = state_22144;
state_22144 = G__24227;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$state_machine__21631__auto__ = function(state_22144){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21631__auto____1.call(this,state_22144);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21631__auto____0;
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21631__auto____1;
return cljs$core$async$state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_22179 = f__21914__auto__();
(statearr_22179[(6)] = c__21913__auto___24198);

return statearr_22179;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));


return to;
}));

(cljs.core.async.pipe.cljs$lang$maxFixedArity = 3);

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var results = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var process__$1 = (function (p__22187){
var vec__22188 = p__22187;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__22188,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__22188,(1),null);
var job = vec__22188;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((1),xf,ex_handler);
var c__21913__auto___24228 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_22204){
var state_val_22205 = (state_22204[(1)]);
if((state_val_22205 === (1))){
var state_22204__$1 = state_22204;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_22204__$1,(2),res,v);
} else {
if((state_val_22205 === (2))){
var inst_22201 = (state_22204[(2)]);
var inst_22202 = cljs.core.async.close_BANG_(res);
var state_22204__$1 = (function (){var statearr_22207 = state_22204;
(statearr_22207[(7)] = inst_22201);

return statearr_22207;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_22204__$1,inst_22202);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____0 = (function (){
var statearr_22208 = [null,null,null,null,null,null,null,null];
(statearr_22208[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__);

(statearr_22208[(1)] = (1));

return statearr_22208;
});
var cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____1 = (function (state_22204){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_22204);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e22209){var ex__21634__auto__ = e22209;
var statearr_22210_24229 = state_22204;
(statearr_22210_24229[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_22204[(4)]))){
var statearr_22211_24230 = state_22204;
(statearr_22211_24230[(1)] = cljs.core.first((state_22204[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24231 = state_22204;
state_22204 = G__24231;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__ = function(state_22204){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____1.call(this,state_22204);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_22239 = f__21914__auto__();
(statearr_22239[(6)] = c__21913__auto___24228);

return statearr_22239;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));


cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var async = (function (p__22240){
var vec__22241 = p__22240;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__22241,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__22241,(1),null);
var job = vec__22241;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
(xf.cljs$core$IFn$_invoke$arity$2 ? xf.cljs$core$IFn$_invoke$arity$2(v,res) : xf.call(null, v,res));

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var n__5593__auto___24239 = n;
var __24240 = (0);
while(true){
if((__24240 < n__5593__auto___24239)){
var G__22244_24241 = type;
var G__22244_24242__$1 = (((G__22244_24241 instanceof cljs.core.Keyword))?G__22244_24241.fqn:null);
switch (G__22244_24242__$1) {
case "compute":
var c__21913__auto___24245 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__24240,c__21913__auto___24245,G__22244_24241,G__22244_24242__$1,n__5593__auto___24239,jobs,results,process__$1,async){
return (function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = ((function (__24240,c__21913__auto___24245,G__22244_24241,G__22244_24242__$1,n__5593__auto___24239,jobs,results,process__$1,async){
return (function (state_22257){
var state_val_22258 = (state_22257[(1)]);
if((state_val_22258 === (1))){
var state_22257__$1 = state_22257;
var statearr_22259_24246 = state_22257__$1;
(statearr_22259_24246[(2)] = null);

(statearr_22259_24246[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22258 === (2))){
var state_22257__$1 = state_22257;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_22257__$1,(4),jobs);
} else {
if((state_val_22258 === (3))){
var inst_22255 = (state_22257[(2)]);
var state_22257__$1 = state_22257;
return cljs.core.async.impl.ioc_helpers.return_chan(state_22257__$1,inst_22255);
} else {
if((state_val_22258 === (4))){
var inst_22247 = (state_22257[(2)]);
var inst_22248 = process__$1(inst_22247);
var state_22257__$1 = state_22257;
if(cljs.core.truth_(inst_22248)){
var statearr_22262_24256 = state_22257__$1;
(statearr_22262_24256[(1)] = (5));

} else {
var statearr_22264_24257 = state_22257__$1;
(statearr_22264_24257[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22258 === (5))){
var state_22257__$1 = state_22257;
var statearr_22266_24258 = state_22257__$1;
(statearr_22266_24258[(2)] = null);

(statearr_22266_24258[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22258 === (6))){
var state_22257__$1 = state_22257;
var statearr_22267_24259 = state_22257__$1;
(statearr_22267_24259[(2)] = null);

(statearr_22267_24259[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22258 === (7))){
var inst_22253 = (state_22257[(2)]);
var state_22257__$1 = state_22257;
var statearr_22288_24260 = state_22257__$1;
(statearr_22288_24260[(2)] = inst_22253);

(statearr_22288_24260[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__24240,c__21913__auto___24245,G__22244_24241,G__22244_24242__$1,n__5593__auto___24239,jobs,results,process__$1,async))
;
return ((function (__24240,switch__21630__auto__,c__21913__auto___24245,G__22244_24241,G__22244_24242__$1,n__5593__auto___24239,jobs,results,process__$1,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____0 = (function (){
var statearr_22290 = [null,null,null,null,null,null,null];
(statearr_22290[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__);

(statearr_22290[(1)] = (1));

return statearr_22290;
});
var cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____1 = (function (state_22257){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_22257);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e22291){var ex__21634__auto__ = e22291;
var statearr_22292_24265 = state_22257;
(statearr_22292_24265[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_22257[(4)]))){
var statearr_22293_24266 = state_22257;
(statearr_22293_24266[(1)] = cljs.core.first((state_22257[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24267 = state_22257;
state_22257 = G__24267;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__ = function(state_22257){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____1.call(this,state_22257);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__;
})()
;})(__24240,switch__21630__auto__,c__21913__auto___24245,G__22244_24241,G__22244_24242__$1,n__5593__auto___24239,jobs,results,process__$1,async))
})();
var state__21915__auto__ = (function (){var statearr_22295 = f__21914__auto__();
(statearr_22295[(6)] = c__21913__auto___24245);

return statearr_22295;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
});})(__24240,c__21913__auto___24245,G__22244_24241,G__22244_24242__$1,n__5593__auto___24239,jobs,results,process__$1,async))
);


break;
case "async":
var c__21913__auto___24268 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__24240,c__21913__auto___24268,G__22244_24241,G__22244_24242__$1,n__5593__auto___24239,jobs,results,process__$1,async){
return (function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = ((function (__24240,c__21913__auto___24268,G__22244_24241,G__22244_24242__$1,n__5593__auto___24239,jobs,results,process__$1,async){
return (function (state_22308){
var state_val_22309 = (state_22308[(1)]);
if((state_val_22309 === (1))){
var state_22308__$1 = state_22308;
var statearr_22311_24276 = state_22308__$1;
(statearr_22311_24276[(2)] = null);

(statearr_22311_24276[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22309 === (2))){
var state_22308__$1 = state_22308;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_22308__$1,(4),jobs);
} else {
if((state_val_22309 === (3))){
var inst_22306 = (state_22308[(2)]);
var state_22308__$1 = state_22308;
return cljs.core.async.impl.ioc_helpers.return_chan(state_22308__$1,inst_22306);
} else {
if((state_val_22309 === (4))){
var inst_22298 = (state_22308[(2)]);
var inst_22299 = async(inst_22298);
var state_22308__$1 = state_22308;
if(cljs.core.truth_(inst_22299)){
var statearr_22312_24277 = state_22308__$1;
(statearr_22312_24277[(1)] = (5));

} else {
var statearr_22313_24279 = state_22308__$1;
(statearr_22313_24279[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22309 === (5))){
var state_22308__$1 = state_22308;
var statearr_22314_24280 = state_22308__$1;
(statearr_22314_24280[(2)] = null);

(statearr_22314_24280[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22309 === (6))){
var state_22308__$1 = state_22308;
var statearr_22315_24281 = state_22308__$1;
(statearr_22315_24281[(2)] = null);

(statearr_22315_24281[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22309 === (7))){
var inst_22304 = (state_22308[(2)]);
var state_22308__$1 = state_22308;
var statearr_22316_24284 = state_22308__$1;
(statearr_22316_24284[(2)] = inst_22304);

(statearr_22316_24284[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__24240,c__21913__auto___24268,G__22244_24241,G__22244_24242__$1,n__5593__auto___24239,jobs,results,process__$1,async))
;
return ((function (__24240,switch__21630__auto__,c__21913__auto___24268,G__22244_24241,G__22244_24242__$1,n__5593__auto___24239,jobs,results,process__$1,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____0 = (function (){
var statearr_22317 = [null,null,null,null,null,null,null];
(statearr_22317[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__);

(statearr_22317[(1)] = (1));

return statearr_22317;
});
var cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____1 = (function (state_22308){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_22308);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e22318){var ex__21634__auto__ = e22318;
var statearr_22319_24285 = state_22308;
(statearr_22319_24285[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_22308[(4)]))){
var statearr_22321_24286 = state_22308;
(statearr_22321_24286[(1)] = cljs.core.first((state_22308[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24289 = state_22308;
state_22308 = G__24289;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__ = function(state_22308){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____1.call(this,state_22308);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__;
})()
;})(__24240,switch__21630__auto__,c__21913__auto___24268,G__22244_24241,G__22244_24242__$1,n__5593__auto___24239,jobs,results,process__$1,async))
})();
var state__21915__auto__ = (function (){var statearr_22322 = f__21914__auto__();
(statearr_22322[(6)] = c__21913__auto___24268);

return statearr_22322;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
});})(__24240,c__21913__auto___24268,G__22244_24241,G__22244_24242__$1,n__5593__auto___24239,jobs,results,process__$1,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__22244_24242__$1)].join('')));

}

var G__24290 = (__24240 + (1));
__24240 = G__24290;
continue;
} else {
}
break;
}

var c__21913__auto___24294 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_22344){
var state_val_22345 = (state_22344[(1)]);
if((state_val_22345 === (7))){
var inst_22340 = (state_22344[(2)]);
var state_22344__$1 = state_22344;
var statearr_22347_24297 = state_22344__$1;
(statearr_22347_24297[(2)] = inst_22340);

(statearr_22347_24297[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22345 === (1))){
var state_22344__$1 = state_22344;
var statearr_22348_24298 = state_22344__$1;
(statearr_22348_24298[(2)] = null);

(statearr_22348_24298[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22345 === (4))){
var inst_22325 = (state_22344[(7)]);
var inst_22325__$1 = (state_22344[(2)]);
var inst_22326 = (inst_22325__$1 == null);
var state_22344__$1 = (function (){var statearr_22349 = state_22344;
(statearr_22349[(7)] = inst_22325__$1);

return statearr_22349;
})();
if(cljs.core.truth_(inst_22326)){
var statearr_22350_24299 = state_22344__$1;
(statearr_22350_24299[(1)] = (5));

} else {
var statearr_22351_24300 = state_22344__$1;
(statearr_22351_24300[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22345 === (6))){
var inst_22330 = (state_22344[(8)]);
var inst_22325 = (state_22344[(7)]);
var inst_22330__$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var inst_22331 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_22332 = [inst_22325,inst_22330__$1];
var inst_22333 = (new cljs.core.PersistentVector(null,2,(5),inst_22331,inst_22332,null));
var state_22344__$1 = (function (){var statearr_22352 = state_22344;
(statearr_22352[(8)] = inst_22330__$1);

return statearr_22352;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_22344__$1,(8),jobs,inst_22333);
} else {
if((state_val_22345 === (3))){
var inst_22342 = (state_22344[(2)]);
var state_22344__$1 = state_22344;
return cljs.core.async.impl.ioc_helpers.return_chan(state_22344__$1,inst_22342);
} else {
if((state_val_22345 === (2))){
var state_22344__$1 = state_22344;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_22344__$1,(4),from);
} else {
if((state_val_22345 === (9))){
var inst_22337 = (state_22344[(2)]);
var state_22344__$1 = (function (){var statearr_22354 = state_22344;
(statearr_22354[(9)] = inst_22337);

return statearr_22354;
})();
var statearr_22355_24302 = state_22344__$1;
(statearr_22355_24302[(2)] = null);

(statearr_22355_24302[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22345 === (5))){
var inst_22328 = cljs.core.async.close_BANG_(jobs);
var state_22344__$1 = state_22344;
var statearr_22356_24306 = state_22344__$1;
(statearr_22356_24306[(2)] = inst_22328);

(statearr_22356_24306[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22345 === (8))){
var inst_22330 = (state_22344[(8)]);
var inst_22335 = (state_22344[(2)]);
var state_22344__$1 = (function (){var statearr_22357 = state_22344;
(statearr_22357[(10)] = inst_22335);

return statearr_22357;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_22344__$1,(9),results,inst_22330);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____0 = (function (){
var statearr_22358 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_22358[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__);

(statearr_22358[(1)] = (1));

return statearr_22358;
});
var cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____1 = (function (state_22344){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_22344);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e22359){var ex__21634__auto__ = e22359;
var statearr_22361_24308 = state_22344;
(statearr_22361_24308[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_22344[(4)]))){
var statearr_22362_24309 = state_22344;
(statearr_22362_24309[(1)] = cljs.core.first((state_22344[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24310 = state_22344;
state_22344 = G__24310;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__ = function(state_22344){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____1.call(this,state_22344);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_22365 = f__21914__auto__();
(statearr_22365[(6)] = c__21913__auto___24294);

return statearr_22365;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));


var c__21913__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_22404){
var state_val_22405 = (state_22404[(1)]);
if((state_val_22405 === (7))){
var inst_22399 = (state_22404[(2)]);
var state_22404__$1 = state_22404;
var statearr_22406_24311 = state_22404__$1;
(statearr_22406_24311[(2)] = inst_22399);

(statearr_22406_24311[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22405 === (20))){
var state_22404__$1 = state_22404;
var statearr_22408_24312 = state_22404__$1;
(statearr_22408_24312[(2)] = null);

(statearr_22408_24312[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22405 === (1))){
var state_22404__$1 = state_22404;
var statearr_22409_24313 = state_22404__$1;
(statearr_22409_24313[(2)] = null);

(statearr_22409_24313[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22405 === (4))){
var inst_22368 = (state_22404[(7)]);
var inst_22368__$1 = (state_22404[(2)]);
var inst_22369 = (inst_22368__$1 == null);
var state_22404__$1 = (function (){var statearr_22412 = state_22404;
(statearr_22412[(7)] = inst_22368__$1);

return statearr_22412;
})();
if(cljs.core.truth_(inst_22369)){
var statearr_22413_24315 = state_22404__$1;
(statearr_22413_24315[(1)] = (5));

} else {
var statearr_22414_24316 = state_22404__$1;
(statearr_22414_24316[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22405 === (15))){
var inst_22381 = (state_22404[(8)]);
var state_22404__$1 = state_22404;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_22404__$1,(18),to,inst_22381);
} else {
if((state_val_22405 === (21))){
var inst_22394 = (state_22404[(2)]);
var state_22404__$1 = state_22404;
var statearr_22415_24317 = state_22404__$1;
(statearr_22415_24317[(2)] = inst_22394);

(statearr_22415_24317[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22405 === (13))){
var inst_22396 = (state_22404[(2)]);
var state_22404__$1 = (function (){var statearr_22416 = state_22404;
(statearr_22416[(9)] = inst_22396);

return statearr_22416;
})();
var statearr_22417_24318 = state_22404__$1;
(statearr_22417_24318[(2)] = null);

(statearr_22417_24318[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22405 === (6))){
var inst_22368 = (state_22404[(7)]);
var state_22404__$1 = state_22404;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_22404__$1,(11),inst_22368);
} else {
if((state_val_22405 === (17))){
var inst_22389 = (state_22404[(2)]);
var state_22404__$1 = state_22404;
if(cljs.core.truth_(inst_22389)){
var statearr_22418_24326 = state_22404__$1;
(statearr_22418_24326[(1)] = (19));

} else {
var statearr_22419_24327 = state_22404__$1;
(statearr_22419_24327[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22405 === (3))){
var inst_22402 = (state_22404[(2)]);
var state_22404__$1 = state_22404;
return cljs.core.async.impl.ioc_helpers.return_chan(state_22404__$1,inst_22402);
} else {
if((state_val_22405 === (12))){
var inst_22378 = (state_22404[(10)]);
var state_22404__$1 = state_22404;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_22404__$1,(14),inst_22378);
} else {
if((state_val_22405 === (2))){
var state_22404__$1 = state_22404;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_22404__$1,(4),results);
} else {
if((state_val_22405 === (19))){
var state_22404__$1 = state_22404;
var statearr_22421_24329 = state_22404__$1;
(statearr_22421_24329[(2)] = null);

(statearr_22421_24329[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22405 === (11))){
var inst_22378 = (state_22404[(2)]);
var state_22404__$1 = (function (){var statearr_22422 = state_22404;
(statearr_22422[(10)] = inst_22378);

return statearr_22422;
})();
var statearr_22423_24331 = state_22404__$1;
(statearr_22423_24331[(2)] = null);

(statearr_22423_24331[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22405 === (9))){
var state_22404__$1 = state_22404;
var statearr_22424_24332 = state_22404__$1;
(statearr_22424_24332[(2)] = null);

(statearr_22424_24332[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22405 === (5))){
var state_22404__$1 = state_22404;
if(cljs.core.truth_(close_QMARK_)){
var statearr_22425_24333 = state_22404__$1;
(statearr_22425_24333[(1)] = (8));

} else {
var statearr_22426_24334 = state_22404__$1;
(statearr_22426_24334[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22405 === (14))){
var inst_22381 = (state_22404[(8)]);
var inst_22383 = (state_22404[(11)]);
var inst_22381__$1 = (state_22404[(2)]);
var inst_22382 = (inst_22381__$1 == null);
var inst_22383__$1 = cljs.core.not(inst_22382);
var state_22404__$1 = (function (){var statearr_22427 = state_22404;
(statearr_22427[(8)] = inst_22381__$1);

(statearr_22427[(11)] = inst_22383__$1);

return statearr_22427;
})();
if(inst_22383__$1){
var statearr_22428_24337 = state_22404__$1;
(statearr_22428_24337[(1)] = (15));

} else {
var statearr_22429_24338 = state_22404__$1;
(statearr_22429_24338[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22405 === (16))){
var inst_22383 = (state_22404[(11)]);
var state_22404__$1 = state_22404;
var statearr_22430_24339 = state_22404__$1;
(statearr_22430_24339[(2)] = inst_22383);

(statearr_22430_24339[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22405 === (10))){
var inst_22375 = (state_22404[(2)]);
var state_22404__$1 = state_22404;
var statearr_22432_24342 = state_22404__$1;
(statearr_22432_24342[(2)] = inst_22375);

(statearr_22432_24342[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22405 === (18))){
var inst_22386 = (state_22404[(2)]);
var state_22404__$1 = state_22404;
var statearr_22433_24343 = state_22404__$1;
(statearr_22433_24343[(2)] = inst_22386);

(statearr_22433_24343[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22405 === (8))){
var inst_22372 = cljs.core.async.close_BANG_(to);
var state_22404__$1 = state_22404;
var statearr_22434_24344 = state_22404__$1;
(statearr_22434_24344[(2)] = inst_22372);

(statearr_22434_24344[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____0 = (function (){
var statearr_22435 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_22435[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__);

(statearr_22435[(1)] = (1));

return statearr_22435;
});
var cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____1 = (function (state_22404){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_22404);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e22436){var ex__21634__auto__ = e22436;
var statearr_22437_24347 = state_22404;
(statearr_22437_24347[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_22404[(4)]))){
var statearr_22439_24349 = state_22404;
(statearr_22439_24349[(1)] = cljs.core.first((state_22404[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24350 = state_22404;
state_22404 = G__24350;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__ = function(state_22404){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____1.call(this,state_22404);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__21631__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_22440 = f__21914__auto__();
(statearr_22440[(6)] = c__21913__auto__);

return statearr_22440;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));

return c__21913__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). The
 *   presumption is that af will return immediately, having launched some
 *   asynchronous operation whose completion/callback will put results on
 *   the channel, then close! it. Outputs will be returned in order
 *   relative to the inputs. By default, the to channel will be closed
 *   when the from channel closes, but can be determined by the close?
 *   parameter. Will stop consuming the from channel if the to channel
 *   closes. See also pipeline, pipeline-blocking.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__22443 = arguments.length;
switch (G__22443) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5(n,to,af,from,true);
}));

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_(n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
}));

(cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5);

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__22445 = arguments.length;
switch (G__22445) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5(n,to,xf,from,true);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6(n,to,xf,from,close_QMARK_,null);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
}));

(cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6);

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__22449 = arguments.length;
switch (G__22449) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4(p,ch,null,null);
}));

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(t_buf_or_n);
var fc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(f_buf_or_n);
var c__21913__auto___24357 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_22476){
var state_val_22477 = (state_22476[(1)]);
if((state_val_22477 === (7))){
var inst_22472 = (state_22476[(2)]);
var state_22476__$1 = state_22476;
var statearr_22478_24358 = state_22476__$1;
(statearr_22478_24358[(2)] = inst_22472);

(statearr_22478_24358[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22477 === (1))){
var state_22476__$1 = state_22476;
var statearr_22479_24359 = state_22476__$1;
(statearr_22479_24359[(2)] = null);

(statearr_22479_24359[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22477 === (4))){
var inst_22453 = (state_22476[(7)]);
var inst_22453__$1 = (state_22476[(2)]);
var inst_22454 = (inst_22453__$1 == null);
var state_22476__$1 = (function (){var statearr_22480 = state_22476;
(statearr_22480[(7)] = inst_22453__$1);

return statearr_22480;
})();
if(cljs.core.truth_(inst_22454)){
var statearr_22481_24360 = state_22476__$1;
(statearr_22481_24360[(1)] = (5));

} else {
var statearr_22482_24361 = state_22476__$1;
(statearr_22482_24361[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22477 === (13))){
var state_22476__$1 = state_22476;
var statearr_22484_24362 = state_22476__$1;
(statearr_22484_24362[(2)] = null);

(statearr_22484_24362[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22477 === (6))){
var inst_22453 = (state_22476[(7)]);
var inst_22459 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_22453) : p.call(null, inst_22453));
var state_22476__$1 = state_22476;
if(cljs.core.truth_(inst_22459)){
var statearr_22485_24363 = state_22476__$1;
(statearr_22485_24363[(1)] = (9));

} else {
var statearr_22486_24364 = state_22476__$1;
(statearr_22486_24364[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22477 === (3))){
var inst_22474 = (state_22476[(2)]);
var state_22476__$1 = state_22476;
return cljs.core.async.impl.ioc_helpers.return_chan(state_22476__$1,inst_22474);
} else {
if((state_val_22477 === (12))){
var state_22476__$1 = state_22476;
var statearr_22487_24365 = state_22476__$1;
(statearr_22487_24365[(2)] = null);

(statearr_22487_24365[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22477 === (2))){
var state_22476__$1 = state_22476;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_22476__$1,(4),ch);
} else {
if((state_val_22477 === (11))){
var inst_22453 = (state_22476[(7)]);
var inst_22463 = (state_22476[(2)]);
var state_22476__$1 = state_22476;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_22476__$1,(8),inst_22463,inst_22453);
} else {
if((state_val_22477 === (9))){
var state_22476__$1 = state_22476;
var statearr_22488_24370 = state_22476__$1;
(statearr_22488_24370[(2)] = tc);

(statearr_22488_24370[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22477 === (5))){
var inst_22456 = cljs.core.async.close_BANG_(tc);
var inst_22457 = cljs.core.async.close_BANG_(fc);
var state_22476__$1 = (function (){var statearr_22489 = state_22476;
(statearr_22489[(8)] = inst_22456);

return statearr_22489;
})();
var statearr_22490_24371 = state_22476__$1;
(statearr_22490_24371[(2)] = inst_22457);

(statearr_22490_24371[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22477 === (14))){
var inst_22470 = (state_22476[(2)]);
var state_22476__$1 = state_22476;
var statearr_22492_24372 = state_22476__$1;
(statearr_22492_24372[(2)] = inst_22470);

(statearr_22492_24372[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22477 === (10))){
var state_22476__$1 = state_22476;
var statearr_22493_24373 = state_22476__$1;
(statearr_22493_24373[(2)] = fc);

(statearr_22493_24373[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22477 === (8))){
var inst_22465 = (state_22476[(2)]);
var state_22476__$1 = state_22476;
if(cljs.core.truth_(inst_22465)){
var statearr_22494_24374 = state_22476__$1;
(statearr_22494_24374[(1)] = (12));

} else {
var statearr_22495_24376 = state_22476__$1;
(statearr_22495_24376[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__21631__auto__ = null;
var cljs$core$async$state_machine__21631__auto____0 = (function (){
var statearr_22496 = [null,null,null,null,null,null,null,null,null];
(statearr_22496[(0)] = cljs$core$async$state_machine__21631__auto__);

(statearr_22496[(1)] = (1));

return statearr_22496;
});
var cljs$core$async$state_machine__21631__auto____1 = (function (state_22476){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_22476);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e22497){var ex__21634__auto__ = e22497;
var statearr_22498_24377 = state_22476;
(statearr_22498_24377[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_22476[(4)]))){
var statearr_22499_24378 = state_22476;
(statearr_22499_24378[(1)] = cljs.core.first((state_22476[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24379 = state_22476;
state_22476 = G__24379;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$state_machine__21631__auto__ = function(state_22476){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21631__auto____1.call(this,state_22476);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21631__auto____0;
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21631__auto____1;
return cljs$core$async$state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_22501 = f__21914__auto__();
(statearr_22501[(6)] = c__21913__auto___24357);

return statearr_22501;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
}));

(cljs.core.async.split.cljs$lang$maxFixedArity = 4);

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__21913__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_22523){
var state_val_22525 = (state_22523[(1)]);
if((state_val_22525 === (7))){
var inst_22519 = (state_22523[(2)]);
var state_22523__$1 = state_22523;
var statearr_22526_24389 = state_22523__$1;
(statearr_22526_24389[(2)] = inst_22519);

(statearr_22526_24389[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22525 === (1))){
var inst_22502 = init;
var inst_22503 = inst_22502;
var state_22523__$1 = (function (){var statearr_22527 = state_22523;
(statearr_22527[(7)] = inst_22503);

return statearr_22527;
})();
var statearr_22528_24390 = state_22523__$1;
(statearr_22528_24390[(2)] = null);

(statearr_22528_24390[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22525 === (4))){
var inst_22506 = (state_22523[(8)]);
var inst_22506__$1 = (state_22523[(2)]);
var inst_22507 = (inst_22506__$1 == null);
var state_22523__$1 = (function (){var statearr_22529 = state_22523;
(statearr_22529[(8)] = inst_22506__$1);

return statearr_22529;
})();
if(cljs.core.truth_(inst_22507)){
var statearr_22530_24392 = state_22523__$1;
(statearr_22530_24392[(1)] = (5));

} else {
var statearr_22531_24394 = state_22523__$1;
(statearr_22531_24394[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22525 === (6))){
var inst_22506 = (state_22523[(8)]);
var inst_22510 = (state_22523[(9)]);
var inst_22503 = (state_22523[(7)]);
var inst_22510__$1 = (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(inst_22503,inst_22506) : f.call(null, inst_22503,inst_22506));
var inst_22511 = cljs.core.reduced_QMARK_(inst_22510__$1);
var state_22523__$1 = (function (){var statearr_22532 = state_22523;
(statearr_22532[(9)] = inst_22510__$1);

return statearr_22532;
})();
if(inst_22511){
var statearr_22533_24399 = state_22523__$1;
(statearr_22533_24399[(1)] = (8));

} else {
var statearr_22534_24400 = state_22523__$1;
(statearr_22534_24400[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22525 === (3))){
var inst_22521 = (state_22523[(2)]);
var state_22523__$1 = state_22523;
return cljs.core.async.impl.ioc_helpers.return_chan(state_22523__$1,inst_22521);
} else {
if((state_val_22525 === (2))){
var state_22523__$1 = state_22523;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_22523__$1,(4),ch);
} else {
if((state_val_22525 === (9))){
var inst_22510 = (state_22523[(9)]);
var inst_22503 = inst_22510;
var state_22523__$1 = (function (){var statearr_22535 = state_22523;
(statearr_22535[(7)] = inst_22503);

return statearr_22535;
})();
var statearr_22537_24401 = state_22523__$1;
(statearr_22537_24401[(2)] = null);

(statearr_22537_24401[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22525 === (5))){
var inst_22503 = (state_22523[(7)]);
var state_22523__$1 = state_22523;
var statearr_22538_24402 = state_22523__$1;
(statearr_22538_24402[(2)] = inst_22503);

(statearr_22538_24402[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22525 === (10))){
var inst_22517 = (state_22523[(2)]);
var state_22523__$1 = state_22523;
var statearr_22539_24409 = state_22523__$1;
(statearr_22539_24409[(2)] = inst_22517);

(statearr_22539_24409[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22525 === (8))){
var inst_22510 = (state_22523[(9)]);
var inst_22513 = cljs.core.deref(inst_22510);
var state_22523__$1 = state_22523;
var statearr_22540_24410 = state_22523__$1;
(statearr_22540_24410[(2)] = inst_22513);

(statearr_22540_24410[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$reduce_$_state_machine__21631__auto__ = null;
var cljs$core$async$reduce_$_state_machine__21631__auto____0 = (function (){
var statearr_22541 = [null,null,null,null,null,null,null,null,null,null];
(statearr_22541[(0)] = cljs$core$async$reduce_$_state_machine__21631__auto__);

(statearr_22541[(1)] = (1));

return statearr_22541;
});
var cljs$core$async$reduce_$_state_machine__21631__auto____1 = (function (state_22523){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_22523);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e22542){var ex__21634__auto__ = e22542;
var statearr_22543_24412 = state_22523;
(statearr_22543_24412[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_22523[(4)]))){
var statearr_22544_24414 = state_22523;
(statearr_22544_24414[(1)] = cljs.core.first((state_22523[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24421 = state_22523;
state_22523 = G__24421;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__21631__auto__ = function(state_22523){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__21631__auto____1.call(this,state_22523);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__21631__auto____0;
cljs$core$async$reduce_$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__21631__auto____1;
return cljs$core$async$reduce_$_state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_22545 = f__21914__auto__();
(statearr_22545[(6)] = c__21913__auto__);

return statearr_22545;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));

return c__21913__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = (xform.cljs$core$IFn$_invoke$arity$1 ? xform.cljs$core$IFn$_invoke$arity$1(f) : xform.call(null, f));
var c__21913__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_22552){
var state_val_22553 = (state_22552[(1)]);
if((state_val_22553 === (1))){
var inst_22547 = cljs.core.async.reduce(f__$1,init,ch);
var state_22552__$1 = state_22552;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_22552__$1,(2),inst_22547);
} else {
if((state_val_22553 === (2))){
var inst_22549 = (state_22552[(2)]);
var inst_22550 = (f__$1.cljs$core$IFn$_invoke$arity$1 ? f__$1.cljs$core$IFn$_invoke$arity$1(inst_22549) : f__$1.call(null, inst_22549));
var state_22552__$1 = state_22552;
return cljs.core.async.impl.ioc_helpers.return_chan(state_22552__$1,inst_22550);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$transduce_$_state_machine__21631__auto__ = null;
var cljs$core$async$transduce_$_state_machine__21631__auto____0 = (function (){
var statearr_22554 = [null,null,null,null,null,null,null];
(statearr_22554[(0)] = cljs$core$async$transduce_$_state_machine__21631__auto__);

(statearr_22554[(1)] = (1));

return statearr_22554;
});
var cljs$core$async$transduce_$_state_machine__21631__auto____1 = (function (state_22552){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_22552);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e22555){var ex__21634__auto__ = e22555;
var statearr_22556_24423 = state_22552;
(statearr_22556_24423[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_22552[(4)]))){
var statearr_22557_24424 = state_22552;
(statearr_22557_24424[(1)] = cljs.core.first((state_22552[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24425 = state_22552;
state_22552 = G__24425;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__21631__auto__ = function(state_22552){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__21631__auto____1.call(this,state_22552);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$transduce_$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__21631__auto____0;
cljs$core$async$transduce_$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__21631__auto____1;
return cljs$core$async$transduce_$_state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_22559 = f__21914__auto__();
(statearr_22559[(6)] = c__21913__auto__);

return statearr_22559;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));

return c__21913__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan_BANG_ = (function cljs$core$async$onto_chan_BANG_(var_args){
var G__22561 = arguments.length;
switch (G__22561) {
case 2:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__21913__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_22586){
var state_val_22587 = (state_22586[(1)]);
if((state_val_22587 === (7))){
var inst_22568 = (state_22586[(2)]);
var state_22586__$1 = state_22586;
var statearr_22589_24430 = state_22586__$1;
(statearr_22589_24430[(2)] = inst_22568);

(statearr_22589_24430[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22587 === (1))){
var inst_22562 = cljs.core.seq(coll);
var inst_22563 = inst_22562;
var state_22586__$1 = (function (){var statearr_22590 = state_22586;
(statearr_22590[(7)] = inst_22563);

return statearr_22590;
})();
var statearr_22591_24431 = state_22586__$1;
(statearr_22591_24431[(2)] = null);

(statearr_22591_24431[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22587 === (4))){
var inst_22563 = (state_22586[(7)]);
var inst_22566 = cljs.core.first(inst_22563);
var state_22586__$1 = state_22586;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_22586__$1,(7),ch,inst_22566);
} else {
if((state_val_22587 === (13))){
var inst_22580 = (state_22586[(2)]);
var state_22586__$1 = state_22586;
var statearr_22592_24437 = state_22586__$1;
(statearr_22592_24437[(2)] = inst_22580);

(statearr_22592_24437[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22587 === (6))){
var inst_22571 = (state_22586[(2)]);
var state_22586__$1 = state_22586;
if(cljs.core.truth_(inst_22571)){
var statearr_22593_24438 = state_22586__$1;
(statearr_22593_24438[(1)] = (8));

} else {
var statearr_22594_24439 = state_22586__$1;
(statearr_22594_24439[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22587 === (3))){
var inst_22584 = (state_22586[(2)]);
var state_22586__$1 = state_22586;
return cljs.core.async.impl.ioc_helpers.return_chan(state_22586__$1,inst_22584);
} else {
if((state_val_22587 === (12))){
var state_22586__$1 = state_22586;
var statearr_22595_24445 = state_22586__$1;
(statearr_22595_24445[(2)] = null);

(statearr_22595_24445[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22587 === (2))){
var inst_22563 = (state_22586[(7)]);
var state_22586__$1 = state_22586;
if(cljs.core.truth_(inst_22563)){
var statearr_22596_24446 = state_22586__$1;
(statearr_22596_24446[(1)] = (4));

} else {
var statearr_22597_24447 = state_22586__$1;
(statearr_22597_24447[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22587 === (11))){
var inst_22577 = cljs.core.async.close_BANG_(ch);
var state_22586__$1 = state_22586;
var statearr_22598_24448 = state_22586__$1;
(statearr_22598_24448[(2)] = inst_22577);

(statearr_22598_24448[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22587 === (9))){
var state_22586__$1 = state_22586;
if(cljs.core.truth_(close_QMARK_)){
var statearr_22600_24449 = state_22586__$1;
(statearr_22600_24449[(1)] = (11));

} else {
var statearr_22601_24450 = state_22586__$1;
(statearr_22601_24450[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22587 === (5))){
var inst_22563 = (state_22586[(7)]);
var state_22586__$1 = state_22586;
var statearr_22602_24451 = state_22586__$1;
(statearr_22602_24451[(2)] = inst_22563);

(statearr_22602_24451[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22587 === (10))){
var inst_22582 = (state_22586[(2)]);
var state_22586__$1 = state_22586;
var statearr_22603_24453 = state_22586__$1;
(statearr_22603_24453[(2)] = inst_22582);

(statearr_22603_24453[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22587 === (8))){
var inst_22563 = (state_22586[(7)]);
var inst_22573 = cljs.core.next(inst_22563);
var inst_22563__$1 = inst_22573;
var state_22586__$1 = (function (){var statearr_22604 = state_22586;
(statearr_22604[(7)] = inst_22563__$1);

return statearr_22604;
})();
var statearr_22605_24456 = state_22586__$1;
(statearr_22605_24456[(2)] = null);

(statearr_22605_24456[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__21631__auto__ = null;
var cljs$core$async$state_machine__21631__auto____0 = (function (){
var statearr_22606 = [null,null,null,null,null,null,null,null];
(statearr_22606[(0)] = cljs$core$async$state_machine__21631__auto__);

(statearr_22606[(1)] = (1));

return statearr_22606;
});
var cljs$core$async$state_machine__21631__auto____1 = (function (state_22586){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_22586);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e22607){var ex__21634__auto__ = e22607;
var statearr_22608_24457 = state_22586;
(statearr_22608_24457[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_22586[(4)]))){
var statearr_22609_24459 = state_22586;
(statearr_22609_24459[(1)] = cljs.core.first((state_22586[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24460 = state_22586;
state_22586 = G__24460;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$state_machine__21631__auto__ = function(state_22586){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21631__auto____1.call(this,state_22586);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21631__auto____0;
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21631__auto____1;
return cljs$core$async$state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_22611 = f__21914__auto__();
(statearr_22611[(6)] = c__21913__auto__);

return statearr_22611;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));

return c__21913__auto__;
}));

(cljs.core.async.onto_chan_BANG_.cljs$lang$maxFixedArity = 3);

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan_BANG_ = (function cljs$core$async$to_chan_BANG_(coll){
var ch = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.bounded_count((100),coll));
cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2(ch,coll);

return ch;
});
/**
 * Deprecated - use onto-chan!
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__22613 = arguments.length;
switch (G__22613) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,close_QMARK_);
}));

(cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - use to-chan!
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
return cljs.core.async.to_chan_BANG_(coll);
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

var cljs$core$async$Mux$muxch_STAR_$dyn_24468 = (function (_){
var x__5350__auto__ = (((_ == null))?null:_);
var m__5351__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__5351__auto__.call(null, _));
} else {
var m__5349__auto__ = (cljs.core.async.muxch_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__5349__auto__.call(null, _));
} else {
throw cljs.core.missing_protocol("Mux.muxch*",_);
}
}
});
cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((((!((_ == null)))) && ((!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
return cljs$core$async$Mux$muxch_STAR_$dyn_24468(_);
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

var cljs$core$async$Mult$tap_STAR_$dyn_24469 = (function (m,ch,close_QMARK_){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$3 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__5351__auto__.call(null, m,ch,close_QMARK_));
} else {
var m__5349__auto__ = (cljs.core.async.tap_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$3 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__5349__auto__.call(null, m,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Mult.tap*",m);
}
}
});
cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
return cljs$core$async$Mult$tap_STAR_$dyn_24469(m,ch,close_QMARK_);
}
});

var cljs$core$async$Mult$untap_STAR_$dyn_24470 = (function (m,ch){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5351__auto__.call(null, m,ch));
} else {
var m__5349__auto__ = (cljs.core.async.untap_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5349__auto__.call(null, m,ch));
} else {
throw cljs.core.missing_protocol("Mult.untap*",m);
}
}
});
cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mult$untap_STAR_$dyn_24470(m,ch);
}
});

var cljs$core$async$Mult$untap_all_STAR_$dyn_24475 = (function (m){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__5351__auto__.call(null, m));
} else {
var m__5349__auto__ = (cljs.core.async.untap_all_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__5349__auto__.call(null, m));
} else {
throw cljs.core.missing_protocol("Mult.untap-all*",m);
}
}
});
cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mult$untap_all_STAR_$dyn_24475(m);
}
});


/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async22617 = (function (ch,cs,meta22618){
this.ch = ch;
this.cs = cs;
this.meta22618 = meta22618;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async22617.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_22619,meta22618__$1){
var self__ = this;
var _22619__$1 = this;
return (new cljs.core.async.t_cljs$core$async22617(self__.ch,self__.cs,meta22618__$1));
}));

(cljs.core.async.t_cljs$core$async22617.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_22619){
var self__ = this;
var _22619__$1 = this;
return self__.meta22618;
}));

(cljs.core.async.t_cljs$core$async22617.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async22617.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async22617.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async22617.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
}));

(cljs.core.async.t_cljs$core$async22617.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch__$1);

return null;
}));

(cljs.core.async.t_cljs$core$async22617.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
}));

(cljs.core.async.t_cljs$core$async22617.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta22618","meta22618",1428184389,null)], null);
}));

(cljs.core.async.t_cljs$core$async22617.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async22617.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async22617");

(cljs.core.async.t_cljs$core$async22617.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async22617");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async22617.
 */
cljs.core.async.__GT_t_cljs$core$async22617 = (function cljs$core$async$__GT_t_cljs$core$async22617(ch,cs,meta22618){
return (new cljs.core.async.t_cljs$core$async22617(ch,cs,meta22618));
});


/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var m = (new cljs.core.async.t_cljs$core$async22617(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = (function (_){
if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,true);
} else {
return null;
}
});
var c__21913__auto___24487 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_22756){
var state_val_22757 = (state_22756[(1)]);
if((state_val_22757 === (7))){
var inst_22752 = (state_22756[(2)]);
var state_22756__$1 = state_22756;
var statearr_22759_24491 = state_22756__$1;
(statearr_22759_24491[(2)] = inst_22752);

(statearr_22759_24491[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (20))){
var inst_22655 = (state_22756[(7)]);
var inst_22668 = cljs.core.first(inst_22655);
var inst_22669 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_22668,(0),null);
var inst_22670 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_22668,(1),null);
var state_22756__$1 = (function (){var statearr_22760 = state_22756;
(statearr_22760[(8)] = inst_22669);

return statearr_22760;
})();
if(cljs.core.truth_(inst_22670)){
var statearr_22761_24492 = state_22756__$1;
(statearr_22761_24492[(1)] = (22));

} else {
var statearr_22762_24493 = state_22756__$1;
(statearr_22762_24493[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (27))){
var inst_22705 = (state_22756[(9)]);
var inst_22700 = (state_22756[(10)]);
var inst_22624 = (state_22756[(11)]);
var inst_22698 = (state_22756[(12)]);
var inst_22705__$1 = cljs.core._nth(inst_22698,inst_22700);
var inst_22706 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_22705__$1,inst_22624,done);
var state_22756__$1 = (function (){var statearr_22764 = state_22756;
(statearr_22764[(9)] = inst_22705__$1);

return statearr_22764;
})();
if(cljs.core.truth_(inst_22706)){
var statearr_22765_24494 = state_22756__$1;
(statearr_22765_24494[(1)] = (30));

} else {
var statearr_22766_24495 = state_22756__$1;
(statearr_22766_24495[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (1))){
var state_22756__$1 = state_22756;
var statearr_22767_24496 = state_22756__$1;
(statearr_22767_24496[(2)] = null);

(statearr_22767_24496[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (24))){
var inst_22655 = (state_22756[(7)]);
var inst_22675 = (state_22756[(2)]);
var inst_22676 = cljs.core.next(inst_22655);
var inst_22633 = inst_22676;
var inst_22634 = null;
var inst_22635 = (0);
var inst_22636 = (0);
var state_22756__$1 = (function (){var statearr_22768 = state_22756;
(statearr_22768[(13)] = inst_22634);

(statearr_22768[(14)] = inst_22635);

(statearr_22768[(15)] = inst_22675);

(statearr_22768[(16)] = inst_22633);

(statearr_22768[(17)] = inst_22636);

return statearr_22768;
})();
var statearr_22769_24498 = state_22756__$1;
(statearr_22769_24498[(2)] = null);

(statearr_22769_24498[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (39))){
var state_22756__$1 = state_22756;
var statearr_22773_24500 = state_22756__$1;
(statearr_22773_24500[(2)] = null);

(statearr_22773_24500[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (4))){
var inst_22624 = (state_22756[(11)]);
var inst_22624__$1 = (state_22756[(2)]);
var inst_22625 = (inst_22624__$1 == null);
var state_22756__$1 = (function (){var statearr_22775 = state_22756;
(statearr_22775[(11)] = inst_22624__$1);

return statearr_22775;
})();
if(cljs.core.truth_(inst_22625)){
var statearr_22776_24504 = state_22756__$1;
(statearr_22776_24504[(1)] = (5));

} else {
var statearr_22777_24505 = state_22756__$1;
(statearr_22777_24505[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (15))){
var inst_22634 = (state_22756[(13)]);
var inst_22635 = (state_22756[(14)]);
var inst_22633 = (state_22756[(16)]);
var inst_22636 = (state_22756[(17)]);
var inst_22651 = (state_22756[(2)]);
var inst_22652 = (inst_22636 + (1));
var tmp22770 = inst_22634;
var tmp22771 = inst_22635;
var tmp22772 = inst_22633;
var inst_22633__$1 = tmp22772;
var inst_22634__$1 = tmp22770;
var inst_22635__$1 = tmp22771;
var inst_22636__$1 = inst_22652;
var state_22756__$1 = (function (){var statearr_22778 = state_22756;
(statearr_22778[(13)] = inst_22634__$1);

(statearr_22778[(14)] = inst_22635__$1);

(statearr_22778[(18)] = inst_22651);

(statearr_22778[(16)] = inst_22633__$1);

(statearr_22778[(17)] = inst_22636__$1);

return statearr_22778;
})();
var statearr_22779_24506 = state_22756__$1;
(statearr_22779_24506[(2)] = null);

(statearr_22779_24506[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (21))){
var inst_22679 = (state_22756[(2)]);
var state_22756__$1 = state_22756;
var statearr_22783_24507 = state_22756__$1;
(statearr_22783_24507[(2)] = inst_22679);

(statearr_22783_24507[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (31))){
var inst_22705 = (state_22756[(9)]);
var inst_22709 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null, inst_22705);
var state_22756__$1 = state_22756;
var statearr_22784_24508 = state_22756__$1;
(statearr_22784_24508[(2)] = inst_22709);

(statearr_22784_24508[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (32))){
var inst_22699 = (state_22756[(19)]);
var inst_22700 = (state_22756[(10)]);
var inst_22697 = (state_22756[(20)]);
var inst_22698 = (state_22756[(12)]);
var inst_22711 = (state_22756[(2)]);
var inst_22712 = (inst_22700 + (1));
var tmp22780 = inst_22699;
var tmp22781 = inst_22697;
var tmp22782 = inst_22698;
var inst_22697__$1 = tmp22781;
var inst_22698__$1 = tmp22782;
var inst_22699__$1 = tmp22780;
var inst_22700__$1 = inst_22712;
var state_22756__$1 = (function (){var statearr_22786 = state_22756;
(statearr_22786[(19)] = inst_22699__$1);

(statearr_22786[(21)] = inst_22711);

(statearr_22786[(10)] = inst_22700__$1);

(statearr_22786[(20)] = inst_22697__$1);

(statearr_22786[(12)] = inst_22698__$1);

return statearr_22786;
})();
var statearr_22787_24509 = state_22756__$1;
(statearr_22787_24509[(2)] = null);

(statearr_22787_24509[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (40))){
var inst_22725 = (state_22756[(22)]);
var inst_22729 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null, inst_22725);
var state_22756__$1 = state_22756;
var statearr_22788_24510 = state_22756__$1;
(statearr_22788_24510[(2)] = inst_22729);

(statearr_22788_24510[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (33))){
var inst_22715 = (state_22756[(23)]);
var inst_22717 = cljs.core.chunked_seq_QMARK_(inst_22715);
var state_22756__$1 = state_22756;
if(inst_22717){
var statearr_22789_24512 = state_22756__$1;
(statearr_22789_24512[(1)] = (36));

} else {
var statearr_22790_24516 = state_22756__$1;
(statearr_22790_24516[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (13))){
var inst_22645 = (state_22756[(24)]);
var inst_22648 = cljs.core.async.close_BANG_(inst_22645);
var state_22756__$1 = state_22756;
var statearr_22796_24520 = state_22756__$1;
(statearr_22796_24520[(2)] = inst_22648);

(statearr_22796_24520[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (22))){
var inst_22669 = (state_22756[(8)]);
var inst_22672 = cljs.core.async.close_BANG_(inst_22669);
var state_22756__$1 = state_22756;
var statearr_22797_24521 = state_22756__$1;
(statearr_22797_24521[(2)] = inst_22672);

(statearr_22797_24521[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (36))){
var inst_22715 = (state_22756[(23)]);
var inst_22719 = cljs.core.chunk_first(inst_22715);
var inst_22720 = cljs.core.chunk_rest(inst_22715);
var inst_22721 = cljs.core.count(inst_22719);
var inst_22697 = inst_22720;
var inst_22698 = inst_22719;
var inst_22699 = inst_22721;
var inst_22700 = (0);
var state_22756__$1 = (function (){var statearr_22798 = state_22756;
(statearr_22798[(19)] = inst_22699);

(statearr_22798[(10)] = inst_22700);

(statearr_22798[(20)] = inst_22697);

(statearr_22798[(12)] = inst_22698);

return statearr_22798;
})();
var statearr_22799_24525 = state_22756__$1;
(statearr_22799_24525[(2)] = null);

(statearr_22799_24525[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (41))){
var inst_22715 = (state_22756[(23)]);
var inst_22731 = (state_22756[(2)]);
var inst_22732 = cljs.core.next(inst_22715);
var inst_22697 = inst_22732;
var inst_22698 = null;
var inst_22699 = (0);
var inst_22700 = (0);
var state_22756__$1 = (function (){var statearr_22800 = state_22756;
(statearr_22800[(19)] = inst_22699);

(statearr_22800[(10)] = inst_22700);

(statearr_22800[(20)] = inst_22697);

(statearr_22800[(25)] = inst_22731);

(statearr_22800[(12)] = inst_22698);

return statearr_22800;
})();
var statearr_22801_24526 = state_22756__$1;
(statearr_22801_24526[(2)] = null);

(statearr_22801_24526[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (43))){
var state_22756__$1 = state_22756;
var statearr_22802_24530 = state_22756__$1;
(statearr_22802_24530[(2)] = null);

(statearr_22802_24530[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (29))){
var inst_22740 = (state_22756[(2)]);
var state_22756__$1 = state_22756;
var statearr_22803_24531 = state_22756__$1;
(statearr_22803_24531[(2)] = inst_22740);

(statearr_22803_24531[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (44))){
var inst_22749 = (state_22756[(2)]);
var state_22756__$1 = (function (){var statearr_22804 = state_22756;
(statearr_22804[(26)] = inst_22749);

return statearr_22804;
})();
var statearr_22805_24532 = state_22756__$1;
(statearr_22805_24532[(2)] = null);

(statearr_22805_24532[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (6))){
var inst_22689 = (state_22756[(27)]);
var inst_22688 = cljs.core.deref(cs);
var inst_22689__$1 = cljs.core.keys(inst_22688);
var inst_22690 = cljs.core.count(inst_22689__$1);
var inst_22691 = cljs.core.reset_BANG_(dctr,inst_22690);
var inst_22696 = cljs.core.seq(inst_22689__$1);
var inst_22697 = inst_22696;
var inst_22698 = null;
var inst_22699 = (0);
var inst_22700 = (0);
var state_22756__$1 = (function (){var statearr_22808 = state_22756;
(statearr_22808[(27)] = inst_22689__$1);

(statearr_22808[(19)] = inst_22699);

(statearr_22808[(28)] = inst_22691);

(statearr_22808[(10)] = inst_22700);

(statearr_22808[(20)] = inst_22697);

(statearr_22808[(12)] = inst_22698);

return statearr_22808;
})();
var statearr_22809_24536 = state_22756__$1;
(statearr_22809_24536[(2)] = null);

(statearr_22809_24536[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (28))){
var inst_22697 = (state_22756[(20)]);
var inst_22715 = (state_22756[(23)]);
var inst_22715__$1 = cljs.core.seq(inst_22697);
var state_22756__$1 = (function (){var statearr_22810 = state_22756;
(statearr_22810[(23)] = inst_22715__$1);

return statearr_22810;
})();
if(inst_22715__$1){
var statearr_22811_24537 = state_22756__$1;
(statearr_22811_24537[(1)] = (33));

} else {
var statearr_22812_24538 = state_22756__$1;
(statearr_22812_24538[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (25))){
var inst_22699 = (state_22756[(19)]);
var inst_22700 = (state_22756[(10)]);
var inst_22702 = (inst_22700 < inst_22699);
var inst_22703 = inst_22702;
var state_22756__$1 = state_22756;
if(cljs.core.truth_(inst_22703)){
var statearr_22813_24546 = state_22756__$1;
(statearr_22813_24546[(1)] = (27));

} else {
var statearr_22814_24547 = state_22756__$1;
(statearr_22814_24547[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (34))){
var state_22756__$1 = state_22756;
var statearr_22815_24548 = state_22756__$1;
(statearr_22815_24548[(2)] = null);

(statearr_22815_24548[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (17))){
var state_22756__$1 = state_22756;
var statearr_22816_24549 = state_22756__$1;
(statearr_22816_24549[(2)] = null);

(statearr_22816_24549[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (3))){
var inst_22754 = (state_22756[(2)]);
var state_22756__$1 = state_22756;
return cljs.core.async.impl.ioc_helpers.return_chan(state_22756__$1,inst_22754);
} else {
if((state_val_22757 === (12))){
var inst_22684 = (state_22756[(2)]);
var state_22756__$1 = state_22756;
var statearr_22817_24550 = state_22756__$1;
(statearr_22817_24550[(2)] = inst_22684);

(statearr_22817_24550[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (2))){
var state_22756__$1 = state_22756;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_22756__$1,(4),ch);
} else {
if((state_val_22757 === (23))){
var state_22756__$1 = state_22756;
var statearr_22818_24554 = state_22756__$1;
(statearr_22818_24554[(2)] = null);

(statearr_22818_24554[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (35))){
var inst_22738 = (state_22756[(2)]);
var state_22756__$1 = state_22756;
var statearr_22819_24555 = state_22756__$1;
(statearr_22819_24555[(2)] = inst_22738);

(statearr_22819_24555[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (19))){
var inst_22655 = (state_22756[(7)]);
var inst_22660 = cljs.core.chunk_first(inst_22655);
var inst_22661 = cljs.core.chunk_rest(inst_22655);
var inst_22662 = cljs.core.count(inst_22660);
var inst_22633 = inst_22661;
var inst_22634 = inst_22660;
var inst_22635 = inst_22662;
var inst_22636 = (0);
var state_22756__$1 = (function (){var statearr_22823 = state_22756;
(statearr_22823[(13)] = inst_22634);

(statearr_22823[(14)] = inst_22635);

(statearr_22823[(16)] = inst_22633);

(statearr_22823[(17)] = inst_22636);

return statearr_22823;
})();
var statearr_22824_24559 = state_22756__$1;
(statearr_22824_24559[(2)] = null);

(statearr_22824_24559[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (11))){
var inst_22655 = (state_22756[(7)]);
var inst_22633 = (state_22756[(16)]);
var inst_22655__$1 = cljs.core.seq(inst_22633);
var state_22756__$1 = (function (){var statearr_22825 = state_22756;
(statearr_22825[(7)] = inst_22655__$1);

return statearr_22825;
})();
if(inst_22655__$1){
var statearr_22826_24561 = state_22756__$1;
(statearr_22826_24561[(1)] = (16));

} else {
var statearr_22827_24562 = state_22756__$1;
(statearr_22827_24562[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (9))){
var inst_22686 = (state_22756[(2)]);
var state_22756__$1 = state_22756;
var statearr_22828_24563 = state_22756__$1;
(statearr_22828_24563[(2)] = inst_22686);

(statearr_22828_24563[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (5))){
var inst_22631 = cljs.core.deref(cs);
var inst_22632 = cljs.core.seq(inst_22631);
var inst_22633 = inst_22632;
var inst_22634 = null;
var inst_22635 = (0);
var inst_22636 = (0);
var state_22756__$1 = (function (){var statearr_22832 = state_22756;
(statearr_22832[(13)] = inst_22634);

(statearr_22832[(14)] = inst_22635);

(statearr_22832[(16)] = inst_22633);

(statearr_22832[(17)] = inst_22636);

return statearr_22832;
})();
var statearr_22836_24567 = state_22756__$1;
(statearr_22836_24567[(2)] = null);

(statearr_22836_24567[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (14))){
var state_22756__$1 = state_22756;
var statearr_22837_24568 = state_22756__$1;
(statearr_22837_24568[(2)] = null);

(statearr_22837_24568[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (45))){
var inst_22746 = (state_22756[(2)]);
var state_22756__$1 = state_22756;
var statearr_22838_24569 = state_22756__$1;
(statearr_22838_24569[(2)] = inst_22746);

(statearr_22838_24569[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (26))){
var inst_22689 = (state_22756[(27)]);
var inst_22742 = (state_22756[(2)]);
var inst_22743 = cljs.core.seq(inst_22689);
var state_22756__$1 = (function (){var statearr_22839 = state_22756;
(statearr_22839[(29)] = inst_22742);

return statearr_22839;
})();
if(inst_22743){
var statearr_22840_24573 = state_22756__$1;
(statearr_22840_24573[(1)] = (42));

} else {
var statearr_22889_24574 = state_22756__$1;
(statearr_22889_24574[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (16))){
var inst_22655 = (state_22756[(7)]);
var inst_22658 = cljs.core.chunked_seq_QMARK_(inst_22655);
var state_22756__$1 = state_22756;
if(inst_22658){
var statearr_22891_24576 = state_22756__$1;
(statearr_22891_24576[(1)] = (19));

} else {
var statearr_22893_24577 = state_22756__$1;
(statearr_22893_24577[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (38))){
var inst_22735 = (state_22756[(2)]);
var state_22756__$1 = state_22756;
var statearr_22896_24578 = state_22756__$1;
(statearr_22896_24578[(2)] = inst_22735);

(statearr_22896_24578[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (30))){
var state_22756__$1 = state_22756;
var statearr_22898_24579 = state_22756__$1;
(statearr_22898_24579[(2)] = null);

(statearr_22898_24579[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (10))){
var inst_22634 = (state_22756[(13)]);
var inst_22636 = (state_22756[(17)]);
var inst_22644 = cljs.core._nth(inst_22634,inst_22636);
var inst_22645 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_22644,(0),null);
var inst_22646 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_22644,(1),null);
var state_22756__$1 = (function (){var statearr_22901 = state_22756;
(statearr_22901[(24)] = inst_22645);

return statearr_22901;
})();
if(cljs.core.truth_(inst_22646)){
var statearr_22902_24583 = state_22756__$1;
(statearr_22902_24583[(1)] = (13));

} else {
var statearr_22903_24584 = state_22756__$1;
(statearr_22903_24584[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (18))){
var inst_22682 = (state_22756[(2)]);
var state_22756__$1 = state_22756;
var statearr_22904_24585 = state_22756__$1;
(statearr_22904_24585[(2)] = inst_22682);

(statearr_22904_24585[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (42))){
var state_22756__$1 = state_22756;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_22756__$1,(45),dchan);
} else {
if((state_val_22757 === (37))){
var inst_22725 = (state_22756[(22)]);
var inst_22624 = (state_22756[(11)]);
var inst_22715 = (state_22756[(23)]);
var inst_22725__$1 = cljs.core.first(inst_22715);
var inst_22726 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_22725__$1,inst_22624,done);
var state_22756__$1 = (function (){var statearr_22906 = state_22756;
(statearr_22906[(22)] = inst_22725__$1);

return statearr_22906;
})();
if(cljs.core.truth_(inst_22726)){
var statearr_22907_24586 = state_22756__$1;
(statearr_22907_24586[(1)] = (39));

} else {
var statearr_22908_24587 = state_22756__$1;
(statearr_22908_24587[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22757 === (8))){
var inst_22635 = (state_22756[(14)]);
var inst_22636 = (state_22756[(17)]);
var inst_22638 = (inst_22636 < inst_22635);
var inst_22639 = inst_22638;
var state_22756__$1 = state_22756;
if(cljs.core.truth_(inst_22639)){
var statearr_22910_24588 = state_22756__$1;
(statearr_22910_24588[(1)] = (10));

} else {
var statearr_22913_24589 = state_22756__$1;
(statearr_22913_24589[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mult_$_state_machine__21631__auto__ = null;
var cljs$core$async$mult_$_state_machine__21631__auto____0 = (function (){
var statearr_22916 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_22916[(0)] = cljs$core$async$mult_$_state_machine__21631__auto__);

(statearr_22916[(1)] = (1));

return statearr_22916;
});
var cljs$core$async$mult_$_state_machine__21631__auto____1 = (function (state_22756){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_22756);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e22920){var ex__21634__auto__ = e22920;
var statearr_22921_24590 = state_22756;
(statearr_22921_24590[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_22756[(4)]))){
var statearr_22922_24591 = state_22756;
(statearr_22922_24591[(1)] = cljs.core.first((state_22756[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24592 = state_22756;
state_22756 = G__24592;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__21631__auto__ = function(state_22756){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__21631__auto____1.call(this,state_22756);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__21631__auto____0;
cljs$core$async$mult_$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__21631__auto____1;
return cljs$core$async$mult_$_state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_22931 = f__21914__auto__();
(statearr_22931[(6)] = c__21913__auto___24487);

return statearr_22931;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__22937 = arguments.length;
switch (G__22937) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(mult,ch,true);
}));

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_(mult,ch,close_QMARK_);

return ch;
}));

(cljs.core.async.tap.cljs$lang$maxFixedArity = 3);

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_(mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_(mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

var cljs$core$async$Mix$admix_STAR_$dyn_24594 = (function (m,ch){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5351__auto__.call(null, m,ch));
} else {
var m__5349__auto__ = (cljs.core.async.admix_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5349__auto__.call(null, m,ch));
} else {
throw cljs.core.missing_protocol("Mix.admix*",m);
}
}
});
cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$admix_STAR_$dyn_24594(m,ch);
}
});

var cljs$core$async$Mix$unmix_STAR_$dyn_24596 = (function (m,ch){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5351__auto__.call(null, m,ch));
} else {
var m__5349__auto__ = (cljs.core.async.unmix_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5349__auto__.call(null, m,ch));
} else {
throw cljs.core.missing_protocol("Mix.unmix*",m);
}
}
});
cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$unmix_STAR_$dyn_24596(m,ch);
}
});

var cljs$core$async$Mix$unmix_all_STAR_$dyn_24597 = (function (m){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__5351__auto__.call(null, m));
} else {
var m__5349__auto__ = (cljs.core.async.unmix_all_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__5349__auto__.call(null, m));
} else {
throw cljs.core.missing_protocol("Mix.unmix-all*",m);
}
}
});
cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mix$unmix_all_STAR_$dyn_24597(m);
}
});

var cljs$core$async$Mix$toggle_STAR_$dyn_24598 = (function (m,state_map){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__5351__auto__.call(null, m,state_map));
} else {
var m__5349__auto__ = (cljs.core.async.toggle_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__5349__auto__.call(null, m,state_map));
} else {
throw cljs.core.missing_protocol("Mix.toggle*",m);
}
}
});
cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
return cljs$core$async$Mix$toggle_STAR_$dyn_24598(m,state_map);
}
});

var cljs$core$async$Mix$solo_mode_STAR_$dyn_24605 = (function (m,mode){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__5351__auto__.call(null, m,mode));
} else {
var m__5349__auto__ = (cljs.core.async.solo_mode_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__5349__auto__.call(null, m,mode));
} else {
throw cljs.core.missing_protocol("Mix.solo-mode*",m);
}
}
});
cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
return cljs$core$async$Mix$solo_mode_STAR_$dyn_24605(m,mode);
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__5732__auto__ = [];
var len__5726__auto___24613 = arguments.length;
var i__5727__auto___24614 = (0);
while(true){
if((i__5727__auto___24614 < len__5726__auto___24613)){
args__5732__auto__.push((arguments[i__5727__auto___24614]));

var G__24615 = (i__5727__auto___24614 + (1));
i__5727__auto___24614 = G__24615;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((3) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__5733__auto__);
});

(cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__23023){
var map__23026 = p__23023;
var map__23026__$1 = cljs.core.__destructure_map(map__23026);
var opts = map__23026__$1;
var statearr_23027_24617 = state;
(statearr_23027_24617[(1)] = cont_block);


var temp__5804__auto__ = cljs.core.async.do_alts((function (val){
var statearr_23041_24618 = state;
(statearr_23041_24618[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state);
}),ports,opts);
if(cljs.core.truth_(temp__5804__auto__)){
var cb = temp__5804__auto__;
var statearr_23045_24619 = state;
(statearr_23045_24619[(2)] = cljs.core.deref(cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}));

(cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3));

/** @this {Function} */
(cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq23017){
var G__23018 = cljs.core.first(seq23017);
var seq23017__$1 = cljs.core.next(seq23017);
var G__23019 = cljs.core.first(seq23017__$1);
var seq23017__$2 = cljs.core.next(seq23017__$1);
var G__23020 = cljs.core.first(seq23017__$2);
var seq23017__$3 = cljs.core.next(seq23017__$2);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__23018,G__23019,G__23020,seq23017__$3);
}));


/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async23047 = (function (change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta23048){
this.change = change;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta23048 = meta23048;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async23047.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_23049,meta23048__$1){
var self__ = this;
var _23049__$1 = this;
return (new cljs.core.async.t_cljs$core$async23047(self__.change,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta23048__$1));
}));

(cljs.core.async.t_cljs$core$async23047.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_23049){
var self__ = this;
var _23049__$1 = this;
return self__.meta23048;
}));

(cljs.core.async.t_cljs$core$async23047.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23047.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
}));

(cljs.core.async.t_cljs$core$async23047.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23047.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null, ));
}));

(cljs.core.async.t_cljs$core$async23047.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null, ));
}));

(cljs.core.async.t_cljs$core$async23047.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null, ));
}));

(cljs.core.async.t_cljs$core$async23047.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.merge_with,cljs.core.merge),state_map);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null, ));
}));

(cljs.core.async.t_cljs$core$async23047.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.solo_modes.cljs$core$IFn$_invoke$arity$1 ? self__.solo_modes.cljs$core$IFn$_invoke$arity$1(mode) : self__.solo_modes.call(null, mode)))){
} else {
throw (new Error(["Assert failed: ",["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join(''),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_(self__.solo_mode,mode);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null, ));
}));

(cljs.core.async.t_cljs$core$async23047.getBasis = (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta23048","meta23048",-708810050,null)], null);
}));

(cljs.core.async.t_cljs$core$async23047.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async23047.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async23047");

(cljs.core.async.t_cljs$core$async23047.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async23047");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async23047.
 */
cljs.core.async.__GT_t_cljs$core$async23047 = (function cljs$core$async$__GT_t_cljs$core$async23047(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta23048){
return (new cljs.core.async.t_cljs$core$async23047(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta23048));
});


/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.async.sliding_buffer((1)));
var changed = (function (){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(change,true);
});
var pick = (function (attr,chs){
return cljs.core.reduce_kv((function (ret,c,v){
if(cljs.core.truth_((attr.cljs$core$IFn$_invoke$arity$1 ? attr.cljs$core$IFn$_invoke$arity$1(v) : attr.call(null, v)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,c);
} else {
return ret;
}
}),cljs.core.PersistentHashSet.EMPTY,chs);
});
var calc_state = (function (){
var chs = cljs.core.deref(cs);
var mode = cljs.core.deref(solo_mode);
var solos = pick(new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick(new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick(new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(((((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && ((!(cljs.core.empty_QMARK_(solos))))))?cljs.core.vec(solos):cljs.core.vec(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(pauses,cljs.core.keys(chs)))),change)], null);
});
var m = (new cljs.core.async.t_cljs$core$async23047(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
var c__21913__auto___24624 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_23119){
var state_val_23120 = (state_23119[(1)]);
if((state_val_23120 === (7))){
var inst_23079 = (state_23119[(2)]);
var state_23119__$1 = state_23119;
if(cljs.core.truth_(inst_23079)){
var statearr_23121_24625 = state_23119__$1;
(statearr_23121_24625[(1)] = (8));

} else {
var statearr_23122_24626 = state_23119__$1;
(statearr_23122_24626[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (20))){
var inst_23072 = (state_23119[(7)]);
var state_23119__$1 = state_23119;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23119__$1,(23),out,inst_23072);
} else {
if((state_val_23120 === (1))){
var inst_23053 = calc_state();
var inst_23054 = cljs.core.__destructure_map(inst_23053);
var inst_23055 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_23054,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_23057 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_23054,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_23058 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_23054,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_23059 = inst_23053;
var state_23119__$1 = (function (){var statearr_23123 = state_23119;
(statearr_23123[(8)] = inst_23057);

(statearr_23123[(9)] = inst_23055);

(statearr_23123[(10)] = inst_23059);

(statearr_23123[(11)] = inst_23058);

return statearr_23123;
})();
var statearr_23124_24627 = state_23119__$1;
(statearr_23124_24627[(2)] = null);

(statearr_23124_24627[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (24))){
var inst_23063 = (state_23119[(12)]);
var inst_23059 = inst_23063;
var state_23119__$1 = (function (){var statearr_23125 = state_23119;
(statearr_23125[(10)] = inst_23059);

return statearr_23125;
})();
var statearr_23126_24628 = state_23119__$1;
(statearr_23126_24628[(2)] = null);

(statearr_23126_24628[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (4))){
var inst_23072 = (state_23119[(7)]);
var inst_23074 = (state_23119[(13)]);
var inst_23071 = (state_23119[(2)]);
var inst_23072__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_23071,(0),null);
var inst_23073 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_23071,(1),null);
var inst_23074__$1 = (inst_23072__$1 == null);
var state_23119__$1 = (function (){var statearr_23127 = state_23119;
(statearr_23127[(7)] = inst_23072__$1);

(statearr_23127[(13)] = inst_23074__$1);

(statearr_23127[(14)] = inst_23073);

return statearr_23127;
})();
if(cljs.core.truth_(inst_23074__$1)){
var statearr_23128_24633 = state_23119__$1;
(statearr_23128_24633[(1)] = (5));

} else {
var statearr_23129_24634 = state_23119__$1;
(statearr_23129_24634[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (15))){
var inst_23064 = (state_23119[(15)]);
var inst_23093 = (state_23119[(16)]);
var inst_23093__$1 = cljs.core.empty_QMARK_(inst_23064);
var state_23119__$1 = (function (){var statearr_23130 = state_23119;
(statearr_23130[(16)] = inst_23093__$1);

return statearr_23130;
})();
if(inst_23093__$1){
var statearr_23131_24635 = state_23119__$1;
(statearr_23131_24635[(1)] = (17));

} else {
var statearr_23132_24636 = state_23119__$1;
(statearr_23132_24636[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (21))){
var inst_23063 = (state_23119[(12)]);
var inst_23059 = inst_23063;
var state_23119__$1 = (function (){var statearr_23133 = state_23119;
(statearr_23133[(10)] = inst_23059);

return statearr_23133;
})();
var statearr_23134_24639 = state_23119__$1;
(statearr_23134_24639[(2)] = null);

(statearr_23134_24639[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (13))){
var inst_23086 = (state_23119[(2)]);
var inst_23087 = calc_state();
var inst_23059 = inst_23087;
var state_23119__$1 = (function (){var statearr_23135 = state_23119;
(statearr_23135[(17)] = inst_23086);

(statearr_23135[(10)] = inst_23059);

return statearr_23135;
})();
var statearr_23136_24642 = state_23119__$1;
(statearr_23136_24642[(2)] = null);

(statearr_23136_24642[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (22))){
var inst_23113 = (state_23119[(2)]);
var state_23119__$1 = state_23119;
var statearr_23137_24643 = state_23119__$1;
(statearr_23137_24643[(2)] = inst_23113);

(statearr_23137_24643[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (6))){
var inst_23073 = (state_23119[(14)]);
var inst_23077 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_23073,change);
var state_23119__$1 = state_23119;
var statearr_23138_24644 = state_23119__$1;
(statearr_23138_24644[(2)] = inst_23077);

(statearr_23138_24644[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (25))){
var state_23119__$1 = state_23119;
var statearr_23139_24645 = state_23119__$1;
(statearr_23139_24645[(2)] = null);

(statearr_23139_24645[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (17))){
var inst_23065 = (state_23119[(18)]);
var inst_23073 = (state_23119[(14)]);
var inst_23095 = (inst_23065.cljs$core$IFn$_invoke$arity$1 ? inst_23065.cljs$core$IFn$_invoke$arity$1(inst_23073) : inst_23065.call(null, inst_23073));
var inst_23096 = cljs.core.not(inst_23095);
var state_23119__$1 = state_23119;
var statearr_23140_24649 = state_23119__$1;
(statearr_23140_24649[(2)] = inst_23096);

(statearr_23140_24649[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (3))){
var inst_23117 = (state_23119[(2)]);
var state_23119__$1 = state_23119;
return cljs.core.async.impl.ioc_helpers.return_chan(state_23119__$1,inst_23117);
} else {
if((state_val_23120 === (12))){
var state_23119__$1 = state_23119;
var statearr_23143_24654 = state_23119__$1;
(statearr_23143_24654[(2)] = null);

(statearr_23143_24654[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (2))){
var inst_23063 = (state_23119[(12)]);
var inst_23059 = (state_23119[(10)]);
var inst_23063__$1 = cljs.core.__destructure_map(inst_23059);
var inst_23064 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_23063__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_23065 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_23063__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_23066 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_23063__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_23119__$1 = (function (){var statearr_23144 = state_23119;
(statearr_23144[(15)] = inst_23064);

(statearr_23144[(12)] = inst_23063__$1);

(statearr_23144[(18)] = inst_23065);

return statearr_23144;
})();
return cljs.core.async.ioc_alts_BANG_(state_23119__$1,(4),inst_23066);
} else {
if((state_val_23120 === (23))){
var inst_23104 = (state_23119[(2)]);
var state_23119__$1 = state_23119;
if(cljs.core.truth_(inst_23104)){
var statearr_23145_24655 = state_23119__$1;
(statearr_23145_24655[(1)] = (24));

} else {
var statearr_23146_24656 = state_23119__$1;
(statearr_23146_24656[(1)] = (25));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (19))){
var inst_23099 = (state_23119[(2)]);
var state_23119__$1 = state_23119;
var statearr_23151_24661 = state_23119__$1;
(statearr_23151_24661[(2)] = inst_23099);

(statearr_23151_24661[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (11))){
var inst_23073 = (state_23119[(14)]);
var inst_23083 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(cs,cljs.core.dissoc,inst_23073);
var state_23119__$1 = state_23119;
var statearr_23156_24664 = state_23119__$1;
(statearr_23156_24664[(2)] = inst_23083);

(statearr_23156_24664[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (9))){
var inst_23064 = (state_23119[(15)]);
var inst_23090 = (state_23119[(19)]);
var inst_23073 = (state_23119[(14)]);
var inst_23090__$1 = (inst_23064.cljs$core$IFn$_invoke$arity$1 ? inst_23064.cljs$core$IFn$_invoke$arity$1(inst_23073) : inst_23064.call(null, inst_23073));
var state_23119__$1 = (function (){var statearr_23158 = state_23119;
(statearr_23158[(19)] = inst_23090__$1);

return statearr_23158;
})();
if(cljs.core.truth_(inst_23090__$1)){
var statearr_23159_24669 = state_23119__$1;
(statearr_23159_24669[(1)] = (14));

} else {
var statearr_23160_24670 = state_23119__$1;
(statearr_23160_24670[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (5))){
var inst_23074 = (state_23119[(13)]);
var state_23119__$1 = state_23119;
var statearr_23161_24671 = state_23119__$1;
(statearr_23161_24671[(2)] = inst_23074);

(statearr_23161_24671[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (14))){
var inst_23090 = (state_23119[(19)]);
var state_23119__$1 = state_23119;
var statearr_23162_24672 = state_23119__$1;
(statearr_23162_24672[(2)] = inst_23090);

(statearr_23162_24672[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (26))){
var inst_23109 = (state_23119[(2)]);
var state_23119__$1 = state_23119;
var statearr_23167_24676 = state_23119__$1;
(statearr_23167_24676[(2)] = inst_23109);

(statearr_23167_24676[(1)] = (22));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (16))){
var inst_23101 = (state_23119[(2)]);
var state_23119__$1 = state_23119;
if(cljs.core.truth_(inst_23101)){
var statearr_23171_24677 = state_23119__$1;
(statearr_23171_24677[(1)] = (20));

} else {
var statearr_23172_24678 = state_23119__$1;
(statearr_23172_24678[(1)] = (21));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (10))){
var inst_23115 = (state_23119[(2)]);
var state_23119__$1 = state_23119;
var statearr_23174_24679 = state_23119__$1;
(statearr_23174_24679[(2)] = inst_23115);

(statearr_23174_24679[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (18))){
var inst_23093 = (state_23119[(16)]);
var state_23119__$1 = state_23119;
var statearr_23175_24680 = state_23119__$1;
(statearr_23175_24680[(2)] = inst_23093);

(statearr_23175_24680[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23120 === (8))){
var inst_23072 = (state_23119[(7)]);
var inst_23081 = (inst_23072 == null);
var state_23119__$1 = state_23119;
if(cljs.core.truth_(inst_23081)){
var statearr_23176_24681 = state_23119__$1;
(statearr_23176_24681[(1)] = (11));

} else {
var statearr_23180_24682 = state_23119__$1;
(statearr_23180_24682[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mix_$_state_machine__21631__auto__ = null;
var cljs$core$async$mix_$_state_machine__21631__auto____0 = (function (){
var statearr_23182 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_23182[(0)] = cljs$core$async$mix_$_state_machine__21631__auto__);

(statearr_23182[(1)] = (1));

return statearr_23182;
});
var cljs$core$async$mix_$_state_machine__21631__auto____1 = (function (state_23119){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_23119);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e23186){var ex__21634__auto__ = e23186;
var statearr_23188_24683 = state_23119;
(statearr_23188_24683[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_23119[(4)]))){
var statearr_23189_24684 = state_23119;
(statearr_23189_24684[(1)] = cljs.core.first((state_23119[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24685 = state_23119;
state_23119 = G__24685;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__21631__auto__ = function(state_23119){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__21631__auto____1.call(this,state_23119);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__21631__auto____0;
cljs$core$async$mix_$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__21631__auto____1;
return cljs$core$async$mix_$_state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_23190 = f__21914__auto__();
(statearr_23190[(6)] = c__21913__auto___24624);

return statearr_23190;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_(mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_(mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_(mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_(mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_(mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

var cljs$core$async$Pub$sub_STAR_$dyn_24692 = (function (p,v,ch,close_QMARK_){
var x__5350__auto__ = (((p == null))?null:p);
var m__5351__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$4 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__5351__auto__.call(null, p,v,ch,close_QMARK_));
} else {
var m__5349__auto__ = (cljs.core.async.sub_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$4 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__5349__auto__.call(null, p,v,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Pub.sub*",p);
}
}
});
cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
return cljs$core$async$Pub$sub_STAR_$dyn_24692(p,v,ch,close_QMARK_);
}
});

var cljs$core$async$Pub$unsub_STAR_$dyn_24693 = (function (p,v,ch){
var x__5350__auto__ = (((p == null))?null:p);
var m__5351__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$3 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__5351__auto__.call(null, p,v,ch));
} else {
var m__5349__auto__ = (cljs.core.async.unsub_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$3 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__5349__auto__.call(null, p,v,ch));
} else {
throw cljs.core.missing_protocol("Pub.unsub*",p);
}
}
});
cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
return cljs$core$async$Pub$unsub_STAR_$dyn_24693(p,v,ch);
}
});

var cljs$core$async$Pub$unsub_all_STAR_$dyn_24695 = (function() {
var G__24696 = null;
var G__24696__1 = (function (p){
var x__5350__auto__ = (((p == null))?null:p);
var m__5351__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__5351__auto__.call(null, p));
} else {
var m__5349__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__5349__auto__.call(null, p));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
var G__24696__2 = (function (p,v){
var x__5350__auto__ = (((p == null))?null:p);
var m__5351__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__5351__auto__.call(null, p,v));
} else {
var m__5349__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__5349__auto__.call(null, p,v));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
G__24696 = function(p,v){
switch(arguments.length){
case 1:
return G__24696__1.call(this,p);
case 2:
return G__24696__2.call(this,p,v);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
G__24696.cljs$core$IFn$_invoke$arity$1 = G__24696__1;
G__24696.cljs$core$IFn$_invoke$arity$2 = G__24696__2;
return G__24696;
})()
;
cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__23218 = arguments.length;
switch (G__23218) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_24695(p);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_24695(p,v);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2);



/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async23242 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta23243){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta23243 = meta23243;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async23242.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_23244,meta23243__$1){
var self__ = this;
var _23244__$1 = this;
return (new cljs.core.async.t_cljs$core$async23242(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta23243__$1));
}));

(cljs.core.async.t_cljs$core$async23242.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_23244){
var self__ = this;
var _23244__$1 = this;
return self__.meta23243;
}));

(cljs.core.async.t_cljs$core$async23242.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23242.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async23242.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23242.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = (self__.ensure_mult.cljs$core$IFn$_invoke$arity$1 ? self__.ensure_mult.cljs$core$IFn$_invoke$arity$1(topic) : self__.ensure_mult.call(null, topic));
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(m,ch__$1,close_QMARK_);
}));

(cljs.core.async.t_cljs$core$async23242.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5804__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(self__.mults),topic);
if(cljs.core.truth_(temp__5804__auto__)){
var m = temp__5804__auto__;
return cljs.core.async.untap(m,ch__$1);
} else {
return null;
}
}));

(cljs.core.async.t_cljs$core$async23242.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_(self__.mults,cljs.core.PersistentArrayMap.EMPTY);
}));

(cljs.core.async.t_cljs$core$async23242.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.mults,cljs.core.dissoc,topic);
}));

(cljs.core.async.t_cljs$core$async23242.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta23243","meta23243",-1189860777,null)], null);
}));

(cljs.core.async.t_cljs$core$async23242.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async23242.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async23242");

(cljs.core.async.t_cljs$core$async23242.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async23242");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async23242.
 */
cljs.core.async.__GT_t_cljs$core$async23242 = (function cljs$core$async$__GT_t_cljs$core$async23242(ch,topic_fn,buf_fn,mults,ensure_mult,meta23243){
return (new cljs.core.async.t_cljs$core$async23242(ch,topic_fn,buf_fn,mults,ensure_mult,meta23243));
});


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__23233 = arguments.length;
switch (G__23233) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3(ch,topic_fn,cljs.core.constantly(null));
}));

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = (function (topic){
var or__5002__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(mults),topic);
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(mults,(function (p1__23227_SHARP_){
if(cljs.core.truth_((p1__23227_SHARP_.cljs$core$IFn$_invoke$arity$1 ? p1__23227_SHARP_.cljs$core$IFn$_invoke$arity$1(topic) : p1__23227_SHARP_.call(null, topic)))){
return p1__23227_SHARP_;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__23227_SHARP_,topic,cljs.core.async.mult(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((buf_fn.cljs$core$IFn$_invoke$arity$1 ? buf_fn.cljs$core$IFn$_invoke$arity$1(topic) : buf_fn.call(null, topic)))));
}
})),topic);
}
});
var p = (new cljs.core.async.t_cljs$core$async23242(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
var c__21913__auto___24708 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_23337){
var state_val_23338 = (state_23337[(1)]);
if((state_val_23338 === (7))){
var inst_23333 = (state_23337[(2)]);
var state_23337__$1 = state_23337;
var statearr_23343_24709 = state_23337__$1;
(statearr_23343_24709[(2)] = inst_23333);

(statearr_23343_24709[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (20))){
var state_23337__$1 = state_23337;
var statearr_23344_24710 = state_23337__$1;
(statearr_23344_24710[(2)] = null);

(statearr_23344_24710[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (1))){
var state_23337__$1 = state_23337;
var statearr_23346_24711 = state_23337__$1;
(statearr_23346_24711[(2)] = null);

(statearr_23346_24711[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (24))){
var inst_23316 = (state_23337[(7)]);
var inst_23325 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(mults,cljs.core.dissoc,inst_23316);
var state_23337__$1 = state_23337;
var statearr_23347_24718 = state_23337__$1;
(statearr_23347_24718[(2)] = inst_23325);

(statearr_23347_24718[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (4))){
var inst_23264 = (state_23337[(8)]);
var inst_23264__$1 = (state_23337[(2)]);
var inst_23265 = (inst_23264__$1 == null);
var state_23337__$1 = (function (){var statearr_23348 = state_23337;
(statearr_23348[(8)] = inst_23264__$1);

return statearr_23348;
})();
if(cljs.core.truth_(inst_23265)){
var statearr_23349_24719 = state_23337__$1;
(statearr_23349_24719[(1)] = (5));

} else {
var statearr_23350_24720 = state_23337__$1;
(statearr_23350_24720[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (15))){
var inst_23310 = (state_23337[(2)]);
var state_23337__$1 = state_23337;
var statearr_23351_24721 = state_23337__$1;
(statearr_23351_24721[(2)] = inst_23310);

(statearr_23351_24721[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (21))){
var inst_23330 = (state_23337[(2)]);
var state_23337__$1 = (function (){var statearr_23352 = state_23337;
(statearr_23352[(9)] = inst_23330);

return statearr_23352;
})();
var statearr_23353_24722 = state_23337__$1;
(statearr_23353_24722[(2)] = null);

(statearr_23353_24722[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (13))){
var inst_23292 = (state_23337[(10)]);
var inst_23294 = cljs.core.chunked_seq_QMARK_(inst_23292);
var state_23337__$1 = state_23337;
if(inst_23294){
var statearr_23354_24723 = state_23337__$1;
(statearr_23354_24723[(1)] = (16));

} else {
var statearr_23356_24724 = state_23337__$1;
(statearr_23356_24724[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (22))){
var inst_23322 = (state_23337[(2)]);
var state_23337__$1 = state_23337;
if(cljs.core.truth_(inst_23322)){
var statearr_23358_24725 = state_23337__$1;
(statearr_23358_24725[(1)] = (23));

} else {
var statearr_23359_24726 = state_23337__$1;
(statearr_23359_24726[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (6))){
var inst_23316 = (state_23337[(7)]);
var inst_23318 = (state_23337[(11)]);
var inst_23264 = (state_23337[(8)]);
var inst_23316__$1 = (topic_fn.cljs$core$IFn$_invoke$arity$1 ? topic_fn.cljs$core$IFn$_invoke$arity$1(inst_23264) : topic_fn.call(null, inst_23264));
var inst_23317 = cljs.core.deref(mults);
var inst_23318__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_23317,inst_23316__$1);
var state_23337__$1 = (function (){var statearr_23360 = state_23337;
(statearr_23360[(7)] = inst_23316__$1);

(statearr_23360[(11)] = inst_23318__$1);

return statearr_23360;
})();
if(cljs.core.truth_(inst_23318__$1)){
var statearr_23361_24727 = state_23337__$1;
(statearr_23361_24727[(1)] = (19));

} else {
var statearr_23362_24728 = state_23337__$1;
(statearr_23362_24728[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (25))){
var inst_23327 = (state_23337[(2)]);
var state_23337__$1 = state_23337;
var statearr_23363_24729 = state_23337__$1;
(statearr_23363_24729[(2)] = inst_23327);

(statearr_23363_24729[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (17))){
var inst_23292 = (state_23337[(10)]);
var inst_23301 = cljs.core.first(inst_23292);
var inst_23302 = cljs.core.async.muxch_STAR_(inst_23301);
var inst_23303 = cljs.core.async.close_BANG_(inst_23302);
var inst_23304 = cljs.core.next(inst_23292);
var inst_23277 = inst_23304;
var inst_23278 = null;
var inst_23279 = (0);
var inst_23280 = (0);
var state_23337__$1 = (function (){var statearr_23364 = state_23337;
(statearr_23364[(12)] = inst_23277);

(statearr_23364[(13)] = inst_23303);

(statearr_23364[(14)] = inst_23280);

(statearr_23364[(15)] = inst_23279);

(statearr_23364[(16)] = inst_23278);

return statearr_23364;
})();
var statearr_23365_24732 = state_23337__$1;
(statearr_23365_24732[(2)] = null);

(statearr_23365_24732[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (3))){
var inst_23335 = (state_23337[(2)]);
var state_23337__$1 = state_23337;
return cljs.core.async.impl.ioc_helpers.return_chan(state_23337__$1,inst_23335);
} else {
if((state_val_23338 === (12))){
var inst_23312 = (state_23337[(2)]);
var state_23337__$1 = state_23337;
var statearr_23366_24734 = state_23337__$1;
(statearr_23366_24734[(2)] = inst_23312);

(statearr_23366_24734[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (2))){
var state_23337__$1 = state_23337;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_23337__$1,(4),ch);
} else {
if((state_val_23338 === (23))){
var state_23337__$1 = state_23337;
var statearr_23367_24735 = state_23337__$1;
(statearr_23367_24735[(2)] = null);

(statearr_23367_24735[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (19))){
var inst_23318 = (state_23337[(11)]);
var inst_23264 = (state_23337[(8)]);
var inst_23320 = cljs.core.async.muxch_STAR_(inst_23318);
var state_23337__$1 = state_23337;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23337__$1,(22),inst_23320,inst_23264);
} else {
if((state_val_23338 === (11))){
var inst_23292 = (state_23337[(10)]);
var inst_23277 = (state_23337[(12)]);
var inst_23292__$1 = cljs.core.seq(inst_23277);
var state_23337__$1 = (function (){var statearr_23368 = state_23337;
(statearr_23368[(10)] = inst_23292__$1);

return statearr_23368;
})();
if(inst_23292__$1){
var statearr_23369_24736 = state_23337__$1;
(statearr_23369_24736[(1)] = (13));

} else {
var statearr_23370_24737 = state_23337__$1;
(statearr_23370_24737[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (9))){
var inst_23314 = (state_23337[(2)]);
var state_23337__$1 = state_23337;
var statearr_23371_24738 = state_23337__$1;
(statearr_23371_24738[(2)] = inst_23314);

(statearr_23371_24738[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (5))){
var inst_23274 = cljs.core.deref(mults);
var inst_23275 = cljs.core.vals(inst_23274);
var inst_23276 = cljs.core.seq(inst_23275);
var inst_23277 = inst_23276;
var inst_23278 = null;
var inst_23279 = (0);
var inst_23280 = (0);
var state_23337__$1 = (function (){var statearr_23372 = state_23337;
(statearr_23372[(12)] = inst_23277);

(statearr_23372[(14)] = inst_23280);

(statearr_23372[(15)] = inst_23279);

(statearr_23372[(16)] = inst_23278);

return statearr_23372;
})();
var statearr_23373_24739 = state_23337__$1;
(statearr_23373_24739[(2)] = null);

(statearr_23373_24739[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (14))){
var state_23337__$1 = state_23337;
var statearr_23377_24741 = state_23337__$1;
(statearr_23377_24741[(2)] = null);

(statearr_23377_24741[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (16))){
var inst_23292 = (state_23337[(10)]);
var inst_23296 = cljs.core.chunk_first(inst_23292);
var inst_23297 = cljs.core.chunk_rest(inst_23292);
var inst_23298 = cljs.core.count(inst_23296);
var inst_23277 = inst_23297;
var inst_23278 = inst_23296;
var inst_23279 = inst_23298;
var inst_23280 = (0);
var state_23337__$1 = (function (){var statearr_23378 = state_23337;
(statearr_23378[(12)] = inst_23277);

(statearr_23378[(14)] = inst_23280);

(statearr_23378[(15)] = inst_23279);

(statearr_23378[(16)] = inst_23278);

return statearr_23378;
})();
var statearr_23379_24746 = state_23337__$1;
(statearr_23379_24746[(2)] = null);

(statearr_23379_24746[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (10))){
var inst_23277 = (state_23337[(12)]);
var inst_23280 = (state_23337[(14)]);
var inst_23279 = (state_23337[(15)]);
var inst_23278 = (state_23337[(16)]);
var inst_23286 = cljs.core._nth(inst_23278,inst_23280);
var inst_23287 = cljs.core.async.muxch_STAR_(inst_23286);
var inst_23288 = cljs.core.async.close_BANG_(inst_23287);
var inst_23289 = (inst_23280 + (1));
var tmp23374 = inst_23277;
var tmp23375 = inst_23279;
var tmp23376 = inst_23278;
var inst_23277__$1 = tmp23374;
var inst_23278__$1 = tmp23376;
var inst_23279__$1 = tmp23375;
var inst_23280__$1 = inst_23289;
var state_23337__$1 = (function (){var statearr_23380 = state_23337;
(statearr_23380[(12)] = inst_23277__$1);

(statearr_23380[(17)] = inst_23288);

(statearr_23380[(14)] = inst_23280__$1);

(statearr_23380[(15)] = inst_23279__$1);

(statearr_23380[(16)] = inst_23278__$1);

return statearr_23380;
})();
var statearr_23381_24747 = state_23337__$1;
(statearr_23381_24747[(2)] = null);

(statearr_23381_24747[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (18))){
var inst_23307 = (state_23337[(2)]);
var state_23337__$1 = state_23337;
var statearr_23382_24748 = state_23337__$1;
(statearr_23382_24748[(2)] = inst_23307);

(statearr_23382_24748[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23338 === (8))){
var inst_23280 = (state_23337[(14)]);
var inst_23279 = (state_23337[(15)]);
var inst_23283 = (inst_23280 < inst_23279);
var inst_23284 = inst_23283;
var state_23337__$1 = state_23337;
if(cljs.core.truth_(inst_23284)){
var statearr_23383_24749 = state_23337__$1;
(statearr_23383_24749[(1)] = (10));

} else {
var statearr_23384_24750 = state_23337__$1;
(statearr_23384_24750[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__21631__auto__ = null;
var cljs$core$async$state_machine__21631__auto____0 = (function (){
var statearr_23385 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_23385[(0)] = cljs$core$async$state_machine__21631__auto__);

(statearr_23385[(1)] = (1));

return statearr_23385;
});
var cljs$core$async$state_machine__21631__auto____1 = (function (state_23337){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_23337);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e23386){var ex__21634__auto__ = e23386;
var statearr_23387_24751 = state_23337;
(statearr_23387_24751[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_23337[(4)]))){
var statearr_23388_24752 = state_23337;
(statearr_23388_24752[(1)] = cljs.core.first((state_23337[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24753 = state_23337;
state_23337 = G__24753;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$state_machine__21631__auto__ = function(state_23337){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21631__auto____1.call(this,state_23337);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21631__auto____0;
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21631__auto____1;
return cljs$core$async$state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_23389 = f__21914__auto__();
(statearr_23389[(6)] = c__21913__auto___24708);

return statearr_23389;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));


return p;
}));

(cljs.core.async.pub.cljs$lang$maxFixedArity = 3);

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__23393 = arguments.length;
switch (G__23393) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4(p,topic,ch,true);
}));

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_(p,topic,ch,close_QMARK_);
}));

(cljs.core.async.sub.cljs$lang$maxFixedArity = 4);

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_(p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__23399 = arguments.length;
switch (G__23399) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_(p);
}));

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_(p,topic);
}));

(cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2);

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__23401 = arguments.length;
switch (G__23401) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3(f,chs,null);
}));

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec(chs);
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var cnt = cljs.core.count(chs__$1);
var rets = cljs.core.object_array.cljs$core$IFn$_invoke$arity$1(cnt);
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2((function (i){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,rets.slice((0)));
} else {
return null;
}
});
}),cljs.core.range.cljs$core$IFn$_invoke$arity$1(cnt));
if((cnt === (0))){
cljs.core.async.close_BANG_(out);
} else {
var c__21913__auto___24768 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_23450){
var state_val_23451 = (state_23450[(1)]);
if((state_val_23451 === (7))){
var state_23450__$1 = state_23450;
var statearr_23453_24770 = state_23450__$1;
(statearr_23453_24770[(2)] = null);

(statearr_23453_24770[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23451 === (1))){
var state_23450__$1 = state_23450;
var statearr_23454_24771 = state_23450__$1;
(statearr_23454_24771[(2)] = null);

(statearr_23454_24771[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23451 === (4))){
var inst_23406 = (state_23450[(7)]);
var inst_23407 = (state_23450[(8)]);
var inst_23409 = (inst_23407 < inst_23406);
var state_23450__$1 = state_23450;
if(cljs.core.truth_(inst_23409)){
var statearr_23456_24772 = state_23450__$1;
(statearr_23456_24772[(1)] = (6));

} else {
var statearr_23457_24773 = state_23450__$1;
(statearr_23457_24773[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23451 === (15))){
var inst_23436 = (state_23450[(9)]);
var inst_23441 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,inst_23436);
var state_23450__$1 = state_23450;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23450__$1,(17),out,inst_23441);
} else {
if((state_val_23451 === (13))){
var inst_23436 = (state_23450[(9)]);
var inst_23436__$1 = (state_23450[(2)]);
var inst_23437 = cljs.core.some(cljs.core.nil_QMARK_,inst_23436__$1);
var state_23450__$1 = (function (){var statearr_23460 = state_23450;
(statearr_23460[(9)] = inst_23436__$1);

return statearr_23460;
})();
if(cljs.core.truth_(inst_23437)){
var statearr_23462_24774 = state_23450__$1;
(statearr_23462_24774[(1)] = (14));

} else {
var statearr_23463_24775 = state_23450__$1;
(statearr_23463_24775[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23451 === (6))){
var state_23450__$1 = state_23450;
var statearr_23464_24776 = state_23450__$1;
(statearr_23464_24776[(2)] = null);

(statearr_23464_24776[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23451 === (17))){
var inst_23443 = (state_23450[(2)]);
var state_23450__$1 = (function (){var statearr_23469 = state_23450;
(statearr_23469[(10)] = inst_23443);

return statearr_23469;
})();
var statearr_23470_24777 = state_23450__$1;
(statearr_23470_24777[(2)] = null);

(statearr_23470_24777[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23451 === (3))){
var inst_23448 = (state_23450[(2)]);
var state_23450__$1 = state_23450;
return cljs.core.async.impl.ioc_helpers.return_chan(state_23450__$1,inst_23448);
} else {
if((state_val_23451 === (12))){
var _ = (function (){var statearr_23471 = state_23450;
(statearr_23471[(4)] = cljs.core.rest((state_23450[(4)])));

return statearr_23471;
})();
var state_23450__$1 = state_23450;
var ex23468 = (state_23450__$1[(2)]);
var statearr_23472_24778 = state_23450__$1;
(statearr_23472_24778[(5)] = ex23468);


if((ex23468 instanceof Object)){
var statearr_23486_24779 = state_23450__$1;
(statearr_23486_24779[(1)] = (11));

(statearr_23486_24779[(5)] = null);

} else {
throw ex23468;

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23451 === (2))){
var inst_23405 = cljs.core.reset_BANG_(dctr,cnt);
var inst_23406 = cnt;
var inst_23407 = (0);
var state_23450__$1 = (function (){var statearr_23500 = state_23450;
(statearr_23500[(7)] = inst_23406);

(statearr_23500[(8)] = inst_23407);

(statearr_23500[(11)] = inst_23405);

return statearr_23500;
})();
var statearr_23501_24780 = state_23450__$1;
(statearr_23501_24780[(2)] = null);

(statearr_23501_24780[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23451 === (11))){
var inst_23415 = (state_23450[(2)]);
var inst_23416 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec);
var state_23450__$1 = (function (){var statearr_23505 = state_23450;
(statearr_23505[(12)] = inst_23415);

return statearr_23505;
})();
var statearr_23506_24781 = state_23450__$1;
(statearr_23506_24781[(2)] = inst_23416);

(statearr_23506_24781[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23451 === (9))){
var inst_23407 = (state_23450[(8)]);
var _ = (function (){var statearr_23510 = state_23450;
(statearr_23510[(4)] = cljs.core.cons((12),(state_23450[(4)])));

return statearr_23510;
})();
var inst_23422 = (chs__$1.cljs$core$IFn$_invoke$arity$1 ? chs__$1.cljs$core$IFn$_invoke$arity$1(inst_23407) : chs__$1.call(null, inst_23407));
var inst_23423 = (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(inst_23407) : done.call(null, inst_23407));
var inst_23424 = cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2(inst_23422,inst_23423);
var ___$1 = (function (){var statearr_23511 = state_23450;
(statearr_23511[(4)] = cljs.core.rest((state_23450[(4)])));

return statearr_23511;
})();
var state_23450__$1 = state_23450;
var statearr_23512_24782 = state_23450__$1;
(statearr_23512_24782[(2)] = inst_23424);

(statearr_23512_24782[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23451 === (5))){
var inst_23434 = (state_23450[(2)]);
var state_23450__$1 = (function (){var statearr_23513 = state_23450;
(statearr_23513[(13)] = inst_23434);

return statearr_23513;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_23450__$1,(13),dchan);
} else {
if((state_val_23451 === (14))){
var inst_23439 = cljs.core.async.close_BANG_(out);
var state_23450__$1 = state_23450;
var statearr_23514_24784 = state_23450__$1;
(statearr_23514_24784[(2)] = inst_23439);

(statearr_23514_24784[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23451 === (16))){
var inst_23446 = (state_23450[(2)]);
var state_23450__$1 = state_23450;
var statearr_23515_24786 = state_23450__$1;
(statearr_23515_24786[(2)] = inst_23446);

(statearr_23515_24786[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23451 === (10))){
var inst_23407 = (state_23450[(8)]);
var inst_23427 = (state_23450[(2)]);
var inst_23428 = (inst_23407 + (1));
var inst_23407__$1 = inst_23428;
var state_23450__$1 = (function (){var statearr_23516 = state_23450;
(statearr_23516[(8)] = inst_23407__$1);

(statearr_23516[(14)] = inst_23427);

return statearr_23516;
})();
var statearr_23517_24787 = state_23450__$1;
(statearr_23517_24787[(2)] = null);

(statearr_23517_24787[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23451 === (8))){
var inst_23432 = (state_23450[(2)]);
var state_23450__$1 = state_23450;
var statearr_23518_24788 = state_23450__$1;
(statearr_23518_24788[(2)] = inst_23432);

(statearr_23518_24788[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__21631__auto__ = null;
var cljs$core$async$state_machine__21631__auto____0 = (function (){
var statearr_23519 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_23519[(0)] = cljs$core$async$state_machine__21631__auto__);

(statearr_23519[(1)] = (1));

return statearr_23519;
});
var cljs$core$async$state_machine__21631__auto____1 = (function (state_23450){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_23450);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e23520){var ex__21634__auto__ = e23520;
var statearr_23521_24789 = state_23450;
(statearr_23521_24789[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_23450[(4)]))){
var statearr_23522_24790 = state_23450;
(statearr_23522_24790[(1)] = cljs.core.first((state_23450[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24791 = state_23450;
state_23450 = G__24791;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$state_machine__21631__auto__ = function(state_23450){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21631__auto____1.call(this,state_23450);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21631__auto____0;
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21631__auto____1;
return cljs$core$async$state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_23527 = f__21914__auto__();
(statearr_23527[(6)] = c__21913__auto___24768);

return statearr_23527;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));

}

return out;
}));

(cljs.core.async.map.cljs$lang$maxFixedArity = 3);

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__23530 = arguments.length;
switch (G__23530) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2(chs,null);
}));

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__21913__auto___24793 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_23570){
var state_val_23571 = (state_23570[(1)]);
if((state_val_23571 === (7))){
var inst_23549 = (state_23570[(7)]);
var inst_23550 = (state_23570[(8)]);
var inst_23549__$1 = (state_23570[(2)]);
var inst_23550__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_23549__$1,(0),null);
var inst_23551 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_23549__$1,(1),null);
var inst_23552 = (inst_23550__$1 == null);
var state_23570__$1 = (function (){var statearr_23574 = state_23570;
(statearr_23574[(7)] = inst_23549__$1);

(statearr_23574[(9)] = inst_23551);

(statearr_23574[(8)] = inst_23550__$1);

return statearr_23574;
})();
if(cljs.core.truth_(inst_23552)){
var statearr_23575_24794 = state_23570__$1;
(statearr_23575_24794[(1)] = (8));

} else {
var statearr_23576_24795 = state_23570__$1;
(statearr_23576_24795[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23571 === (1))){
var inst_23539 = cljs.core.vec(chs);
var inst_23540 = inst_23539;
var state_23570__$1 = (function (){var statearr_23577 = state_23570;
(statearr_23577[(10)] = inst_23540);

return statearr_23577;
})();
var statearr_23579_24796 = state_23570__$1;
(statearr_23579_24796[(2)] = null);

(statearr_23579_24796[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23571 === (4))){
var inst_23540 = (state_23570[(10)]);
var state_23570__$1 = state_23570;
return cljs.core.async.ioc_alts_BANG_(state_23570__$1,(7),inst_23540);
} else {
if((state_val_23571 === (6))){
var inst_23566 = (state_23570[(2)]);
var state_23570__$1 = state_23570;
var statearr_23584_24798 = state_23570__$1;
(statearr_23584_24798[(2)] = inst_23566);

(statearr_23584_24798[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23571 === (3))){
var inst_23568 = (state_23570[(2)]);
var state_23570__$1 = state_23570;
return cljs.core.async.impl.ioc_helpers.return_chan(state_23570__$1,inst_23568);
} else {
if((state_val_23571 === (2))){
var inst_23540 = (state_23570[(10)]);
var inst_23542 = cljs.core.count(inst_23540);
var inst_23543 = (inst_23542 > (0));
var state_23570__$1 = state_23570;
if(cljs.core.truth_(inst_23543)){
var statearr_23586_24803 = state_23570__$1;
(statearr_23586_24803[(1)] = (4));

} else {
var statearr_23587_24814 = state_23570__$1;
(statearr_23587_24814[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23571 === (11))){
var inst_23540 = (state_23570[(10)]);
var inst_23559 = (state_23570[(2)]);
var tmp23585 = inst_23540;
var inst_23540__$1 = tmp23585;
var state_23570__$1 = (function (){var statearr_23588 = state_23570;
(statearr_23588[(10)] = inst_23540__$1);

(statearr_23588[(11)] = inst_23559);

return statearr_23588;
})();
var statearr_23589_24815 = state_23570__$1;
(statearr_23589_24815[(2)] = null);

(statearr_23589_24815[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23571 === (9))){
var inst_23550 = (state_23570[(8)]);
var state_23570__$1 = state_23570;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23570__$1,(11),out,inst_23550);
} else {
if((state_val_23571 === (5))){
var inst_23564 = cljs.core.async.close_BANG_(out);
var state_23570__$1 = state_23570;
var statearr_23590_24817 = state_23570__$1;
(statearr_23590_24817[(2)] = inst_23564);

(statearr_23590_24817[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23571 === (10))){
var inst_23562 = (state_23570[(2)]);
var state_23570__$1 = state_23570;
var statearr_23591_24818 = state_23570__$1;
(statearr_23591_24818[(2)] = inst_23562);

(statearr_23591_24818[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23571 === (8))){
var inst_23540 = (state_23570[(10)]);
var inst_23549 = (state_23570[(7)]);
var inst_23551 = (state_23570[(9)]);
var inst_23550 = (state_23570[(8)]);
var inst_23554 = (function (){var cs = inst_23540;
var vec__23545 = inst_23549;
var v = inst_23550;
var c = inst_23551;
return (function (p1__23528_SHARP_){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(c,p1__23528_SHARP_);
});
})();
var inst_23555 = cljs.core.filterv(inst_23554,inst_23540);
var inst_23540__$1 = inst_23555;
var state_23570__$1 = (function (){var statearr_23592 = state_23570;
(statearr_23592[(10)] = inst_23540__$1);

return statearr_23592;
})();
var statearr_23593_24820 = state_23570__$1;
(statearr_23593_24820[(2)] = null);

(statearr_23593_24820[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__21631__auto__ = null;
var cljs$core$async$state_machine__21631__auto____0 = (function (){
var statearr_23594 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_23594[(0)] = cljs$core$async$state_machine__21631__auto__);

(statearr_23594[(1)] = (1));

return statearr_23594;
});
var cljs$core$async$state_machine__21631__auto____1 = (function (state_23570){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_23570);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e23595){var ex__21634__auto__ = e23595;
var statearr_23596_24822 = state_23570;
(statearr_23596_24822[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_23570[(4)]))){
var statearr_23597_24823 = state_23570;
(statearr_23597_24823[(1)] = cljs.core.first((state_23570[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24824 = state_23570;
state_23570 = G__24824;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$state_machine__21631__auto__ = function(state_23570){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21631__auto____1.call(this,state_23570);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21631__auto____0;
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21631__auto____1;
return cljs$core$async$state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_23602 = f__21914__auto__();
(statearr_23602[(6)] = c__21913__auto___24793);

return statearr_23602;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));


return out;
}));

(cljs.core.async.merge.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce(cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__23606 = arguments.length;
switch (G__23606) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__21913__auto___24833 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_23631){
var state_val_23633 = (state_23631[(1)]);
if((state_val_23633 === (7))){
var inst_23613 = (state_23631[(7)]);
var inst_23613__$1 = (state_23631[(2)]);
var inst_23614 = (inst_23613__$1 == null);
var inst_23615 = cljs.core.not(inst_23614);
var state_23631__$1 = (function (){var statearr_23638 = state_23631;
(statearr_23638[(7)] = inst_23613__$1);

return statearr_23638;
})();
if(inst_23615){
var statearr_23639_24834 = state_23631__$1;
(statearr_23639_24834[(1)] = (8));

} else {
var statearr_23640_24842 = state_23631__$1;
(statearr_23640_24842[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23633 === (1))){
var inst_23608 = (0);
var state_23631__$1 = (function (){var statearr_23641 = state_23631;
(statearr_23641[(8)] = inst_23608);

return statearr_23641;
})();
var statearr_23642_24852 = state_23631__$1;
(statearr_23642_24852[(2)] = null);

(statearr_23642_24852[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23633 === (4))){
var state_23631__$1 = state_23631;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_23631__$1,(7),ch);
} else {
if((state_val_23633 === (6))){
var inst_23626 = (state_23631[(2)]);
var state_23631__$1 = state_23631;
var statearr_23643_24854 = state_23631__$1;
(statearr_23643_24854[(2)] = inst_23626);

(statearr_23643_24854[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23633 === (3))){
var inst_23628 = (state_23631[(2)]);
var inst_23629 = cljs.core.async.close_BANG_(out);
var state_23631__$1 = (function (){var statearr_23644 = state_23631;
(statearr_23644[(9)] = inst_23628);

return statearr_23644;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_23631__$1,inst_23629);
} else {
if((state_val_23633 === (2))){
var inst_23608 = (state_23631[(8)]);
var inst_23610 = (inst_23608 < n);
var state_23631__$1 = state_23631;
if(cljs.core.truth_(inst_23610)){
var statearr_23645_24858 = state_23631__$1;
(statearr_23645_24858[(1)] = (4));

} else {
var statearr_23646_24859 = state_23631__$1;
(statearr_23646_24859[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23633 === (11))){
var inst_23608 = (state_23631[(8)]);
var inst_23618 = (state_23631[(2)]);
var inst_23619 = (inst_23608 + (1));
var inst_23608__$1 = inst_23619;
var state_23631__$1 = (function (){var statearr_23647 = state_23631;
(statearr_23647[(10)] = inst_23618);

(statearr_23647[(8)] = inst_23608__$1);

return statearr_23647;
})();
var statearr_23648_24868 = state_23631__$1;
(statearr_23648_24868[(2)] = null);

(statearr_23648_24868[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23633 === (9))){
var state_23631__$1 = state_23631;
var statearr_23649_24869 = state_23631__$1;
(statearr_23649_24869[(2)] = null);

(statearr_23649_24869[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23633 === (5))){
var state_23631__$1 = state_23631;
var statearr_23650_24871 = state_23631__$1;
(statearr_23650_24871[(2)] = null);

(statearr_23650_24871[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23633 === (10))){
var inst_23623 = (state_23631[(2)]);
var state_23631__$1 = state_23631;
var statearr_23651_24872 = state_23631__$1;
(statearr_23651_24872[(2)] = inst_23623);

(statearr_23651_24872[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23633 === (8))){
var inst_23613 = (state_23631[(7)]);
var state_23631__$1 = state_23631;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23631__$1,(11),out,inst_23613);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__21631__auto__ = null;
var cljs$core$async$state_machine__21631__auto____0 = (function (){
var statearr_23652 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_23652[(0)] = cljs$core$async$state_machine__21631__auto__);

(statearr_23652[(1)] = (1));

return statearr_23652;
});
var cljs$core$async$state_machine__21631__auto____1 = (function (state_23631){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_23631);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e23653){var ex__21634__auto__ = e23653;
var statearr_23654_24878 = state_23631;
(statearr_23654_24878[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_23631[(4)]))){
var statearr_23655_24879 = state_23631;
(statearr_23655_24879[(1)] = cljs.core.first((state_23631[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24882 = state_23631;
state_23631 = G__24882;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$state_machine__21631__auto__ = function(state_23631){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21631__auto____1.call(this,state_23631);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21631__auto____0;
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21631__auto____1;
return cljs$core$async$state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_23656 = f__21914__auto__();
(statearr_23656[(6)] = c__21913__auto___24833);

return statearr_23656;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));


return out;
}));

(cljs.core.async.take.cljs$lang$maxFixedArity = 3);


/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async23669 = (function (f,ch,meta23663,_,fn1,meta23670){
this.f = f;
this.ch = ch;
this.meta23663 = meta23663;
this._ = _;
this.fn1 = fn1;
this.meta23670 = meta23670;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async23669.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_23671,meta23670__$1){
var self__ = this;
var _23671__$1 = this;
return (new cljs.core.async.t_cljs$core$async23669(self__.f,self__.ch,self__.meta23663,self__._,self__.fn1,meta23670__$1));
}));

(cljs.core.async.t_cljs$core$async23669.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_23671){
var self__ = this;
var _23671__$1 = this;
return self__.meta23670;
}));

(cljs.core.async.t_cljs$core$async23669.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23669.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.fn1);
}));

(cljs.core.async.t_cljs$core$async23669.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async23669.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit(self__.fn1);
return (function (p1__23658_SHARP_){
var G__23672 = (((p1__23658_SHARP_ == null))?null:(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(p1__23658_SHARP_) : self__.f.call(null, p1__23658_SHARP_)));
return (f1.cljs$core$IFn$_invoke$arity$1 ? f1.cljs$core$IFn$_invoke$arity$1(G__23672) : f1.call(null, G__23672));
});
}));

(cljs.core.async.t_cljs$core$async23669.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta23663","meta23663",-2036797184,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async23662","cljs.core.async/t_cljs$core$async23662",-1593183262,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta23670","meta23670",-471345168,null)], null);
}));

(cljs.core.async.t_cljs$core$async23669.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async23669.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async23669");

(cljs.core.async.t_cljs$core$async23669.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async23669");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async23669.
 */
cljs.core.async.__GT_t_cljs$core$async23669 = (function cljs$core$async$__GT_t_cljs$core$async23669(f,ch,meta23663,_,fn1,meta23670){
return (new cljs.core.async.t_cljs$core$async23669(f,ch,meta23663,_,fn1,meta23670));
});



/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async23662 = (function (f,ch,meta23663){
this.f = f;
this.ch = ch;
this.meta23663 = meta23663;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async23662.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_23664,meta23663__$1){
var self__ = this;
var _23664__$1 = this;
return (new cljs.core.async.t_cljs$core$async23662(self__.f,self__.ch,meta23663__$1));
}));

(cljs.core.async.t_cljs$core$async23662.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_23664){
var self__ = this;
var _23664__$1 = this;
return self__.meta23663;
}));

(cljs.core.async.t_cljs$core$async23662.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23662.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async23662.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async23662.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23662.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_(self__.ch,(new cljs.core.async.t_cljs$core$async23669(self__.f,self__.ch,self__.meta23663,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY)));
if(cljs.core.truth_((function (){var and__5000__auto__ = ret;
if(cljs.core.truth_(and__5000__auto__)){
return (!((cljs.core.deref(ret) == null)));
} else {
return and__5000__auto__;
}
})())){
return cljs.core.async.impl.channels.box((function (){var G__23673 = cljs.core.deref(ret);
return (self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(G__23673) : self__.f.call(null, G__23673));
})());
} else {
return ret;
}
}));

(cljs.core.async.t_cljs$core$async23662.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23662.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
}));

(cljs.core.async.t_cljs$core$async23662.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta23663","meta23663",-2036797184,null)], null);
}));

(cljs.core.async.t_cljs$core$async23662.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async23662.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async23662");

(cljs.core.async.t_cljs$core$async23662.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async23662");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async23662.
 */
cljs.core.async.__GT_t_cljs$core$async23662 = (function cljs$core$async$__GT_t_cljs$core$async23662(f,ch,meta23663){
return (new cljs.core.async.t_cljs$core$async23662(f,ch,meta23663));
});


/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
return (new cljs.core.async.t_cljs$core$async23662(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async23676 = (function (f,ch,meta23677){
this.f = f;
this.ch = ch;
this.meta23677 = meta23677;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async23676.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_23678,meta23677__$1){
var self__ = this;
var _23678__$1 = this;
return (new cljs.core.async.t_cljs$core$async23676(self__.f,self__.ch,meta23677__$1));
}));

(cljs.core.async.t_cljs$core$async23676.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_23678){
var self__ = this;
var _23678__$1 = this;
return self__.meta23677;
}));

(cljs.core.async.t_cljs$core$async23676.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23676.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async23676.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23676.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async23676.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23676.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(val) : self__.f.call(null, val)),fn1);
}));

(cljs.core.async.t_cljs$core$async23676.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta23677","meta23677",-129276061,null)], null);
}));

(cljs.core.async.t_cljs$core$async23676.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async23676.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async23676");

(cljs.core.async.t_cljs$core$async23676.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async23676");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async23676.
 */
cljs.core.async.__GT_t_cljs$core$async23676 = (function cljs$core$async$__GT_t_cljs$core$async23676(f,ch,meta23677){
return (new cljs.core.async.t_cljs$core$async23676(f,ch,meta23677));
});


/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
return (new cljs.core.async.t_cljs$core$async23676(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async23682 = (function (p,ch,meta23683){
this.p = p;
this.ch = ch;
this.meta23683 = meta23683;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async23682.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_23684,meta23683__$1){
var self__ = this;
var _23684__$1 = this;
return (new cljs.core.async.t_cljs$core$async23682(self__.p,self__.ch,meta23683__$1));
}));

(cljs.core.async.t_cljs$core$async23682.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_23684){
var self__ = this;
var _23684__$1 = this;
return self__.meta23683;
}));

(cljs.core.async.t_cljs$core$async23682.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23682.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async23682.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async23682.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23682.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async23682.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23682.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.p.cljs$core$IFn$_invoke$arity$1 ? self__.p.cljs$core$IFn$_invoke$arity$1(val) : self__.p.call(null, val)))){
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box(cljs.core.not(cljs.core.async.impl.protocols.closed_QMARK_(self__.ch)));
}
}));

(cljs.core.async.t_cljs$core$async23682.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta23683","meta23683",1342019560,null)], null);
}));

(cljs.core.async.t_cljs$core$async23682.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async23682.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async23682");

(cljs.core.async.t_cljs$core$async23682.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async23682");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async23682.
 */
cljs.core.async.__GT_t_cljs$core$async23682 = (function cljs$core$async$__GT_t_cljs$core$async23682(p,ch,meta23683){
return (new cljs.core.async.t_cljs$core$async23682(p,ch,meta23683));
});


/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
return (new cljs.core.async.t_cljs$core$async23682(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_(cljs.core.complement(p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__23686 = arguments.length;
switch (G__23686) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__21913__auto___24896 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_23707){
var state_val_23708 = (state_23707[(1)]);
if((state_val_23708 === (7))){
var inst_23703 = (state_23707[(2)]);
var state_23707__$1 = state_23707;
var statearr_23709_24901 = state_23707__$1;
(statearr_23709_24901[(2)] = inst_23703);

(statearr_23709_24901[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23708 === (1))){
var state_23707__$1 = state_23707;
var statearr_23710_24902 = state_23707__$1;
(statearr_23710_24902[(2)] = null);

(statearr_23710_24902[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23708 === (4))){
var inst_23689 = (state_23707[(7)]);
var inst_23689__$1 = (state_23707[(2)]);
var inst_23690 = (inst_23689__$1 == null);
var state_23707__$1 = (function (){var statearr_23711 = state_23707;
(statearr_23711[(7)] = inst_23689__$1);

return statearr_23711;
})();
if(cljs.core.truth_(inst_23690)){
var statearr_23712_24904 = state_23707__$1;
(statearr_23712_24904[(1)] = (5));

} else {
var statearr_23713_24905 = state_23707__$1;
(statearr_23713_24905[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23708 === (6))){
var inst_23689 = (state_23707[(7)]);
var inst_23694 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_23689) : p.call(null, inst_23689));
var state_23707__$1 = state_23707;
if(cljs.core.truth_(inst_23694)){
var statearr_23714_24906 = state_23707__$1;
(statearr_23714_24906[(1)] = (8));

} else {
var statearr_23715_24907 = state_23707__$1;
(statearr_23715_24907[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23708 === (3))){
var inst_23705 = (state_23707[(2)]);
var state_23707__$1 = state_23707;
return cljs.core.async.impl.ioc_helpers.return_chan(state_23707__$1,inst_23705);
} else {
if((state_val_23708 === (2))){
var state_23707__$1 = state_23707;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_23707__$1,(4),ch);
} else {
if((state_val_23708 === (11))){
var inst_23697 = (state_23707[(2)]);
var state_23707__$1 = state_23707;
var statearr_23716_24908 = state_23707__$1;
(statearr_23716_24908[(2)] = inst_23697);

(statearr_23716_24908[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23708 === (9))){
var state_23707__$1 = state_23707;
var statearr_23717_24913 = state_23707__$1;
(statearr_23717_24913[(2)] = null);

(statearr_23717_24913[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23708 === (5))){
var inst_23692 = cljs.core.async.close_BANG_(out);
var state_23707__$1 = state_23707;
var statearr_23718_24927 = state_23707__$1;
(statearr_23718_24927[(2)] = inst_23692);

(statearr_23718_24927[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23708 === (10))){
var inst_23700 = (state_23707[(2)]);
var state_23707__$1 = (function (){var statearr_23719 = state_23707;
(statearr_23719[(8)] = inst_23700);

return statearr_23719;
})();
var statearr_23720_24928 = state_23707__$1;
(statearr_23720_24928[(2)] = null);

(statearr_23720_24928[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23708 === (8))){
var inst_23689 = (state_23707[(7)]);
var state_23707__$1 = state_23707;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23707__$1,(11),out,inst_23689);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__21631__auto__ = null;
var cljs$core$async$state_machine__21631__auto____0 = (function (){
var statearr_23721 = [null,null,null,null,null,null,null,null,null];
(statearr_23721[(0)] = cljs$core$async$state_machine__21631__auto__);

(statearr_23721[(1)] = (1));

return statearr_23721;
});
var cljs$core$async$state_machine__21631__auto____1 = (function (state_23707){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_23707);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e23722){var ex__21634__auto__ = e23722;
var statearr_23723_24934 = state_23707;
(statearr_23723_24934[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_23707[(4)]))){
var statearr_23724_24938 = state_23707;
(statearr_23724_24938[(1)] = cljs.core.first((state_23707[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24939 = state_23707;
state_23707 = G__24939;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$state_machine__21631__auto__ = function(state_23707){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21631__auto____1.call(this,state_23707);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21631__auto____0;
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21631__auto____1;
return cljs$core$async$state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_23725 = f__21914__auto__();
(statearr_23725[(6)] = c__21913__auto___24896);

return statearr_23725;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));


return out;
}));

(cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__23728 = arguments.length;
switch (G__23728) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(cljs.core.complement(p),ch,buf_or_n);
}));

(cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3);

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__21913__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_23793){
var state_val_23794 = (state_23793[(1)]);
if((state_val_23794 === (7))){
var inst_23789 = (state_23793[(2)]);
var state_23793__$1 = state_23793;
var statearr_23795_24959 = state_23793__$1;
(statearr_23795_24959[(2)] = inst_23789);

(statearr_23795_24959[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (20))){
var inst_23759 = (state_23793[(7)]);
var inst_23770 = (state_23793[(2)]);
var inst_23771 = cljs.core.next(inst_23759);
var inst_23742 = inst_23771;
var inst_23743 = null;
var inst_23744 = (0);
var inst_23745 = (0);
var state_23793__$1 = (function (){var statearr_23796 = state_23793;
(statearr_23796[(8)] = inst_23742);

(statearr_23796[(9)] = inst_23770);

(statearr_23796[(10)] = inst_23745);

(statearr_23796[(11)] = inst_23743);

(statearr_23796[(12)] = inst_23744);

return statearr_23796;
})();
var statearr_23797_24984 = state_23793__$1;
(statearr_23797_24984[(2)] = null);

(statearr_23797_24984[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (1))){
var state_23793__$1 = state_23793;
var statearr_23802_24985 = state_23793__$1;
(statearr_23802_24985[(2)] = null);

(statearr_23802_24985[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (4))){
var inst_23731 = (state_23793[(13)]);
var inst_23731__$1 = (state_23793[(2)]);
var inst_23732 = (inst_23731__$1 == null);
var state_23793__$1 = (function (){var statearr_23803 = state_23793;
(statearr_23803[(13)] = inst_23731__$1);

return statearr_23803;
})();
if(cljs.core.truth_(inst_23732)){
var statearr_23804_24986 = state_23793__$1;
(statearr_23804_24986[(1)] = (5));

} else {
var statearr_23805_24987 = state_23793__$1;
(statearr_23805_24987[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (15))){
var state_23793__$1 = state_23793;
var statearr_23809_24988 = state_23793__$1;
(statearr_23809_24988[(2)] = null);

(statearr_23809_24988[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (21))){
var state_23793__$1 = state_23793;
var statearr_23810_24989 = state_23793__$1;
(statearr_23810_24989[(2)] = null);

(statearr_23810_24989[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (13))){
var inst_23742 = (state_23793[(8)]);
var inst_23745 = (state_23793[(10)]);
var inst_23743 = (state_23793[(11)]);
var inst_23744 = (state_23793[(12)]);
var inst_23752 = (state_23793[(2)]);
var inst_23753 = (inst_23745 + (1));
var tmp23806 = inst_23742;
var tmp23807 = inst_23743;
var tmp23808 = inst_23744;
var inst_23742__$1 = tmp23806;
var inst_23743__$1 = tmp23807;
var inst_23744__$1 = tmp23808;
var inst_23745__$1 = inst_23753;
var state_23793__$1 = (function (){var statearr_23811 = state_23793;
(statearr_23811[(8)] = inst_23742__$1);

(statearr_23811[(14)] = inst_23752);

(statearr_23811[(10)] = inst_23745__$1);

(statearr_23811[(11)] = inst_23743__$1);

(statearr_23811[(12)] = inst_23744__$1);

return statearr_23811;
})();
var statearr_23812_24992 = state_23793__$1;
(statearr_23812_24992[(2)] = null);

(statearr_23812_24992[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (22))){
var state_23793__$1 = state_23793;
var statearr_23813_24993 = state_23793__$1;
(statearr_23813_24993[(2)] = null);

(statearr_23813_24993[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (6))){
var inst_23731 = (state_23793[(13)]);
var inst_23740 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_23731) : f.call(null, inst_23731));
var inst_23741 = cljs.core.seq(inst_23740);
var inst_23742 = inst_23741;
var inst_23743 = null;
var inst_23744 = (0);
var inst_23745 = (0);
var state_23793__$1 = (function (){var statearr_23814 = state_23793;
(statearr_23814[(8)] = inst_23742);

(statearr_23814[(10)] = inst_23745);

(statearr_23814[(11)] = inst_23743);

(statearr_23814[(12)] = inst_23744);

return statearr_23814;
})();
var statearr_23815_24995 = state_23793__$1;
(statearr_23815_24995[(2)] = null);

(statearr_23815_24995[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (17))){
var inst_23759 = (state_23793[(7)]);
var inst_23763 = cljs.core.chunk_first(inst_23759);
var inst_23764 = cljs.core.chunk_rest(inst_23759);
var inst_23765 = cljs.core.count(inst_23763);
var inst_23742 = inst_23764;
var inst_23743 = inst_23763;
var inst_23744 = inst_23765;
var inst_23745 = (0);
var state_23793__$1 = (function (){var statearr_23816 = state_23793;
(statearr_23816[(8)] = inst_23742);

(statearr_23816[(10)] = inst_23745);

(statearr_23816[(11)] = inst_23743);

(statearr_23816[(12)] = inst_23744);

return statearr_23816;
})();
var statearr_23817_24999 = state_23793__$1;
(statearr_23817_24999[(2)] = null);

(statearr_23817_24999[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (3))){
var inst_23791 = (state_23793[(2)]);
var state_23793__$1 = state_23793;
return cljs.core.async.impl.ioc_helpers.return_chan(state_23793__$1,inst_23791);
} else {
if((state_val_23794 === (12))){
var inst_23779 = (state_23793[(2)]);
var state_23793__$1 = state_23793;
var statearr_23818_25000 = state_23793__$1;
(statearr_23818_25000[(2)] = inst_23779);

(statearr_23818_25000[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (2))){
var state_23793__$1 = state_23793;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_23793__$1,(4),in$);
} else {
if((state_val_23794 === (23))){
var inst_23787 = (state_23793[(2)]);
var state_23793__$1 = state_23793;
var statearr_23819_25002 = state_23793__$1;
(statearr_23819_25002[(2)] = inst_23787);

(statearr_23819_25002[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (19))){
var inst_23774 = (state_23793[(2)]);
var state_23793__$1 = state_23793;
var statearr_23820_25003 = state_23793__$1;
(statearr_23820_25003[(2)] = inst_23774);

(statearr_23820_25003[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (11))){
var inst_23742 = (state_23793[(8)]);
var inst_23759 = (state_23793[(7)]);
var inst_23759__$1 = cljs.core.seq(inst_23742);
var state_23793__$1 = (function (){var statearr_23821 = state_23793;
(statearr_23821[(7)] = inst_23759__$1);

return statearr_23821;
})();
if(inst_23759__$1){
var statearr_23822_25006 = state_23793__$1;
(statearr_23822_25006[(1)] = (14));

} else {
var statearr_23831_25007 = state_23793__$1;
(statearr_23831_25007[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (9))){
var inst_23781 = (state_23793[(2)]);
var inst_23782 = cljs.core.async.impl.protocols.closed_QMARK_(out);
var state_23793__$1 = (function (){var statearr_23838 = state_23793;
(statearr_23838[(15)] = inst_23781);

return statearr_23838;
})();
if(cljs.core.truth_(inst_23782)){
var statearr_23839_25009 = state_23793__$1;
(statearr_23839_25009[(1)] = (21));

} else {
var statearr_23840_25011 = state_23793__$1;
(statearr_23840_25011[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (5))){
var inst_23734 = cljs.core.async.close_BANG_(out);
var state_23793__$1 = state_23793;
var statearr_23847_25012 = state_23793__$1;
(statearr_23847_25012[(2)] = inst_23734);

(statearr_23847_25012[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (14))){
var inst_23759 = (state_23793[(7)]);
var inst_23761 = cljs.core.chunked_seq_QMARK_(inst_23759);
var state_23793__$1 = state_23793;
if(inst_23761){
var statearr_23848_25014 = state_23793__$1;
(statearr_23848_25014[(1)] = (17));

} else {
var statearr_23849_25015 = state_23793__$1;
(statearr_23849_25015[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (16))){
var inst_23777 = (state_23793[(2)]);
var state_23793__$1 = state_23793;
var statearr_23856_25019 = state_23793__$1;
(statearr_23856_25019[(2)] = inst_23777);

(statearr_23856_25019[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23794 === (10))){
var inst_23745 = (state_23793[(10)]);
var inst_23743 = (state_23793[(11)]);
var inst_23750 = cljs.core._nth(inst_23743,inst_23745);
var state_23793__$1 = state_23793;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23793__$1,(13),out,inst_23750);
} else {
if((state_val_23794 === (18))){
var inst_23759 = (state_23793[(7)]);
var inst_23768 = cljs.core.first(inst_23759);
var state_23793__$1 = state_23793;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23793__$1,(20),out,inst_23768);
} else {
if((state_val_23794 === (8))){
var inst_23745 = (state_23793[(10)]);
var inst_23744 = (state_23793[(12)]);
var inst_23747 = (inst_23745 < inst_23744);
var inst_23748 = inst_23747;
var state_23793__$1 = state_23793;
if(cljs.core.truth_(inst_23748)){
var statearr_23871_25021 = state_23793__$1;
(statearr_23871_25021[(1)] = (10));

} else {
var statearr_23872_25025 = state_23793__$1;
(statearr_23872_25025[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__21631__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__21631__auto____0 = (function (){
var statearr_23873 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_23873[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__21631__auto__);

(statearr_23873[(1)] = (1));

return statearr_23873;
});
var cljs$core$async$mapcat_STAR__$_state_machine__21631__auto____1 = (function (state_23793){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_23793);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e23876){var ex__21634__auto__ = e23876;
var statearr_23877_25026 = state_23793;
(statearr_23877_25026[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_23793[(4)]))){
var statearr_23878_25027 = state_23793;
(statearr_23878_25027[(1)] = cljs.core.first((state_23793[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__25028 = state_23793;
state_23793 = G__25028;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__21631__auto__ = function(state_23793){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__21631__auto____1.call(this,state_23793);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__21631__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__21631__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_23880 = f__21914__auto__();
(statearr_23880[(6)] = c__21913__auto__);

return statearr_23880;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));

return c__21913__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__23882 = arguments.length;
switch (G__23882) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3(f,in$,null);
}));

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return out;
}));

(cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__23887 = arguments.length;
switch (G__23887) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3(f,out,null);
}));

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return in$;
}));

(cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__23889 = arguments.length;
switch (G__23889) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2(ch,null);
}));

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__21913__auto___25041 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_23913){
var state_val_23914 = (state_23913[(1)]);
if((state_val_23914 === (7))){
var inst_23908 = (state_23913[(2)]);
var state_23913__$1 = state_23913;
var statearr_23915_25042 = state_23913__$1;
(statearr_23915_25042[(2)] = inst_23908);

(statearr_23915_25042[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23914 === (1))){
var inst_23890 = null;
var state_23913__$1 = (function (){var statearr_23916 = state_23913;
(statearr_23916[(7)] = inst_23890);

return statearr_23916;
})();
var statearr_23917_25049 = state_23913__$1;
(statearr_23917_25049[(2)] = null);

(statearr_23917_25049[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23914 === (4))){
var inst_23893 = (state_23913[(8)]);
var inst_23893__$1 = (state_23913[(2)]);
var inst_23894 = (inst_23893__$1 == null);
var inst_23895 = cljs.core.not(inst_23894);
var state_23913__$1 = (function (){var statearr_23918 = state_23913;
(statearr_23918[(8)] = inst_23893__$1);

return statearr_23918;
})();
if(inst_23895){
var statearr_23919_25061 = state_23913__$1;
(statearr_23919_25061[(1)] = (5));

} else {
var statearr_23920_25062 = state_23913__$1;
(statearr_23920_25062[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23914 === (6))){
var state_23913__$1 = state_23913;
var statearr_23921_25063 = state_23913__$1;
(statearr_23921_25063[(2)] = null);

(statearr_23921_25063[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23914 === (3))){
var inst_23910 = (state_23913[(2)]);
var inst_23911 = cljs.core.async.close_BANG_(out);
var state_23913__$1 = (function (){var statearr_23922 = state_23913;
(statearr_23922[(9)] = inst_23910);

return statearr_23922;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_23913__$1,inst_23911);
} else {
if((state_val_23914 === (2))){
var state_23913__$1 = state_23913;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_23913__$1,(4),ch);
} else {
if((state_val_23914 === (11))){
var inst_23893 = (state_23913[(8)]);
var inst_23902 = (state_23913[(2)]);
var inst_23890 = inst_23893;
var state_23913__$1 = (function (){var statearr_23923 = state_23913;
(statearr_23923[(7)] = inst_23890);

(statearr_23923[(10)] = inst_23902);

return statearr_23923;
})();
var statearr_23924_25073 = state_23913__$1;
(statearr_23924_25073[(2)] = null);

(statearr_23924_25073[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23914 === (9))){
var inst_23893 = (state_23913[(8)]);
var state_23913__$1 = state_23913;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23913__$1,(11),out,inst_23893);
} else {
if((state_val_23914 === (5))){
var inst_23890 = (state_23913[(7)]);
var inst_23893 = (state_23913[(8)]);
var inst_23897 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_23893,inst_23890);
var state_23913__$1 = state_23913;
if(inst_23897){
var statearr_23926_25079 = state_23913__$1;
(statearr_23926_25079[(1)] = (8));

} else {
var statearr_23927_25080 = state_23913__$1;
(statearr_23927_25080[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23914 === (10))){
var inst_23905 = (state_23913[(2)]);
var state_23913__$1 = state_23913;
var statearr_23928_25081 = state_23913__$1;
(statearr_23928_25081[(2)] = inst_23905);

(statearr_23928_25081[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23914 === (8))){
var inst_23890 = (state_23913[(7)]);
var tmp23925 = inst_23890;
var inst_23890__$1 = tmp23925;
var state_23913__$1 = (function (){var statearr_23929 = state_23913;
(statearr_23929[(7)] = inst_23890__$1);

return statearr_23929;
})();
var statearr_23930_25086 = state_23913__$1;
(statearr_23930_25086[(2)] = null);

(statearr_23930_25086[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__21631__auto__ = null;
var cljs$core$async$state_machine__21631__auto____0 = (function (){
var statearr_23936 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_23936[(0)] = cljs$core$async$state_machine__21631__auto__);

(statearr_23936[(1)] = (1));

return statearr_23936;
});
var cljs$core$async$state_machine__21631__auto____1 = (function (state_23913){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_23913);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e23940){var ex__21634__auto__ = e23940;
var statearr_23941_25089 = state_23913;
(statearr_23941_25089[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_23913[(4)]))){
var statearr_23942_25090 = state_23913;
(statearr_23942_25090[(1)] = cljs.core.first((state_23913[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__25091 = state_23913;
state_23913 = G__25091;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$state_machine__21631__auto__ = function(state_23913){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21631__auto____1.call(this,state_23913);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21631__auto____0;
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21631__auto____1;
return cljs$core$async$state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_23946 = f__21914__auto__();
(statearr_23946[(6)] = c__21913__auto___25041);

return statearr_23946;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));


return out;
}));

(cljs.core.async.unique.cljs$lang$maxFixedArity = 2);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__23948 = arguments.length;
switch (G__23948) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__21913__auto___25096 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_23986){
var state_val_23987 = (state_23986[(1)]);
if((state_val_23987 === (7))){
var inst_23982 = (state_23986[(2)]);
var state_23986__$1 = state_23986;
var statearr_23988_25098 = state_23986__$1;
(statearr_23988_25098[(2)] = inst_23982);

(statearr_23988_25098[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23987 === (1))){
var inst_23949 = (new Array(n));
var inst_23950 = inst_23949;
var inst_23951 = (0);
var state_23986__$1 = (function (){var statearr_23989 = state_23986;
(statearr_23989[(7)] = inst_23950);

(statearr_23989[(8)] = inst_23951);

return statearr_23989;
})();
var statearr_23990_25099 = state_23986__$1;
(statearr_23990_25099[(2)] = null);

(statearr_23990_25099[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23987 === (4))){
var inst_23954 = (state_23986[(9)]);
var inst_23954__$1 = (state_23986[(2)]);
var inst_23955 = (inst_23954__$1 == null);
var inst_23956 = cljs.core.not(inst_23955);
var state_23986__$1 = (function (){var statearr_23991 = state_23986;
(statearr_23991[(9)] = inst_23954__$1);

return statearr_23991;
})();
if(inst_23956){
var statearr_23992_25101 = state_23986__$1;
(statearr_23992_25101[(1)] = (5));

} else {
var statearr_23993_25103 = state_23986__$1;
(statearr_23993_25103[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23987 === (15))){
var inst_23976 = (state_23986[(2)]);
var state_23986__$1 = state_23986;
var statearr_23994_25105 = state_23986__$1;
(statearr_23994_25105[(2)] = inst_23976);

(statearr_23994_25105[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23987 === (13))){
var state_23986__$1 = state_23986;
var statearr_23995_25109 = state_23986__$1;
(statearr_23995_25109[(2)] = null);

(statearr_23995_25109[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23987 === (6))){
var inst_23951 = (state_23986[(8)]);
var inst_23972 = (inst_23951 > (0));
var state_23986__$1 = state_23986;
if(cljs.core.truth_(inst_23972)){
var statearr_23997_25110 = state_23986__$1;
(statearr_23997_25110[(1)] = (12));

} else {
var statearr_23999_25111 = state_23986__$1;
(statearr_23999_25111[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23987 === (3))){
var inst_23984 = (state_23986[(2)]);
var state_23986__$1 = state_23986;
return cljs.core.async.impl.ioc_helpers.return_chan(state_23986__$1,inst_23984);
} else {
if((state_val_23987 === (12))){
var inst_23950 = (state_23986[(7)]);
var inst_23974 = cljs.core.vec(inst_23950);
var state_23986__$1 = state_23986;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23986__$1,(15),out,inst_23974);
} else {
if((state_val_23987 === (2))){
var state_23986__$1 = state_23986;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_23986__$1,(4),ch);
} else {
if((state_val_23987 === (11))){
var inst_23966 = (state_23986[(2)]);
var inst_23967 = (new Array(n));
var inst_23950 = inst_23967;
var inst_23951 = (0);
var state_23986__$1 = (function (){var statearr_24003 = state_23986;
(statearr_24003[(7)] = inst_23950);

(statearr_24003[(10)] = inst_23966);

(statearr_24003[(8)] = inst_23951);

return statearr_24003;
})();
var statearr_24005_25113 = state_23986__$1;
(statearr_24005_25113[(2)] = null);

(statearr_24005_25113[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23987 === (9))){
var inst_23950 = (state_23986[(7)]);
var inst_23964 = cljs.core.vec(inst_23950);
var state_23986__$1 = state_23986;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23986__$1,(11),out,inst_23964);
} else {
if((state_val_23987 === (5))){
var inst_23959 = (state_23986[(11)]);
var inst_23954 = (state_23986[(9)]);
var inst_23950 = (state_23986[(7)]);
var inst_23951 = (state_23986[(8)]);
var inst_23958 = (inst_23950[inst_23951] = inst_23954);
var inst_23959__$1 = (inst_23951 + (1));
var inst_23960 = (inst_23959__$1 < n);
var state_23986__$1 = (function (){var statearr_24006 = state_23986;
(statearr_24006[(11)] = inst_23959__$1);

(statearr_24006[(12)] = inst_23958);

return statearr_24006;
})();
if(cljs.core.truth_(inst_23960)){
var statearr_24008_25114 = state_23986__$1;
(statearr_24008_25114[(1)] = (8));

} else {
var statearr_24010_25115 = state_23986__$1;
(statearr_24010_25115[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23987 === (14))){
var inst_23979 = (state_23986[(2)]);
var inst_23980 = cljs.core.async.close_BANG_(out);
var state_23986__$1 = (function (){var statearr_24012 = state_23986;
(statearr_24012[(13)] = inst_23979);

return statearr_24012;
})();
var statearr_24013_25117 = state_23986__$1;
(statearr_24013_25117[(2)] = inst_23980);

(statearr_24013_25117[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23987 === (10))){
var inst_23970 = (state_23986[(2)]);
var state_23986__$1 = state_23986;
var statearr_24014_25119 = state_23986__$1;
(statearr_24014_25119[(2)] = inst_23970);

(statearr_24014_25119[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23987 === (8))){
var inst_23959 = (state_23986[(11)]);
var inst_23950 = (state_23986[(7)]);
var tmp24011 = inst_23950;
var inst_23950__$1 = tmp24011;
var inst_23951 = inst_23959;
var state_23986__$1 = (function (){var statearr_24015 = state_23986;
(statearr_24015[(7)] = inst_23950__$1);

(statearr_24015[(8)] = inst_23951);

return statearr_24015;
})();
var statearr_24016_25125 = state_23986__$1;
(statearr_24016_25125[(2)] = null);

(statearr_24016_25125[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__21631__auto__ = null;
var cljs$core$async$state_machine__21631__auto____0 = (function (){
var statearr_24017 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_24017[(0)] = cljs$core$async$state_machine__21631__auto__);

(statearr_24017[(1)] = (1));

return statearr_24017;
});
var cljs$core$async$state_machine__21631__auto____1 = (function (state_23986){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_23986);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e24018){var ex__21634__auto__ = e24018;
var statearr_24019_25130 = state_23986;
(statearr_24019_25130[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_23986[(4)]))){
var statearr_24020_25131 = state_23986;
(statearr_24020_25131[(1)] = cljs.core.first((state_23986[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__25132 = state_23986;
state_23986 = G__25132;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$state_machine__21631__auto__ = function(state_23986){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21631__auto____1.call(this,state_23986);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21631__auto____0;
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21631__auto____1;
return cljs$core$async$state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_24021 = f__21914__auto__();
(statearr_24021[(6)] = c__21913__auto___25096);

return statearr_24021;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));


return out;
}));

(cljs.core.async.partition.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__24023 = arguments.length;
switch (G__24023) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3(f,ch,null);
}));

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__21913__auto___25134 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__21914__auto__ = (function (){var switch__21630__auto__ = (function (state_24071){
var state_val_24072 = (state_24071[(1)]);
if((state_val_24072 === (7))){
var inst_24067 = (state_24071[(2)]);
var state_24071__$1 = state_24071;
var statearr_24076_25141 = state_24071__$1;
(statearr_24076_25141[(2)] = inst_24067);

(statearr_24076_25141[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24072 === (1))){
var inst_24027 = [];
var inst_24028 = inst_24027;
var inst_24029 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_24071__$1 = (function (){var statearr_24084 = state_24071;
(statearr_24084[(7)] = inst_24029);

(statearr_24084[(8)] = inst_24028);

return statearr_24084;
})();
var statearr_24085_25147 = state_24071__$1;
(statearr_24085_25147[(2)] = null);

(statearr_24085_25147[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24072 === (4))){
var inst_24032 = (state_24071[(9)]);
var inst_24032__$1 = (state_24071[(2)]);
var inst_24033 = (inst_24032__$1 == null);
var inst_24034 = cljs.core.not(inst_24033);
var state_24071__$1 = (function (){var statearr_24089 = state_24071;
(statearr_24089[(9)] = inst_24032__$1);

return statearr_24089;
})();
if(inst_24034){
var statearr_24090_25153 = state_24071__$1;
(statearr_24090_25153[(1)] = (5));

} else {
var statearr_24091_25158 = state_24071__$1;
(statearr_24091_25158[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24072 === (15))){
var inst_24028 = (state_24071[(8)]);
var inst_24059 = cljs.core.vec(inst_24028);
var state_24071__$1 = state_24071;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_24071__$1,(18),out,inst_24059);
} else {
if((state_val_24072 === (13))){
var inst_24054 = (state_24071[(2)]);
var state_24071__$1 = state_24071;
var statearr_24099_25164 = state_24071__$1;
(statearr_24099_25164[(2)] = inst_24054);

(statearr_24099_25164[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24072 === (6))){
var inst_24028 = (state_24071[(8)]);
var inst_24056 = inst_24028.length;
var inst_24057 = (inst_24056 > (0));
var state_24071__$1 = state_24071;
if(cljs.core.truth_(inst_24057)){
var statearr_24103_25165 = state_24071__$1;
(statearr_24103_25165[(1)] = (15));

} else {
var statearr_24104_25166 = state_24071__$1;
(statearr_24104_25166[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24072 === (17))){
var inst_24064 = (state_24071[(2)]);
var inst_24065 = cljs.core.async.close_BANG_(out);
var state_24071__$1 = (function (){var statearr_24105 = state_24071;
(statearr_24105[(10)] = inst_24064);

return statearr_24105;
})();
var statearr_24109_25167 = state_24071__$1;
(statearr_24109_25167[(2)] = inst_24065);

(statearr_24109_25167[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24072 === (3))){
var inst_24069 = (state_24071[(2)]);
var state_24071__$1 = state_24071;
return cljs.core.async.impl.ioc_helpers.return_chan(state_24071__$1,inst_24069);
} else {
if((state_val_24072 === (12))){
var inst_24028 = (state_24071[(8)]);
var inst_24047 = cljs.core.vec(inst_24028);
var state_24071__$1 = state_24071;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_24071__$1,(14),out,inst_24047);
} else {
if((state_val_24072 === (2))){
var state_24071__$1 = state_24071;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_24071__$1,(4),ch);
} else {
if((state_val_24072 === (11))){
var inst_24036 = (state_24071[(11)]);
var inst_24028 = (state_24071[(8)]);
var inst_24032 = (state_24071[(9)]);
var inst_24044 = inst_24028.push(inst_24032);
var tmp24117 = inst_24028;
var inst_24028__$1 = tmp24117;
var inst_24029 = inst_24036;
var state_24071__$1 = (function (){var statearr_24127 = state_24071;
(statearr_24127[(7)] = inst_24029);

(statearr_24127[(8)] = inst_24028__$1);

(statearr_24127[(12)] = inst_24044);

return statearr_24127;
})();
var statearr_24128_25171 = state_24071__$1;
(statearr_24128_25171[(2)] = null);

(statearr_24128_25171[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24072 === (9))){
var inst_24029 = (state_24071[(7)]);
var inst_24040 = cljs.core.keyword_identical_QMARK_(inst_24029,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var state_24071__$1 = state_24071;
var statearr_24129_25177 = state_24071__$1;
(statearr_24129_25177[(2)] = inst_24040);

(statearr_24129_25177[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24072 === (5))){
var inst_24029 = (state_24071[(7)]);
var inst_24036 = (state_24071[(11)]);
var inst_24032 = (state_24071[(9)]);
var inst_24037 = (state_24071[(13)]);
var inst_24036__$1 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_24032) : f.call(null, inst_24032));
var inst_24037__$1 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_24036__$1,inst_24029);
var state_24071__$1 = (function (){var statearr_24130 = state_24071;
(statearr_24130[(11)] = inst_24036__$1);

(statearr_24130[(13)] = inst_24037__$1);

return statearr_24130;
})();
if(inst_24037__$1){
var statearr_24131_25178 = state_24071__$1;
(statearr_24131_25178[(1)] = (8));

} else {
var statearr_24132_25179 = state_24071__$1;
(statearr_24132_25179[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24072 === (14))){
var inst_24036 = (state_24071[(11)]);
var inst_24032 = (state_24071[(9)]);
var inst_24049 = (state_24071[(2)]);
var inst_24050 = [];
var inst_24051 = inst_24050.push(inst_24032);
var inst_24028 = inst_24050;
var inst_24029 = inst_24036;
var state_24071__$1 = (function (){var statearr_24133 = state_24071;
(statearr_24133[(7)] = inst_24029);

(statearr_24133[(8)] = inst_24028);

(statearr_24133[(14)] = inst_24051);

(statearr_24133[(15)] = inst_24049);

return statearr_24133;
})();
var statearr_24134_25180 = state_24071__$1;
(statearr_24134_25180[(2)] = null);

(statearr_24134_25180[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24072 === (16))){
var state_24071__$1 = state_24071;
var statearr_24135_25181 = state_24071__$1;
(statearr_24135_25181[(2)] = null);

(statearr_24135_25181[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24072 === (10))){
var inst_24042 = (state_24071[(2)]);
var state_24071__$1 = state_24071;
if(cljs.core.truth_(inst_24042)){
var statearr_24136_25183 = state_24071__$1;
(statearr_24136_25183[(1)] = (11));

} else {
var statearr_24137_25184 = state_24071__$1;
(statearr_24137_25184[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24072 === (18))){
var inst_24061 = (state_24071[(2)]);
var state_24071__$1 = state_24071;
var statearr_24138_25185 = state_24071__$1;
(statearr_24138_25185[(2)] = inst_24061);

(statearr_24138_25185[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24072 === (8))){
var inst_24037 = (state_24071[(13)]);
var state_24071__$1 = state_24071;
var statearr_24139_25186 = state_24071__$1;
(statearr_24139_25186[(2)] = inst_24037);

(statearr_24139_25186[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__21631__auto__ = null;
var cljs$core$async$state_machine__21631__auto____0 = (function (){
var statearr_24140 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_24140[(0)] = cljs$core$async$state_machine__21631__auto__);

(statearr_24140[(1)] = (1));

return statearr_24140;
});
var cljs$core$async$state_machine__21631__auto____1 = (function (state_24071){
while(true){
var ret_value__21632__auto__ = (function (){try{while(true){
var result__21633__auto__ = switch__21630__auto__(state_24071);
if(cljs.core.keyword_identical_QMARK_(result__21633__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21633__auto__;
}
break;
}
}catch (e24141){var ex__21634__auto__ = e24141;
var statearr_24142_25187 = state_24071;
(statearr_24142_25187[(2)] = ex__21634__auto__);


if(cljs.core.seq((state_24071[(4)]))){
var statearr_24143_25188 = state_24071;
(statearr_24143_25188[(1)] = cljs.core.first((state_24071[(4)])));

} else {
throw ex__21634__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__21632__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__25189 = state_24071;
state_24071 = G__25189;
continue;
} else {
return ret_value__21632__auto__;
}
break;
}
});
cljs$core$async$state_machine__21631__auto__ = function(state_24071){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21631__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21631__auto____1.call(this,state_24071);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21631__auto____0;
cljs$core$async$state_machine__21631__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21631__auto____1;
return cljs$core$async$state_machine__21631__auto__;
})()
})();
var state__21915__auto__ = (function (){var statearr_24144 = f__21914__auto__();
(statearr_24144[(6)] = c__21913__auto___25134);

return statearr_24144;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__21915__auto__);
}));


return out;
}));

(cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3);


//# sourceMappingURL=cljs.core.async.js.map
