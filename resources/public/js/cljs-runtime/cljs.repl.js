goog.provide('cljs.repl');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__24799){
var map__24801 = p__24799;
var map__24801__$1 = cljs.core.__destructure_map(map__24801);
var m = map__24801__$1;
var n = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__24801__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__24801__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["-------------------------"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function (){var or__5002__auto__ = new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return [(function (){var temp__5804__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5804__auto__)){
var ns = temp__5804__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})(),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('');
}
})()], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Protocol"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__24805_25204 = cljs.core.seq(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__24806_25205 = null;
var count__24807_25206 = (0);
var i__24808_25207 = (0);
while(true){
if((i__24808_25207 < count__24807_25206)){
var f_25208 = chunk__24806_25205.cljs$core$IIndexed$_nth$arity$2(null, i__24808_25207);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_25208], 0));


var G__25209 = seq__24805_25204;
var G__25210 = chunk__24806_25205;
var G__25211 = count__24807_25206;
var G__25212 = (i__24808_25207 + (1));
seq__24805_25204 = G__25209;
chunk__24806_25205 = G__25210;
count__24807_25206 = G__25211;
i__24808_25207 = G__25212;
continue;
} else {
var temp__5804__auto___25213 = cljs.core.seq(seq__24805_25204);
if(temp__5804__auto___25213){
var seq__24805_25214__$1 = temp__5804__auto___25213;
if(cljs.core.chunked_seq_QMARK_(seq__24805_25214__$1)){
var c__5525__auto___25215 = cljs.core.chunk_first(seq__24805_25214__$1);
var G__25216 = cljs.core.chunk_rest(seq__24805_25214__$1);
var G__25217 = c__5525__auto___25215;
var G__25218 = cljs.core.count(c__5525__auto___25215);
var G__25219 = (0);
seq__24805_25204 = G__25216;
chunk__24806_25205 = G__25217;
count__24807_25206 = G__25218;
i__24808_25207 = G__25219;
continue;
} else {
var f_25220 = cljs.core.first(seq__24805_25214__$1);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_25220], 0));


var G__25221 = cljs.core.next(seq__24805_25214__$1);
var G__25222 = null;
var G__25223 = (0);
var G__25224 = (0);
seq__24805_25204 = G__25221;
chunk__24806_25205 = G__25222;
count__24807_25206 = G__25223;
i__24808_25207 = G__25224;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_25225 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__5002__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([arglists_25225], 0));
} else {
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first(arglists_25225)))?cljs.core.second(arglists_25225):arglists_25225)], 0));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Special Form"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.contains_QMARK_(m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
} else {
return null;
}
} else {
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Macro"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["REPL Special Function"], 0));
} else {
}

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__24838_25228 = cljs.core.seq(new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__24839_25229 = null;
var count__24840_25230 = (0);
var i__24841_25231 = (0);
while(true){
if((i__24841_25231 < count__24840_25230)){
var vec__24874_25232 = chunk__24839_25229.cljs$core$IIndexed$_nth$arity$2(null, i__24841_25231);
var name_25233 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24874_25232,(0),null);
var map__24877_25234 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24874_25232,(1),null);
var map__24877_25235__$1 = cljs.core.__destructure_map(map__24877_25234);
var doc_25236 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__24877_25235__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_25237 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__24877_25235__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_25233], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_25237], 0));

if(cljs.core.truth_(doc_25236)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_25236], 0));
} else {
}


var G__25238 = seq__24838_25228;
var G__25239 = chunk__24839_25229;
var G__25240 = count__24840_25230;
var G__25241 = (i__24841_25231 + (1));
seq__24838_25228 = G__25238;
chunk__24839_25229 = G__25239;
count__24840_25230 = G__25240;
i__24841_25231 = G__25241;
continue;
} else {
var temp__5804__auto___25242 = cljs.core.seq(seq__24838_25228);
if(temp__5804__auto___25242){
var seq__24838_25245__$1 = temp__5804__auto___25242;
if(cljs.core.chunked_seq_QMARK_(seq__24838_25245__$1)){
var c__5525__auto___25246 = cljs.core.chunk_first(seq__24838_25245__$1);
var G__25247 = cljs.core.chunk_rest(seq__24838_25245__$1);
var G__25248 = c__5525__auto___25246;
var G__25249 = cljs.core.count(c__5525__auto___25246);
var G__25250 = (0);
seq__24838_25228 = G__25247;
chunk__24839_25229 = G__25248;
count__24840_25230 = G__25249;
i__24841_25231 = G__25250;
continue;
} else {
var vec__24883_25251 = cljs.core.first(seq__24838_25245__$1);
var name_25252 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24883_25251,(0),null);
var map__24886_25253 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24883_25251,(1),null);
var map__24886_25254__$1 = cljs.core.__destructure_map(map__24886_25253);
var doc_25255 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__24886_25254__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_25256 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__24886_25254__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_25252], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_25256], 0));

if(cljs.core.truth_(doc_25255)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_25255], 0));
} else {
}


var G__25257 = cljs.core.next(seq__24838_25245__$1);
var G__25258 = null;
var G__25259 = (0);
var G__25260 = (0);
seq__24838_25228 = G__25257;
chunk__24839_25229 = G__25258;
count__24840_25230 = G__25259;
i__24841_25231 = G__25260;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5804__auto__ = cljs.spec.alpha.get_spec(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name(n)),cljs.core.name(nm)));
if(cljs.core.truth_(temp__5804__auto__)){
var fnspec = temp__5804__auto__;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));

var seq__24887 = cljs.core.seq(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__24888 = null;
var count__24889 = (0);
var i__24890 = (0);
while(true){
if((i__24890 < count__24889)){
var role = chunk__24888.cljs$core$IIndexed$_nth$arity$2(null, i__24890);
var temp__5804__auto___25261__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5804__auto___25261__$1)){
var spec_25262 = temp__5804__auto___25261__$1;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_25262)], 0));
} else {
}


var G__25263 = seq__24887;
var G__25264 = chunk__24888;
var G__25265 = count__24889;
var G__25266 = (i__24890 + (1));
seq__24887 = G__25263;
chunk__24888 = G__25264;
count__24889 = G__25265;
i__24890 = G__25266;
continue;
} else {
var temp__5804__auto____$1 = cljs.core.seq(seq__24887);
if(temp__5804__auto____$1){
var seq__24887__$1 = temp__5804__auto____$1;
if(cljs.core.chunked_seq_QMARK_(seq__24887__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__24887__$1);
var G__25267 = cljs.core.chunk_rest(seq__24887__$1);
var G__25268 = c__5525__auto__;
var G__25269 = cljs.core.count(c__5525__auto__);
var G__25270 = (0);
seq__24887 = G__25267;
chunk__24888 = G__25268;
count__24889 = G__25269;
i__24890 = G__25270;
continue;
} else {
var role = cljs.core.first(seq__24887__$1);
var temp__5804__auto___25271__$2 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5804__auto___25271__$2)){
var spec_25272 = temp__5804__auto___25271__$2;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_25272)], 0));
} else {
}


var G__25273 = cljs.core.next(seq__24887__$1);
var G__25274 = null;
var G__25275 = (0);
var G__25276 = (0);
seq__24887 = G__25273;
chunk__24888 = G__25274;
count__24889 = G__25275;
i__24890 = G__25276;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Constructs a data representation for a Error with keys:
 *  :cause - root cause message
 *  :phase - error phase
 *  :via - cause chain, with cause keys:
 *           :type - exception class symbol
 *           :message - exception message
 *           :data - ex-data
 *           :at - top stack element
 *  :trace - root cause stack elements
 */
cljs.repl.Error__GT_map = (function cljs$repl$Error__GT_map(o){
return cljs.core.Throwable__GT_map(o);
});
/**
 * Returns an analysis of the phase, error, cause, and location of an error that occurred
 *   based on Throwable data, as returned by Throwable->map. All attributes other than phase
 *   are optional:
 *  :clojure.error/phase - keyword phase indicator, one of:
 *    :read-source :compile-syntax-check :compilation :macro-syntax-check :macroexpansion
 *    :execution :read-eval-result :print-eval-result
 *  :clojure.error/source - file name (no path)
 *  :clojure.error/line - integer line number
 *  :clojure.error/column - integer column number
 *  :clojure.error/symbol - symbol being expanded/compiled/invoked
 *  :clojure.error/class - cause exception class symbol
 *  :clojure.error/cause - cause exception message
 *  :clojure.error/spec - explain-data for spec error
 */
cljs.repl.ex_triage = (function cljs$repl$ex_triage(datafied_throwable){
var map__24994 = datafied_throwable;
var map__24994__$1 = cljs.core.__destructure_map(map__24994);
var via = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__24994__$1,new cljs.core.Keyword(null,"via","via",-1904457336));
var trace = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__24994__$1,new cljs.core.Keyword(null,"trace","trace",-1082747415));
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__24994__$1,new cljs.core.Keyword(null,"phase","phase",575722892),new cljs.core.Keyword(null,"execution","execution",253283524));
var map__24996 = cljs.core.last(via);
var map__24996__$1 = cljs.core.__destructure_map(map__24996);
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__24996__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var message = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__24996__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var data = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__24996__$1,new cljs.core.Keyword(null,"data","data",-232669377));
var map__24997 = data;
var map__24997__$1 = cljs.core.__destructure_map(map__24997);
var problems = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__24997__$1,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814));
var fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__24997__$1,new cljs.core.Keyword("cljs.spec.alpha","fn","cljs.spec.alpha/fn",408600443));
var caller = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__24997__$1,new cljs.core.Keyword("cljs.spec.test.alpha","caller","cljs.spec.test.alpha/caller",-398302390));
var map__24998 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.first(via));
var map__24998__$1 = cljs.core.__destructure_map(map__24998);
var top_data = map__24998__$1;
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__24998__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3((function (){var G__25001 = phase;
var G__25001__$1 = (((G__25001 instanceof cljs.core.Keyword))?G__25001.fqn:null);
switch (G__25001__$1) {
case "read-source":
var map__25004 = data;
var map__25004__$1 = cljs.core.__destructure_map(map__25004);
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25004__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25004__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var G__25005 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.second(via)),top_data], 0));
var G__25005__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__25005,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__25005);
var G__25005__$2 = (cljs.core.truth_((function (){var fexpr__25010 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__25010.cljs$core$IFn$_invoke$arity$1 ? fexpr__25010.cljs$core$IFn$_invoke$arity$1(source) : fexpr__25010.call(null, source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__25005__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__25005__$1);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__25005__$2,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__25005__$2;
}

break;
case "compile-syntax-check":
case "compilation":
case "macro-syntax-check":
case "macroexpansion":
var G__25013 = top_data;
var G__25013__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__25013,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__25013);
var G__25013__$2 = (cljs.core.truth_((function (){var fexpr__25020 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__25020.cljs$core$IFn$_invoke$arity$1 ? fexpr__25020.cljs$core$IFn$_invoke$arity$1(source) : fexpr__25020.call(null, source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__25013__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__25013__$1);
var G__25013__$3 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__25013__$2,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__25013__$2);
var G__25013__$4 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__25013__$3,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__25013__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__25013__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__25013__$4;
}

break;
case "read-eval-result":
case "print-eval-result":
var vec__25022 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25022,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25022,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25022,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25022,(3),null);
var G__25030 = top_data;
var G__25030__$1 = (cljs.core.truth_(line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__25030,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),line):G__25030);
var G__25030__$2 = (cljs.core.truth_(file)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__25030__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file):G__25030__$1);
var G__25030__$3 = (cljs.core.truth_((function (){var and__5000__auto__ = source__$1;
if(cljs.core.truth_(and__5000__auto__)){
return method;
} else {
return and__5000__auto__;
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__25030__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null))):G__25030__$2);
var G__25030__$4 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__25030__$3,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__25030__$3);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__25030__$4,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__25030__$4;
}

break;
case "execution":
var vec__25058 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25058,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25058,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25058,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25058,(3),null);
var file__$1 = cljs.core.first(cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p1__24991_SHARP_){
var or__5002__auto__ = (p1__24991_SHARP_ == null);
if(or__5002__auto__){
return or__5002__auto__;
} else {
var fexpr__25072 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__25072.cljs$core$IFn$_invoke$arity$1 ? fexpr__25072.cljs$core$IFn$_invoke$arity$1(p1__24991_SHARP_) : fexpr__25072.call(null, p1__24991_SHARP_));
}
}),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(caller),file], null)));
var err_line = (function (){var or__5002__auto__ = new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(caller);
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return line;
}
})();
var G__25078 = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type], null);
var G__25078__$1 = (cljs.core.truth_(err_line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__25078,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),err_line):G__25078);
var G__25078__$2 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__25078__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__25078__$1);
var G__25078__$3 = (cljs.core.truth_((function (){var or__5002__auto__ = fn;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
var and__5000__auto__ = source__$1;
if(cljs.core.truth_(and__5000__auto__)){
return method;
} else {
return and__5000__auto__;
}
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__25078__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(function (){var or__5002__auto__ = fn;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null));
}
})()):G__25078__$2);
var G__25078__$4 = (cljs.core.truth_(file__$1)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__25078__$3,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file__$1):G__25078__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__25078__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__25078__$4;
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__25001__$1)].join('')));

}
})(),new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358),phase);
});
/**
 * Returns a string from exception data, as produced by ex-triage.
 *   The first line summarizes the exception phase and location.
 *   The subsequent lines describe the cause.
 */
cljs.repl.ex_str = (function cljs$repl$ex_str(p__25100){
var map__25102 = p__25100;
var map__25102__$1 = cljs.core.__destructure_map(map__25102);
var triage_data = map__25102__$1;
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25102__$1,new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358));
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25102__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25102__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25102__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var symbol = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25102__$1,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994));
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25102__$1,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890));
var cause = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25102__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742));
var spec = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25102__$1,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595));
var loc = [cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__5002__auto__ = source;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return "<cljs repl>";
}
})()),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__5002__auto__ = line;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return (1);
}
})()),(cljs.core.truth_(column)?[":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column)].join(''):"")].join('');
var class_name = cljs.core.name((function (){var or__5002__auto__ = class$;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return "";
}
})());
var simple_class = class_name;
var cause_type = ((cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["RuntimeException",null,"Exception",null], null), null),simple_class))?"":[" (",simple_class,")"].join(''));
var format = goog.string.format;
var G__25118 = phase;
var G__25118__$1 = (((G__25118 instanceof cljs.core.Keyword))?G__25118.fqn:null);
switch (G__25118__$1) {
case "read-source":
return (format.cljs$core$IFn$_invoke$arity$3 ? format.cljs$core$IFn$_invoke$arity$3("Syntax error reading source at (%s).\n%s\n",loc,cause) : format.call(null, "Syntax error reading source at (%s).\n%s\n",loc,cause));

break;
case "macro-syntax-check":
var G__25120 = "Syntax error macroexpanding %sat (%s).\n%s";
var G__25121 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__25122 = loc;
var G__25123 = (cljs.core.truth_(spec)?(function (){var sb__5647__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__25126_25295 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__25127_25296 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__25128_25297 = true;
var _STAR_print_fn_STAR__temp_val__25129_25298 = (function (x__5648__auto__){
return sb__5647__auto__.append(x__5648__auto__);
});
(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__25128_25297);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__25129_25298);

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),(function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__25093_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__25093_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
}),probs);
}))
);
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__25127_25296);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__25126_25295);
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__5647__auto__);
})():(format.cljs$core$IFn$_invoke$arity$2 ? format.cljs$core$IFn$_invoke$arity$2("%s\n",cause) : format.call(null, "%s\n",cause)));
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__25120,G__25121,G__25122,G__25123) : format.call(null, G__25120,G__25121,G__25122,G__25123));

break;
case "macroexpansion":
var G__25136 = "Unexpected error%s macroexpanding %sat (%s).\n%s\n";
var G__25137 = cause_type;
var G__25138 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__25139 = loc;
var G__25140 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__25136,G__25137,G__25138,G__25139,G__25140) : format.call(null, G__25136,G__25137,G__25138,G__25139,G__25140));

break;
case "compile-syntax-check":
var G__25142 = "Syntax error%s compiling %sat (%s).\n%s\n";
var G__25143 = cause_type;
var G__25144 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__25145 = loc;
var G__25146 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__25142,G__25143,G__25144,G__25145,G__25146) : format.call(null, G__25142,G__25143,G__25144,G__25145,G__25146));

break;
case "compilation":
var G__25148 = "Unexpected error%s compiling %sat (%s).\n%s\n";
var G__25149 = cause_type;
var G__25150 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__25151 = loc;
var G__25152 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__25148,G__25149,G__25150,G__25151,G__25152) : format.call(null, G__25148,G__25149,G__25150,G__25151,G__25152));

break;
case "read-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null, "Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "print-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null, "Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "execution":
if(cljs.core.truth_(spec)){
var G__25154 = "Execution error - invalid arguments to %s at (%s).\n%s";
var G__25155 = symbol;
var G__25156 = loc;
var G__25157 = (function (){var sb__5647__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__25159_25309 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__25160_25310 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__25161_25311 = true;
var _STAR_print_fn_STAR__temp_val__25162_25312 = (function (x__5648__auto__){
return sb__5647__auto__.append(x__5648__auto__);
});
(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__25161_25311);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__25162_25312);

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),(function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__25094_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__25094_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
}),probs);
}))
);
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__25160_25310);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__25159_25309);
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__5647__auto__);
})();
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__25154,G__25155,G__25156,G__25157) : format.call(null, G__25154,G__25155,G__25156,G__25157));
} else {
var G__25172 = "Execution error%s at %s(%s).\n%s\n";
var G__25173 = cause_type;
var G__25174 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__25175 = loc;
var G__25176 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__25172,G__25173,G__25174,G__25175,G__25176) : format.call(null, G__25172,G__25173,G__25174,G__25175,G__25176));
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__25118__$1)].join('')));

}
});
cljs.repl.error__GT_str = (function cljs$repl$error__GT_str(error){
return cljs.repl.ex_str(cljs.repl.ex_triage(cljs.repl.Error__GT_map(error)));
});

//# sourceMappingURL=cljs.repl.js.map
