(ns storm.cgol.env
  (:require
    [clojure.tools.logging :as log]
    [storm.cgol.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init       (fn []
                 (log/info "\n-=[cgol starting using the development or test profile]=-"))
   :start      (fn []
                 (log/info "\n-=[cgol started successfully using the development or test profile]=-"))
   :stop       (fn []
                 (log/info "\n-=[cgol has shut down successfully]=-"))
   :middleware wrap-dev
   :opts       {:profile       :dev
                :persist-data? true}})
