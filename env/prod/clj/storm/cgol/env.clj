(ns storm.cgol.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init       (fn []
                 (log/info "\n-=[cgol starting]=-"))
   :start      (fn []
                 (log/info "\n-=[cgol started successfully]=-"))
   :stop       (fn []
                 (log/info "\n-=[cgol has shut down successfully]=-"))
   :middleware (fn [handler _] handler)
   :opts       {:profile :prod}})
